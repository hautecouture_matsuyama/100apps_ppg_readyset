﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="NavEnvSoundEffectsData", menuName="CF/NavEnvSoundEffectsData Asset")]
public class NavEnvSoundEffectsData : ScriptableObject
{
	public AudioClip[] MoveWhooshSounds;
	public AudioClip CoinPickupSound;
	public AudioClip HeartShardPickupSound;
	public AudioClip GiftPickupSound;
	public AudioClip BlossomPickupSound;
	public AudioClip BubblesPickupSound;
	public AudioClip ButtercupPickupSound;
	public AudioClip GiftBotTapSound;
	public AudioClip StationActivateSound;
	public AudioClip EnterSecretRoomSound;
}
