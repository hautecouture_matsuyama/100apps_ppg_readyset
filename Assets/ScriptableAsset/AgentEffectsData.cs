﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="EffectsData", menuName="CF/AgentEffectsData Asset")]
public class AgentEffectsData : ScriptableObject
{
	//SFX
	public AudioClip TelegraphSound;
	public AudioClip[] AttackSounds;
	public AudioClip CounterAttackSound;
	public AudioClip BlockStartSound;
	public AudioClip PerfectBlockSound;
	public AudioClip ShieldBreakSound;
	public AudioClip[] TakehitNormalSounds;
	public AudioClip TakehitBlockSound;
	public AudioClip DeathSound;
	public AudioClip GameOverSting;
	public AudioClip VictorySting;
	public AudioClip GroundPoundSound;
	public AudioClip DodgeSound;

	//VO
	public AudioClip[] BattleStartVOClips;
	public AudioClip[] AttackVOClips;
	public AudioClip[] ChargeAttackVOClips;
	public AudioClip FaintVO;
	public AudioClip[] VictoryVOClips;
	public AudioClip StunnedVO;
	public AudioClip[] TakeHitVOClips;
}
