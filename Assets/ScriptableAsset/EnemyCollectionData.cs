﻿using UnityEngine;

[CreateAssetMenu(fileName="EnemyData", menuName="CF/EnemyData Asset")]
public class EnemyCollectionData : ScriptableObject
{
	[SerializeField] GameObject brutePrefab;
	[SerializeField] GameObject flowerPrefab;
	[SerializeField] GameObject flyerPrefab;
	[SerializeField] GameObject bigBrutePrefab;
	[SerializeField] GameObject bigFlowerPrefab;
	[SerializeField] GameObject bigFlyerPrefab;
	public GameObject this[EnemyType _t]
	{
		get{
			switch (_t) {
			case EnemyType.Brute:
				return brutePrefab;
			case EnemyType.Flower:
				return flowerPrefab;
			case EnemyType.Flyer:
				return flyerPrefab;
			case EnemyType.BigBrute:
				return bigBrutePrefab;
			case EnemyType.BigFlower:
				return bigFlowerPrefab;
			case EnemyType.BigFlyer:
				return bigFlyerPrefab;
			default:
				throw new System.ArgumentOutOfRangeException ();
			}
		}
	}
}
