﻿using UnityEngine;

[CreateAssetMenu(fileName="EnvironmentData", menuName="CF/EnvironmentData Asset")]
public class EnvironmentCollectionData : ScriptableObject
{
	public GameObject earnStation;
	public GameObject healStation;

	public GameObject startChunk;
	[SerializeField] GameObject[] straight;
	[SerializeField] GameObject[] pathUp;
	[SerializeField] GameObject[] pathDown;
	[SerializeField] GameObject[] secretUp;
	[SerializeField] GameObject[] secretDown;
	[SerializeField] GameObject[] sponsorship;
	public GameObject endChunk;

	GameObject getRandom(GameObject[] _rooms)
	{
		return _rooms[Random.Range(0, _rooms.Length)];
	}

	public GameObject RandomStraight
	{
		get { return getRandom(straight); }
	}
	public GameObject RandomUp
	{
		get { return getRandom(pathUp); }
	}
	public GameObject RandomDown
	{
		get { return getRandom(pathDown); }
	}
	public GameObject RandomSecretUp
	{
		get { return getRandom(secretUp); }
	}
	public GameObject RandomSecretDown
	{
		get { return getRandom(secretDown); }
	}
	public GameObject RandomSponsorship
	{
		get { return getRandom(sponsorship); }
	}
}
