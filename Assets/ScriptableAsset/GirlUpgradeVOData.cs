﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="GirlUpgradeVOData", menuName="CF/GirlUpgradeVOData Asset")]
public class GirlUpgradeVOData : ScriptableObject 
{
	public AudioClip[] BlossomUpgradedVO;
	public AudioClip[] BubblesUpgradedVO;
	public AudioClip[] ButtercupUpgradedVO;

	public AudioClip GetRandomClipForGirl(PPG _girl) 
	{
		AudioClip voClip = null;
		switch(_girl) {
			case PPG.Blossom:
				voClip = BlossomUpgradedVO[Random.Range(0, BlossomUpgradedVO.Length)];
				break;
			case PPG.Bubbles:
				voClip = BubblesUpgradedVO[Random.Range(0, BubblesUpgradedVO.Length)];
				break;
			case PPG.Buttercup:
				voClip = ButtercupUpgradedVO[Random.Range(0, ButtercupUpgradedVO.Length)];
				break;
			default:
				break;
		}
		return voClip;
	}
}
