credits_image_game	<gamelogo>
credits1_h1	DEVELOPED BY:
credits_image_dev	<devlogos>
credits2_h2	Game Design & Programming
credits3	Chris Sumsky
credits4	Ankit Trivedi
credits5_h2	Art & Animation
credits6	Jeff MacDonald
credits7	Jenna Zona
credits9	Shiho Pate
credits8	Katherine Tsai
credits99	Lisa Gelsomini
credits99	Kara Zona
credits10_h2	Music
credits10	Andrew Matteson
credits11_h2	Sound Design
credits12	Dustin Bozovich
credits13_h1	PUBLISHED BY:
credits_image_cn	<cnlogo>
credits14_h2	Producer
credits15	Caroline Guevara
credits16_h2	Director, Game Production
credits17	Jeff Riggall
credits18_h2	VP, Digital Production
credits19	Sherri Glass
credits20_h2	Associate Resource Manager
credits21	Betsy Gooch
credits22_h2	Production Assistant
credits23	Eric Cook
credits24_h2	Production Interns
credits25	Michaela Deguzman
credits26	Denearrius Wooten
credits26_h2	Art Director
credits27	Skyler Flygare
credits28_h2	Creative Director
credits29	Mario Piedra
credits30_h2	Senior Writer
credits31	Alan Moore
credits32_h2	Director, Game Design
credits33	Brad Merritt
credits34_h2	UX Architect
credits35	Justin Smith
credits36_h2	Senior QA Analyst
credits37	Jared Collins
credits38_h2	QA Interns
credits39	Regina Baker
credits40	Joshua Redding 
credits41	Cynthia He
credits42	Tre'Shaun Thomas
credits40_h2	Tools Engineer
credits41	Jarred Riggall
credits42_h2	Programmer
credits43	Rowan Gibbs
credits44_h2	International Manager
credits45	Blaine Marowitz
credits48_h2	VP, Product Management
credits49	Beau Teague
credits50_h2	VP, CN Digital
credits51	Chris Waldron
credits52_h2	Special Thanks
credits53	Amy Birnbaum
credits54	Sara Griggs
credits55	Malia Mask
credits56	Leah Randall
credits58	All of CN Digital
credits59	
credits60	
credits61	
credits62	
credits63	
credits64	
credits65_h2	Production Babies
credits66	Rhys Lennon Sommer