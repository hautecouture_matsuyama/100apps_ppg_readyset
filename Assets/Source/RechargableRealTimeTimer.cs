﻿using System;
using UnityEngine;

[System.Serializable]
public class RechargableRealTimeTimer
{
	#region Variables
	/// <summary>
	/// The time it takes for the timer to be complete
	/// </summary>
	readonly TimeSpan rechargeDuration;
	DateTime timeOfLastReset;
	#endregion

	#region Constructors
	public RechargableRealTimeTimer(TimeSpan _rechargetTime, bool _startReady = false)
		: this(_rechargetTime.Hours, _rechargetTime.Minutes, _rechargetTime.Seconds, _startReady)
	{
	}
	public RechargableRealTimeTimer(int _hours, int _minutes, int _seconds, bool _startReady = false)
	{
		rechargeDuration = new TimeSpan(_hours, _minutes, _seconds);
		ResetTimer();
		if (_startReady) {
			ForceExpireTimer();
		}
	}
	#endregion

	#region Interface
	/// <summary>
	/// Resets the timer to the current UTC date-time
	/// </summary>
	public void ResetTimer()
	{
		timeOfLastReset = DateTime.UtcNow;
	}
	/// <summary>
	/// Forces the timer to be in the past exactly <see cref="rechargeDuration"/> units ago
	/// </summary>
	public void ForceExpireTimer()
	{
		timeOfLastReset = DateTime.UtcNow.Subtract(rechargeDuration);
	}
	/// <summary>
	/// Gets a value indicating whether this timer has completed
	/// </summary>
	/// <value><c>true</c> if this instance has timer expired; otherwise, <c>false</c>.</value>
	public bool IsTimerComplete
	{
		get{ return UnityEngine.Mathf.Approximately(1f,  TicksRemaining);}
	}
	/// <summary>
	/// Gets the percent of time completed.
	/// </summary>
	/// <value>The percent comlete.</value>
	/// <remarks>A value of 0 means the timer was just reset, and a value of 1 means the timer is complete</remarks>
	public float PercentComlete
	{
		get { 
			return (float)(rechargeDuration.Ticks - TicksRemaining) / (float)(rechargeDuration.Ticks);}
	}
	public TimeSpan TimeRemaining
	{
		get {return rechargeDuration.Subtract(DateTime.UtcNow.Subtract(timeOfLastReset));}
	}
	public float TicksRemaining
	{
		get { 
			return UnityEngine.Mathf.Clamp((float)TimeRemaining.Ticks, 0, (float)(rechargeDuration.Ticks));}
	}
	#endregion

	public override string ToString ()
	{
		var builder = new System.Text.StringBuilder();
		var tr = TimeRemaining;
		builder.Append(tr.Minutes);
		builder.Append('m');
		builder.Append(' ');
		builder.Append(tr.Seconds);
		builder.Append('s');
		return builder.ToString();
	}
}