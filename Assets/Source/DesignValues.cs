﻿using System.Collections.Generic;

public enum AgentActions
{
	/// <summary>
	/// The base attack weight.
	/// </summary>
	Attack,
	/// <summary>
	/// The secondary (if applicable) attack weight.
	/// </summary>
	Attack2,
	/// <summary>
	/// The attack weight while in rage.
	/// </summary>
	RageAttack,
	/// <summary>
	/// The block weight
	/// </summary>
	Block,
	/// <summary>
	/// The weight for doing nothing while not in rage.
	/// </summary>
	DoNothing,
	/// <summary>
	/// The weight for doing nothing while in rage.
	/// </summary>
	RageDoNothing
}

public class ComboData
{
	public readonly int ComboAttackAnimNum;
	public readonly float HoldAttackDuration;
	public readonly float EarlyTimingPercent; //percent [0, 1]
	public readonly float LateTimingDuration; // seconds
	public readonly float ReallowBlockingPercent; //percent [0, 1]
	public readonly int AttackLandFrame; //frame index
	public readonly float DamageModifier; //multiplier
	public ComboData(int _comboAninNum, float _holdAttackDuration, float _earlyTimingPercent, float _lateTimingDuration, float _reallowBlockingPercent, int _attackLandFrame, float _damageModifer)
	{
		ComboAttackAnimNum = _comboAninNum;
		HoldAttackDuration = _holdAttackDuration;
		EarlyTimingPercent = _earlyTimingPercent;
		LateTimingDuration = _lateTimingDuration;
		ReallowBlockingPercent = _reallowBlockingPercent;
		AttackLandFrame = _attackLandFrame;
		DamageModifier = _damageModifer;
	}
}

public class StaminaData
{
	public float Attack; //absolute value
	public float FillCooldownTime;
	public float FillRatePerSecond;
	public float FillRateWhileBlocking;
	public StaminaData(float _attack, float _fillCooldownTime, float _fillRatePerSecond, float _fillRateWhileBlocking)
	{
		Attack = _attack;
		FillCooldownTime = _fillCooldownTime;
		FillRatePerSecond = _fillRatePerSecond;
		FillRateWhileBlocking = _fillRateWhileBlocking;
	}
}

public static class DesignValues 
{
	#region UI Feel
	public const float PostDeathWaitTime = 1.5f;
	#endregion

	#region Upgrade UI
	public static class Costs
	{
		public const CurrencyType StatCurrency = CurrencyType.Gold;
		public static uint GetCostForStat(AcquiredStats _stat)
		{
			const float a = 1.5f;
			const float b = 24.5f; //a negative value here will shift the parabola to the right
			const float c = 50; //this is base value
			var x = _stat[StatType.Attack] + _stat[StatType.Health] + _stat[StatType.Block] + _stat[StatType.Stamina];
			var calculated = (a * x * x) + (b * x) + c;
			calculated = ((int)(calculated / 5f)) * 5f;
			return (uint)calculated;
		}
		public const CurrencyType AbilityCurrency = CurrencyType.Shard;
		public const uint AbilityCost = 1;

		public const CurrencyType AuraCurrency = CurrencyType.Gold;
		public const uint AuraCost = 150;

		public const CurrencyType FMCurrency = CurrencyType.Gold;
		public const uint FMCost = 250;

		public const CurrencyType NormalSuperCurrency = CurrencyType.Shard;
		public const uint NormalSuperCost = 2;

		public const CurrencyType BlissSuperCurrency = CurrencyType.Shard;
		public const uint BlissSuperCost = 2;

		public const uint WinAPrizeCost = 200;
	}
	#endregion

	#region Base Battle Values
	public static class PlayerBattleFeel
	{
		public const float PerfectBlockTimingWindow = 0.1f;//seconds
		public const float CounterAttackWindow = 0.5f;
		public const float NormalHitSleepTime = 0.1f;
		public const float PerfectBlockSleepTime = 0.3f;
		public const float SuperArmorHitSleepTime = 0.3f;
		public const float ArbitraryBattleNumbersScalar = 10f;
		public const float CriticalDamageModifier = 2f;
	}

	public static class BaseAgentStats
	{
		public const float BaseAgentHealth = 20f; //An agent would have this health even if their Health stat was zero
		public const float MaxHealthModifier = 5f; //Agents get this value of Health per Health stat point
		public const float BaseAgentStamina = 20f; //An agent would have this stamina even if their Stamina stat was zero
		public const float MaxStaminaModifier = 5f; //Agents get this value of Stamina per Stamina stat point
		public const float AttackDamageDivisor = 4f;  
		public const float ShieldReductionDivisor = 14f; 

		public static readonly StaminaData StaminaData = new StaminaData
			(
				_attack: 5f,
				_fillCooldownTime: 0.75f,
				_fillRatePerSecond: 7f,
				_fillRateWhileBlocking: 1f
			);
		public const float StunnedTime = 2f;
	}
	#endregion

	#region Player Agent Stats
	public static class Blossom
	{
		public static readonly BaseStats BaseStats = new BaseStats(3,2,3,4); //Must add to 12
		public const float Attack1Power = 2.9f;
		public const float ComboCooldownTime = 0f;
		public static readonly ComboData[] ComboData = new []
		{
			new ComboData(
				_comboAninNum: 1,
				_holdAttackDuration: 0.2f,
				_earlyTimingPercent: 0.5f,
				_lateTimingDuration: 0.2f,
				_reallowBlockingPercent: 0.25f,
				_attackLandFrame: 5,
				_damageModifer: 1
			),
			new ComboData(
				_comboAninNum: 2,
				_holdAttackDuration: 0.2f,
				_earlyTimingPercent: 0.5f,
				_lateTimingDuration: 0.2f,
				_reallowBlockingPercent: 0.25f,
				_attackLandFrame: 1,
				_damageModifer: 1.4f
			),
			new ComboData(
				_comboAninNum: 3,
				_holdAttackDuration: 0.3f,
				_earlyTimingPercent: 0.5f,
				_lateTimingDuration: 0.2f,
				_reallowBlockingPercent: 0.25f,
				_attackLandFrame: 1,
				_damageModifer: 1.9f
			)
		};
		public static readonly StaminaData StaminaData = new StaminaData
			(
				_attack: 3.5f,
				_fillCooldownTime: 0.75f,
				_fillRatePerSecond: 3f,
				_fillRateWhileBlocking: 1f
			);
	}
	public static class Bubbles
	{
		public static readonly BaseStats BaseStats = new BaseStats(1,5,1,5); //Must add to 12
		public const float Attack1Power = 2.4f;
		public const float ComboCooldownTime = 0.4f;
		public static readonly ComboData[] ComboData = new []
		{
			new ComboData(
				_comboAninNum: 1,
				_holdAttackDuration: 0f,
				_earlyTimingPercent: 0.5f,
				_lateTimingDuration: 0.2f,
				_reallowBlockingPercent: 0.15f,
				_attackLandFrame: 2,
				_damageModifer: 1
			),
			new ComboData(
				_comboAninNum: 2,
				_holdAttackDuration: 0f,
				_earlyTimingPercent: 0.5f,
				_lateTimingDuration: 0.2f,
				_reallowBlockingPercent: 0.15f,
				_attackLandFrame: 1,
				_damageModifer: 1.33f
			),
			new ComboData(
				_comboAninNum: 3,
				_holdAttackDuration: 0f,
				_earlyTimingPercent: 0.5f,
				_lateTimingDuration: 0.2f,
				_reallowBlockingPercent: 0.15f,
				_attackLandFrame: 1,
				_damageModifer: 1.66f
			),
			new ComboData(
				_comboAninNum: 2,
				_holdAttackDuration: 0f,
				_earlyTimingPercent: 0.5f,
				_lateTimingDuration: 0.2f,
				_reallowBlockingPercent: 0.15f,
				_attackLandFrame: 1,
				_damageModifer: 2f
			),
		};
		public static readonly StaminaData StaminaData = new StaminaData
			(
				_attack: 3f,
				_fillCooldownTime: 0.75f,
				_fillRatePerSecond: 3f,
				_fillRateWhileBlocking: 1f
			);
	}
	public static class Buttercup
	{
		public static readonly BaseStats BaseStats = new BaseStats(4,3,4,1); //Must add to 12
		public const float Attack1Power = 3.7f;
		public const float ComboCooldownTime = 0.3f;
		public static readonly ComboData[] ComboData = new []
		{
			new ComboData(
				_comboAninNum: 1,
				_holdAttackDuration: 0f,
				_earlyTimingPercent: 0.4f,
				_lateTimingDuration: 0.2f,
				_reallowBlockingPercent: 0.2f,
				_attackLandFrame: 9,
				_damageModifer: 1f
			),
			new ComboData(
				_comboAninNum: 2,
				_holdAttackDuration: 0f,
				_earlyTimingPercent: 0.4f,
				_lateTimingDuration: 0.2f,
				_reallowBlockingPercent: 0.18f,
				_attackLandFrame: 3,
				_damageModifer: 1.3f
			),
			new ComboData(
				_comboAninNum: 3,
				_holdAttackDuration: 0f,
				_earlyTimingPercent: 0f,
				_lateTimingDuration: 0f,
				_reallowBlockingPercent: 0.2f,
				_attackLandFrame: 12,
				_damageModifer: 2.6f
			)
		};
		public static readonly StaminaData StaminaData = new StaminaData
			(
				_attack: 5f,
				_fillCooldownTime: 0.75f,
				_fillRatePerSecond: 3f,
				_fillRateWhileBlocking: 1f
			);
	}

	public static class NormalSuperMove
	{
		public const float Damage = 100;
		public const float Duration = 3f;//in seconds
		public const float Steps = 4f;
		public const float AngleVariation = 40f;
		public static readonly RandomRangedValue SpawnDistance = new RandomRangedValue(100f, 200f);
		public const float SpawnYOffset = -400;
		public const float LoopYOffset = -100;
		public readonly static System.TimeSpan RechargeTime = new System.TimeSpan(0,20,0); 
	}
	public static class BlissSuperMove
	{
		public const float Damage = 999;
		public const float AngleVariation = 20f;
		public static readonly RandomRangedValue SpawnDistance = new RandomRangedValue(100f, 200f);
		public const float HangTime = 0f;
		public readonly static System.TimeSpan RechargeTime = new System.TimeSpan(0,20,0); 
	}
	#endregion

	#region Enemy Agent Stats
	public static class Tutorial
	{
		public static readonly BaseStats Stats = new BaseStats(10f, 1.5f, 3f, 2f);
		public const float Attack1Power = 1f;
		public const float TelegraphTime = 1f;
		public static readonly HiddenBaseStats HiddenStats = new HiddenBaseStats(0.85f, TelegraphTime, TelegraphTime, 1f, 3f);
		public const float IdleTime = 1f;
		public static readonly Dictionary<AgentActions, float> Weights = new Dictionary<AgentActions, float>()
		{
			{AgentActions.Attack, 4.5f},
			{AgentActions.Block, 1f},
			{AgentActions.DoNothing, 2f}
		};
	}
	public static class Brute
	{
		public static readonly BaseStats Stats = new BaseStats(7f, 1.5f, 3f, 2f);
		public const float Attack1Power = 4f;
		public static readonly HiddenBaseStats HiddenStats = new HiddenBaseStats(0.85f, 1f, 1.2f, 3f, 3f);
		public const float IdleTime = 1f;
		public static readonly Dictionary<AgentActions, float> Weights = new Dictionary<AgentActions, float>()
		{
			{AgentActions.Attack, 4.5f},
			{AgentActions.Block, 1f},
			{AgentActions.DoNothing, 2f}
		};
		public const float MinBlockTime = 1f;
		public const float MaxBlockTime = 3f;
	}
	public static class BigBrute
	{
		public static readonly BaseStats Stats = new BaseStats(10f, 2.5f, 3.5f, 2f);
		public const float Attack1Power = 5.5f;
		public static readonly HiddenBaseStats HiddenStats = new HiddenBaseStats(0.85f, 1f, 1.2f, 3.5f, 3f);

		/// <summary>
		/// Attack value when agent is in rage
		/// </summary>
		public const float RageAttack = 5f;
		/// <summary>
		/// The attacks till the agent is in rage.
		/// </summary>
		public const int AttacksTillRage = 4;
		public const float IdleTime = 1f;
		public static readonly Dictionary<AgentActions, float> Weights = new Dictionary<AgentActions, float>()
		{
			{AgentActions.Attack, 4f},
			{AgentActions.RageAttack, 5f},
			{AgentActions.Block, 1f},
			{AgentActions.DoNothing, 4f},
			{AgentActions.RageDoNothing, 1.5f}
		};
		public const float MinBlockTime = 1f;
		public const float MaxBlockTime = 4f;
	}
	public static class Flower
	{
		public static readonly BaseStats Stats = new BaseStats(5f, 2f, 2.5f, 1f);
		public const float Attack1Power = 3.5f;
		public static readonly HiddenBaseStats HiddenStats = new HiddenBaseStats(1f, 0.7f, 0.8f, 3f, 3f);
		public const float IdleTime = 1f;
		public static readonly Dictionary<AgentActions, float> Weights = new Dictionary<AgentActions, float>()
		{
			{AgentActions.Attack, 4f},
			{AgentActions.Block, 2f},
			{AgentActions.DoNothing, 2f}
		};
		/// <summary>
		/// The chance to perform a double attack
		/// </summary>
		/// <remarks>Value must be between 0 and 1</remarks>
		public const float DoubleAttackChance = 0.1f;//NOT A WEIGHT, DON'T MOVE
		public static readonly RangedValue TimeBetweenAttacks = new RangedValue(0.1f, 0.15f);
		public static readonly RangedValue SecondAttackTelegraphTime = new RangedValue(0.4f, 0.5f);

		public const float MinBlockTime = 2f;
		public const float MaxBlockTime = 4f;

		public const int MaxAnticipateCount = 4;
	}
	public static class BigFlower
	{
		public static readonly BaseStats Stats = new BaseStats(9f, 2.25f, 3f, 1f);
		public const float Attack1Power = 4f;
		public static readonly HiddenBaseStats HiddenStats = new HiddenBaseStats(1f, 0.75f, 0.9f, 3.5f, 3f);
		public const float IdleTime = 1f;
		public static readonly Dictionary<AgentActions, float> Weights = new Dictionary<AgentActions, float>()
		{
			{AgentActions.Attack, 4f},
			{AgentActions.Block, 2f},
			{AgentActions.DoNothing, 2f}
		};
		/// <summary>
		/// The chance to perform a double attack
		/// </summary>
		/// <remarks>Value must be between 0 and 1</remarks>
		public const float DoubleAttackChance = 0.1f;//NOT A WEIGHT, DON'T MOVE
		public const float CounterAttackChance = 0.05f;//NOT A WEIGHT, DON'T MOVE

		public static readonly RandomRangedValue TimeBetweenAttacks = new RandomRangedValue(0.35f, 0.45f);
		public static readonly RandomRangedValue SecondAttackTelegraphTime = new RandomRangedValue(0.55f, 0.75f);

		public const float MinBlockTime = 2f;
		public const float MaxBlockTime = 5f;

		public const int MaxAnticipateCount = 4;
	}
	public static class Flyer
	{
		public static readonly BaseStats Stats = new BaseStats(4.5f, 2.25f, 3f, 1f);
		public const float Attack1Power = 3f;
		public const float RawStaminaDamage = 10f;
		public static readonly HiddenBaseStats HiddenStats = new HiddenBaseStats(0.5f, 0.7f, 0.8f, 15f, 3f);
		public const float IdleTime = 1.5f;
		public const float IdleTimeWhenPlayerStunned = 0f;
		public static readonly Dictionary<AgentActions, float> Weights = new Dictionary<AgentActions, float>()
		{
			{AgentActions.Attack, 5f},
			{AgentActions.Attack2, 2f},
			{AgentActions.DoNothing, 1f}
		};
		public static readonly Dictionary<AgentActions, float> WeightsWhenPlayerStunned = new Dictionary<AgentActions, float>()
		{
			{AgentActions.Attack, 1f},
			{AgentActions.Attack2, 0f},
			{AgentActions.DoNothing, 0f}
		};
		public const float ChanceToDodge = 0.7f;
		public const float DodgeCooldown = 1.5f;
		public static readonly RandomRangedValue Pattern1TravelTime = new RandomRangedValue(0.8f, 1.2f);
		public const int MaxAnticipateCount = 4;
	}
	public static class BigFlyer
	{
		public static readonly BaseStats Stats = new BaseStats(8.5f, 2.75f, 4f, 2f);
		public const float Attack1Power = 3.5f;
		public const float RawStaminaDamage = 10f;
		public static readonly HiddenBaseStats HiddenStats = new HiddenBaseStats(0.5f, 0.7f, 0.8f, 15f, 3f);
		public const float IdleTime = 1.5f;
		public const float IdleTimeWhenPlayerStunned = 0f;
		public static readonly Dictionary<AgentActions, float> Weights = new Dictionary<AgentActions, float>()
		{
			{AgentActions.Attack, 5f},
			{AgentActions.Attack2, 2f},
			{AgentActions.DoNothing, 1f}
		};
		public static readonly Dictionary<AgentActions, float> WeightsWhenPlayerStunned = new Dictionary<AgentActions, float>()
		{
			{AgentActions.Attack, 1f},
			{AgentActions.Attack2, 0f},
			{AgentActions.DoNothing, 0f}
		};
		public const float ChanceToDodge = 0.7f;
		public const float DodgeCooldown = 1f;
		public static readonly RandomRangedValue Pattern1TravelTime = new RandomRangedValue(0.6f, 0.9f);
		public static readonly RandomRangedValue Pattern2TravelTime = new RandomRangedValue(0.7f,1.1f);
		public static readonly RandomRangedValue Pattern3TravelTime = new RandomRangedValue(0.7f,1f);
		public const int MaxAnticipateCount = 4;
	}
	#endregion

	#region World Gen Values
	public static class WorldGenerator
	{
		/// <summary>
		/// The min/max for how many coins will spawn in a regular room, if Gold is going to spawn.
		/// </summary>
		public static readonly RandomRangedValue GoldCountRegular = new RandomRangedValue(1, 2);
		/// <summary>
		/// The percent chance a regular room will have a branch to a Special Room.
		/// </summary>
		public const float SpecialRoomSpawnChance = 0.15f;
		/// <summary>
		/// The percent chance a Secret Room will contain a Heart Shard instead of Gold.
		/// </summary>
		public const float HeartShardSpawnChance = 0.05f;
		/// <summary>
		/// The min/max for how many coins will spawn in a Secret Room, if Gold is going to spawn.
		/// </summary>
		public static readonly RandomRangedValue GoldCountSpecial = new RandomRangedValue(5, 8);
		/// <summary>
		/// The difficulty weight index (which picks weight, which determines which enemies can spawn with what chance) will increase by this amount every time difficulty step is increased.
		/// Right now that step is increased every enemy death. So if this value is 0.2, the difficulty weight will increase every 5 enemy deaths. 
		/// </summary>
		public const float DifficultyWeightStep = 0.34f;

		public static readonly Dictionary<int, float> EnemySpawnWeights = new Dictionary<int, float> () {
			{ 0, 1f },
			{ 1, 2f },
			{ 2, 5f },
			{ 3, 3f }
		};
		/// <summary>
		/// The max amount of rooms a player can traverse before a battle is forced.
		/// </summary>
		public static int GetRoomsForNextEnemy()
		{
			return Utilities.GetWeightedRandomItem (EnemySpawnWeights);
		}
		/// <summary>
		/// The min/max for the number of rooms until the next Earn Station, reset every time the player sees an Earn Station.
		/// </summary>
		public static readonly RandomRangedValue RoomsTillEarnStation = new RandomRangedValue(25, 40);
		/// <summary>
		/// The rooms till first earn station.
		/// </summary>
		public const int RoomsTillFirstEarnStation = 10;
		/// <summary>
		/// The min/max for the number of rooms until the next Heal Station, reset every time the player sees an Heal Station.
		/// </summary>
		public static readonly RandomRangedValue RoomsTillHealStation = new RandomRangedValue(15, 30); //Can only happen once per run
	}
	#endregion

	#region Sponsorship
	public static class SponsorshipBot
	{
		public const float TravelTime = 2f;
		public const float HangTime = 1f;
		public const float AngleVariation = 40f;
		public static readonly RandomRangedValue SpawnDistance = new RandomRangedValue(400f, 600f);
	}
	#endregion
}
