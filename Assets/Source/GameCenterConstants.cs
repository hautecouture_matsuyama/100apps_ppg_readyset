﻿
public class GameCenterConstants 
{
	public static readonly string ACH_SURVIVED_TROPICAL = "ach_survivedtropical";
	public static readonly string ACH_SURVIVED_VOLCANO = "ach_survivedvolcano";
	public static readonly string ACH_SURVIVED_MONSTER = "ach_survivedmonster";
	public static readonly string ACH_LOOPED_2X = "ach_looped2x";
	public static readonly string ACH_LOOPED_10X = "ach_looped10x";
	public static readonly string ACH_BLOSSOM_UPGRADED = "ach_blossomupgraded";
	public static readonly string ACH_BUBBLES_UPGRADED = "ach_bubblesupgraded";
	public static readonly string ACH_BUTTERCUP_UPGRADED = "ach_buttercupupgraded";
	public static readonly string ACH_ALL_AURAS= "ach_allauras";
	public static readonly string ACH_ALL_FMS = "ach_allfms";
	public static readonly string ACH_ALL_SUPERS = "ach_allsupers";
	public static readonly string ACH_BLISS_USED = "ach_blissused";

	public static readonly string LB_HIGH_SCORES = "lb_highscores_rsm";
}
