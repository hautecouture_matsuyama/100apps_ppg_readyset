﻿using System;

public enum FriendlyMonsterType
{
	Fighter,
	Medic,
	Sapper,
	Guard,
	Patron
}