﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class PlayerManager
{
	public delegate void OnValueChange(uint _old, uint _new);
	public static OnValueChange HandleScoreChanged;
	public static OnValueChange HandleGoldCollected;
	public static OnValueChange HandleHeartShardCollected;

	public delegate void OnCharacterChange(PPG _girl);
	public static OnCharacterChange HandleCharacterChange;

	public static float BlossomHealth;
	public static float BubblesHealth;
	public static float ButtercupHealth;

	public static void HealActiveGirl()
	{
		var girl = UnityEngine.Object.FindObjectOfType<BPlayerAgent>();
		if (girl != null) {
			girl.UpdateHealthAndUpdateUI(float.MaxValue);
		}
	}
	public static void HealAllGirls()
	{
		BlossomHealth = BattleManager.CalculateHealth(DesignValues.Blossom.BaseStats.Health, Instance.BlossomAcquiredStats.Health);
		BubblesHealth = BattleManager.CalculateHealth(DesignValues.Bubbles.BaseStats.Health, Instance.BubblesAcquiredStats.Health);
		ButtercupHealth = BattleManager.CalculateHealth(DesignValues.Buttercup.BaseStats.Health, Instance.ButtercupAcquiredStats.Health);
	}
	public static bool AreAllGirlsDead()
	{
		return (!IsGirlUnlocked(PPG.Blossom) || BlossomHealth <= 0) &&
			(!IsGirlUnlocked(PPG.Bubbles) || BubblesHealth <= 0) &&
			(!IsGirlUnlocked(PPG.Buttercup) || ButtercupHealth <= 0);
	}
	/// <summary>
	/// Switchs to next PPG in battle.
	/// </summary>
	public static object SwitchToNextPPGInBattle()
	{
		var blossomOut  = !IsGirlUnlocked(PPG.Blossom) || BlossomHealth <= 0f;
		var bubblesOut  = !IsGirlUnlocked(PPG.Bubbles) || BubblesHealth <= 0f;
		var buttercupOut  = !IsGirlUnlocked(PPG.Buttercup) || ButtercupHealth <= 0f;
		if ((!blossomOut && bubblesOut && buttercupOut) ||
			(blossomOut && !bubblesOut && buttercupOut) ||
			(blossomOut && bubblesOut && !buttercupOut)) {
			PPG nextGirl = PPG.None;
			if (!blossomOut) {
				nextGirl = PPG.Blossom;
			} else if (!bubblesOut) {
				nextGirl = PPG.Bubbles;
			} else if (!buttercupOut) {
				nextGirl = PPG.Buttercup;
			}
			return nextGirl;
		} else if (NumberofGirlUnlocked() == 3) {
			BGirlSelectOverlay.SpawnGirlSelect(false, true);
			BGirlSelectOverlay.Instance.CallerWillCallSetActiveGirl = true;
			return BGirlSelectOverlay.Instance;
		}
		throw new System.ArgumentOutOfRangeException(string.Format("blossom out: {0}, bubbles out: {1}, buttercup out: {2} AND there are {3} unlocked girls",
			blossomOut, bubblesOut, buttercupOut, NumberofGirlUnlocked()
		));
	}


	#region Interface
	public static PlayerManager Instance = new PlayerManager();
	public static void ResetStreakScore()
	{
		CurrentScore = 0;
	}
	public static void IncrementProgress(bool _special)
	{
		CurrentScore += BDebugOverlay.ScoreModifier;
		if (!_special) {
			var delta = (ObjectiveManager.ProgressPerRoom * BDebugOverlay.ScoreModifier);
			Instance.globalProgressUnclamped += delta;
		}
	}

	static uint currentScore;
	public static uint CurrentScore
	{
		get{
			return currentScore;
		}
		private set{
			if (currentScore != value) {
				if (HandleScoreChanged != null) {
					HandleScoreChanged(currentScore, value);
				}
				currentScore = value;
			}
		}
	}
	public static uint BestScore
	{
		get{
			return Instance.bestScore;
		}
		set{
			Instance.bestScore = value;
			SaveLoadManager.Save();
		}
	}
	public static uint GoldCount
	{
		get{
			return Instance.goldCount;
		}
		set{
			if (Instance.goldCount != value) {
				if (HandleGoldCollected != null) {
					HandleGoldCollected(Instance.goldCount, value);
				}
				Instance.goldCount = value;
				SaveLoadManager.Save();
			}
		}
	}
	public static uint HeartShardCount
	{
		get{
			return Instance.heartShardCount;
		}
		set{
			if (Instance.heartShardCount != value) {
				if (HandleHeartShardCollected != null) {
					HandleHeartShardCollected(Instance.heartShardCount, value);
				}
				Instance.heartShardCount = value;
				SaveLoadManager.Save();
			}
		}
	}
	public static uint GlobalProgress
	{
		get {
			return Utilities.ClampValueToCircle(GlobalProgressUnclamped);
		}
	}
	public static long GlobalProgressUnclamped
	{
		get {
			return Instance.globalProgressUnclamped;
		}
	}
	public static int MapLoops
	{
		get {
			return (int) (Instance.globalProgressUnclamped / Utilities.FullCircle);
		}
	}
	public static int NumberofGirlUnlocked()
	{
		return Instance.unlockedGirls.Count;
	}
	public static bool IsGirlUnlocked(PPG _girl)
	{
		return Instance.unlockedGirls.Contains(_girl);
	}
	public static PPG GetNextGirlForUnlocked()
	{
		var girls = Utilities.GetValues<PPG>().ToArray();
		var indices = new int[]{1,2,3,1};
		for(int i = 0;i<indices.Length-1;i++) {
			if(NumberofGirlUnlocked() == 1) {
				if(girls[indices[i]] == GetActiveGirl()) {
					return girls[indices[i+1]];
				}
			} else if(!IsGirlUnlocked(girls[indices[i]])){
				return girls[indices[i]];
			}
		}
		return PPG.None;
	}
	public static void UnlockGirl(PPG _girl)
	{
		if (!IsGirlUnlocked(_girl)) {
			Instance.unlockedGirls.Add(_girl);
			if (_girl == PPG.Blossom && IsGirlUnlocked(PPG.Blossom)) {
				BlossomHealth = BattleManager.CalculateHealth(DesignValues.Blossom.BaseStats.Health, Instance.BlossomAcquiredStats.Health);
			} else if (_girl == PPG.Bubbles && IsGirlUnlocked(PPG.Bubbles)) {
				BubblesHealth = BattleManager.CalculateHealth(DesignValues.Bubbles.BaseStats.Health, Instance.BubblesAcquiredStats.Health);
			} else if (_girl == PPG.Buttercup && IsGirlUnlocked(PPG.Buttercup)) {
				ButtercupHealth = BattleManager.CalculateHealth(DesignValues.Buttercup.BaseStats.Health, Instance.ButtercupAcquiredStats.Health);
			}
		}
	}
	public static PPG GetActiveGirl()
	{
		if (Instance.unlockedGirlIndex == -1) {
			return PPG.None;
		}
		return Instance.unlockedGirls[Instance.unlockedGirlIndex];
	}
	public static void SetActiveGirl(PPG _girl)
	{
		if (IsGirlUnlocked(_girl)) {
			if (HandleCharacterChange != null) {
				HandleCharacterChange(_girl);
			}
			Instance.unlockedGirlIndex = Instance.unlockedGirls.IndexOf(_girl);
		}
	}
	public static void UnlockSuperAttack(SuperAttack _attack)
	{
		if (_attack == SuperAttack.TornadoTrio && Instance.TornadoTrio == null) {
			Instance.TornadoTrio = new RechargableRealTimeTimer(DesignValues.NormalSuperMove.RechargeTime, true);
		} else if (_attack == SuperAttack.BlissBlitz && Instance.BlissBlitz == null) {
			Instance.BlissBlitz = new RechargableRealTimeTimer(DesignValues.BlissSuperMove.RechargeTime, true);
		}
		BGameCenterManager.CheckAllSupers();
	}
	public static bool IsSuperAttackUnlocked(SuperAttack _attack)
	{
		if (_attack == SuperAttack.TornadoTrio) {
			return Instance.TornadoTrio != null;
		} else if (_attack == SuperAttack.BlissBlitz) {
			return Instance.BlissBlitz != null;
		}
		throw new System.ArgumentOutOfRangeException(string.Format("{0} is not a recognized super attack", _attack));
	}
	public static void EquipSuperAttack(SuperAttack _attack)
	{
		if (_attack == SuperAttack.TornadoTrio && Instance.TornadoTrio != null) {
			Instance.currentSuper = Instance.TornadoTrio;
		} else if (_attack == SuperAttack.BlissBlitz && Instance.BlissBlitz != null) {
			Instance.currentSuper = Instance.BlissBlitz;
		}
	}
	public static bool IsSuperAttackEquipped(SuperAttack _attack)
	{
		if (_attack == SuperAttack.TornadoTrio) {
			return IsSuperAttackUnlocked(_attack) && Instance.currentSuper == Instance.TornadoTrio;
		} else if (_attack == SuperAttack.BlissBlitz) {
			return IsSuperAttackUnlocked(_attack) && Instance.currentSuper == Instance.BlissBlitz;
		}
		throw new System.ArgumentOutOfRangeException(string.Format("{0} is not a recognized super attack", _attack));
	}
	public static RechargableRealTimeTimer CurrentSuperAttack
	{
		get{return Instance.currentSuper;}
	}
	public static RechargableRealTimeTimer GetSuperAttackHandle(SuperAttack _attack)
	{
		if (_attack == SuperAttack.TornadoTrio) {
			return Instance.TornadoTrio;
		} else if (_attack == SuperAttack.BlissBlitz) {
			return Instance.BlissBlitz;
		}
		throw new System.ArgumentOutOfRangeException(string.Format("{0} is not a recognized super attack", _attack));
	}

	public static ObjectiveManager Objectives
	{
		get {
			return Instance.objectives;
		}
	}
	public static bool UnlockedBlisstina
	{
		get {
			return Instance.unlockedBlisstina;
		}
		set {
			Instance.unlockedBlisstina = value;
		}
	}
	#endregion

	public PlayerManager()
	{
		unlockedGirlIndex = -1;
		unlockedGirls = new List<PPG>(3);

		blossomAcquiredStats = new AcquiredStats();
		bubblesAcquiredStats = new AcquiredStats();
		buttercupAcquiredStats = new AcquiredStats();

		unlockedFighters = new BitArray(6);
		unlockedMedics = new BitArray(6);
		unlockedCoaches = new BitArray(6);
		unlockedGuards = new BitArray(6);
		unlockedPatrons = new BitArray(6);

		CurrentFriendlyMonsterSkinIndex = -1;

		objectives = new ObjectiveManager();
	}

	public AcquiredStats GetAcquiredStatForGirl(PPG _girl)
	{
		switch (_girl) {
		case PPG.Blossom:
			return blossomAcquiredStats;
		case PPG.Bubbles:
			return bubblesAcquiredStats;
		case PPG.Buttercup:
			return buttercupAcquiredStats;
		default:
			throw new System.ArgumentOutOfRangeException ();
		}
	}
	public BitArray GetUnlockedFriendlyMonsterArray(FriendlyMonsterType _fm)
	{
		switch (_fm) {
		case FriendlyMonsterType.Fighter:
			return unlockedFighters;
		case FriendlyMonsterType.Patron:
			return unlockedPatrons;
		case FriendlyMonsterType.Medic:
			return unlockedMedics;
		case FriendlyMonsterType.Guard:
			return unlockedGuards;
		case FriendlyMonsterType.Sapper:
			return unlockedCoaches;
		default:
			throw new System.ArgumentOutOfRangeException ();
		}
	}
	#region Persistant Data
	uint goldCount;
	uint heartShardCount;
	uint bestScore;
	long globalProgressUnclamped;

	int unlockedGirlIndex;
	List<PPG> unlockedGirls;

	bool unlockedBlisstina;

	AcquiredStats blossomAcquiredStats;
	public AcquiredStats BlossomAcquiredStats{get{return blossomAcquiredStats;}}
	AcquiredStats bubblesAcquiredStats;
	public AcquiredStats BubblesAcquiredStats{get{return bubblesAcquiredStats;}}
	AcquiredStats buttercupAcquiredStats;
	public AcquiredStats ButtercupAcquiredStats{get{return buttercupAcquiredStats;}}

	public FriendlyMonsterType? CurrentFriendlyMonster;
	public int CurrentFriendlyMonsterSkinIndex;

	BitArray unlockedFighters;
	public BitArray UnlockedFighters{get {return unlockedFighters;}}
	BitArray unlockedPatrons;
	public BitArray UnlockedPatrons{get {return unlockedPatrons;}}
	BitArray unlockedMedics;
	public BitArray UnlockedMedics{get {return unlockedMedics;}}
	BitArray unlockedGuards;
	public BitArray UnlockedGuards{get {return unlockedGuards;}}
	BitArray unlockedCoaches;
	public BitArray UnlockedCoaches{get {return unlockedCoaches;}}

	public RechargableRealTimeTimer TornadoTrio;
	public RechargableRealTimeTimer BlissBlitz;

	RechargableRealTimeTimer currentSuper;

	ObjectiveManager objectives;
	#endregion
}
