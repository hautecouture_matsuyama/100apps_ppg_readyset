﻿using System.Collections;

public enum StatType
{
	Health,
	Stamina,
	Attack,
	Block
}

public enum AbiltyType
{
	ChargeAttack,
	BiggerCombo,
	Counter
}

/// <summary>
/// This class holds all non-user-facing stats, used for battle.
/// </summary>
public class HiddenBaseStats
{
	public readonly float AttackCooldownTime;
	public readonly RandomRangedValue TelegraphTime;
	public readonly float StaminaForAttack;
	public readonly float BlockCooldownTime;
	public HiddenBaseStats(float _attackCooldownTime, float _minTelegraphTime, float _maxTelegraphTime, float _staminaForAttack, float _blockCooldown)
	{
		AttackCooldownTime = _attackCooldownTime;
		TelegraphTime = new RandomRangedValue(_minTelegraphTime, _maxTelegraphTime);
		StaminaForAttack = _staminaForAttack;
		BlockCooldownTime = _blockCooldown;
	}
}

/// <summary>
/// This class holds the user-facing base stats, relevant to battle, used by all agents
/// </summary>
public class BaseStats
{
	public readonly float Health;
	public readonly float Stamina;
	public readonly float Attack;
	public readonly float Shield;
	public BaseStats(float _health, float _stamina, float _attack, float _shield)
	{
		Health = _health;
		Stamina = _stamina;
		Attack = _attack;
		Shield = _shield;
	}
	public float this[StatType _type] {
		get {
			switch (_type) {
			case StatType.Health:
				return Health;
			case StatType.Stamina:
				return Stamina;
			case StatType.Attack:
				return Attack;
			case StatType.Block:
				return Shield;
			default:
				throw new System.ArgumentOutOfRangeException ();
			}
		}
	}
}

[System.Serializable]
/// <summary>
/// This class has all acuired (read, upgradable) stats for player character agents
/// </summary>
public class AcquiredStats
{
	public const int MaxStatCount = 3;
	
	int health;
	public int Health{get{return health;}}
	int stamina;
	public int Stamina{get{return stamina;}}
	int attack;
	public int Attack{get{return attack;}}
	int shield;
	public int Shield{get{return shield;}}

	bool auraUnlocked;
	public bool AuraUnlocked {get{return auraUnlocked;}}
	bool x2AttackUnlocked;
	public bool X2Unlocked {get{return x2AttackUnlocked;}}
	bool x3AttackUnlocked;
	public bool X3Unlocked {get{return x3AttackUnlocked;}}

	BitArray unlockedAuras = new BitArray(10);
	public BitArray UnlockedAuras {get{return unlockedAuras;}}

	public int EquippedAuraIndex = -1;
	public bool IsStatMaxed(StatType _type)
	{
		switch (_type) {
		case StatType.Health:
			return Health == 3;
		case StatType.Stamina:
			return Stamina == 3;
		case StatType.Attack:
			return Attack == 3;
		case StatType.Block:
			return Shield == 3;
		default:
			return false;
		}
	}

	public int this[StatType _type] {
		get {
			switch (_type) {
				case StatType.Health:
					return Health;
				case StatType.Stamina:
					return Stamina;
				case StatType.Attack:
					return Attack;
				case StatType.Block:
					return Shield;
				default:
					return -1;
			}
		}
	}
	/// <summary>
	/// Gets whether the <see cref="AcquiredStats"/> with the specified _type is unlocked.
	/// </summary>
	/// <param name="_type">Type.</param>
	public bool this[AbiltyType _type] {
		get {
			switch (_type) {
				case AbiltyType.ChargeAttack:
					return auraUnlocked;
				case AbiltyType.BiggerCombo:
					return x2AttackUnlocked;
				case AbiltyType.Counter:
					return x3AttackUnlocked;
				default:
					return false;
			}
		}
	}
	public void IncrementStat(StatType _type)
	{
		switch (_type) {
			case StatType.Health:
				incrementStat(ref health);
				break;
			case StatType.Stamina:
				incrementStat(ref stamina);
				break;
			case StatType.Attack:
				incrementStat(ref attack);
				break;
			case StatType.Block:
				incrementStat(ref shield);
				break;
			default:
				throw new System.ArgumentOutOfRangeException ();
		}
		BGameCenterManager.CheckGirlUpgraded(PlayerManager.GetActiveGirl());
	}
	public void UnlockAbility(AbiltyType _type) {
		switch (_type) {
			case AbiltyType.ChargeAttack:
				auraUnlocked = true;
				break;
			case AbiltyType.BiggerCombo:
				x2AttackUnlocked = true;	
				break;
			case AbiltyType.Counter:
				x3AttackUnlocked = true;	
				break;
		}
		BGameCenterManager.CheckGirlUpgraded(PlayerManager.GetActiveGirl());
	}
	public void UnlockAura(int _index)
	{
		unlockedAuras.Set(_index, true);
		EquippedAuraIndex = _index;
		BGameCenterManager.CheckAllAuras();
	}
	public bool AllAurasUnlocked()
	{
		var unlocked = true;
		for(int i = 0; i < unlockedAuras.Length; ++i) {
			unlocked &= unlockedAuras.Get(i);
		}
		return unlocked;
	}
	static void incrementStat(ref int _value)
	{
		_value = UnityEngine.Mathf.Clamp(_value + 1, 0, MaxStatCount);
	}
}
