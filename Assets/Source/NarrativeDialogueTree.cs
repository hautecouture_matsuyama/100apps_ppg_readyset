﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//basically typedefs
using GirlDialogue = System.Collections.Generic.Dictionary<PPG, System.Collections.Generic.Dictionary<PPG, System.Func<DialogueBuilder> >>;

public static class NarrativeDialogueTree
{
	static GirlDialogue firstEncounterDialog = new GirlDialogue();
	static GirlDialogue secondEncounterDialog = new GirlDialogue();
	static NarrativeDialogueTree()
	{
		var actionMapping = new Dictionary<PPG, System.Func<DialogueBuilder>>();
		actionMapping.Add(PPG.Bubbles, firstEncounterBlossomToBubbles);
		firstEncounterDialog.Add(PPG.Blossom, actionMapping);

		actionMapping = new Dictionary<PPG, System.Func<DialogueBuilder>>();
		actionMapping.Add(PPG.Buttercup, firstEncounterBubblesToButtercup);
		firstEncounterDialog.Add(PPG.Bubbles, actionMapping);

		actionMapping = new Dictionary<PPG, System.Func<DialogueBuilder>>();
		actionMapping.Add(PPG.Blossom, firstEncounterButtercupToBlossom);
		firstEncounterDialog.Add(PPG.Buttercup, actionMapping);

		actionMapping = new Dictionary<PPG, System.Func<DialogueBuilder>>();
		actionMapping.Add(PPG.Bubbles, secondEncounterBlossomToBubbles);
		actionMapping.Add(PPG.Buttercup, secondEncounterBlossomToButtercup);
		secondEncounterDialog.Add(PPG.Blossom, actionMapping);

		actionMapping = new Dictionary<PPG, System.Func<DialogueBuilder>>();
		actionMapping.Add(PPG.Blossom, secondEncounterBubblesToBlossom);
		actionMapping.Add(PPG.Buttercup, secondEncounterBubblesToButtercup);
		secondEncounterDialog.Add(PPG.Bubbles, actionMapping);

		actionMapping = new Dictionary<PPG, System.Func<DialogueBuilder>>();
		actionMapping.Add(PPG.Blossom, secondEncounterButtercupToBlossom);
		actionMapping.Add(PPG.Bubbles, secondEncounterButtercupToBubbles);
		secondEncounterDialog.Add(PPG.Buttercup, actionMapping);
	}

	public static void DoDialogBetweenCurrentAnd(PPG _to, AudioClip _startVO, System.Action _onComplete)
	{
		var activeGirl = PlayerManager.GetActiveGirl();
		DialogueBuilder builder = null;
		if (PlayerManager.NumberofGirlUnlocked() == 1) {
			builder = firstEncounterDialog[activeGirl][_to]();
		} else if (PlayerManager.NumberofGirlUnlocked() == 2) {
			builder = secondEncounterDialog[activeGirl][_to]();
		}
		if (builder != null) {
			builder.dialogueStartVO = _startVO;
			builder.SetDelay(1f).OnComplete(_onComplete).Go();
		}
	}
	//----------------- First Encounter ---------------//
	static DialogueBuilder firstEncounterBlossomToBubbles()
	{
		var builer = new DialogueBuilder();
		return builer.AddAsCurrentGirl("バブルス！大丈夫？")
			.Add(DialogueSpeaker.Bubbles, "あ！うん、大丈夫かな。間抜けな怪獣たちがずっと私に攻撃してくるの、それでみんなのことも見つけられなかったの。")
			.AddAsCurrentGirl("わかったわ。とりあえず一緒にいましょう。そしてバターカップを見つけにいきましょう！");
	}
	static DialogueBuilder firstEncounterBubblesToButtercup()
	{
		var builer = new DialogueBuilder();
		return builer.AddAsCurrentGirl("バターカップ！見つけた！")
			.Add(DialogueSpeaker.Buttercup, "ねえバブルス、この場所なんか変じゃない？でもパンチの練習場所としてはピッタリ！")
			.AddAsCurrentGirl("そうかも！いっしょにブロッサムを探しに行きましょ！");
	}
	static DialogueBuilder firstEncounterButtercupToBlossom()
	{
		var builer = new DialogueBuilder();
		return builer.AddAsCurrentGirl("ブロッサム！元気？")
			.Add(DialogueSpeaker.Blossom, "バターカップ！会えてよかった、私は元気よ。でも、バブルスはどこかしら？")
			.AddAsCurrentGirl("知らない。見てないけど、でもモンスターはそこら中にいるよ。")
			.Add(DialogueSpeaker.Blossom, "よし、とりあえず行きましょう。バブルスは直ぐにみつかるでしょ！");
	}
	//----------------- Second Encounter ---------------//
	static DialogueBuilder secondEncounterBlossomToBubbles()
	{
		var builer = new DialogueBuilder();
		return builer.AddAsCurrentGirl("バブルス！大丈夫？バターカップと一緒にずっと探してたんだよ")
			.Add(DialogueSpeaker.Bubbles, "ヒュー、見つかっちゃった。大丈夫！でもモンスターがいっぱい！")
			.AddAsCurrentGirl("そうね、この島は危険よ。みんな一緒にいたほうがいいね。")
			.AddAsCurrentGirl("モンスター島が見えてるね。たぶん向こうまでいけば何かわかるんじゃないかな");
	}
	static DialogueBuilder secondEncounterBlossomToButtercup()
	{
		var builer = new DialogueBuilder();
		return builer.AddAsCurrentGirl("バターカップ！大丈夫？")
			.Add(DialogueSpeaker.Buttercup, "はぁはぁ。うん、ただ、、このクレイジーなやつらをを止めないと・・・。")
			.AddAsCurrentGirl("うん、この島は危険ね。みんな一緒にいたほうがいいわ。")
			.AddAsCurrentGirl("モンスター島が見えてるね。たぶん向こうまでいけば何かわかるんじゃないかな");
		}
	static DialogueBuilder secondEncounterBubblesToBlossom()
	{
		var builer = new DialogueBuilder();
		return builer.AddAsCurrentGirl("あっ、ブロッサム！みぃーっけっ！")
			.Add(DialogueSpeaker.Blossom, "バブルス！大丈夫？ずっと探し回ってたんだよ")
			.AddAsCurrentGirl("うん、バターカップと一緒にモンスターのお尻を蹴ってただけ、もちろん探してたよ")
			.Add(DialogueSpeaker.Blossom, "よし、これでまた全員揃ったね！")
			.Add(DialogueSpeaker.Blossom, "ちょうどこの岩の向こうから来たんだけど、モンスター島が見えたよ")
			.Add(DialogueSpeaker.Blossom, "たぶんあそこに行けば何かわかるんじゃないかな");
		}
	static DialogueBuilder secondEncounterBubblesToButtercup()
	{
		var builer = new DialogueBuilder();
		return builer.AddAsCurrentGirl("あ！バターカップ！みぃーっけっ！")
			.Add(DialogueSpeaker.Buttercup, "よう！イカしてるでしょ、この場所。かなりパンチの練習になったよ")
			.AddAsCurrentGirl("やったね！一緒なら、もっといっぱいのモンスターにもパンチできるんじゃない！")
			.Add(DialogueSpeaker.Buttercup, "いいねえ！じゃあどっちに行けばいいかだけ教えて")
			.AddAsCurrentGirl("うん、ブロッサムがモンスター島を見たって行ってたのはあの岩からだから")
			.AddAsCurrentGirl("たぶんあそこに行けば何かわかるんじゃないかな");
	}
	static DialogueBuilder secondEncounterButtercupToBlossom()
	{
		var builer = new DialogueBuilder();
		return builer.AddAsCurrentGirl("ブロッサム！ずっと探してたんだよ")
			.Add(DialogueSpeaker.Blossom, "ほんとうに！会えてうれしいわ！でも、あとどのくらいもつかは自信はないわ。")
			.Add(DialogueSpeaker.Blossom, "ちょうどこの岩の向こうから来たんだけど、モンスター島が見えたよ")
			.Add(DialogueSpeaker.Blossom, "たぶんあそこに行けば何かわかるんじゃないかな");
	}
	static DialogueBuilder secondEncounterButtercupToBubbles()
	{
		var builer = new DialogueBuilder();
		return builer.AddAsCurrentGirl("ん？バブルス？あのおっきいモンスターと遊んでた？")
			.Add(DialogueSpeaker.Bubbles, "ん？何？ちがう！")
			.AddAsCurrentGirl("ふーん、あんたの顔についてるよだれは、どっからきたのかな？")
			.Add(DialogueSpeaker.Bubbles, "...")
			.Add(DialogueSpeaker.Bubbles, "だってめっちゃキュートなんだもん！")
			.AddAsCurrentGirl("バブルス！モンスターたちは私達のこと食べようとしてるんだよ！")
			.AddAsCurrentGirl("ふーん、変なの！")
			.AddAsCurrentGirl("よし、一緒に来て！モンスター島が見えるよ。向こうに行ってみたら何かわかるかもしれない。");
	}
	//----------------- Third Encounter ---------------//
	public static void DoDialogWithBliss(AudioClip _startVO, System.Action _onComplete)
	{
		var builer = new DialogueBuilder();
		builer.AddAsCurrentGirl("ブリス!? ここで何してるの？")
			.Add(DialogueSpeaker.Bliss, "ハーイみんな！モジョがここに連れてきたのよ。（彼は友達が欲しくてたまらなくて。。。。なんか悲しい。。。。）")
			.Add(DialogueSpeaker.Bliss, "でも彼はタウンズヴィルに送り返したわ！")
			.Add(DialogueSpeaker.Bliss, "まだまだバトルは続くわよ！この島に入ってきたモンスター達をキレイにしないと")
			.Add(DialogueSpeaker.Bliss, "でももうアタシを見つけたから、アタシのスーパーアタックを使えるようになるわ。")
			.Add(DialogueSpeaker.Bliss, "もしトラブルに巻き込まれたらそれをつかってアタシを呼んで。きょうだいの力を見せてあげる！")
			.Add(DialogueSpeaker.Bliss, "よし、じゃあアタシはいくね。あなたたちも気をつけて。またね！")
			.AddAsCurrentGirl("オッケー！じゃあね、ブリス！")
			.OnComplete(_onComplete)
			.SetDelay(1f);
		builer.dialogueStartVO = _startVO;
		builer.Go();
	}
}
