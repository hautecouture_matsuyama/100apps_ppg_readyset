﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AdType
{
	Loading,
	Preroll
}

public class FreewheelManager {

	public static System.Action OnStart;
	public static System.Action OnEnd;

	public static bool AdIsPlaying;

	public static void RequestAd(AdType _adType, System.Action _onEnd, System.Action _onStart = null)
	{
		OnStart = () => {
			AdIsPlaying = true;
			if(_onStart != null) {
				_onStart();
			}
		};
		OnEnd = () => {
			AdIsPlaying = false;
			if(_onEnd != null) {
				_onEnd();
			}
		};
		if(_adType == AdType.Loading) {
			FreeWheelController.instance.RequestLoadingAd();
		} else if (_adType == AdType.Preroll) {
			FreeWheelController.instance.RequestPrerollAd();
		}
	}
}
