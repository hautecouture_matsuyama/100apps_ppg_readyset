﻿using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class SaveLoadManager
{
	const int chunkSize = 100;
	const string prefixKey = "chunk_{0}";
	const string lengthKey = "length";
	const string androidCom = "com.android.vending";

	#if UNITY_EDITOR
	[MenuItem ("Window/Reset Data")]
	public static void  ShowWindow () {
		SaveLoadManager.SaveDefaultValues();
	}
	#endif

	PlayerManager playerData;
	GiftSpawner spawnerData;
	bool inTutorial;
	bool shownCharacterSwap;
	bool shownStun;
	bool shownCharge;
	bool shownCounter;
	int battlesSinceStarted;
	int roomsTIllFirstHeartShard;
	int roundsSinceReviveOpportunity;
	bool musicMuted;
	bool sfxMuted;
	bool hasSeenAgreement;
	int roomsTillSeenSponsorshipBot;
	bool allowNotifications;

	bool survivedTropical;
	bool survivedVolcano;
	bool survivedMonster;
	bool hasUsedBlissOnce;

	SaveLoadManager()
	{
		spawnerData = new GiftSpawner();
		roundsSinceReviveOpportunity = BattleManager.RoundsTillFirstReviveOpportunity;
		musicMuted = BAudioManager.MusicMuted;
		sfxMuted = BAudioManager.SfxMuted;
		roomsTillSeenSponsorshipBot = BSponsorshipManager.RoomsTilFirstSponsorshipBot;
		inTutorial = true;
		roomsTIllFirstHeartShard = 2;
		allowNotifications = true;
		hasSeenAgreement = BTitleOverlay.HasSeenAgreement;
	}

	static string filePath {get{return Application.persistentDataPath + "/savedgame.dat";}}
	static bool saveLoadBlocked = false;

	static void playerPresSave (BinaryFormatter binFormatter, SaveLoadManager data)
	{
		var bytes = Utilities.ObjectToByteArray(data, binFormatter);
		var str = System.Convert.ToBase64String (bytes);
		var strLen = str.Length;
		var chunks = Mathf.CeilToInt (strLen / (float)chunkSize);
		PlayerPrefs.SetInt (lengthKey, chunks);
		for (int i = 0; i < chunks; ++i) {
			var start = i * chunkSize;
			var end = Mathf.Min (start + chunkSize, strLen);
			var cd = str.Substring (i * chunkSize, end - start);
			PlayerPrefs.SetString (string.Format (prefixKey, i), cd);
		}
		PlayerPrefs.Save();
	}

	static void saveImpl (SaveLoadManager data)
	{
		data.playerData = PlayerManager.Instance;
		data.spawnerData = GiftSpawner.Instance;
		data.inTutorial = BTutorialManager.InTutorial;
		data.shownCharacterSwap = BTutorialManager.ShownCharacterSwap;
		data.shownStun = BTutorialManager.ShownStun;
		data.shownCharge = BTutorialManager.ShownChargeAttackTutorial;
		data.shownCounter = BTutorialManager.ShownCounterAttackTutorial;
		data.battlesSinceStarted = BTutorialManager.BattlesSinceStarted;
		data.roomsTIllFirstHeartShard = BTutorialManager.RoomsTillFirstHeartShard;
		data.roundsSinceReviveOpportunity = BattleManager.RoundsSinceLastReviveOpportunity;
		data.musicMuted = BAudioManager.MusicMuted;
		data.sfxMuted = BAudioManager.SfxMuted;
		data.hasSeenAgreement = BTitleOverlay.HasSeenAgreement;
		data.roomsTillSeenSponsorshipBot = BSponsorshipManager.RoomsTilCanSeeSponsorshipBot;
		data.allowNotifications = GameNotifications.AllowNotifications;
		data.survivedTropical = BGameCenterManager.SurvivedTropical;
		data.survivedVolcano = BGameCenterManager.SurvivedVolcano;
		data.survivedMonster = BGameCenterManager.SurvivedMonster;
		data.hasUsedBlissOnce = BGameCenterManager.HasUsedBlissOnce;
	}

	//This one saves/creates the file
	public static void Save()
	{
		if(saveLoadBlocked){
			return;
		}
		saveLoadBlocked = true;

		var binFormatter = new BinaryFormatter();

		var data = new SaveLoadManager();
		saveImpl (data);

		if (Application.installerName == androidCom) {
			playerPresSave (binFormatter, data);
		} else {
			var file = File.Create(filePath);
			binFormatter.Serialize(file, data);
			file.Close();
		}
		saveLoadBlocked = false;
	}
	public static void SaveDefaultValues()
	{
		var binFormatter = new BinaryFormatter();
		var freshSave = new SaveLoadManager();
		if (Application.installerName == androidCom) {
			PlayerPrefs.DeleteAll();
			playerPresSave(binFormatter, freshSave);
		} else {
			var file = File.Create(filePath);
			binFormatter.Serialize(file, freshSave);
			file.Close();
		}
		Load();
	}

	static SaveLoadManager playerPrefsLoad (BinaryFormatter binFormatter)
	{
		if (PlayerPrefs.HasKey (lengthKey)) {
			var len = PlayerPrefs.GetInt (lengthKey);
			var sb = new System.Text.StringBuilder ();
			for (int i = 0; i < len; ++i) {
				sb.Append (PlayerPrefs.GetString (string.Format (prefixKey, i)));
			}
			var byteArray = System.Convert.FromBase64String (sb.ToString ());
			return Utilities.ByteArrayToObject<SaveLoadManager>(byteArray, binFormatter);
		}
		return null;
	}

	static void loadImpl (SaveLoadManager data)
	{
		if (data.playerData != null) {
			PlayerManager.Instance = data.playerData;
		} else {
			PlayerManager.Instance = new PlayerManager ();
		}

		if (data.spawnerData != null) {
			GiftSpawner.Instance = data.spawnerData;
		} else {
			GiftSpawner.Instance = new GiftSpawner ();
		}
		BTutorialManager.InTutorial = data.inTutorial;
		BTutorialManager.ShownCharacterSwap = data.shownCharacterSwap;
		BTutorialManager.ShownStun = data.shownStun;
		BTutorialManager.ShownChargeAttackTutorial = data.shownCharge;
		BTutorialManager.ShownCounterAttackTutorial = data.shownCounter;
		BTutorialManager.BattlesSinceStarted = data.battlesSinceStarted;
		BTutorialManager.RoomsTillFirstHeartShard = data.roomsTIllFirstHeartShard;
		BattleManager.RoundsSinceLastReviveOpportunity = data.roundsSinceReviveOpportunity;
		BAudioManager.MusicMuted = data.musicMuted;
		BAudioManager.SfxMuted = data.sfxMuted;
		BTitleOverlay.HasSeenAgreement = data.hasSeenAgreement;
		BSponsorshipManager.RoomsTilCanSeeSponsorshipBot = data.roomsTillSeenSponsorshipBot;
		GameNotifications.AllowNotifications = data.allowNotifications;
		BGameCenterManager.SurvivedTropical = data.survivedTropical;
		BGameCenterManager.SurvivedVolcano = data.survivedVolcano;
		BGameCenterManager.SurvivedMonster = data.survivedMonster;
		BGameCenterManager.HasUsedBlissOnce = data.hasUsedBlissOnce;
	}

	//And this one loads it 
	public static void Load()
	{
		var binFormatter = new BinaryFormatter();
		if (Application.installerName == androidCom) {
			try {
				var data = playerPrefsLoad (binFormatter);
				if (data != null) {
					if(saveLoadBlocked){
						return;
					}
					saveLoadBlocked = true;
					loadImpl(data);
					saveLoadBlocked = false;
					return;
				}
			} catch(System.Exception){}
			SaveDefaultValues();
		} else {
			if (File.Exists (filePath)) {
				if(saveLoadBlocked){
					return;
				}
				saveLoadBlocked = true;

				var file = File.Open(filePath, FileMode.Open);
				var data = (SaveLoadManager)binFormatter.Deserialize(file);
				file.Close();
				loadImpl (data);
				saveLoadBlocked = false;
			} else {
				SaveDefaultValues();
			}
		}
	}
}
