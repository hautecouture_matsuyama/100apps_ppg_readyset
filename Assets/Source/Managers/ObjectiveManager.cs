﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ObjectiveManager
{
	public const uint ProgressPerRoom = 1;
	const uint minGiftObjectiveDelta = 160;
	const uint maxGiftObjectiveDelta = 200;

	public ObjectiveManager()
	{
		UpdateNextNarrativeObject();
		UpdateNextGiftObjective();
	}

	public uint NextNarrativeObjectivePercent{get; private set;}
	public uint NextGiftObjectivePercent{get; private set;}

	public bool CheckIfReachedNarrativeObjective(uint _percent)
	{
		return _percent >= NextNarrativeObjectivePercent;
	}
	public void UpdateNextNarrativeObject()
	{
		if (NextNarrativeObjectivePercent < 360) {
			NextNarrativeObjectivePercent += 120;
		} else {
			NextNarrativeObjectivePercent = uint.MaxValue;
		}
	}

	public bool CheckIfReachedGiftObjective(uint _percent)
	{
		return _percent >= NextGiftObjectivePercent;
	}
	public void UpdateNextGiftObjective()
	{
		NextGiftObjectivePercent += (uint)Random.Range(minGiftObjectiveDelta, maxGiftObjectiveDelta);
	}
}