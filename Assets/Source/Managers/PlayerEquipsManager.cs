﻿using System.Collections;
using System.Collections.Generic;

public class PlayerEquipsManager
{
	public static readonly Dictionary<PPG, ShopItem[]> Auras;
	public static readonly Dictionary<FriendlyMonsterType, FMShopItem[]> FriendlyMonsters;
	public static string[] AurasText = new string[30];

	static PlayerEquipsManager()
	{
		AurasText[0] = "Sponge";
		AurasText[1] = "Tape";
		AurasText[2] = "Vaccume";
		AurasText[3] = "Broom";
		AurasText[4] = "Dust Pan";
		AurasText[5] = "Glue";
		AurasText[6] = "Scrub Brush";
		AurasText[7] = "Spray Bottle";
		AurasText[8] = "Stapler";
		AurasText[9] = "Sticky Notes";
		AurasText[10] = "Bunny";
		AurasText[11] = "Cat";
		AurasText[12] = "Giraffe";
		AurasText[13] = "Bear";
		AurasText[14] = "Monkey";
		AurasText[15] = "Piggie";
		AurasText[16] = "Porpoise";
		AurasText[17] = "Rhino";
		AurasText[18] = "Seal";
		AurasText[19] = "Kangaroo";
		AurasText[20] = "Bomber";
		AurasText[21] = "Cannon";
		AurasText[22] = "Tank";
		AurasText[23] = "Grenade";
		AurasText[24] = "Helicopter";
		AurasText[25] = "Mace";
		AurasText[26] = "Missaile Launcher";
		AurasText[27] = "Rocket";
		AurasText[28] = "Stealth Fighter";
		AurasText[29] = "Submarine";


		Auras = new Dictionary<PPG, ShopItem[]>()
		{
			{
				PPG.Blossom, new [] {
					new ShopItem("スポンジ", "あの厄介なモンスター達を片付けよう", CurrencyType.Gold, 100),
					new ShopItem("テープ", "これがあれば何でも元どおり。", CurrencyType.Gold, 100),
					new ShopItem("掃除機", "ブーン　ブーン", CurrencyType.Gold, 100),
					new ShopItem("ホウキ", "サッサッサッ", CurrencyType.Gold, 100),
					new ShopItem("チリトリ", "厄介なやつらを集めよう", CurrencyType.Gold, 100),
					new ShopItem("接着剤", "あいつらにくらわせろ", CurrencyType.Gold, 100),
					new ShopItem("洗浄用ブラシ", "食器洗浄機ほしい？", CurrencyType.Gold, 100),
					new ShopItem("スプレーボトル", "いい子にしてろよ、さもなくばこれを顔面にお見舞いするぞ", CurrencyType.Gold, 100),
					new ShopItem("ホッチキス", "紙をまとめるのにこれ以上の方法はない", CurrencyType.Gold, 100),
					new ShopItem("メモ帳", "やるべきこと：姉をみつける、モンスターを倒す、島から脱出する", CurrencyType.Gold, 100)
				}
			},
			{
				PPG.Bubbles, new [] {
					new ShopItem("ウサギ", "勝利にむかってホップ！ホップ！ホップ！", CurrencyType.Gold, 100),
					new ShopItem("ネコ", "かわいい！抱きしめたい！でもそっけない。。。", CurrencyType.Gold, 100),
					new ShopItem("キリン", "普通のくまよりも背が高い", CurrencyType.Gold, 100),
					new ShopItem("クマ", "普通のキリンよりも背が低い", CurrencyType.Gold, 100),
					new ShopItem("サル", "これ以上ない最高のペット", CurrencyType.Gold, 100),
					new ShopItem("ブタ", "ブーブー", CurrencyType.Gold, 100),
					new ShopItem("ネズミイルカ", "一番頭のいい動物？たぶん。一番スタイリッシュ？間違いない", CurrencyType.Gold, 100),
					new ShopItem("サイ", "怒った彼女の方が怖いってホント？", CurrencyType.Gold, 100),
					new ShopItem("アザラシ", "見て！川にアザラシがいるよ！", CurrencyType.Gold, 100),
					new ShopItem("カンガルー", "ボクシングしようよ！", CurrencyType.Gold, 100)
				}
			},
			{
				PPG.Buttercup, new [] {
					new ShopItem("爆撃機", "爆弾投下！", CurrencyType.Gold, 100),
					new ShopItem("大砲", "タマ込め〜", CurrencyType.Gold, 100),
					new ShopItem("戦車", "鉄と鋼の荒れ狂う塊", CurrencyType.Gold, 100),
					new ShopItem("手りゅう弾", "爆発するぞ！", CurrencyType.Gold, 100),
					new ShopItem("ヘリコプター", "のりこめ!!", CurrencyType.Gold, 100),
					new ShopItem("トゲ鉄球", "振り回して、放す", CurrencyType.Gold, 100),
					new ShopItem("ミサイルランチャー", "スゴく爆発する", CurrencyType.Gold, 100),
					new ShopItem("ロケットミサイル", "もっとスゴく爆発する", CurrencyType.Gold, 100),
					new ShopItem("ステルス戦闘機", "卑きょうなかたち", CurrencyType.Gold, 100),
					new ShopItem("潜水艦", "警備隊長、ブイを浮かべる", CurrencyType.Gold, 100)
				}
			}
		};
			
		FriendlyMonsters = new Dictionary<FriendlyMonsterType, FMShopItem[]>()
		{
			{
				//Maps to the "A" type of FM assets
				FriendlyMonsterType.Fighter, new [] {
					new FMShopItem("Ciara", "数秒ごとに小ダメージで敵を攻撃してくれる。", CurrencyType.Gold, 100, FriendlyMonsterType.Fighter),
					new FMShopItem("Sue", "数秒ごとに小ダメージで敵を攻撃してくれる。", CurrencyType.Gold, 100, FriendlyMonsterType.Fighter),
					new FMShopItem("Sim", "数秒ごとに小ダメージで敵を攻撃してくれる。", CurrencyType.Gold, 100, FriendlyMonsterType.Fighter),
					new FMShopItem("Lucian", "数秒ごとに小ダメージで敵を攻撃してくれる。", CurrencyType.Gold, 100, FriendlyMonsterType.Fighter),
					new FMShopItem("Peep", "数秒ごとに小ダメージで敵を攻撃してくれる。", CurrencyType.Gold, 100, FriendlyMonsterType.Fighter),
					new FMShopItem("Pepe", "数秒ごとに小ダメージで敵を攻撃してくれる。", CurrencyType.Gold, 100, FriendlyMonsterType.Fighter)
				}
			},

			{
				//Maps to the "B" type of FM assets
				FriendlyMonsterType.Medic, new [] {
					new FMShopItem("Piper", "数秒ごとにガールズを小回復してくれる。", CurrencyType.Gold, 100, FriendlyMonsterType.Medic),
					new FMShopItem("MJ", "数秒ごとにガールズを小回復してくれる。", CurrencyType.Gold, 100, FriendlyMonsterType.Medic),
					new FMShopItem("PJ", "数秒ごとにガールズを小回復してくれる。", CurrencyType.Gold, 100, FriendlyMonsterType.Medic),
					new FMShopItem("Mick", "数秒ごとにガールズを小回復してくれる。", CurrencyType.Gold, 100, FriendlyMonsterType.Medic),
					new FMShopItem("Tishbi", "数秒ごとにガールズを小回復してくれる。", CurrencyType.Gold, 100, FriendlyMonsterType.Medic),
					new FMShopItem("Rose", "数秒ごとにガールズを小回復してくれる。", CurrencyType.Gold, 100, FriendlyMonsterType.Medic)
				}
			},

			{
				//Maps to the "C" type of FM assets
				FriendlyMonsterType.Sapper, new [] {
					new FMShopItem("Hank", "ガールズの攻撃がヒットした際、敵のSTにもダメージをあたえる。", CurrencyType.Gold, 100, FriendlyMonsterType.Sapper),
					new FMShopItem("Alex", "ガールズの攻撃がヒットした際、敵のSTにもダメージをあたえる。", CurrencyType.Gold, 100, FriendlyMonsterType.Sapper),
					new FMShopItem("Munchy", "ガールズの攻撃がヒットした際、敵のSTにもダメージをあたえる。", CurrencyType.Gold, 100, FriendlyMonsterType.Sapper),
					new FMShopItem("Sam", "ガールズの攻撃がヒットした際、敵のSTにもダメージをあたえる。", CurrencyType.Gold, 100, FriendlyMonsterType.Sapper),
					new FMShopItem("Poops", "ガールズの攻撃がヒットした際、敵のSTにもダメージをあたえる。", CurrencyType.Gold, 100, FriendlyMonsterType.Sapper),
					new FMShopItem("Frank", "ガールズの攻撃がヒットした際、敵のSTにもダメージをあたえる。", CurrencyType.Gold, 100, FriendlyMonsterType.Sapper)
				}
			},

			{
				//Maps to the "D" type of FM assets
				FriendlyMonsterType.Guard, new [] {
					new FMShopItem("Lu", "敵の攻撃を時々ガードしてくれる。", CurrencyType.Gold, 100, FriendlyMonsterType.Guard),
					new FMShopItem("Jack", "敵の攻撃を時々ガードしてくれる。", CurrencyType.Gold, 100, FriendlyMonsterType.Guard),
					new FMShopItem("Bits", "敵の攻撃を時々ガードしてくれる。", CurrencyType.Gold, 100, FriendlyMonsterType.Guard),
					new FMShopItem("Linus", "敵の攻撃を時々ガードしてくれる。", CurrencyType.Gold, 100, FriendlyMonsterType.Guard),
					new FMShopItem("Isabelle", "敵の攻撃を時々ガードしてくれる。", CurrencyType.Gold, 100, FriendlyMonsterType.Guard),
					new FMShopItem("Archer", "敵の攻撃を時々ガードしてくれる。", CurrencyType.Gold, 100, FriendlyMonsterType.Guard)
				}
			},

			{
				//Maps to the "E" type of FM assets
				FriendlyMonsterType.Patron, new [] {
					new FMShopItem("Tika", "数秒ごとにコインを投げてくれる。", CurrencyType.Gold, 100, FriendlyMonsterType.Patron),
					new FMShopItem("Fuzzy Pants", "数秒ごとにコインを投げてくれる。", CurrencyType.Gold, 100, FriendlyMonsterType.Patron),
					new FMShopItem("Ernie", "数秒ごとにコインを投げてくれる。", CurrencyType.Gold, 100, FriendlyMonsterType.Patron),
					new FMShopItem("Asche", "数秒ごとにコインを投げてくれる。", CurrencyType.Gold, 100, FriendlyMonsterType.Patron),
					new FMShopItem("Ginger", "数秒ごとにコインを投げてくれる。", CurrencyType.Gold, 100, FriendlyMonsterType.Patron),
					new FMShopItem("Mele", "数秒ごとにコインを投げてくれる。", CurrencyType.Gold, 100, FriendlyMonsterType.Patron)
				}
			}
		};
	}
		


}

public class ShopItem
{
	public string Name;
	public string Description;
	public CurrencyType CurrencyType;
	public int Cost;

	public ShopItem(string _name, string _desc, CurrencyType _currType, int _cost) 
	{
		Name = _name;
		Description = _desc;
		CurrencyType = _currType;
		Cost = _cost;
	}
}

public class FMShopItem : ShopItem
{
	//TODO if we make the different monsters of each class type slightly different in their stat values, we need a way to pull those stat variations into the descriptions
	public FriendlyMonsterType Type;

	public FMShopItem(string _name, string _desc, CurrencyType _currType, int _cost, FriendlyMonsterType _type) : base(_name, _desc, _currType, _cost)
	{
		Type = _type;
	}
}
