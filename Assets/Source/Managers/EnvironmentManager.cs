﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EnvironmentManager
{
	public enum EnvironmentType
	{
		Tropical,
		Volcano,
		Monster
	}
	public static EnvironmentType CurrentEnvironment{get; private set;}
	public static EnvironmentType GetEnvironmentBasedOnProgress(uint _progress)
	{
		_progress = (_progress + 360) % 360;
		if (_progress < 120) {
			return EnvironmentType.Tropical;
		} else if (_progress < 240) {
			return EnvironmentType.Volcano;
		} else {
			return EnvironmentType.Monster;
		}
	}
	public static void SetEnvironmentBasedOnProgress(uint _progress)
	{
		CurrentEnvironment = GetEnvironmentBasedOnProgress(_progress);
	}
}
