﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public enum GameState
{
	CharacterSwap,
	Navigation,
	ObjectiveEvent,
	PreBattle,
	Battle,
	PostBattle,
	SuperAttack,
	Limbo,
	GameOver
}

public static class BattleManager
{
	public delegate void OnStateSwitch(GameState _state);
	public static event OnStateSwitch HandleStateSwitch;

	public delegate void OnDefeatEnemy(BEnemyAgent _enemy);
	public static event OnDefeatEnemy HandleDefeatEnemy;

	public delegate void OnDefeatPlayer(BPlayerAgent _player);
	public static event OnDefeatPlayer HandleDefeatPlayer;

	public static BPlayerAgent PlayerPartyLead;
	public static BEnemyAgent EnemyPartyLead;

	static bool lockState = false;
	static GameState currentState;
	public static GameState CurrentGameState
	{
		get{ return currentState;}
		set{
			if (!lockState) {
				currentState = value;
				if (HandleStateSwitch != null) {
					HandleStateSwitch(CurrentGameState);
				}
			}
		}
	}
	public static bool InBattle{get{return CurrentGameState == GameState.Battle;}}

	public const int RoundsTillReviveOpportunity = 3;
	public const int RoundsTillFirstReviveOpportunity = 2;
	public static int RoundsSinceLastReviveOpportunity{get; set;}
	public static int DecrementRoundsSinceLastReviveOpportunity()
	{
		RoundsSinceLastReviveOpportunity = Mathf.Clamp(RoundsSinceLastReviveOpportunity - 1, 0, RoundsSinceLastReviveOpportunity - 1);
		SaveLoadManager.Save();
		return RoundsSinceLastReviveOpportunity;
	}
	public static void ResetRoundsSinceLastReviveOpportunity()
	{
		RoundsSinceLastReviveOpportunity = Random.Range(2, RoundsTillReviveOpportunity + 1);
		SaveLoadManager.Save();
	}
	static bool hasSeenReviveThisRound;

	public static void StartCharacterSwap()
	{
		CurrentGameState = GameState.CharacterSwap;
	}
	public static void StartNavigation()
	{
		CurrentGameState = GameState.Navigation;
		hasSeenReviveThisRound = false;
	}

	public static void StartBattle()
	{
		PlayerPartyLead = Object.FindObjectOfType<BPlayerAgent>();
		EnemyPartyLead = Object.FindObjectOfType<BEnemyAgent>();
		BPlayerInput.Instance.SwitchToBattleUI();
		BPlayerInput.Instance.DisableBlockButton();
		BPlayerInput.Instance.DisableAttackButton();
		lockState = false;
		CurrentGameState = GameState.PreBattle;

		BDelayInvoker.Instance.InvokeAfterDelay(() => {
			CurrentGameState = GameState.Battle;
			if (!BTutorialManager.InTutorial) {
				BPlayerInput.Instance.EnableBlockButton();
				BPlayerInput.Instance.EnableAttackButton();
			}
			PlayerPartyLead.OnBattleStart();
		}, 1f);
	}
	public static void EndBattle()
	{
		PlayerPartyLead.OnBattleEnd();
		EnemyPartyLead.OnBattleEnd();
		BPlayerInput.Instance.SwitchToNavUI();
		if (EnemyPartyLead.IsDead) {
			CurrentGameState = GameState.PostBattle;
			PlayerPartyLead.OnBattleWon();
			BCoroutine.WaitAndPerform (BPlayerNavParty.Instance.MoveGirlToCenter, DesignValues.PostDeathWaitTime);
		} else if (PlayerPartyLead.IsDead) {
			if (PlayerManager.AreAllGirlsDead()) {
				System.Action reviveAction = () => {
					CurrentGameState = GameState.Limbo;
					BDebugOverlay.ForceReviveOnNextDeath = false;
					hasSeenReviveThisRound = true;
					BCoroutine.WaitAndPerform(() => {
						BGenericConfirmOverlay.SpawnConfirm("この場で復活する!", "広告を見てこのまま続ける？", "見る", PlayReviveAd, launchResults);
					}, DesignValues.PostDeathWaitTime);
				};
				System.Action deathAction = () => {
					CurrentGameState = GameState.PostBattle;
					//enemies win
					launchResults();
				};

				if (BDebugOverlay.ForceReviveOnNextDeath) {
					reviveAction ();
				} else {
					BCoroutine.PerformInternetCheck(
						() => {
							if(!hasSeenReviveThisRound && DecrementRoundsSinceLastReviveOpportunity() == 0) {
								reviveAction();
							} else {
								deathAction();
							}
						}, () => {
							deathAction();
					});
				}
			} else {
				CurrentGameState = GameState.Limbo;
				lockState = true;
				BCoroutine.WaitAndPerformRealtime(() => {
					var script = PlayerManager.SwitchToNextPPGInBattle();
					BCoroutine.Instance.StartCoroutine(waitForSpawnOverlayToClose(script));
				}, DesignValues.PostDeathWaitTime);
				return;
			}
		}
		BCoroutine.WaitAndPerform(() => {
			PlayerPartyLead = null;
			EnemyPartyLead = null;
		}, DesignValues.PostDeathWaitTime);
		BTutorialManager.BattlesSinceStarted++;
	}
	static IEnumerator waitForSpawnOverlayToClose(object _data)
	{
		var script = _data as BGirlSelectOverlay;
		var selected = _data is PPG ? (PPG)_data : PPG.None;
		while (selected == PPG.None) {
			yield return null;
			if (script != null) {
				selected = script.Selected;
			}
		}
		var rends = PlayerPartyLead.GetComponentsInChildren<tk2dSprite>();
		var tweens = new Tweener[rends.Length];
		for (int i = 0; i < rends.Length; ++i) {
			tweens[i] = rends[i].DOFade(0f, 0.1f).SetEase(Ease.Linear).SetLoops(-1,LoopType.Yoyo);
		}
		yield return new WaitForSeconds(DesignValues.PostDeathWaitTime);
		for (int i = 0; i < rends.Length; ++i) {
			tweens[i].Kill(true);
			rends[i].DOFade(0f,0f).Kill(true);
		}
		tweens = null;
		PlayerManager.SetActiveGirl(selected);
		while (GameObject.FindObjectOfType<BPlayerAgent>() != null) yield return null;
		while (GameObject.FindObjectOfType<BPlayerAgent>() == null) yield return null;
		StartBattle();
		System.GC.Collect();
	}
	public static BAgent GetOpponent(BAgent _agent)
	{
		return _agent == PlayerPartyLead ? (BAgent)EnemyPartyLead : (BAgent)PlayerPartyLead;;
	}
	public static void AttackOpponent(AttackPacket _packet)
	{
		if (InBattle) {
			GetOpponent(_packet.Source).SetTakeDamagePacket(_packet);
		}
	}
	public static void PerformSuperAttack(SuperAttack _attack)
	{
		BPlayerInput.Instance.SwitchToSuperUI();
		CurrentGameState = GameState.SuperAttack;
		EnemyPartyLead.PrepareForSuper();
		PlayerPartyLead.PrepareForSuper();
		{
			var e = EnemyPartyLead;
			MonoBehaviour enemyAI = null;
			if (e is BBruteAgent) {
				enemyAI = e.GetComponent<BEnemyAI<BBruteAgent>> ();
			} else if (e is BBigBruteAgent) {
				enemyAI = e.GetComponent<BEnemyAI<BBigBruteAgent>> ();
			} else if (e is BFlowerAgent) {
				enemyAI = e.GetComponent<BEnemyAI<BFlowerAgent>> ();
			} else if (e is BBigFlowerAgent) {
				enemyAI = e.GetComponent<BEnemyAI<BBigFlowerAgent>> ();
			} else if (e is BFlyerAgent) {
				enemyAI = e.GetComponent<BEnemyAI<BFlyerAgent>> ();
			} else if (e is BBigFlyerAgent) {
				enemyAI = e.GetComponent<BEnemyAI<BBigFlyerAgent>> ();
			}  
			enemyAI.enabled = false;
		}
		BCoroutine.Instance.StartCoroutine(_attack == SuperAttack.TornadoTrio ? performNormalSuper() : performBlissSuper());
	}
	static IEnumerator onSuperEnd(Object _instance, float _damage, float _waitForAnimEndTime = 0)
	{
		yield return new WaitForSeconds(0.25f);
		CurrentGameState = GameState.Battle;
		AttackOpponent(new AttackPacket(PlayerPartyLead, -1, _damage, false));
		yield return new WaitForSeconds(_waitForAnimEndTime);
		Object.Destroy(_instance);
		Resources.UnloadUnusedAssets();
		PlayerPartyLead.gameObject.SetActive(true);
		BPlayerInput.Instance.SwitchFromSuperUI();
		if(PlayerManager.IsSuperAttackEquipped(SuperAttack.BlissBlitz)) {
			if(!BGameCenterManager.HasUsedBlissOnce) {
				BGameCenterManager.HasUsedBlissOnce = true;
				SaveLoadManager.Save();
			}
			BGameCenterManager.ReportProgress(GameCenterConstants.ACH_BLISS_USED, 1);
		}
	}
	static IEnumerator performNormalSuper()
	{
		PlayerPartyLead.gameObject.SetActive(false);
		var prefab = Resources.Load<GameObject>("Prefabs/Characters/Players/TornadoSuperMove");
		var instance = Object.Instantiate(prefab);
		instance.transform.position = PlayerPartyLead.transform.position + new Vector3(0, DesignValues.NormalSuperMove.SpawnYOffset, 0);

		yield return new WaitForSeconds(DesignValues.NormalSuperMove.Duration + 1f);
		yield return BCoroutine.Instance.StartCoroutine(onSuperEnd(instance, DesignValues.NormalSuperMove.Damage));
	}
	static IEnumerator performBlissSuper()
	{
		var prefab = Resources.Load<GameObject>("Prefabs/Characters/Players/Blisstina");
		var instance = Object.Instantiate(prefab);

		yield return BCoroutine.Instance.StartCoroutine (instance.GetComponent<BBlisstinaAnimator> ().PerformAnimation ());
		yield return BCoroutine.Instance.StartCoroutine(onSuperEnd(instance, DesignValues.BlissSuperMove.Damage, 1.333f));
	}
	public static void OnCurrentEnemyLeadDied()
	{
		var curEnemies = BWorldMapDragger.Instance.CurrentRoom.Enemies;
		if (curEnemies.Contains(EnemyPartyLead)) {
			curEnemies.Remove(EnemyPartyLead);
		}
		if (HandleDefeatEnemy != null) {
			HandleDefeatEnemy(EnemyPartyLead);
		}
		BDifficultyManager.Instance.IncrementDifficulty();
		EndBattle();
	}
	public static void OnCurrentPlayerLeadDied()
	{
		if (HandleDefeatPlayer != null) {
			HandleDefeatPlayer(PlayerPartyLead);
		}
		EndBattle();
	}



	public static void PlayReviveAd()
	{
		BattleManager.ResetRoundsSinceLastReviveOpportunity();

		Utilities.DisableRenderingForAds ();
		BAudioManager.Instance.ForceMuteForAds(true);

		ApplovinManager.Instance.ShowReword ();

	   /*BattleManager.ResetRoundsSinceLastReviveOpportunity();
		if (!BDebugOverlay.SkipAds) {
			Utilities.DisableRenderingForAds ();
			BAudioManager.Instance.ForceMuteForAds(true);
			//FreewheelManager.RequestAd(AdType.Preroll, OnReviveAdWatched);

			if(AppLovin.IsIncentInterstitialReady()){
				AppLovin.ShowRewardedInterstitial ();
			}


			#if UNITY_EDITOR
			BCoroutine.Instance.StartCoroutine(Utilities.WaitForFakeAdTime());
			#endif
		} else {
			OnReviveAdWatched();
		}

*/
	}



	public static void OnReviveAdWatched()
	{
		Utilities.EnableRenderingForAds();
		BAudioManager.Instance.ForceMuteForAds (false);
		StartBattle();
		PlayerPartyLead.Revive();
		//Turn the results bg music back off, and turn the volume back to normal, in case nav continues
		BAudioManager.Instance.TogglePauseMusic();
		BAudioManager.Instance.AdjustMusicVolume(0.5f, 0.3f);
		//Play the battle music again
		BPlayerNavParty.Instance.PlayBattleMusic();
	}
	static void launchResults()
	{
		CurrentGameState = GameState.GameOver;
		BCoroutine.WaitAndPerform(() => BResultsOverlay.Instance.gameObject.SetActive (true), DesignValues.PostDeathWaitTime);
	}


	#region Calc Methods
	public static float CalculateHealth(float _baseHealthStat, float _aquiredHealthStat)
	{
		return DesignValues.BaseAgentStats.BaseAgentHealth + ((_baseHealthStat + _aquiredHealthStat) * DesignValues.BaseAgentStats.MaxHealthModifier);
	}
	#endregion
}
