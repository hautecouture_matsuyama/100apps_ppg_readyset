﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// A GameObject pool
/// </summary>
public class ObjectPool
{
	//Just so the scene view isn't a giant CF
	static Transform poolParent
	{
		get {
			var pool = GameObject.Find ("Pool");
			if (pool == null) {
				pool = new GameObject ("Pool");
			}
			return pool.transform;
		}
	}
	/// <summary>
	/// The pool. Used as a deque
	/// </summary>
	readonly LinkedList<GameObject> pool = new LinkedList<GameObject>();
	/// <summary>
	/// The function that creates the item for the pool
	/// </summary>
	readonly System.Func<GameObject> creator;
	/// <summary>
	/// The function taht inits the item after being created or pulled form the pool
	/// </summary>
	readonly System.Action<GameObject, GameObject> initializer;
	/// <summary>
	/// The default game object to model newly acquired GOs after
	/// </summary>
	readonly GameObject defaultMimic;
	/// <summary>
	/// Initializes a new instance of the <see cref="ObjectPool"/> class.
	/// </summary>
	/// <param name="_creator">A function that returns a gameObject</param>
	/// <param name="_initializer">A function that inits the game object</param>
	public ObjectPool(GameObject _prefab, System.Action<GameObject, GameObject> _initializer)
	{
		if (_prefab == null) {
			throw new System.Exception("Need a prefab to make the gameobject");
		}
		creator = () => Object.Instantiate (_prefab);
		initializer = _initializer;
		defaultMimic = _prefab;
	}
	/// <summary>
	/// Fill the pool with the specified count and yields at _yieldEvery.
	/// </summary>
	/// <param name="_count">Number of items to fill the pool with.</param>
	public void Fill(int _count)
	{
		if (pool.Count > 0) {
			foreach(var go in pool) {
				Object.Destroy(go);
			}
			pool.Clear();
		}
		for(int i = 0; i < _count; ++i) {
			Release(creator());
		}
	}
	/// <summary>
	/// Fill the pool with the specified count and yields at _yieldEvery.
	/// </summary>
	/// <param name="_count">Number of items to fill the pool with.</param>
	/// <param name="_yieldEvery">How often to yield return.</param>
	public IEnumerator Fill(int _count, int _yieldEvery)
	{
		if (pool.Count > 0) {
			foreach(var go in pool) {
				Object.Destroy(go);
			}
			pool.Clear();
		}
		int counter = 0;
		for(int i = 0; i < _count; ++i) {
			Release(creator());
			counter++;
			if (counter == _yieldEvery) {
				yield return null;
				counter = 0;
			}
		}
	}
	public GameObject Acquire()
	{
		return Acquire(defaultMimic);
	}
	/// <summary>
	/// Acquires an object from the pool. If the pool is empty, a new item will be created and returned
	/// </summary>
	/// <param name="_mimic">The game object to mimic</param>
	/// <returns>>The acquired game object</returns>
	public GameObject Acquire(GameObject _mimic)
	{
		if (pool.Count == 0) {
			Release(creator());
		}
		var instance = pool.First.Value;
		pool.RemoveFirst();
		if (instance == null) {
			return Acquire(_mimic);
		}
		instance.SetActive(true);
		#if UNITY_EDITOR
		instance.transform.SetParent(null);
		#endif
		if (initializer != null) {
			initializer(instance, _mimic);
		}
		return instance;
	}
	/// <summary>
	/// Release the specified _item back to the pool.
	/// </summary>
	/// <param name="_item">The item to put back in the pool.</param>
	public void Release(GameObject _item)
	{
		if (_item != null && !pool.Contains(_item)) {
			pool.AddLast(_item);
			_item.SetActive(false);
			#if UNITY_EDITOR
			_item.transform.SetParent(poolParent);
			#endif
		}
	}
	/// <summary>
	/// Gets the number of items currently in the pool
	/// </summary>
	/// <value>The count.</value>
	public int Count
	{
		get{return pool.Count;}
	}
}
