﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A static "stuff-doer" class to decide which gift to give of a specified type
/// </summary>
public static class GiftAvailability
{
	#region Auras
	/// <summary>
	/// Gets a value indicating whether there are any auras remaining.
	/// </summary>
	/// <value><c>true</c> if any auras remaining; otherwise, <c>false</c>.</value>
	public static bool AnyAurasRemaining(Dictionary<PPG, List<int>> _cachedAuras = null)
	{
		return getCountFromDict(_cachedAuras ?? RemainingAuras ());
	}
	/// <summary>
	/// Gets a value indicating whether there are any auras remaining using the raw listing of remaining auras.
	/// </summary>
	/// <value><c>true</c> if any auras remaining; otherwise, <c>false</c>.</value>
	public static bool AnyAurasRemainingRaw(Dictionary<PPG, List<int>> _cachedAuras = null)
	{
		return getCountFromDict(_cachedAuras ?? RemainingAurasRaw ());
	}
	/// <summary>
	/// Gets the remaining auras
	/// </summary>
	/// <value>The indicies fo the remaining auras.</value>
	public static Dictionary<PPG, List<int>> RemainingAuras()
	{
		var ret = getRemaining<PPG>(new[] {
			PPG.Blossom,
			PPG.Bubbles,
			PPG.Buttercup
		}, girl => PlayerManager.Instance.GetAcquiredStatForGirl (girl).UnlockedAuras);
		if (!PlayerManager.IsGirlUnlocked(PPG.Blossom)) {
			ret.Remove(PPG.Blossom);
		}
		if (!PlayerManager.IsGirlUnlocked(PPG.Bubbles)) {
			ret.Remove(PPG.Bubbles);
		}
		if (!PlayerManager.IsGirlUnlocked(PPG.Buttercup)) {
			ret.Remove(PPG.Buttercup);
		}
		return ret;
	}
	/// <summary>
	/// Gets the remaining auras but ignores whether the girl is unlocked or not
	/// </summary>
	/// <value>The indicies fo the remaining auras.</value>
	public static Dictionary<PPG, List<int>> RemainingAurasRaw()
	{
		var ret = getRemaining<PPG>(new[] {
			PPG.Blossom,
			PPG.Bubbles,
			PPG.Buttercup
		}, girl => PlayerManager.Instance.GetAcquiredStatForGirl (girl).UnlockedAuras);
		return ret;
	}
	/// <summary>
	/// Unlocks a random aura.
	/// </summary>
	/// <returns>A pairing of the girl to whom the aura belongs and the aura index.</returns>
	public static KeyValuePair<PPG,int> UnlockRandomAura(Dictionary<PPG, List<int>> _cachedAuras = null)
	{
		var remaining = (_cachedAuras ?? RemainingAuras ());
		if (AnyAurasRemaining(remaining)) {
			return getPairing<PPG>(remaining);
		}
		return new KeyValuePair<PPG,int>(PPG.None, -1);
	}
	#endregion

	#region Friendly Monster
	/// <summary>
	/// Gets a value indicating whether there are any friendly monsters remaining.
	/// </summary>
	/// <value><c>true</c> if any friendly monsters remaining; otherwise, <c>false</c>.</value>
	public static bool AnyFriendlyMonsterRemaining(Dictionary<FriendlyMonsterType, List<int>> _cachedFMs = null)
	{
		return getCountFromDict(_cachedFMs ?? RemainingFriendlyMonsters ());
	}
	/// <summary>
	/// Gets the remaining friendly monsters
	/// </summary>
	/// <value>The indicies fo the remaining friendy monsters.</value>
	public static Dictionary<FriendlyMonsterType, List<int>> RemainingFriendlyMonsters()
	{
		return getRemaining<FriendlyMonsterType> (Utilities.GetValues<FriendlyMonsterType> ().ToArray (),
			PlayerManager.Instance.GetUnlockedFriendlyMonsterArray);
	}
	/// <summary>
	/// Unlocks a random friendly manster.
	/// </summary>
	/// <returns>A pairing of the friendly monster and the index.</returns>
	public static KeyValuePair<FriendlyMonsterType,int> UnlockRandomFriendlyMonster(Dictionary<FriendlyMonsterType, List<int>> _cachedFMs = null)
	{
		var remaining = (_cachedFMs ?? RemainingFriendlyMonsters());
		if (AnyFriendlyMonsterRemaining(remaining)) {
			return getPairing<FriendlyMonsterType>(remaining);
		}
		throw new System.ArgumentOutOfRangeException("no more remaining FMs");
	}
	#endregion

	#region Helper
	static bool getCountFromDict<T>(Dictionary<T, List<int>> _dict)
	{
		return _dict.Values.ToList ().Sum (s => s.Count) > 0;
	}
	static Dictionary<T, List<int>> getRemaining<T>(T[] _array, Func<T, BitArray> _getter)
	{
		var ret = new Dictionary<T, List<int>>();
		foreach(var constituent in _array) {
			var subList = _getter(constituent);
			var list = new List<int>();
			for(int i = 0; i < subList.Length; ++i) {
				if (!subList.Get(i)) {
					list.Add(i);
				}
			}
			ret.Add(constituent, list);
		}
		return ret;
	}
	static KeyValuePair<T, int> getPairing<T>(Dictionary<T, List<int>> _remaining)
	{
		var weighted = new Dictionary<T, float>();
		foreach(var kv in _remaining) {
			weighted.Add(kv.Key, (float)kv.Value.Count);
		}
		var type = Utilities.GetWeightedRandomItem(weighted);
		var list = _remaining[type];
		return new KeyValuePair<T, int>(type, list.ToArray().RandomElement());
	}
	#endregion
}
