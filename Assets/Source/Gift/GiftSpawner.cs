﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// A class that determines WHICH gift to spawn (but not the exact type - <see cref="GiftDecider"/> does that job)
/// </summary>
[System.Serializable]
public class GiftSpawner
{
	public const int RoundsTillEarnOpportunity = 3;
	public const int RoundsTillFirstEarnOpportunity = 3;
	public enum GiftItem
	{
		Coin50,
		Coin100,
		Coin200,
		HeartShard,
		Aura,
		FriendlyMonster
	}
	readonly static System.TimeSpan timeTilResetFreeGiftWaitTier = new System.TimeSpan(7, 0, 0, 0);
	static System.DateTime TimeSinceFreeGiftTierCapped{get; set;}

	public static GiftSpawner Instance = new GiftSpawner();

	public System.DateTime TimeSinceLastFreeGift;
	public int RoundsSinceLastEarnOppurtunty;
	public int FreeGiftWaitTier;

	public GiftSpawner()
	{
		TimeSinceLastFreeGift = System.DateTime.UtcNow;
		RoundsSinceLastEarnOppurtunty = RoundsTillFirstEarnOpportunity;
		FreeGiftWaitTier = 0;
	}

	public readonly static System.TimeSpan[] HoursTillFreeGift = {
		new System.TimeSpan(0, 0, 0),
		new System.TimeSpan(0, 5, 0),
		new System.TimeSpan(0, 15, 0),
		new System.TimeSpan(0, 30, 0),
		new System.TimeSpan(1, 0, 0),
		new System.TimeSpan(3, 0, 0),
		new System.TimeSpan(6, 0, 0)
	};
	public static void ResetTimeSinceLastFreeGift()
	{
		//If the FreeGiftWaitTier is about to cap, start the timer for resetting the tier
		if(Instance.FreeGiftWaitTier == HoursTillFreeGift.Length - 2) {
			TimeSinceFreeGiftTierCapped = System.DateTime.UtcNow;
		}
		//Increase the tier, but make sure it caps out
		Instance.FreeGiftWaitTier = Mathf.Clamp( Instance.FreeGiftWaitTier + 1, 0, HoursTillFreeGift.Length - 1);

		//If ther tier is capped AND the timer for resetting it is done, reset the tier
		if((Instance.FreeGiftWaitTier == HoursTillFreeGift.Length - 1) && (System.DateTime.UtcNow - TimeSinceFreeGiftTierCapped >= timeTilResetFreeGiftWaitTier)) {
			Instance.FreeGiftWaitTier = 1;
		}
		//Reset the free gift timer, save and gtfo
		Instance.TimeSinceLastFreeGift = System.DateTime.UtcNow;
		SaveLoadManager.Save();
	}
	public static int DecrementRoundsSinceLastEarnOpportunity()
	{
		Instance.RoundsSinceLastEarnOppurtunty = Mathf.Max(Instance.RoundsSinceLastEarnOppurtunty - 1, 0);
		SaveLoadManager.Save();
		return Instance.RoundsSinceLastEarnOppurtunty;
	}
	public static void ResetRoundsSinceLastEarnOpportunity()
	{
		Instance.RoundsSinceLastEarnOppurtunty = Random.Range(1, RoundsTillEarnOpportunity + 1);
		SaveLoadManager.Save();
	}
	/// <summary>
	/// Helper method that gets an item based on the provided chances
	/// </summary>
	/// <returns>The random item from chance.</returns>
	/// <param name="_chances">A dictionary of item to chance</param>
	/// <remarks>Uses the total sum of all floats as the max value - meaning something MUST return</remarks>
	static GiftItem getRandomItemFromChance(IDictionary<GiftItem, float> _chances)
	{
		float running = 0;
		var chance = Random.Range(0f, _chances.Sum(kv => kv.Value));
		foreach(var kv in _chances) {
			running += kv.Value;
			if (chance <= running) {
				return kv.Key;
			}
		}
		throw new System.Exception("Something went horribly awry - couldn't find an item to yeild");
	}	

	public static GiftItem GetItemForFreeGift()
	{
		return getRandomItemFromChance(new Dictionary<GiftItem, float>() {
			{GiftItem.Coin50, 0.3f},
			{GiftItem.Coin100, 0.4f},
			{GiftItem.Coin200, 0.15f},
			{GiftItem.Aura, canGiveAura() ? 0.1f :  0f},
			{GiftItem.FriendlyMonster, GiftAvailability.AnyFriendlyMonsterRemaining() ? 0.05f : 0f},
		});
	}
	public static GiftItem GetItemForWinAPrizeGift()
	{
		return getRandomItemFromChance(new Dictionary<GiftItem, float>() {
			{GiftItem.HeartShard, 0.15f},
			{GiftItem.Aura, canGiveAura() ? 0.5f : 0f},
			{GiftItem.FriendlyMonster, GiftAvailability.AnyFriendlyMonsterRemaining() ? 0.35f : 0f},
		});
	}
	public static GiftItem GetItemForEarnStation()
	{
		return getRandomItemFromChance(new Dictionary<GiftItem, float>() {
			{GiftItem.Coin100, 0.6f},
			{GiftItem.Coin200, 0.3f},
			{GiftItem.HeartShard, 0.1f}
		});
	}
	public static GiftItem GetItemForEarnResults(bool _earnCoins)
	{
		if(_earnCoins) {
			return getRandomItemFromChance(new Dictionary<GiftItem, float>() {
				{GiftItem.Coin100, 0.7f},
				{GiftItem.Coin200, 0.3f}
			});
		} else {
			return getRandomItemFromChance(new Dictionary<GiftItem, float>() {
				{GiftItem.HeartShard, 0.2f},
				{GiftItem.Aura, canGiveAura() ? 0.45f : 0f},
				{GiftItem.FriendlyMonster, GiftAvailability.AnyFriendlyMonsterRemaining() ? 0.35f : 0f},
			});
		}
	}
	static bool canGiveAura()
	{
		var girls = new []{PPG.Blossom, PPG.Bubbles, PPG.Buttercup};
		foreach(var girl in girls) {
			if (PlayerManager.IsGirlUnlocked(girl) &&
				PlayerManager.Instance.GetAcquiredStatForGirl(girl).AuraUnlocked &&
				!PlayerManager.Instance.GetAcquiredStatForGirl(girl).AllAurasUnlocked()) {
				return true;
			} 
		}
		return false;
	}
}
