﻿using UnityEngine;
using System.Collections.Generic;

public class RoomNode
{
	public readonly Transform Handle;
	public readonly bool IsSpecialRoom;
	//public RoomNode Left;
	public RoomNode Right;
	public RoomNode Up;
	public RoomNode Down;

	public readonly List<BEnemyAgent> Enemies;
	public RoomNode(Transform _transform, bool _special = false)
	{
		Handle = _transform;
		IsSpecialRoom = _special;
		Enemies = new List<BEnemyAgent>();
	}
}

public class ConnectedRooms
{
	public RoomNode Head{get; private set;}
	public RoomNode Tail{get; private set;}
	/// <summary>
	/// Initializes a new instance of the <see cref="ConnectedRooms"/> class.
	/// </summary>
	/// <param name="_transform">Transform for the root room.</param>
	public ConnectedRooms(Transform _transform)
	{
		Head = new RoomNode(_transform);
		Tail = Head;
	}
	public void RemoveFirst()
	{
		if (Head == null) {
			return;
		}
		Object.Destroy(Head.Handle.gameObject);
		if (Head.Up != null) {
			Object.Destroy(Head.Up.Handle.gameObject);
		}
		if (Head.Down != null) {
			Object.Destroy(Head.Down.Handle.gameObject);
		}
		if (Head == Tail) {
			Head = Tail = null;
		} else {
			Head = Head.Right;
		}
	}
	/// <summary>
	/// Adds the a room to the end of the rooms.
	/// </summary>
	/// <param name="_transform">Transform for the new room.</param>
	public void AddLast(Transform _transform)
	{
		var node = new RoomNode(_transform);
		//node.Left = Tail;
		Tail.Right = node;
		Tail = node;
	}
	/// <summary>
	/// Adds the a room above the end of the rooms.
	/// </summary>
	/// <param name="_transform">Transform for the new room.</param>
	public void AddAboveLast(Transform _transform)
	{
		var node = new RoomNode(_transform, true);
		node.Down = Tail;
		Tail.Up = node;
	}
	/// <summary>
	/// Adds the a room below the end of the rooms.
	/// </summary>
	/// <param name="_transform">Transform for the new room.</param>
	public void AddBelowLast(Transform _transform)
	{
		var node = new RoomNode(_transform, true);
		node.Up = Tail;
		Tail.Down = node;
	}
}
