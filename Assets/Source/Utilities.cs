﻿using System;
using System.Text;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using Spine;
using Spine.Unity;

public static class Utilities
{
	static Renderer[] AllRects;
	static TextMeshProUGUI[] AllText;
	static Canvas[] AllCanvas;
	public static bool WatchingAd;
	public static void DisableRenderingForAds()
	{
		AllRects = UnityEngine.Object.FindObjectsOfType<Renderer>();
		AllText = UnityEngine.Object.FindObjectsOfType<TextMeshProUGUI>();
		AllCanvas = UnityEngine.Object.FindObjectsOfType<Canvas>();
		for(int i = 0; i < AllRects.Length; ++i) {
			AllRects[i].enabled = false;
		}
		for(int i = 0; i < AllText.Length; ++i) {
			AllText[i].enabled = false;
		}
		for(int i = 0; i < AllCanvas.Length; ++i) {
			AllCanvas[i].enabled = false;
		}
		WatchingAd = true;
	}

	public static void EnableRenderingForAds()
	{
		if (AllRects != null) {
			for(int i = 0; i < AllRects.Length; ++i) {
				if (AllRects[i] != null) {
					AllRects[i].enabled = true;
				}
			}
			AllRects = null;
		}
		if (AllText != null) {
			for(int i = 0; i < AllText.Length; ++i) {
				if (AllText[i] != null) {
					AllText[i].enabled = true;
				}
			}
			AllText = null;
		}
		if (AllCanvas != null) {
			for(int i = 0; i < AllCanvas.Length; ++i) {
				if (AllCanvas[i] != null) {
					AllCanvas[i].enabled = true;
				}
			}
			AllCanvas = null;
		}
		WatchingAd = false;
	}
	public static IEnumerator WaitForFakeAdTime()
	{
		yield return new WaitForSeconds(0.25f);
		FreeWheelController.instance.OnFreeWheelMessage(FreeWheel.kShowEnded);
	}
	public static System.Collections.Generic.IEnumerable<T> GetValues<T>()
	{
		return Enum.GetValues(typeof(T)).Cast<T>();
	}
	public static int GetIndex<T>(T _enum)
	{
		return Array.IndexOf(Enum.GetValues(_enum.GetType()), _enum);
	}
	public static T GetEnum<T>(int _index)
	{
		return  (T)(Enum.GetValues(typeof(T))).GetValue(_index);
	}
	public static T GetEnum<T>(string _enum)
	{
		return (T)Enum.Parse(typeof(T), _enum);
	}

	public static string FormatTextForCount(uint _count, int _textLength, bool _addX)
	{
		var builder = new StringBuilder();
		builder.Append(_count);
		while (builder.Length < _textLength) {
			builder.Insert(0, 0);
		}
		if (_addX) {
			builder.Insert(0, 'x');
		}
		return builder.ToString();
	}
	public static bool HasClip (this Spine.Unity.SkeletonAnimation spineAnim, string _clip)
	{
		return spineAnim.AnimationState.Data.SkeletonData.FindAnimation(_clip) != null;
	}
	public static float GetTimeForClip (this Spine.Unity.SkeletonAnimation spineAnim, string _clip)
	{
		return spineAnim.AnimationState.Data.SkeletonData.FindAnimation(_clip).Duration;
	}
	public static bool HasClip (this tk2dSpriteAnimator anim, string _clip)
	{
		return anim.GetClipByName (_clip) != null;
	}
	public static float GetTimeForClip (this tk2dSpriteAnimator anim, string _clip)
	{
		var clip = anim.GetClipByName (_clip);
		return clip.frames.Length / clip.fps;
	}

	public static void SafeStopCoroutine(this MonoBehaviour _self, ref Coroutine _routine)
	{
		if (_routine != null) {
			_self.StopCoroutine(_routine);
			_routine = null;
		}
	}

	public static string URLAntiCacheRandomizer(this string url)
	{
		string r = "";
		r += UnityEngine.Random.Range(
			1000000,8000000).ToString();
		r += UnityEngine.Random.Range(
			1000000,8000000).ToString();
		string result = url + "?p=" + r;
		return result;
	}

	public static Tweener DOFade(this SkeletonRenderer _skel, float _endvalue, float _duration, int _loops, LoopType _loopType)
	{
		return DOTween.To(() => _skel.skeleton.A, a => _skel.skeleton.A = a, _endvalue, _duration).SetLoops(_loops, _loopType);
	}
	public static T GetWeightedRandomItem<T>(Dictionary<T, float> _mapping)
	{
		if (_mapping.Count == 0) {
			return default(T);
		}

		var totalweight = _mapping.Sum(c => c.Value);
		var choice = UnityEngine.Random.value * totalweight;
		var sum = 0f;

		foreach (var obj in _mapping) {
			sum += obj.Value;
			if (sum >= choice) {
				return obj.Key;
			}
		}
		throw new ArgumentOutOfRangeException();
	}
	public static bool FiftyFifty
	{
		get {
			return UnityEngine.Random.value <= 0.5f;
		}
	}

	public static T RandomElement<T>(this T[] _self)
	{
		return _self.Length > 0 ? _self [UnityEngine.Random.Range (0, _self.Length)] : default(T);
	}
	public const uint FullCircle = 360;
	public static uint ClampValueToCircle(long _progress)
	{
		return (uint)((_progress + FullCircle) % FullCircle);
	}
	public static uint RotationCount(long _progress)
	{
		return (uint)(_progress / FullCircle);
	}
	public static byte[] ObjectToByteArray<T>(T obj, BinaryFormatter binFormatter = null)
	{
		if (binFormatter == null) {
			binFormatter = new BinaryFormatter();
		}
		var ms = new MemoryStream();
		binFormatter.Serialize(ms, obj);

		return ms.ToArray();
	}

	// Convert a byte array to an Object
	public static T ByteArrayToObject<T>(byte[] arrBytes, BinaryFormatter binFormatter = null)
	{
		MemoryStream memStream = new MemoryStream();
		if (binFormatter == null) {
			binFormatter = new BinaryFormatter();
		}
		memStream.Write(arrBytes, 0, arrBytes.Length);
		memStream.Seek(0, SeekOrigin.Begin);
		T obj = (T) binFormatter.Deserialize(memStream);

		return obj;
	}

}

[System.Serializable]
public class RangedValue
{
	[SerializeField] float m_min;
	[SerializeField] float m_max;
	public float min{get {return m_min;}}
	public float max{get {return m_max;}}
	public RangedValue(float _min, float _max)
	{
		m_min = _min;
		m_max = _max;
	}
	public RangedValue(double _min, double _max)
		: this ((float)_min, (float)_max)
	{
	}
	public RangedValue(int _min, int _max)
		: this ((float)_min, (float)_max)
	{
	}
	/// <summary>
	/// Gets the difference between max and min
	/// </summary>
	/// <value>The range.</value>
	/// <remarks>Always equal to <c>max</c> - <c>min</c></remarks>
	public float range
	{
		get {
			return max - min;	
		}
	}

	public float Lerp(float _t)
	{
		return Mathf.Lerp(min, max, _t);
	}
	public float LerpUnclamped(float _t)
	{
		return Mathf.LerpUnclamped(min, max, _t);
	}
}

[System.Serializable]
public class RandomRangedValue : RangedValue
{
	public RandomRangedValue(float _min, float _max)
		: base(_min, _max)
	{
	}
	public RandomRangedValue(double _min, double _max)
		: base(_min, _max)
	{
	}
	public RandomRangedValue(int _min, int _max)
		: base(_min, _max)
	{
	}
	public float floatValue
	{
		get {
			return UnityEngine.Random.Range(min, max);
		}
	}
	public int intValue
	{
		get {
			return UnityEngine.Random.Range((int)min, ((int)max) + 1);
		}
	}
}