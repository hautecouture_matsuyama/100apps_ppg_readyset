﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using VoxelBusters.NativePlugins;

public static class GameNotifications
{
	const string title = "Ready, Set, Monsters!";
	public static bool AllowNotifications = true;
	public static void CreateNotifications()
	{
		ClearNotifications();
		if (AllowNotifications && LocalNotification.SystemsNotificationsEnabled) {
			//LocalNotification.SendNotification(0, new System.TimeSpan(0,0,5) , title, @"This is a test, queued to send 5 seconds after leaving the app.");
			var timeRemaining = GiftSpawner.HoursTillFreeGift[GiftSpawner.Instance.FreeGiftWaitTier] - (System.DateTime.UtcNow - GiftSpawner.Instance.TimeSinceLastFreeGift);
			if (timeRemaining.TotalMinutes > 1) {
				LocalNotification.SendNotification(timeRemaining, title, @"Your Free Gift is ready!");
				LocalNotification.SendNotification(timeRemaining.Add(new System.TimeSpan(1, 0, 0,0)), title, @"Your Free Gift is waiting!");
				LocalNotification.SendNotification(timeRemaining.Add(new System.TimeSpan(3, 0, 0,0)), title, @"Who doesn't like treasure!? Get your Free Gift right now!");
			}
			if(PlayerManager.IsSuperAttackUnlocked(SuperAttack.TornadoTrio)) {
				var tornadoSuper = PlayerManager.GetSuperAttackHandle(SuperAttack.TornadoTrio);
				if (tornadoSuper != null && tornadoSuper.TimeRemaining.TotalMinutes > 1) {
					LocalNotification.SendNotification(tornadoSuper.TimeRemaining, title, @"The Tornado Trio Super Attack is charged and ready! Play now!");
					LocalNotification.SendNotification(tornadoSuper.TimeRemaining.Add(new System.TimeSpan(2, 0, 0,0)), title, @"The girls are ready to unleash the Tornado Trio! It's time to wreck some monsters!");
					LocalNotification.SendNotification(tornadoSuper.TimeRemaining.Add(new System.TimeSpan(4, 0, 0,0)), title, @"The Tornado Trio can't wreak havoc if you don't come play! What are you waiting for?");
				}
			}
			if(PlayerManager.IsSuperAttackUnlocked(SuperAttack.BlissBlitz)) {
				var blissSuper = PlayerManager.GetSuperAttackHandle(SuperAttack.BlissBlitz);
				if (blissSuper != null && blissSuper.TimeRemaining.TotalMinutes > 1) {
					LocalNotification.SendNotification(blissSuper.TimeRemaining, title, @"Blisstina is charged up and ready to fight! Come use her Super Attack now!");
					LocalNotification.SendNotification(blissSuper.TimeRemaining.Add(new System.TimeSpan(2, 0, 0,0)), title, @"Blisstina is just waiting to be called in! Play now and let her dish out the damage!");
					LocalNotification.SendNotification(blissSuper.TimeRemaining.Add(new System.TimeSpan(4, 0, 0,0)), title, @"Who's so powerful she pushed Saturn back into orbit? Blisstina! And she wants to fight for you! Play now!");
				}
			}
		}
	}
	public static void ClearNotifications()
	{
		LocalNotification.CancelAllNotifications();
	}
		
}
