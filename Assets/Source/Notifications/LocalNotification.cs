﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using VoxelBusters.NativePlugins;

static class LocalNotification
{

	static LocalNotification()
	{
		NPBinding.NotificationService.RegisterNotificationTypes(NotificationType.Alert | NotificationType.Badge | NotificationType.Sound);
	}
	//use this function below to see if the user has manually disabled notifications for this app, outside of the app
	public static bool SystemsNotificationsEnabled
	{
		#if UNITY_IOS && !UNITY_EDITOR
		get{
			return UnityEngine.iOS.NotificationServices.enabledNotificationTypes != UnityEngine.iOS.NotificationType.None;
		}
		#else
		get {
			return true;
		}
		#endif
	}
	//Create an instance of CrossPlatformNotification and fill with details.
	public static void SendNotification(TimeSpan _delay, string _title, string _message)
	{
		// User info - Is used to set custom data. Create a dictionary and set your data if any.
		IDictionary _userInfo            = new Dictionary<string, string>();
		_userInfo["data"]                = "";

		// Set iOS specific properties
		CrossPlatformNotification.iOSSpecificProperties _iosProperties            = new CrossPlatformNotification.iOSSpecificProperties();
		_iosProperties.HasAction        = true;
		_iosProperties.AlertAction        = _title;

		// Set Android specific properties
		CrossPlatformNotification.AndroidSpecificProperties _androidProperties    = new CrossPlatformNotification.AndroidSpecificProperties();
		_androidProperties.ContentTitle    = _title;
		_androidProperties.TickerText    = "ticker";
		_androidProperties.LargeIcon    = "SH_Android_Icon_NoteBig.png"; //Keep the files in Assets/PluginResources/VoxelBusters/NativePlugins/Android folder.

		// Create CrossPlatformNotification instance
		CrossPlatformNotification _notification    = new CrossPlatformNotification();
		_notification.AlertBody            = _message; //On Android, this is considered as ContentText
		_notification.FireDate            = System.DateTime.Now.AddSeconds(_delay.TotalSeconds);
		_notification.RepeatInterval    = eNotificationRepeatInterval.NONE;
		_notification.UserInfo            = _userInfo;
		//_notification.SoundName            = "Notification.mp3"; //Keep the files in Assets/PluginResources/NativePlugins/Android or iOS or Common folder.

		_notification.iOSProperties        = _iosProperties;
		_notification.AndroidProperties    = _androidProperties;

		NPBinding.NotificationService.ScheduleLocalNotification(_notification);
	
	}
	public static void CancelAllNotifications()
    {
		NPBinding.NotificationService.CancelAllLocalNotification();
    }
}
