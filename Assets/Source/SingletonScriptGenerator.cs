﻿using UnityEngine;
using System.Collections;

public static class SingletonScriptGenerator
{
	public static T Create<T>() where T: Component
	{
		const string gameManager = "GameManager";
		var gObj = GameObject.FindWithTag(gameManager);
		if (gObj == null) {
			gObj = new GameObject(gameManager);
			gObj.tag = gameManager;
		}
		var instance = gObj.GetComponent<T> ();
		if (instance == null) {
			instance  = gObj.AddComponent<T>();
		} else {
			Debug.LogWarningFormat("Can't have two instances of {0}", (typeof(T)));
		}
		return instance;
	}
}
