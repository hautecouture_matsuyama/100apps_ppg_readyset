﻿using System.IO;
using UnityEngine;
using UnityEditor;
using Newtonsoft.Json;

[CustomEditor(typeof(DFPManager))]
public class DFPManagerUI : Editor
{
    /// <summary>
    /// Resource paths
    /// </summary>
    private static readonly string path = "Assets/Resources/DFPConfiguration.json";

    /// <summary>
    /// The location overrides.
    /// </summary>
    private string exportLocationOverride = string.Empty;
    private string importLocationOverride = string.Empty;

    public DFPManagerUI()
    {
    }

    /// <summary>
    /// Ons the inspector GUI.
    /// </summary>
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        DFPManager manager = (DFPManager)target;

        if(GUILayout.Button("Add Config"))
        {
            AddGameConfig();
        }

        exportLocationOverride = GUILayout.TextField(exportLocationOverride);

        if (GUILayout.Button("Export"))
        {
            ExportGameConfig(exportLocationOverride);
        }

        importLocationOverride = GUILayout.TextField(importLocationOverride);

        if(GUILayout.Button("Import"))
        {
            ImportGameConfig(importLocationOverride);
        }
    }

    /// <summary>
    /// Add a new game config.  Copy last.
    /// </summary>
    private void AddGameConfig()
    {
        DFPManager manager = (DFPManager)target;
        if(manager.Config.GameConfigs.Count > 0)
        {
            DFPGameConfiguration config = manager.Config.GameConfigs[manager.Config.GameConfigs.Count-1];
            DFPGameConfiguration newConfig = new DFPGameConfiguration(config);
            manager.Config.GameConfigs.Add(newConfig);
        }
    }

    /// <summary>
    /// Exports the game config.
    /// </summary>
    /// <param name="json">Json.</param>
    private void ExportGameConfig(string locOverride)
    {
        DFPManager manager = (DFPManager)target;
        string json = JsonConvert.SerializeObject(manager.Config);

        Debug.Log(json);
        if(string.IsNullOrEmpty(json))
        {
            Debug.LogError("ExportGameConfig got a null json string!");
            return;
        }

        string location = string.IsNullOrEmpty(locOverride) ? path : locOverride;

        StreamWriter writer = new StreamWriter(location, false);
        writer.WriteLine(json);
        writer.Close();

        Debug.Log("Data exported to: " + location);
    }

    /// <summary>
    /// Imports the game config.
    /// </summary>
    private void ImportGameConfig(string locOverride)
    {
        DFPManager manager = (DFPManager)target;

        if(manager == null)
        {
            Debug.LogError("DFPManager is null!");
            return;
        }

        string location = string.IsNullOrEmpty(locOverride) ? path : locOverride;

        StreamReader reader = new StreamReader(path);
        string json = reader.ReadToEnd();
        reader.Close();

        DFPConfiguration dfpConfig = JsonConvert.DeserializeObject<DFPConfiguration>(json);

        manager.Config = dfpConfig;

        Debug.Log("Import completed successfully!");
    }
}

