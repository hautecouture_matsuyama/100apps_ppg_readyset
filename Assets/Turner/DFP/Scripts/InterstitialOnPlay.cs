﻿using UnityEngine;
using System.Collections;

public class InterstitialOnPlay : MonoBehaviour 
{
    /// <summary>
    /// Interstitial atrributes
    /// </summary>

	// Use this for initialization
	void Start () 
    {
		if (BTitleOverlay.HasSeenAgreement) {
			StartCoroutine (WaitForInterstitial ());
		}
	}

	IEnumerator WaitForInterstitial(){
		yield return new WaitForSeconds (5f);
		DFPKeyValues.Instance.LoadValues (DFP.Instance.DFPGameConfig);
		DFP.Instance.RequestInterstitial (DFPKeyValues.Instance);
	}
}


