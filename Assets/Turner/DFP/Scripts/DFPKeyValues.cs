﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DFPKeyValues {

	public List<int> timesPlayedThresholds; //a list of the thresholds for times played. Max size of 10
	public List<int> timeSpentThresholds; //a list of the thresholds for time spent in minutes. Max size of 10
	public List<int> moneySpentThresholds; //a list of the thresholds for money spent. Max size of 10

	private string timespent = "timespent"; //playerprefs saved name
	private string timesplayed = "timesplayed";//playerprefs saved name
	private string moneyspent = "moneyspent";//playerprefs saved name
	private int timespentOld;
	private DateTime startTime;
	private int timesplayedOld;
	private int moneyspentOld;
	private int moneyspentCurrent; //add any new IAP to this, round to nearest whole number if applicable


	private static DFPKeyValues instance;
	public static DFPKeyValues Instance
	{
		get
		{
			if (instance == null)
			{
				instance = new DFPKeyValues();
			}

			return instance;
		}
	}
    /// <summary>
    /// Loads the values.
    /// </summary>
    /// <param name="gameConfig">Game config.</param>
    public void LoadValues(DFPGameConfiguration gameConfig){
        if(gameConfig == null)
        {
            Debug.LogWarning("DFPGameConfiguration is null!");
            return;
        }

        timesPlayedThresholds = gameConfig.TimesPlayedThresholds;
        timeSpentThresholds = gameConfig.TimeSpentThresholds;
        moneySpentThresholds = gameConfig.MoneySpentThresholds;

		startTime = System.DateTime.Now;
		loadTimeSpent ();
		loadTimesPlayed ();
		loadMoneySpent ();
		moneyspentCurrent = 0;
	}

    /// <summary>
    /// On application quit.
    /// </summary>
	void OnApplicationQuit(){
		saveTimeSpent ();
		saveTimesPlayed ();
		saveMoneySpent ();
	}

    /// <summary>
    /// Gets the time spent.
    /// </summary>
    /// <returns>The time spent.</returns>
	public string getTimeSpent(){
		int index = 0;
		foreach (int i in timeSpentThresholds) {
			if (timespentOld <= i && timeSpentThresholds [0] == i) { //less than or equal to the first item
				return index.ToString ();
			} else if (index == timeSpentThresholds.Count - 1) { //last time in the list, must be greater than or equal to
				return index.ToString ();
			} else if (timespentOld >= timeSpentThresholds [index] && timespentOld < timeSpentThresholds [index+1]) {
				return index.ToString ();
			}
			index++;
		}
		return timespentOld.ToString();
	}

    /// <summary>
    /// Gets the times played.
    /// </summary>
    /// <returns>The times played.</returns>
	public string getTimesPlayed(){
		int index = 0;
		foreach (int i in timesPlayedThresholds) {
			if (timesplayedOld <= i && timesPlayedThresholds [0] == i) { //less than or equal to the first item
				return index.ToString ();
			} else if (index == timesPlayedThresholds.Count - 1) { //last time in the list, must be greater than or equal to
				return index.ToString ();
			} else if (timesplayedOld >= timesPlayedThresholds [index] && timesplayedOld < timesPlayedThresholds [index+1]) {
				return index.ToString ();
			}
			index++;
		}
		return timesplayedOld.ToString();
	}

    /// <summary>
    /// Gets the money spent.
    /// </summary>
    /// <returns>The money spent.</returns>
	public string getMoneySpent(){
		int index = 0;
		foreach (int i in moneySpentThresholds) {
			if (moneyspentOld <= i && moneySpentThresholds [0] == i) { //less than or equal to the first item
				return index.ToString ();
			} else if (index == moneySpentThresholds.Count - 1) { //last time in the list, must be greater than or equal to
				return index.ToString ();
			} else if (moneyspentOld >= moneySpentThresholds [index] && moneyspentOld < moneySpentThresholds [index+1]) {
				return index.ToString ();
			}
			index++;
		}
		return moneyspentOld.ToString();
	}

	//If the player has spent time on the game, load and append to the timespent
	//Else start the timer at zero 
	public void loadTimeSpent(){
		if (PlayerPrefs.HasKey (timespent)) {
			timespentOld = PlayerPrefs.GetInt (timespent);
			Debug.Log("loaded "+timespentOld + " minutes timespent");
		} else {
			timespentOld = 0;
		}
	}

	//Add the old time spent to the current time spent and save it.
	public void saveTimeSpent(){
		TimeSpan span = System.DateTime.Now.Subtract (startTime);
		int timespentCurrent = (int)Math.Round(span.TotalMinutes);
		Debug.Log("saved "+(timespentOld + timespentCurrent) + " minutes timespent");
		PlayerPrefs.SetInt (timespent, timespentOld + timespentCurrent);
	}

	//If the player has spent time on the game, load and append the timesplayed
	//Else start the times played at zero
	public void loadTimesPlayed(){
		if (PlayerPrefs.HasKey (timesplayed)) {
			timesplayedOld = PlayerPrefs.GetInt (timesplayed);
			Debug.Log("loaded "+ timesplayedOld + " timesplayed");

		} else {
			timesplayedOld = 0;
		}
	}

	//Add one to the old timesplayed value when you save
	public void saveTimesPlayed(){
		Debug.Log("saved "+(timesplayedOld+1) +" timesplayed");
		PlayerPrefs.SetInt (timesplayed, timesplayedOld + 1);
	}

	//If the player has spent time on the game, load and append the timesplayed
	//Else start the times played at zero
	public void loadMoneySpent(){
		if (PlayerPrefs.HasKey (moneyspent)) {
			moneyspentOld = PlayerPrefs.GetInt (moneyspent);
			Debug.Log("loaded "+ moneyspentOld + " money spent");

		} else {
			moneyspentOld = 0;
		}
	}

	//Add one to the old moneyspent value with the new moneyspentcurrent
	public void saveMoneySpent(){
		Debug.Log("saved "+(moneyspentOld+moneyspentCurrent) +" money spent");
		PlayerPrefs.SetInt (moneyspent, moneyspentOld+moneyspentCurrent);
	}
		
}
