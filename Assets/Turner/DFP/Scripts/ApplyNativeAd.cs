﻿using System.Collections.Generic;
using UnityEngine;

public class ApplyNativeAd : MonoBehaviour
{
    /// <summary>
    /// Google test data
    /// </summary>
    //public static readonly string TEST_AD_UNIT_ID = "/6499/example/native";
    //public static readonly string NATIVE_AD_1 = "10063170";
    //public static readonly string NATIVE_AD_2 = "10063171";
    //public static readonly string NATIVE_AD_3 = "10063172";

    /// <summary>
    /// The template identifier from DFP.
    /// </summary>
    public string AdUnitId;
    public List<string> TemplateIds;

    /// <summary>
    /// Start this instance.
    /// </summary>
    public void Start()
    {
        if (transform.parent != null)
        {
            DFP.Instance.LoadCustomNativeTemplateAd(transform.parent.gameObject, AdUnitId, TemplateIds);
        }
    }
}

