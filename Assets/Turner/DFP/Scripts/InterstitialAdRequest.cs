﻿using System;

public class InterstitialAdRequest
{
    /// <summary>
    /// Attributes
    /// </summary>
    public readonly DFPKeyValues KeyValues;

    public InterstitialAdRequest(DFPKeyValues keyValues)
    {
        KeyValues = keyValues;
    }
}

