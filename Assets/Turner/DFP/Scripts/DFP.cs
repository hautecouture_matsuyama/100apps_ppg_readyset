﻿using System;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

public class DFP
{
    /// <summary>
    /// The debug string to send prod or test in the key values.
    /// </summary>
    public string debugString;

    /// <summary>
    /// True if phone, false if tablet.
    /// </summary>
    public bool isPhone;
    /// <summary>
    /// Keys
    /// </summary>
    private List<string> timesPlayedKeyNames; //a list of the names for times played keys. Max size of 10
    private List<string> timeSpentKeyNames; //a list of the names for time spent in minutes keys. Max size of 10
    private List<string> moneySpentKeyNames; //a list of the names for money spent keys. Max size of 10 

    /// <summary>
    /// Ad Objects
    /// </summary>
    private BannerView bannerView;
    private InterstitialAd interstitial;

	/// <summary>
	/// The banner loaded.
	/// </summary>
	public bool bannerLoaded;

    /// <summary>
    /// Native Ad objects
    /// </summary>
    private Texture2D mainImageTexture;
    private string headline;
    private GameObject adObject;

    /// <summary>
    /// The initialized.
    /// </summary>
    private bool initialized = false;

    /// <summary>
    /// Singleton instance
    /// </summary>
    private static DFP instance;

    /// <summary>
    /// The dfp game config.
    /// </summary>
    private DFPGameConfiguration dfpGameConfig;

    /// <summary>
    /// Queued ads.
    /// </summary>
    private BannerAdRequest queuedBannerAd;
    private InterstitialAdRequest queuedInterstitialAd;


    /// <summary>
    /// The game config.
    /// </summary>
    public DFPGameConfiguration DFPGameConfig
    {
        get
        {
            return dfpGameConfig;
        }
    }

    /// <summary>
    /// Initialized?
    /// </summary>
    public bool Initialized
    {
        get
        {
            return initialized;
        }
    }

    /// <summary>
    /// Gets the instance.
    /// </summary>
    /// <value>The instance.</value>
    public static DFP Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new DFP();
            }

            return instance;
        }
    }

    /// <summary>
    /// Initialize the specified gameConfig.
    /// </summary>
    /// <returns>The initialize.</returns>
    /// <param name="gameConfig">Game config.</param>
    public void Initialize(DFPGameConfiguration gameConfig)
    {
        if (gameConfig == null)
        {
            Debug.Log("game config is null");
        }
        dfpGameConfig = gameConfig;
        if (dfpGameConfig != null)
        {
            initialized = true;
            SendQueuedAds();
        }
    }

    #region Banners
    /// <summary>
    /// Requests the banner.
    /// </summary>
	public void RequestBanner()
    {
        if (!initialized)
        {
            Debug.Log("DFPGameConfiguration not initialized!");

            if (queuedBannerAd == null)
            {
				queuedBannerAd = new BannerAdRequest(AdPosition.Bottom);
            }

            return;
        }

        Debug.Log("RequestBanner call. " + dfpGameConfig.AdUnitId + " " + dfpGameConfig.Promo);

        // Create a wxh banner at the position.
        AdSize bannerSize = new AdSize(320, 50);
        string phoneOrTab = "";
        if (isPhone)
        {
            phoneOrTab = "phone";
			bannerSize = new AdSize(320, 50);
        }
        else
        {
            phoneOrTab = "tab";
			bannerSize = new AdSize(320, 50);
		}
        
        bannerView = new BannerView(dfpGameConfig.AdUnitId, bannerSize, AdPosition.Center);
        // Create an empty ad request.
        bannerView.OnAdLoaded += HandleOnBannerLoaded;
        bannerView.OnAdFailedToLoad += HandleOnAdFailedToLoad;

        var appList = new List<string>();

		#if UNITY_IOS && !UNITY_EDITOR
        appList = GetInstalledIOSApps();
		#elif UNITY_ANDROID && !UNITY_EDITOR
        appList = GetInstalledAndroidApps();
		#elif UNITY_EDITOR
		appList.Add("None available in Editor");
		#endif

        var installedAppValues = string.Join(",", appList.ToArray());

        Debug.Log("Installed apps: " + installedAppValues);

        AdRequest request = new AdRequest.Builder()
            .TagForChildDirectedTreatment(true)
            .AddExtra("app", "true") // Category exclusions for DFP.
            .AddExtra("plat", phoneOrTab) // Category exclusions for DFP.
            .AddExtra("appmode", debugString) // Category exclusions for DFP
            .AddExtra("game", dfpGameConfig.Promo)
            .AddExtra("installed", installedAppValues)
            .Build();

        // Load the banner with the request.
        bannerView.LoadAd(request);
    }

    /// <summary>
    /// Destroies the banner.
    /// </summary>
    public void DestroyBanner()
    {
        if (bannerView != null)
        {
            bannerView.Destroy();
        }
    }
    #endregion

    #region Interstitial
    public void RequestInterstitial(DFPKeyValues keyValues)
    {
        if (!initialized)
        {
            Debug.Log("DFPGameConfiguration not initialized!");

            if (queuedInterstitialAd == null)
            {
                queuedInterstitialAd = new InterstitialAdRequest(keyValues);
            }
            return;
        }

        if (keyValues == null)
        {
            Debug.LogError("DFPKeyValues are NULL! Not sending ad request.");
            return;
        }

        // Initialize an InterstitialAd.
        interstitial = new InterstitialAd(dfpGameConfig.AdUnitId);
        interstitial.OnAdLoaded += HandleOnInterstitialLoaded;
        // Create an empty ad request.

        string timespent = "timespent_" + keyValues.getTimeSpent();
        string timesplayed = "timesplayed_" + keyValues.getTimesPlayed();
        string moneyspent = "moneyspent_" + keyValues.getMoneySpent();
        string phoneOrTab = "";
        if (isPhone)
        {
            phoneOrTab = "phone";
        }
        else
        {
            phoneOrTab = "tab";
        }

        var appList = new List<string>();

		#if UNITY_IOS && !UNITY_EDITOR
		appList = GetInstalledIOSApps();
		#elif UNITY_ANDROID && !UNITY_EDITOR
		appList = GetInstalledAndroidApps();
		#elif UNITY_EDITOR
		appList.Add("None available in Editor");
		#endif

        var installedAppValues = string.Join(",", appList.ToArray());

        Debug.Log("Installed apps: " + installedAppValues);

        Debug.Log("RequestInterstitial call. " + dfpGameConfig.AdUnitId + " " + dfpGameConfig.Promo + " " + timespent + " " + timesplayed + " " + moneyspent);
        AdRequest request = new AdRequest.Builder()
            .TagForChildDirectedTreatment(true)
            .AddExtra("app", "true") // Category exclusions for DFP.
            .AddExtra("plat", phoneOrTab) // Category exclusions for DFP.
            .AddExtra("appmode", debugString) // Category exclusions for DFP.
            .AddExtra("game", dfpGameConfig.Promo)
            .AddExtra("installed", installedAppValues)
            .AddExtra(timespent, "true")
            .AddExtra(timesplayed, "true")
            .AddExtra(moneyspent, "true")
            .Build();
        // Load the interstitial with the request.
        interstitial.LoadAd(request);
    }

    /// <summary>
    /// Destroies the interstitial.
    /// </summary>
    public void DestroyInterstitial()
    {
        if (interstitial != null)
        {
            interstitial.Destroy();
        }
    }
    #endregion


    #region NativeAd
    /// <summary>
    /// Load a native ad template onto a gameobject
    /// </summary>
    /// <param name="go">Go.</param>
    public void LoadCustomNativeTemplateAd(GameObject go, string adUnitId, List<string> templateIds)
    {
        if (!initialized)
        {
            Debug.LogError("DFPGameConfiguration not initialized!");
            return;
        }

        Debug.Log("RequestInterstitial call. " + adUnitId + " " + go.name);

        adObject = go;

        if (templateIds == null || templateIds.Count == 0)
        {
            Debug.LogError("LoadCustomNativeTemplateAd no template ids provided!");
            return;
        }

        var builder = new AdLoader.Builder(adUnitId);
        for (int i = 0; i < templateIds.Count; i++)
        {
            Debug.Log("Adding custom template: " + templateIds[i]);
            builder.ForCustomNativeAd(templateIds[i]);
        }

        var adLoader = builder.Build();
        var request = new AdRequest.Builder().Build();

        adLoader.OnCustomNativeTemplateAdLoaded += HandleCustomNativeAdLoaded;
        adLoader.OnAdFailedToLoad += HandleCustomNativeAdFailedToLoad;
        adLoader.LoadAd(request);
    }
    #endregion

    #region Handlers
    //Event handlers
    /// <summary>
    /// Handles the on ad loaded.
    /// </summary>
    /// <param name="sender">Sender.</param>
    /// <param name="args">Arguments.</param>
    private void HandleOnBannerLoaded(object sender, EventArgs args)
    {
		bannerView.Hide ();
		bannerLoaded = true;
        Debug.Log("HandleOnBannerLoaded event received.");
    }

	public bool ShowBanner(){
		#if UNITY_EDITOR
		bannerLoaded = true;
		#endif
		if (bannerLoaded) {
			//bannerView.Show ();
		}
		return bannerLoaded;
	}

	public void HideBanner(){
		if (bannerView != null) {
			bannerView.Hide ();
		}
	}

	public void ShowInterstitial(){
		if (interstitial.IsLoaded ()) {
			interstitial.Show ();
		}
	}

    /// <summary>
    /// Handles the on interstitial loaded.
    /// </summary>
    /// <param name="sender">Sender.</param>
    /// <param name="args">Arguments.</param>
    private void HandleOnInterstitialLoaded(object sender, EventArgs args)
    {
		ShowInterstitial ();
		Debug.Log ("HandleInterstitialOnLoaded even received");
        // Handle the ad loaded event.
    }

    /// <summary>
    /// Handles the on ad failed to load.
    /// </summary>
    /// <param name="sender">Sender.</param>
    /// <param name="args">Arguments.</param>
    private void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        Debug.Log("HandleOnAdFailedToLoad: " + sender.GetType() + " " + args.Message);
    }

    /// <summary>
    /// Handles the custom native ad loaded.
    /// </summary>
    /// <param name="sender">Sender.</param>
    /// <param name="args">Arguments.</param>
    private void HandleCustomNativeAdLoaded(object sender, CustomNativeEventArgs args)
    {
        Debug.Log("HandleCustomNativeAdLoaded.");
        ApplyAdTexture(args.nativeAd);
    }

    /// <summary>
    /// Handles the custom native ad failed to load.
    /// </summary>
    /// <param name="sender">Sender.</param>
    /// <param name="args">Arguments.</param>
    private void HandleCustomNativeAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        Debug.Log("HandleCustomNativeAdFailedToLoad: " + args.Message);
    }

    /// <summary>
    /// Handles the custom native ad clicked.
    /// </summary>
    /// <param name="customNativeTemplateAd">Custom native template ad.</param>
    /// <param name="assetName">Asset name.</param>
    private void HandleCustomNativeAdClicked(CustomNativeTemplateAd customNativeTemplateAd, string assetName)
    {
        Debug.Log("Native ad asset with name " + assetName + " was clicked.");
    }
    #endregion

    /// <summary>
    /// Applies the ad texture to the selected game object.
    /// </summary>
    /// <param name="nativeAd">Native ad.</param>
    private void ApplyAdTexture(CustomNativeTemplateAd nativeAd)
    {
        if (nativeAd == null)
        {
            Debug.Log("nativeAd is NULL!");
            return;
        }

        Debug.Log("Ad Loaded...  Getting texture");
        mainImageTexture = nativeAd.GetTexture2D("MainImage");
        headline = nativeAd.GetText("Headline");

        if (adObject != null)
        {
            Debug.Log("Applying texture");
            //This makes too many assumptions about the object structure to get the material.
            adObject.GetComponent<MeshRenderer>().sharedMaterial.SetTexture("_MainTex", mainImageTexture);
        }

        string templateId = nativeAd.GetCustomTemplateId();
        Debug.Log("Playing ad " + templateId);
    }

    /// <summary>
    /// Sends the queued ads.
    /// </summary>
    private void SendQueuedAds()
    {
        if (queuedBannerAd != null)
        {
            RequestBanner();
            queuedBannerAd = null;
        }

        if (queuedInterstitialAd != null)
        {
            RequestInterstitial(queuedInterstitialAd.KeyValues);
            queuedInterstitialAd = null;
        }
    }

    /// <summary>
    /// Trims for max app installs.
    /// </summary>
    /// <returns>The for max app installs.</returns>
    /// <param name="installedApps">Installed apps.</param>
    private List<string> TrimForMaxAppInstalls(List<string> installedApps)
    {
        var maxApps = DFPManager.Instance.Config.MaxInstalledApps;
        if (maxApps > DFPConfiguration.UNLIMITED_APPS)
        {
            if (maxApps == 0)
            {
                Debug.Log("Max installed apps 0.  Clear installed apps.");
                installedApps.Clear();
                return installedApps;
            }

            if (installedApps.Count > maxApps)
            {
                Debug.Log("Trimming installed app list...  count: " + installedApps.Count + " max: " + maxApps);
                installedApps.RemoveRange(maxApps, (installedApps.Count - maxApps));
                Debug.Log("Trimmed apps to " + installedApps.Count);
            }
        }

        return installedApps;
    }

#if UNITY_IOS
    /// <summary>
    /// Gets the installed IOS apps.
    /// </summary>
    /// <returns>The installed IOS apps.</returns>
    private List<string> GetInstalledIOSApps()
    {
        var installedApps = new List<string>();

        var schemes = DFPManager.Instance.Config.GetGameSchemes();

        for (int i = 0; i < schemes.Count; i++)
        {
            var schemeName = schemes[i];
            if(CheckURLExists.CheckURL(schemeName+"://"))
            {
                installedApps.Add(schemeName);
            }
        }

        installedApps.Add("app1");
        installedApps.Add("app2");
        installedApps.Add("app3");
        installedApps.Add("app4");
        installedApps.Add("app5");

        return TrimForMaxAppInstalls(installedApps);        
    }
#endif

#if UNITY_ANDROID
    /// <summary>
    /// Gets the installed android apps.
    /// </summary>
    /// <returns>The installed android apps.</returns>
    private List<string> GetInstalledAndroidApps()
    {
        var installedApps = new List<string>();

        var schemes = DFPManager.Instance.Config.GetPackages();

        for (int i = 0; i < schemes.Count; i++)
        {
            var schemeName = schemes[i];
            if(CheckURLExists.AndroidAppInstalled(schemeName))
            {
                installedApps.Add(schemeName);
            }
        }
        
        installedApps = DFPManager.Instance.Config.GetSchemeNamesFromAndroidPackages(installedApps);

        return TrimForMaxAppInstalls(installedApps);       
    }
#endif

}
