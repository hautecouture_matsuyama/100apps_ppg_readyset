﻿using System;
using System.Collections;
using UnityEngine;
using Newtonsoft.Json;
using GoogleMobileAds.Api;

public class DFPManager : MonoBehaviour
{
	
    /// <summary>
    /// Config data.
    /// </summary>
    public string ConfigURL = "https://i.cdn.turner.com/toon/games/dfp/";
    public int TargetVersion = 1;
    public string GameName;
	public bool debugMode;
    public DFPConfiguration Config;

    /// <summary>
    /// Simulated singleton
    /// </summary>
    /// <value>The instance.</value>
    public static DFPManager Instance
    {
        get
        {
            return instance;    
        }
    }

    private static DFPManager instance;

    /// <summary>
    /// The filename.
    /// </summary>
    private string filename = "DFPConfiguration.json";

    /// <summary>
    /// Initialization vars.
    /// </summary>
    private int attempts = 3;

    public DFPManager()
    {
		
    }

    /// <summary>
    /// Awake this instance.
    /// </summary>
    private void Awake()
    {
		//RequestBanner();
        DontDestroyOnLoad(this);
    }

    /// <summary>
    /// Start this instance.
    /// </summary>
    private void Start()
    {
		instance = this;
		//ShowBanner ();
		/*SetPhoneOrTablet ();
        if (!DFP.Instance.Initialized)
        {
            StartCoroutine(GetConfig());
        }
		if (debugMode) {
			DFP.Instance.debugString = "test";
		} else
			DFP.Instance.debugString = "prod";
			*/
    }
		

	private BannerView bannerView;

	public void Request()
	{
		RequestBanner ();
	}



	private void RequestBanner()
	{
		#if UNITY_ANDROID
		string adUnitId = "/17192736/CN_GameAPP_17_header";
		#elif UNITY_IPHONE
		string adUnitId = "/17192736/CN_GameAPP_17_header";
		#else
		string adUnitId = "unexpected_platform";
		#endif
		// Create a 320x50 banner at the top of the screen.
		bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);
		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder().Build();
		// Load the banner with the request.
		bannerView.LoadAd(request);
		}
		public void HideBanner()
		{
		bannerView.Hide();
		}

		public void ShowBanner()
		{
		bannerView.Show();
		}



    /// <summary>
    /// Generates the URL.
    /// </summary>
    /// <returns>The URL.</returns>
    public string GenerateURL()
    {
        var url = ConfigURL + TargetVersion + "/" + filename;
        Debug.Log("Config url: "+url);

        return url;
    }

    /// <summary>
    /// Gets the config.
    /// </summary>
    /// <returns>The config.</returns>
    private IEnumerator GetConfig()
    {
        WWW www = new WWW(GenerateURL());
        yield return www;

        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.LogError(www.error);
            attempts--;

            if (attempts > 0)
            {
                Debug.Log("Retrying... ");

                yield return new WaitForSeconds(0.5f);

                StartCoroutine(GetConfig());
            }
            else
            {
                Debug.Log("Maximum retries hit.  Cannot retrieve config file.");    
            }

            yield break;
        }
        else
        {
            // Show results as text
            Debug.Log(www.text);
            Initialize(www.text);
        }
    }

    /// <summary>
    /// Initialize with the specified config.
    /// </summary>
    /// <returns>The initialize.</returns>
    /// <param name="json">Config.</param>
    private void Initialize(string json)
    {
        if (string.IsNullOrEmpty(json))
        {
            Debug.LogError("Web config for DFP is empty!");
            return;
        }

        try
        {
            Config = JsonConvert.DeserializeObject<DFPConfiguration>(json);
			if(Config.GetGameConfigByName(GameName) == null){
				Debug.LogError ("Game Name field on the DFPManager gameobject is null or incorrect! Cannot initialize!");
			}
			else{
				DFP.Instance.Initialize(Config.GetGameConfigByName(GameName));
				Debug.Log("DFP initialized!");
				DFP.Instance.RequestBanner();
			}
            
        }
        catch (Exception e)
        {
            Debug.LogError("Deserialization of DFPConfiguration failed!: " + e.Message);
        }
    }

	public static float DeviceDiagonalSizeInInches ()
	{
		float screenWidth = Screen.width / Screen.dpi;
		float screenHeight = Screen.height / Screen.dpi;
		float diagonalInches = Mathf.Sqrt (Mathf.Pow (screenWidth, 2) + Mathf.Pow (screenHeight, 2));

		Debug.Log ("Getting device inches: " + diagonalInches);

		return diagonalInches;

	}

	public void SetPhoneOrTablet(){
		if (DeviceDiagonalSizeInInches () > 6.5f) {
			Debug.Log ("Tablet");
			DFP.Instance.isPhone = false;
		} else {
			Debug.Log ("Phone");
			DFP.Instance.isPhone = true;
		}
	}

}

