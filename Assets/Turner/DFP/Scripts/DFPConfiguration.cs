﻿using System;
using System.Collections.Generic;

[Serializable]
public class DFPConfiguration
{
    public DFPConfiguration()
    {
    }

    /// <summary>
    /// Report all installed apps
    /// </summary>
    public static readonly int UNLIMITED_APPS = -1;

    /// <summary>
    /// Configuration
    /// </summary>
    public int Version;
	public int MaxInstalledApps;
    public List<DFPGameConfiguration> GameConfigs;

    

    /// <summary>
    /// Gets the game config by name.
    /// </summary>
    /// <returns>The game config by name.</returns>
    /// <param name="name">Name.</param>
    public DFPGameConfiguration GetGameConfigByName(string name)
    {
        for (int i = 0; i < GameConfigs.Count; i++)
        {
            if (string.Equals(name, GameConfigs[i].GameName))
            {
                return GameConfigs[i];
            }
        }

        return null;
    }

    /// <summary>
    /// Gets the game schemes.
    /// </summary>
    /// <returns>The game schemes.</returns>
    public List<string> GetGameSchemes()
    {
        var schemes = new List<string>();
        for (int i = 0; i < GameConfigs.Count; i++)
        {
            schemes.Add(GameConfigs[i].SchemeName);
        }

        return schemes;
    }

    /// <summary>
    /// Gets the game packages.
    /// </summary>
	/// <returns>The game packages.</returns>
    public List<string> GetPackages()
    {
        var packages = new List<string>();
        for (int i = 0; i < GameConfigs.Count; i++)
        {
            packages.Add(GameConfigs[i].PackageName);
        }

        return packages;
    }

	/// <summary>
	/// Gets the scheme name from android packages installed to send in the DFP request for the installed list.
	/// </summary>
	/// <returns>The scheme name from android packages.</returns>
	/// <param name="packageList">Package list.</param>
	public List<string> GetSchemeNamesFromAndroidPackages(List<string> packageList)
    {
		var schemes = new List<string>();
		foreach (string p in packageList) 
        {
			for (int i = 0; i < GameConfigs.Count; i++)
			{
				if (p == GameConfigs[i].PackageName) 
                {
					schemes.Add(GameConfigs[i].SchemeName);
				}
			}
		}
		return schemes;
	}
}

