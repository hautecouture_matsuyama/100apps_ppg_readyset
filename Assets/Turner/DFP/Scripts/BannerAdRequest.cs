﻿using System;
using GoogleMobileAds.Api;

public class BannerAdRequest
{
    /// <summary>
    /// Attributes
    /// </summary>
    public readonly AdPosition Position;

    public BannerAdRequest(AdPosition position)
    {
        Position = position;
    }
}

