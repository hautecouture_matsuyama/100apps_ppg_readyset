﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;

public class BannerOnLoad : MonoBehaviour 
{
	/// <summary>
	/// Next scene to go to if no internet.
	/// </summary>
	public string NextScene;

	public bool bannerTimeout = true;

	void Start () 
	{
		/*StartCoroutine (BInternetConnection.IsInternetAvailable((isConnected) => {
			if(isConnected) {
				if (DFP.Instance.ShowBanner ()) {
					StartCoroutine (WaitFiveSecsForBanner ());
				} else {
					Application.LoadLevel (NextScene);
				}
			} else {
				Application.LoadLevel (NextScene);
			}
		}));
		*/
	}
	void OnDisable()
	{
		DFP.Instance.HideBanner();
	}

	public void StartWaiting()
	{
		StartCoroutine (WaitFiveSecsForBanner ());
	}

	IEnumerator WaitFiveSecsForBanner()
	{
		yield return new WaitForSeconds (5f);
		Application.LoadLevel (NextScene);
	}
}

