﻿/// <summary>
/// Used to give hints to il2cpp to create the proper types.
/// </summary>
public class iOSHelper
{
    public iOSHelper()
    {
    }

    /// <summary>
    /// Used to help create proper types for AOT compiler when using il2cpp
    /// </summary>
    public void Dummy()
    {
        Newtonsoft.Json.Utilities.AotHelper.EnsureList<DFPGameConfiguration>();
    }
}

