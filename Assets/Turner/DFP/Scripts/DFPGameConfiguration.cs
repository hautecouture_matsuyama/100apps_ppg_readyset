﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DFPGameConfiguration
{
    public DFPGameConfiguration()
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="T:DFPGameConfiguration"/> class.
    /// Copies data from existing instance.
    /// </summary>
    /// <param name="config">Config.</param>
    public DFPGameConfiguration(DFPGameConfiguration config)
    {
        GameName = config.GameName;
        PropertyName = config.PropertyName;
        SchemeName = config.SchemeName;
		AdUnitId = "/17192736/CN_GameAPP_17_header";
        Promo = config.Promo;
        PackageName = config.PackageName;

        TimesPlayedThresholds = config.TimeSpentThresholds;
        TimeSpentThresholds = config.TimeSpentThresholds;
        MoneySpentThresholds = config.MoneySpentThresholds;
    }

    public string GameName;
    public string PropertyName;
    //iOS
    public string SchemeName;
    public string AdUnitId;
    public string Promo;
    //Android
    public string PackageName;

    public List<int> TimesPlayedThresholds; //a list of the thresholds for times played. Max size of 10
    public List<int> TimeSpentThresholds; //a list of the thresholds for time spent in minutes. Max size of 10
    public List<int> MoneySpentThresholds; //a list of the thresholds for money spent. Max size of 10
}

