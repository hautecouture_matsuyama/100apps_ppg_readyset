using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ParticleSystem))]
public class CFX_AutoDestructShuriken : MonoBehaviour
{
	public bool OnlyDeactivate;
	public ObjectPool PoolHandle{get; set;}
	void OnEnable()
	{
		StartCoroutine("CheckIfAlive");
	}

	IEnumerator CheckIfAlive ()
	{
		yield return null;
		while(true)
		{
			yield return new WaitForSeconds(0.5f);
			if(!GetComponent<ParticleSystem>().IsAlive(true))
			{
				if (PoolHandle != null) {
					PoolHandle.Release(gameObject);
					PoolHandle = null;
				} else {
					if(OnlyDeactivate)
					{
						#if UNITY_3_5
						this.gameObject.SetActiveRecursively(false);
						#else
						this.gameObject.SetActive(false);
						#endif
					}
					else 
						GameObject.Destroy(this.gameObject);
				}
				break;
			}
		}
	}
}
