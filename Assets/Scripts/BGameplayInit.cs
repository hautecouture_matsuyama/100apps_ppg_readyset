﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEngine.SceneManagement;
#endif

public class BGameplayInit : MonoBehaviour
{
	public static void PersistScore()
	{
		persistScore = true;
	}
	static bool persistScore;

	public static long StartingProgressValue{get; private set;}
	void Awake ()
	{
		#if UNITY_EDITOR
		var cur = SceneManager.GetActiveScene();
		if (cur.name == "sandbox") {
			return;
		}
		#endif
		if (!persistScore) {
			SaveLoadManager.Load();
			PlayerManager.ResetStreakScore();
			PlayerManager.HealAllGirls();
			BWorldMapGenerator.ResetRoomsTillHeal();
			StartingProgressValue = PlayerManager.GlobalProgressUnclamped;
		}
		//uncomment to test super
//		var attack = SuperAttack.TornadoTrio;
//		PlayerManager.UnlockSuperAttack(attack);
//		PlayerManager.EquipSuperAttack(attack);
//		PlayerManager.CurrentSuperAttack.ForceExpireTimer();
	}
	void Start()
	{
		#if UNITY_EDITOR
		var cur = SceneManager.GetActiveScene();
		if (cur.name == "sandbox") {
			return;
		}
		#endif
		if (!persistScore) {
			BDifficultyManager.Instance.Reset();
		}
		persistScore = false;
		EnvironmentManager.SetEnvironmentBasedOnProgress(PlayerManager.GlobalProgress);
		if (PlayerManager.NumberofGirlUnlocked() == 0) {
			BGirlSelectOverlay.SpawnGirlSelect(true, true);
			BGirlSelectOverlay.Instance.CallerWillCallSetActiveGirl = true;
			BGirlSelectOverlay.Instance.CallerWillCloseOverlay = true;
		} else {
			BGameCenterManager.CheckLoops();
		}
	}
}
