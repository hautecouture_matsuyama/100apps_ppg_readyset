﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BBillboardSponsorshipImage : MonoBehaviour
{

	[SerializeField] GameObject sponsorContainer;
	[SerializeField] SpriteRenderer sponsorSprite;

	void Start () 
	{
		if(BSponsorshipManager.CheckIfActive(BSponsorshipManager.billboardKey)) {
			sponsorSprite.sprite = BSponsorshipManager.GetSponsorImage(BSponsorshipManager.billboardKey);
		} else {
			Destroy(sponsorContainer);
		}
	}

}
