﻿using UnityEngine;
using System.Collections.Generic;
using Spine;
using Spine.Unity;
using System.Collections;
using VoxelBusters.NativePlugins;

[System.Serializable]
public class GiftPrefabs
{
	public GameObject BlossomGiftPrefab;
	public GameObject BubblesGiftPrefab;
	public GameObject ButtercupGiftPrefab;
	public GameObject BlissGiftPrefab;
	public GameObject GiftPrefab;
	public GameObject SponsorshipPrefab;

	public GameObject this[PPG _girl]
	{
		get {
			switch (_girl) {
			case PPG.Blossom:
				return BlossomGiftPrefab;
			case PPG.Bubbles:
				return BubblesGiftPrefab;
			case PPG.Buttercup:
				return ButtercupGiftPrefab;
			case PPG.None:
				return BlissGiftPrefab;
			default:
				throw new System.ArgumentOutOfRangeException ();
			}
		}
	}
}

public class BWorldMapGenerator : MonoBehaviour
{
	public static bool WorldGenerated{get; private set;}
	public static void ResetRoomsTillEarn()
	{
		roomsTillEarnStation = DesignValues.WorldGenerator.RoomsTillEarnStation.intValue;
	}
	public static void ResetRoomsTillHeal()
	{
		roomsTillHealStation = DesignValues.WorldGenerator.RoomsTillHealStation.intValue;
	}

	[SerializeField] GameObject secretSparkle;
	[SerializeField] GiftPrefabs giftPickupPrefabs;
	[SerializeField] NavEnvSoundEffectsData effectsData;

	[SerializeField] GameObject tutorialEnemy;
	[SerializeField] GameObject tutorialStartRoom;

	//VO clips
	[SerializeField] AudioClip blossomToBubblesVO;
	[SerializeField] AudioClip blossomToButtercupVO;
	[SerializeField] AudioClip bubblesToBlossomVO;
	[SerializeField] AudioClip bubblesToButtercupVO;
	[SerializeField] AudioClip buttercupToBlossomVO;
	[SerializeField] AudioClip buttecupToBubblesVO;
	[SerializeField] AudioClip allGirlsToBlissVO;

	public ConnectedRooms Rooms{get; private set;}

	EnvironmentCollectionData curEnvData;
	EnemyCollectionData curEnemyData;
	tk2dCamera tkCam;
	BPickup[] pickupsToCleanup;
	int lagRemoveRoom = 1;

	int roomsTillNextEnemySpawn;
	static int roomsTillEarnStation = -1;
	static int roomsTillHealStation = -1;
	bool seenHealStationThisRun = false;
	int roomsTillSponsorship;
	bool spawnedSpecialPreviousRoom;
	float pickupSpawnWidth;
	float pickupSpawnHeight;

	const float coinSpawnBoxYOffset = -230f;

	void Awake()
	{
		tkCam = FindObjectOfType<tk2dCamera>();
		pickupSpawnWidth = tkCam.nativeResolutionWidth * 0.22f;
		pickupSpawnHeight = tkCam.nativeResolutionHeight * 0.16f;
	}
	IEnumerator Start ()
	{
		WorldGenerated = false;
		const string basePath = "Prefabs/EnvironmentAsset/";
		var path = string.Format("{0}{1}EnvironmentData", basePath, EnvironmentManager.CurrentEnvironment);
		var envAsset = Resources.LoadAsync<EnvironmentCollectionData>(path);
		yield return envAsset;
		curEnvData = (EnvironmentCollectionData)envAsset.asset;

		path = string.Format("{0}{1}EnemyData", basePath, EnvironmentManager.CurrentEnvironment);
		var enAsset = Resources.LoadAsync<EnemyCollectionData>(path);
		yield return enAsset;
		curEnemyData = (EnemyCollectionData)enAsset.asset;

		var enemyPrefabs = new []{
			curEnemyData[EnemyType.Brute],
			curEnemyData[EnemyType.BigBrute],
			curEnemyData[EnemyType.Flower],
			curEnemyData[EnemyType.BigFlower],
			curEnemyData[EnemyType.Flyer],
			curEnemyData[EnemyType.BigFlyer]
		};
		foreach(var ep in enemyPrefabs) {
			if (ep != null) {
				var spineObject = ep.GetComponentInChildren<SkeletonAnimation>();
				spineObject.SkeletonDataAsset.GetSkeletonData(false);
			}
		}
		GameObject floor= null;
		const uint thirdCircle = Utilities.FullCircle / 3;
		if (BTutorialManager.InTutorial) {
			floor = Instantiate(tutorialStartRoom, transform, true);
		} else if ((PlayerManager.GlobalProgress + thirdCircle) % thirdCircle == 0) {
			floor = Instantiate(curEnvData.startChunk, transform, true);
		} else {
			floor = Instantiate(curEnvData.RandomStraight, transform, true);
		}
		Rooms = new ConnectedRooms(floor.transform);

		BWorldMapDragger.HandleBeforeEnteredNewRoom += OnBeforeEnteredNewRoom;
		BWorldMapDragger.HandleEnteredNewRoom += onEnteredNewRoom;
		BattleManager.HandleDefeatEnemy += onEnemyDefeated;
		roomsTillNextEnemySpawn = BTutorialManager.Instance.ShouldShowSwipeHand ? BTutorialManager.Instance.RoomsForHand : 1;
		if (roomsTillEarnStation == -1) {
			roomsTillEarnStation = DesignValues.WorldGenerator.RoomsTillFirstEarnStation;
		}
		if (roomsTillHealStation == -1) {
			roomsTillHealStation = DesignValues.WorldGenerator.RoomsTillHealStation.intValue;
		}
		resetRoomsTillSponsorship();
		OnBeforeEnteredNewRoom (Rooms.Head, true);
		if (!BTutorialManager.InTutorial) {
			tutorialEnemy = null;
			tutorialStartRoom = null;
		}
		yield return null;
		WorldGenerated = true;
		while (PlayerManager.GetActiveGirl() == PPG.None) {
			yield return null;
		}
		var nextInLine = PlayerManager.GetNextGirlForUnlocked();
		if (PlayerManager.IsGirlUnlocked(PPG.Blossom) || nextInLine != PPG.Blossom) {
			giftPickupPrefabs.BlossomGiftPrefab = null;
		}
		if (PlayerManager.IsGirlUnlocked(PPG.Bubbles) || nextInLine != PPG.Bubbles) {
			giftPickupPrefabs.BubblesGiftPrefab = null;
		}
		if (PlayerManager.IsGirlUnlocked(PPG.Buttercup) || nextInLine != PPG.Buttercup) {
			giftPickupPrefabs.ButtercupGiftPrefab = null;
		}
		if (PlayerManager.UnlockedBlisstina || EnvironmentManager.CurrentEnvironment != EnvironmentManager.EnvironmentType.Monster) {
			giftPickupPrefabs.BlissGiftPrefab = null;
		}
		Resources.UnloadUnusedAssets();
		System.GC.Collect();
	}
	void OnDestroy()
	{
		BWorldMapDragger.HandleBeforeEnteredNewRoom -= OnBeforeEnteredNewRoom;
		BWorldMapDragger.HandleEnteredNewRoom -= onEnteredNewRoom;
		BattleManager.HandleDefeatEnemy -= onEnemyDefeated;
	}

	void resetRoomsTillSponsorship()
	{
		roomsTillSponsorship = Random.Range(10, 41);//exclusive
	}

	void onEnemyDefeated(BEnemyAgent _enemy)
	{
		int count = _enemy.GetGoldYield();
		for(int i = 0; i < count; ++i) {
			var pos = BWorldMapDragger.Instance.CurrentRoom.Handle.transform.position + new Vector3( Random.Range(-207, 207), Random.Range(-200, 50), 0);
			var start = pos;
			start.x = _enemy.transform.position.x;
			BCurrencyPool.CreateGoldAtLocation(pos, start);
		}
	}
	void OnBeforeEnteredNewRoom(RoomNode _aboutToEnter, bool _isNewRoom)
	{
		if (!_isNewRoom) return;
		bool justSpawnedEnemies = false;
		if (!_aboutToEnter.IsSpecialRoom) {
			pickupsToCleanup = FindObjectsOfType<BPickup>();
			var specialChance = Random.value;
			var futureProgress = PlayerManager.GlobalProgress + (ObjectiveManager.ProgressPerRoom * BDebugOverlay.ScoreModifier);
			var futureProgressUnclamped = (uint)PlayerManager.GlobalProgressUnclamped + (ObjectiveManager.ProgressPerRoom * BDebugOverlay.ScoreModifier);
			var doubleFutureProgress = PlayerManager.GlobalProgress + (2 * ObjectiveManager.ProgressPerRoom * BDebugOverlay.ScoreModifier);
			var willSwapEnvNext = EnvironmentManager.GetEnvironmentBasedOnProgress (futureProgress) != EnvironmentManager.CurrentEnvironment;
			var willSwapEnvAfterNext = EnvironmentManager.GetEnvironmentBasedOnProgress (doubleFutureProgress) != EnvironmentManager.CurrentEnvironment;
			var narrativeObjective = PlayerManager.Objectives.CheckIfReachedNarrativeObjective(futureProgress);
			var giftObjective = !willSwapEnvNext && PlayerManager.Objectives.CheckIfReachedGiftObjective(futureProgressUnclamped);

			var canSpecialAction = !BTutorialManager.InTutorial &&
				!spawnedSpecialPreviousRoom &&
				!narrativeObjective &&
				!giftObjective &&
				!willSwapEnvNext;
			var shouldSpawnSpecial = canSpecialAction &&
				specialChance <= DesignValues.WorldGenerator.SpecialRoomSpawnChance &&
				!willSwapEnvAfterNext;
			var placeAbove = Random.value <= 0.5f;
			if (!WorldGenerated || !willSwapEnvNext) {
				addRoomToTail(shouldSpawnSpecial, placeAbove, willSwapEnvAfterNext && WorldGenerated);
			}
			if (BTutorialManager.InTutorial || !WorldGenerated) {
				spawnEnemiesIfApplicable (_aboutToEnter); 
				return;
			}

			if (BTutorialManager.RoomsTillFirstHeartShard == 0) {
				BattleManager.CurrentGameState = GameState.ObjectiveEvent;
				var giftPickup = spawnGiftPickup(_aboutToEnter);
				giftPickup.OnCollectAction = () => collectFirstHeartShardAction ();
				return;
			} else if (processAnyObjective(_aboutToEnter, narrativeObjective, giftObjective, willSwapEnvNext)) {
				return;
			}
			//add enemies if applicable
			justSpawnedEnemies = spawnEnemiesIfApplicable (_aboutToEnter); 

			//Station
			spawnStationsIfApplicable (_aboutToEnter, justSpawnedEnemies);
			//check for sponsorship bot
			var canSpawnGiftBot = !BTutorialManager.InTutorial &&
				!justSpawnedEnemies &&
				!narrativeObjective &&
				!giftObjective &&
				!willSwapEnvNext;
			spawnSponsorshipBotIfApplicable (_aboutToEnter, canSpawnGiftBot);
		}
		//spawn gold or heart shard
		spawnCollectibleIfApplicable (_aboutToEnter, justSpawnedEnemies);
	}
	void collectFirstHeartShardAction()
	{
		var overlay = BGiftOverlay.SpawnGift(GiftSpawner.GiftItem.HeartShard);
		overlay.OnGiftOpenedExternalCallback = () => {
			BTutorialManager.RoomsTillFirstHeartShard = -1;
			SaveLoadManager.Save();
		};
		overlay.OnClose = () => {
			(new DialogueBuilder())
				.Add(DialogueSpeaker.HeartShard, "キミはハートのカケラを手に入れた！これがあればアップグレード画面でガールズに新しいアビリティを覚えさせられるぞ。")
				.Add(DialogueSpeaker.HeartShard, "ハートのカケラはとても貴重なアイテムだ。よく考えて使おう！")
				.SetDelay(0.25f)
				.OnComplete(() => {
					BattleManager.CurrentGameState = GameState.Navigation;
				}
			).Go();
		};
	}
	void onEnteredNewRoom(RoomNode _current, bool _newRoom)
	{
		if (!_newRoom) {
			return;
		}

		if (!_current.IsSpecialRoom) {
			if (pickupsToCleanup != null) {
				foreach(var p in pickupsToCleanup) {
					p.CleanUp();
				}
				pickupsToCleanup = null;
			}
			if (lagRemoveRoom > 0) {
				--lagRemoveRoom;
			} else {
				Rooms.RemoveFirst();
			}
		}

		if (_newRoom && !BTutorialManager.InTutorial) {
			if (_current.IsSpecialRoom) {
				BAudioManager.Instance.CreateAndPlayAudio(effectsData.EnterSecretRoomSound);
			} else if (BTutorialManager.RoomsTillFirstHeartShard > 0) {
				BTutorialManager.RoomsTillFirstHeartShard--;
			}
			PlayerManager.IncrementProgress(_current.IsSpecialRoom);
			var nextEnv =  EnvironmentManager.GetEnvironmentBasedOnProgress(PlayerManager.GlobalProgress);
			var switchEnvironments = nextEnv != EnvironmentManager.CurrentEnvironment;
			if (switchEnvironments) {
				StartCoroutine(delayChangeEnvironment());
			}
		}
		SaveLoadManager.Save();
	}
	public static float AgentOffset
	{
		get {
			var currentAspect = (float)Screen.width / (float)Screen.height;
			var designedAspect = (float)tk2dCamera.Instance.nativeResolutionWidth / (float)tk2dCamera.Instance.nativeResolutionHeight;
			return (currentAspect / designedAspect) * (tk2dCamera.Instance.nativeResolutionWidth / 4f);
		}
	}
	void addEnemiesToRoom(RoomNode _room)
	{
		GameObject toSpawn = null;
		if (BTutorialManager.InTutorial) {
			toSpawn = tutorialEnemy;
		} else {
			toSpawn = curEnemyData[BDebugOverlay.ShouldForceEnemy ? BDebugOverlay.DebugForceEnemy : BDifficultyManager.Instance.EnemyToSpawn];
		}
		var enemy = Instantiate(toSpawn) as GameObject;
		enemy.transform.position = _room.Handle.position + (Vector3.right * AgentOffset);
		var enemyMesh = enemy.GetComponentInChildren<MeshRenderer>();
		enemy.transform.position += (enemyMesh.bounds.extents.y * Vector3.down);
		_room.Enemies.Add(enemy.GetComponent<BEnemyAgent>());
	}
	void addRoomToTail(bool _hasSpecialRoomAttached, bool _placeAbove, bool _useEndChunk)
	{
		GameObject bgPrefab = _useEndChunk ? curEnvData.endChunk : curEnvData.RandomStraight;
		if (_hasSpecialRoomAttached) {
			bgPrefab = _placeAbove ? curEnvData.RandomUp : curEnvData.RandomDown;
		}
		GameObject floor = null;
		if (!_hasSpecialRoomAttached && roomsTillSponsorship > 0 && BSponsorshipManager.CheckIfActive(BSponsorshipManager.billboardKey)) {
			--roomsTillSponsorship;
			if (roomsTillSponsorship == 0) {
				resetRoomsTillSponsorship();
				floor = Instantiate(curEnvData.RandomSponsorship, transform, true);
			}
		}
		if (floor == null) {
			floor = Instantiate(bgPrefab, transform, true) as GameObject;
		}
		floor.transform.position = Rooms.Tail.Handle.position + (Vector3.right * tkCam.nativeResolutionWidth);
		Rooms.AddLast(floor.transform);
		//add special room if applicable
		if (_hasSpecialRoomAttached) {
			spawnedSpecialPreviousRoom = true;
			addSecretRoomToTail(_placeAbove);
		} else {
			spawnedSpecialPreviousRoom = false;
		}
	}
	void addSecretRoomToTail(bool _placeAbove)
	{
		var bgPrefab = _placeAbove ? curEnvData.RandomSecretUp : curEnvData.RandomSecretDown;
		var floor = Instantiate(bgPrefab, transform, true) as GameObject;
		var offset = _placeAbove ? Vector3.up : Vector3.down;
		if (_placeAbove) {
			Rooms.AddAboveLast(floor.transform);
		} else {
			Rooms.AddBelowLast(floor.transform);
		}
		floor.transform.position = Rooms.Tail.Handle.position + (offset * tkCam.nativeResolutionHeight);
		var sparkle = Instantiate(secretSparkle, floor.transform, true);
		sparkle.transform.localPosition = (-1f * offset) * 1.2f * (tkCam.nativeResolutionHeight / 2);
	}
	//Turn on to see coin spawn box
//	void Update()
//	{
//		if (Rooms != null && Rooms.Tail != null) {
//			var a = Camera.main.transform.position + new Vector3 (-pickupSpawnWidth / 2, -pickupSpawnHeight + coinSpawnBoxYOffset, 0);
//			var b = Camera.main.transform.position + new Vector3 (-pickupSpawnWidth / 2, pickupSpawnHeight + coinSpawnBoxYOffset, 0);
//			var c = Camera.main.transform.position + new Vector3 (pickupSpawnWidth / 2, -pickupSpawnHeight + coinSpawnBoxYOffset, 0);
//			var d = Camera.main.transform.position + new Vector3 (pickupSpawnWidth / 2, pickupSpawnHeight + coinSpawnBoxYOffset, 0);
//			Debug.DrawLine(a, c);
//			Debug.DrawLine(b, d);
//			Debug.DrawLine(b, a);
//			Debug.DrawLine(c, d);
//		}
//	}
	void spawnGoldInRoom(RoomNode _current, int _count)
	{
		var secretSparkleInstance = FindObjectOfType<BHiddenPFXCoinManager> ();
		for(int i = 0; i < _count; ++i) {
			var pos = _current.Handle.position + new Vector3 (Random.Range (-pickupSpawnWidth / 2, pickupSpawnWidth / 2), Random.Range (-pickupSpawnHeight + coinSpawnBoxYOffset, pickupSpawnHeight + coinSpawnBoxYOffset), 0);
			var gold = BCurrencyPool.CreateGoldAtLocation (pos);
			if (secretSparkleInstance  != null && _current.IsSpecialRoom) {
				secretSparkleInstance .Coins.Add (gold);
			}
		}
	}
	void spawnHeartShardInRoom(RoomNode _current)
	{
		var secretSparkleInstance  = FindObjectOfType<BHiddenPFXCoinManager> ();
		var pos = _current.Handle.position;
		var hs = BCurrencyPool.CreateHeartShardAtLocation (pos);
		if (secretSparkleInstance  != null && _current.IsSpecialRoom) {
			secretSparkleInstance.Coins.Add (hs);
		}
	}

	GameObject spawnNarrativeGiftPickup(PPG _girl, RoomNode _current)
	{
		var go = Object.Instantiate(giftPickupPrefabs[_girl]);
		go.transform.position = _current.Handle.position + new Vector3(BWorldMapGenerator.AgentOffset, -196, 0);
		return go;
	}
	BGiftPickup spawnGiftPickup(RoomNode _current)
	{
		var go = Object.Instantiate(giftPickupPrefabs.GiftPrefab);
		go.transform.position = _current.Handle.position + Vector3.right * (Screen.width / 4f);
		return go.GetComponent<BGiftPickup>();
	}
	IEnumerator delayChangeEnvironment()
	{
		if (PlayerManager.NumberofGirlUnlocked() < 3 || !PlayerManager.UnlockedBlisstina) {
			while (!BDialogOverlay.Instance.gameObject.activeSelf){
				yield return null;
			}
			while (BDialogOverlay.Instance.gameObject.activeSelf){
				yield return null;
			}
		}
		yield return null;

		BattleManager.CurrentGameState = GameState.ObjectiveEvent;
		yield return new WaitForSeconds(0.5f);
		BGameplayInit.PersistScore();
		BScreenFade.FadeToScene("Gameplay", true);
	}
	AudioClip getAudioClipForNarrative(PPG _currentGirl, PPG _foundGirl) 
	{
		AudioClip dialogueVOToPlay = null;
		if (_currentGirl == PPG.Blossom && _foundGirl == PPG.Bubbles) {
			dialogueVOToPlay = blossomToBubblesVO;
		} else if (_currentGirl == PPG.Blossom && _foundGirl == PPG.Buttercup) {
			dialogueVOToPlay = blossomToButtercupVO;
		} else if (_currentGirl == PPG.Bubbles && _foundGirl == PPG.Blossom) {
			dialogueVOToPlay = bubblesToBlossomVO;
		} else if (_currentGirl == PPG.Bubbles && _foundGirl == PPG.Buttercup) {
			dialogueVOToPlay = bubblesToButtercupVO;
		} else if (_currentGirl == PPG.Buttercup && _foundGirl == PPG.Blossom) {
			dialogueVOToPlay = buttercupToBlossomVO;
		} else if (_currentGirl == PPG.Buttercup && _foundGirl == PPG.Bubbles) {
			dialogueVOToPlay = buttecupToBubblesVO;
		}
		return dialogueVOToPlay;
	}
	bool processAnyObjective(RoomNode _aboutToEnter, bool _narrativeObjective, bool _giftObjective, bool _willSwapEnvironment)
	{
		if (_narrativeObjective) {
			//spawn narrative gift and bail
			PlayerManager.Objectives.UpdateNextNarrativeObject();
			var girl = PlayerManager.GetNextGirlForUnlocked();
			if (girl != PPG.None) {
				spawnNarrativeGiftPickup (girl, _aboutToEnter);
				NarrativeDialogueTree.DoDialogBetweenCurrentAnd(girl, getAudioClipForNarrative(PlayerManager.GetActiveGirl(), girl), () => {
					BattleManager.CurrentGameState = GameState.Navigation;
					if(EnvironmentManager.CurrentEnvironment == EnvironmentManager.EnvironmentType.Tropical) {
						BGameCenterManager.SurvivedTropical = true;
						SaveLoadManager.Save ();
						BGameCenterManager.ReportProgress(GameCenterConstants.ACH_SURVIVED_TROPICAL, 1);
					} else if(EnvironmentManager.CurrentEnvironment == EnvironmentManager.EnvironmentType.Volcano) {
						BGameCenterManager.SurvivedVolcano = true;
						SaveLoadManager.Save ();
						BGameCenterManager.ReportProgress(GameCenterConstants.ACH_SURVIVED_VOLCANO, 1);
					}
					SaveLoadManager.Save ();
				});
				PlayerManager.UnlockGirl (girl);
				BattleManager.CurrentGameState = GameState.ObjectiveEvent;
				return true;
			} else if (!PlayerManager.UnlockedBlisstina) {
				BattleManager.CurrentGameState = GameState.ObjectiveEvent;
				var bliss = spawnNarrativeGiftPickup (girl, _aboutToEnter);
				bliss.transform.position += Vector3.up * 150f;
				NarrativeDialogueTree.DoDialogWithBliss(allGirlsToBlissVO, () => {
					PlayerManager.UnlockedBlisstina = true;
					BattleManager.CurrentGameState = GameState.Navigation;
					BGameCenterManager.SurvivedMonster = true;
					SaveLoadManager.Save ();
					BGameCenterManager.ReportProgress(GameCenterConstants.ACH_SURVIVED_MONSTER, 1);
				});
				return true;
			}
		}
		if (_giftObjective) {
			//spawn gift and bail
			BattleManager.CurrentGameState = GameState.ObjectiveEvent;
			PlayerManager.Objectives.UpdateNextGiftObjective();
			var giftPickup = spawnGiftPickup(_aboutToEnter);
			giftPickup.OnCollectAction = () => {
				var overlay = BGiftOverlay.SpawnGift(GiftSpawner.GetItemForWinAPrizeGift());
				overlay.OnClose = () => {
					BattleManager.CurrentGameState = GameState.Navigation;
				};
				SaveLoadManager.Save();
			};
			return true;
		}
		return _willSwapEnvironment;
	}

	void spawnSponsorshipBotIfApplicable (RoomNode _aboutToEnter, bool _canSpawnGiftBot)
	{
		if (BSponsorshipManager.CheckIfActive (BSponsorshipManager.giftbotKey)) {
			if (BSponsorshipManager.RoomsTilCanSeeSponsorshipBot > 0) {
				BSponsorshipManager.RoomsTilCanSeeSponsorshipBot--;
			}
			if (_canSpawnGiftBot && BSponsorshipManager.RoomsTilCanSeeSponsorshipBot == 0) {
				var bot = Object.Instantiate (giftPickupPrefabs.SponsorshipPrefab);
				bot.transform.position = _aboutToEnter.Handle.position;
				var sc = bot.GetComponent<BBotPickup> ();
				//StartCoroutine(waitForCanMoveBotAround(_aboutToEnter, sc));
				sc.StartMovingAround(_aboutToEnter.Handle.position);
				BSponsorshipManager.RoomsTilCanSeeSponsorshipBot = BSponsorshipManager.RoomsBetweenSponsorshipBot;
			}
		}
	}
//	IEnumerator waitForCanMoveBotAround(RoomNode _aboutToEnter, BBotPickup _bot)
//	{
//		_bot.gameObject.SetActive(false);
//		while(_aboutToEnter.Enemies.Count > 0) {
//			yield return null;
//		}
//		_bot.gameObject.SetActive(true);
//		_bot.StartMovingAround (_aboutToEnter.Handle.position);
//	}
	bool spawnEnemiesIfApplicable (RoomNode _aboutToEnter)
	{
		if (BDebugOverlay.ShouldForceEnemy) {
			roomsTillNextEnemySpawn = 0;
		}
		if (roomsTillNextEnemySpawn == 0) {
			if (BTutorialManager.InTutorial) {
				roomsTillNextEnemySpawn = 1;
			} else {
				roomsTillNextEnemySpawn = DesignValues.WorldGenerator.GetRoomsForNextEnemy ();
			}
			addEnemiesToRoom (_aboutToEnter);
			return true;
		}
		if (roomsTillNextEnemySpawn > 0) {
			roomsTillNextEnemySpawn--;
		}

		return false;
	}

	void spawnStationsIfApplicable (RoomNode _aboutToEnter, bool justSpawnedEnemies)
	{
		if (BDebugOverlay.ShouldForceStation) {
			roomsTillEarnStation = 0;
		}
		else if (roomsTillEarnStation > 0) {
			--roomsTillEarnStation;
		}
		if (BDebugOverlay.ShouldForceStation) {
			roomsTillHealStation = 0;
		}
		else if (roomsTillHealStation > 0 && !seenHealStationThisRun) {
			--roomsTillHealStation;
		}
		StartCoroutine(BInternetConnection.IsInternetAvailable( hasInternet => {
			if (hasInternet && !justSpawnedEnemies && roomsTillEarnStation == 0) {
				ResetRoomsTillEarn ();
				var station = Object.Instantiate (curEnvData.earnStation);
				station.transform.position = _aboutToEnter.Handle.position + (Vector3.right * tkCam.nativeResolutionWidth / 5f);
			}
			if (hasInternet && !justSpawnedEnemies && roomsTillHealStation == 0) {
				ResetRoomsTillHeal ();
				var station = Object.Instantiate (curEnvData.healStation);
				station.transform.position = _aboutToEnter.Handle.position + (Vector3.left * tkCam.nativeResolutionWidth / 5f);
				seenHealStationThisRun = true;
			}
		}));
	}

	void spawnCollectibleIfApplicable (RoomNode _aboutToEnter, bool justSpawnedEnemies)
	{
		var heartShardChance = Random.value;
		var goldCount = _aboutToEnter.IsSpecialRoom ? DesignValues.WorldGenerator.GoldCountSpecial : DesignValues.WorldGenerator.GoldCountRegular;
		if (!justSpawnedEnemies) {
			if (_aboutToEnter.IsSpecialRoom && heartShardChance <= DesignValues.WorldGenerator.HeartShardSpawnChance) {
				spawnHeartShardInRoom (_aboutToEnter);
			} else {
				spawnGoldInRoom (_aboutToEnter, goldCount.intValue);
			}
		}
	}
}
