﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;

public class BWorldMapDragger : MonoBehaviour
{
	const float dragThreshold = 5f;
	const float dragMultiplier = 1f;

	const float screenWidthPercentToSnap = 0.1f;
	const float screenHeightPercentToSnap = 0.1f;
	const float tweenTime = 0.6f;

	public delegate void OnEnteredNewRoom(RoomNode _current, bool _firstTime);
	public static event OnEnteredNewRoom HandleBeforeEnteredNewRoom;
	public static event OnEnteredNewRoom HandleEnteredNewRoom;

	public static bool PickingGirlForFirstTime{get; private set;}
	public static BWorldMapDragger Instance{get; private set;}

	public RoomNode CurrentRoom{get; private set;}

	BSwipeManager swipeManager;
	tk2dCamera tkCam;
	BWorldMapGenerator generator;

	RoomNode next;
	Vector3 startPosition;
	Vector2 motionAxis;

	readonly HashSet<Transform> enteredRoom = new HashSet<Transform>();

	void Awake()
	{
		generator = GetComponent<BWorldMapGenerator>();
		tkCam = FindObjectOfType<tk2dCamera>();
		swipeManager = BSwipeManager.Instance;
		Instance = this;
	}

	IEnumerator Start()
	{
		while(!BWorldMapGenerator.WorldGenerated) {
			yield return null;
		}
		CurrentRoom = generator.Rooms.Head;
		enteredRoom.Add(CurrentRoom.Handle);

		BSwipeManager.HandleSwipeStarted += handleSwipeStarted;
		BSwipeManager.HandleSwipeMoved += handleSwipeMoved;
		BSwipeManager.HandleSwipeEnded  += handleSwipeEnded;
		if (PlayerManager.NumberofGirlUnlocked() == 0) {
			BScreenFade.StartFadeToClear (1f, true);
			while (PlayerManager.NumberofGirlUnlocked() == 0) {
				yield return null;
			}
			PlayerManager.SetActiveGirl(BGirlSelectOverlay.Instance.Selected);
			SaveLoadManager.Save();
		} else {
			do {
				yield return null;
			} while( FindObjectOfType<BPlayerAgent>() == null);
			BScreenFade.StartFadeToClear (1f, true);
		}
	}

	#if UNITY_EDITOR
	void Update()
	{
		if (notAllowedToSwipe ()) { return; }
		bool foundInput = false;
		if (Input.GetKeyDown(KeyCode.RightArrow) && CurrentRoom.Right != null) {
			swipeManager.enabled = false;
			motionAxis = Vector2.right;
			next = CurrentRoom.Right;
			foundInput = true;
		} else if (Input.GetKeyDown(KeyCode.UpArrow) && CurrentRoom.Up != null) {
			swipeManager.enabled = false;
			motionAxis = Vector2.up;
			next = CurrentRoom.Up;
			foundInput = true;
		} else if (Input.GetKeyDown(KeyCode.DownArrow) && CurrentRoom.Down != null) {
			swipeManager.enabled = false;
			motionAxis = Vector2.up;
			next = CurrentRoom.Down;
			foundInput = true;
		}
		if (foundInput) {
			swipeManager.enabled = false;
			StartCoroutine(moveCamToRoom (next));
			BPlayerNavParty.Instance.MoveGirlsToNewRoom(next, next != CurrentRoom, tweenTime, 0.8f * tweenTime, ()=> {
				swipeManager.enabled = true;
			});
		}
	}
	#endif

	void OnDestroy()
	{
		Instance = null;
		BSwipeManager.HandleSwipeStarted -= handleSwipeStarted;
		BSwipeManager.HandleSwipeMoved -= handleSwipeMoved;
		BSwipeManager.HandleSwipeEnded  -= handleSwipeEnded;
	}

	bool notAllowedToSwipe ()
	{
		return !swipeManager.enabled ||
			Utilities.WatchingAd ||
			BattleManager.CurrentGameState != GameState.Navigation ||
			CurrentRoom == null ||
			CurrentRoom.Enemies.Count > 0 ||
			BGiftOverlay.IsGiftOverlayOpen;
	}

	void handleSwipeStarted(Vector2 _startPos)
	{
		if (notAllowedToSwipe ()) { return; }
		startPosition = Camera.main.transform.position;
	}
	void handleSwipeMoved(Vector2 _dist)
	{
		if (notAllowedToSwipe ()) { return; }
		if (_dist.x < -dragThreshold && CurrentRoom.Right != null) {
			if (motionAxis == Vector2.zero) {
				motionAxis = Vector2.right;
			}
			next = CurrentRoom.Right;
		} else if (_dist.y < -dragThreshold && CurrentRoom.Up != null) {
			if (motionAxis == Vector2.zero) {
				motionAxis = Vector2.up;
			}
			next = CurrentRoom.Up;
		} else if (_dist.y > dragThreshold && CurrentRoom.Down != null) {
			if (motionAxis == Vector2.zero) {
				motionAxis = Vector2.up;
			}
			next = CurrentRoom.Down;
		}
		if (next == null) return;
		var currentAndNext = new []{CurrentRoom, next};

		var drag = Vector2.Dot(_dist, motionAxis) * motionAxis;
		drag *= -1f;
		Camera.main.transform.Translate(drag * dragMultiplier * (float)Application.targetFrameRate * Time.deltaTime);
		var campos = Camera.main.transform.position;
		if (motionAxis == Vector2.right) {
			var sorted = currentAndNext.OrderBy(r => r.Handle.position.x);
			var clampedX = Mathf.Clamp(campos.x, sorted.First().Handle.position.x, sorted.Last().Handle.position.x);
			campos.x = clampedX;
		} else if (motionAxis == Vector2.up) {
			var sorted = currentAndNext.OrderBy(r => r.Handle.position.y);
			var clampedY = Mathf.Clamp(campos.y, sorted.First().Handle.position.y, sorted.Last().Handle.position.y);
			campos.y = clampedY;
		}
		Camera.main.transform.position = campos;

		var snapThreshold = motionAxis == Vector2.right ?
			(screenWidthPercentToSnap * tkCam.nativeResolutionHeight) :
			(screenHeightPercentToSnap * tkCam.nativeResolutionWidth);
		var doSnap = (Camera.main.transform.position - startPosition).magnitude > snapThreshold;
		if (doSnap) {
			swipeManager.enabled = false;
			StartCoroutine(moveCamToRoom (next));
			BPlayerNavParty.Instance.MoveGirlsToNewRoom(next, next != CurrentRoom, tweenTime, 0.8f * tweenTime, ()=> {
				swipeManager.enabled = true;
			});
		}
	}
	void handleSwipeEnded(Vector2 _dist)
	{
		if (notAllowedToSwipe ()) { return; }
		swipeManager.enabled = false;
		if (motionAxis == Vector2.right && Mathf.Abs(_dist.x) < dragThreshold) {
			next = CurrentRoom;
		} else if (motionAxis == Vector2.up && Mathf.Abs(_dist.y) < dragThreshold) {
			next = CurrentRoom;
		}
		StartCoroutine(moveCamToRoom(CurrentRoom, true));
	}

	IEnumerator moveCamToRoom (RoomNode _room, bool _renableSwipe = false)
	{
		if (motionAxis == Vector2.right) {
			Camera.main.transform.DOMoveX (_room.Handle.position.x, tweenTime).SetEase(Ease.OutCubic);
		}
		else if (motionAxis == Vector2.up) {
			Camera.main.transform.DOMoveY (_room.Handle.position.y, tweenTime).SetEase(Ease.OutCubic);
		}
		motionAxis = Vector2.zero;
		if (_room != CurrentRoom) {
			bool newRoom = false;
			if (next != null && !enteredRoom.Contains(next.Handle)) {
				newRoom = true;
				if (HandleBeforeEnteredNewRoom != null) {
					HandleBeforeEnteredNewRoom(_room, newRoom);
				}
			}
			yield return new WaitForSeconds(tweenTime);
			CurrentRoom = _room;
			if (newRoom) {
				enteredRoom.Add(CurrentRoom.Handle);
			}
			if (HandleEnteredNewRoom != null) {
				HandleEnteredNewRoom(CurrentRoom, newRoom);
			}
		}
		swipeManager.enabled |= _renableSwipe;
	}
}
