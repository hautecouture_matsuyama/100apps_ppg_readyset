﻿using UnityEngine;
using DG.Tweening;
public class BMedicFriendlyMonster : BTimeBasedFrienlyMonster
{
	[SerializeField] float healAmount;
	protected override void doAction ()
	{
		var fm = transform.GetChild(0);
		fm.DOLocalMoveY(100f, tweenTime).SetRelative(true).SetEase(Ease.OutQuad).OnComplete(() => {
			if(BattleManager.PlayerPartyLead != null && !BattleManager.PlayerPartyLead.IsDead) {
				BattleManager.PlayerPartyLead.UpdateHealthAndUpdateUI(healAmount);
			}
			fm.DOLocalMoveY(originalFMLocalPos.y, tweenTime).SetEase(Ease.OutBounce);
		});
		doShadowBounce();
	}
}
