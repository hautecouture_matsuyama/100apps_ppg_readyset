﻿using UnityEngine;
using System.Collections;

public class BTimeBasedFrienlyMonster : BBaseFriendlyMonster
{

	[SerializeField] float timeInterval;

	protected override void onStartBattle ()
	{
		base.onStartBattle();
		StartCoroutine(waitForTimeBasedAction(timeInterval));
	}
	protected override void onEndBattle ()
	{
		StopAllCoroutines();
		base.onEndBattle();
	}
	IEnumerator waitForTimeBasedAction(float _time)
	{
		while (true) {
			yield return new WaitForSeconds(_time);
			if (!isPaused && BattleManager.CurrentGameState == GameState.Battle) {
				doAction();
			}
		}
	}
}
