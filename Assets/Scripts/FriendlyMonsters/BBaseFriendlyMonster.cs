﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

public class BBaseFriendlyMonster : MonoBehaviour
{
	protected virtual float tweenTime {get {return 0.5f;}}

	protected bool inBattle{get; private set;}
	protected Vector3 originalFMLocalPos;
	protected Vector3 originalShadowLocalPos;
	protected Vector3 originalShadowScale;
	BPlayerAgent player;
	protected bool isPaused = false;
	public void SetPaused(bool _pause)
	{
		isPaused = _pause;
	}
	void Awake()
	{
		BattleManager.HandleStateSwitch += onStateChange;
		originalFMLocalPos = transform.GetChild(0).localPosition;
		originalShadowLocalPos = transform.GetChild(1).localPosition;
		originalShadowScale= transform.GetChild(1).localScale;
	}
	void Start()
	{
		gameObject.SetActive(false);
	}

	void OnDestroy()
	{
		BattleManager.HandleStateSwitch -= onStateChange;
	}
	void onStateChange(GameState _state)
	{
		if (_state == GameState.SuperAttack) {
			return;
		}
		if (!inBattle && _state == GameState.Battle) {
			inBattle = true;
			onStartBattle();
		} else if (inBattle && _state != GameState.Battle) {
			inBattle = false;
			onEndBattle();
		}
	}
	protected virtual void onStartBattle()
	{
		if (!gameObject.activeSelf) {
			BEffectsPool.CreatePoofEffectAtPos(gameObject.transform.position + new Vector3(0, 50, 0));
			gameObject.SetActive(true);
		}
		player = FindObjectOfType<BPlayerAgent>();
	}
	protected virtual void onEndBattle()
	{
		transform.GetChild(0).localPosition = originalFMLocalPos;
		transform.GetChild(1).localPosition = originalShadowLocalPos;
		transform.GetChild(1).localScale = originalShadowScale;
		if (!player.IsDead) {
			BCoroutine.WaitAndPerform(() => {
				BEffectsPool.CreatePoofEffectAtPos(gameObject.transform.position + new Vector3(0, 50, 0));
				gameObject.SetActive(false);
			}, 1.25f);
		}
	}
	protected virtual void doAction()
	{
	}
	protected void doShadowBounce()
	{
		var shadow = transform.GetChild(1);
		var scale = shadow.localScale;
		shadow.DOScale(scale * 0.5f, tweenTime).SetEase(Ease.OutQuad).OnComplete(() => {
			shadow.DOScale(originalShadowScale, tweenTime).SetEase(Ease.OutBounce);
		});
	}
	protected void doYHop()
	{
		var shadow = transform.GetChild(1);
		var scale = shadow.localScale;
		shadow.DOScale(scale * 0.5f, tweenTime / 2f).SetEase(Ease.OutQuad).OnComplete(() => {
			shadow.DOScale(originalShadowScale, tweenTime / 2f).SetEase(Ease.InQuad);
		});
		var fm = transform.GetChild(0);
		fm.DOLocalMoveY(100, tweenTime / 2f).SetRelative(true).SetEase(Ease.OutQuad).OnComplete(() => {
			fm.DOLocalMoveY(originalFMLocalPos.y, tweenTime / 2f).SetEase(Ease.InQuad);
		});
	}
}
