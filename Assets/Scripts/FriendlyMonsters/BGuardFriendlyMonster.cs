﻿using UnityEngine;
using DG.Tweening;
using Spine;
using Spine.Unity;

public class BGuardFriendlyMonster : BBaseFriendlyMonster
{
	public static bool IsDefending{get; private set;}
	protected override float tweenTime {get {return 0.2f;}}

	[SerializeField]
	[Range(0,1f)]
	float chanceToBlock;
	[SerializeField]
	[Range(0,1f)]
	float whenToBlock;

	bool attemptedDuringCurrentTelegraph;
	protected override void onStartBattle()
	{
		base.onStartBattle();
		BattleManager.EnemyPartyLead.HandleTelegraphUpdate += onTelegraphUpdate;
	}
	protected override void onEndBattle()
	{
		base.onEndBattle();
		BattleManager.EnemyPartyLead.HandleTelegraphUpdate -= onTelegraphUpdate;
	}

	void onTelegraphUpdate(float _percent, float _time)
	{
		if (isPaused || BattleManager.CurrentGameState != GameState.Battle) {
			return;
		}
		if (Mathf.Approximately(_percent, 0f)) {
			attemptedDuringCurrentTelegraph = IsDefending = false;
		} else if (_percent >= whenToBlock && !attemptedDuringCurrentTelegraph) {
			attemptedDuringCurrentTelegraph = true;
			var chance = Random.value;
			if (chance <= chanceToBlock) {
				IsDefending = true;
				var center = BWorldMapDragger.Instance.CurrentRoom.Handle.position;

				doYHop();
				var shadow = transform.GetChild(1);
				shadow.DOMoveX(center.x, tweenTime).SetEase(Ease.Linear);
				var fm = transform.GetChild(0);
				fm.DOMoveX(center.x, tweenTime).SetEase(Ease.Linear).OnComplete(() => {
					ObjectPool pool = null;
					var fx = BEffectsPool.CreateBlockEffectAtPos(fm.position, true, ref pool);
					fx.transform.Translate(Vector3.down * 150F);
					BattleManager.PlayerPartyLead.PlayTakeHitBlockSound();
					const float delay = 0.25f;
					BGameplaySleep.Sleep(0.3f, delay);
					BCoroutine.WaitAndPerformRealtime(() => {
						fx.GetComponent<SkeletonAnimation>().AnimationState.AddAnimation(0, "Regular_End", false, 0).Complete += delegate {
							pool.Release(fx);
							shadow.DOLocalMoveX(originalShadowLocalPos.x,tweenTime).SetEase(Ease.Linear);
							doYHop();
							fm.DOLocalMoveX(originalFMLocalPos.x,tweenTime).SetEase(Ease.Linear);
						};
					}, delay);
				});
			}
		}
	}
}
