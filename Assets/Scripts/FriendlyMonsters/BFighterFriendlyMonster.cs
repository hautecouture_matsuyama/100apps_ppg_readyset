﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class BFighterFriendlyMonster : BTimeBasedFrienlyMonster
{
	const float powerAsPercentOfEnemyMaxHealth = 0.02f;

	protected override void doAction ()
	{
		var power = BattleManager.EnemyPartyLead.MaxHealth * powerAsPercentOfEnemyMaxHealth;
		var packet = new FMAttackPacket(BattleManager.PlayerPartyLead, -1, power, false);
		var fm = transform.GetChild(0);
		var shadow = transform.GetChild(1);
		fm.DOLocalMoveX(100, tweenTime).SetRelative(true).SetEase(Ease.Linear).OnComplete(() => {
			BattleManager.AttackOpponent(packet);
			fm.DOLocalMoveX(originalFMLocalPos.x, tweenTime).SetEase(Ease.Linear);
			doYHop();
			shadow.DOLocalMoveX(originalShadowLocalPos.x, tweenTime).SetEase(Ease.Linear);
		});
		doYHop();
		shadow.DOLocalMoveX(100, tweenTime).SetRelative(true).SetEase(Ease.Linear);
	}
}
