﻿using UnityEngine;
using DG.Tweening;

public class BCoachFriendlyMonster : BBaseFriendlyMonster
{
	[SerializeField]
	[Range(0,1)]
	float chanceToDoStaminaDamage;

	/// <summary>
	/// The stamina damage dealt per player attack, as a percent of that attack.
	/// </summary>
	[SerializeField] float staminaDamagePercent;

	public float StaminaDamagePercent
	{
		get {
			if (isPaused || BattleManager.CurrentGameState != GameState.Battle) {
				return 0f;
			}
			return Random.value <= chanceToDoStaminaDamage ? staminaDamagePercent : 0f;
		}
	}
	public void ShakeForSapping()
	{
		var fm = transform.GetChild(0);
		fm.DOKill(true);
		fm.DOShakeRotation(tweenTime, Vector3.forward * 40f, 100).OnComplete(()=>{
			fm.rotation = Quaternion.identity;
		});
		fm.DOLocalMoveY(100f, tweenTime).SetRelative(true).SetEase(Ease.OutQuad).OnComplete(() => fm.DOLocalMoveY (originalFMLocalPos.y, tweenTime).SetEase (Ease.OutBounce));
		doShadowBounce();
	}
}
