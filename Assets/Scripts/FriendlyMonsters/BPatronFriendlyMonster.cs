﻿using UnityEngine;
using DG.Tweening;

public class BPatronFriendlyMonster : BTimeBasedFrienlyMonster
{
	[SerializeField] int maxThrows;

	int throwCount;
	protected override void onStartBattle ()
	{
		base.onStartBattle();
		throwCount = 0;
	}

	protected override void doAction ()
	{
		if (throwCount < maxThrows) {
			++throwCount;
			var fm = transform.GetChild(0);
			fm.DOLocalMoveY(100f, tweenTime).SetRelative(true).SetEase(Ease.OutQuad).OnComplete(() => {
				var endPos = BWorldMapDragger.Instance.CurrentRoom.Handle.transform.position + new Vector3( Random.Range(-307, 307), Random.Range(-200, 50), 0);
				BCurrencyPool.CreateGoldAtLocation (endPos, transform.position);
				fm.DOLocalMoveY(originalFMLocalPos.y, tweenTime).SetEase(Ease.OutBounce);
			});
			doShadowBounce();
		}
	}
}
