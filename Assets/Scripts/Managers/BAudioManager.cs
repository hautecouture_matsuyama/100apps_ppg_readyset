﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class BAudioManager : MonoBehaviour
{
	static BAudioManager instance;

	public static BAudioManager Instance {
		get {
			if (instance == null) {
				instance = SingletonScriptGenerator.Create<BAudioManager> ();
			}
			return instance;
		}
	}

	[SerializeField] AudioClip healSound;
	[SerializeField] AudioClip menuWhooshIn;
	[SerializeField] AudioClip menuWhooshOut;
	public static bool MusicMuted;
	public static bool SfxMuted;

	public float CurrentMusicDefaultVolume { get; private set; }

	const int maxMusicChannels = 2;
	List<AudioSource> musicSources = new List<AudioSource>();

	AudioSource currentSongIntroSource;
	AudioSource currentAmbienceSource;
	bool musicPaused = false;
			
	void Awake()
	{
		for (int i = 0; i < maxMusicChannels; i++) {
			musicSources.Add (null);
		}
		if (instance == null) {
			instance = this;
		}
	}
	void Start()
	{
		setAudioToCurrentMute(AudioType.Music, MusicMuted);
		setAudioToCurrentMute(AudioType.Sfx, SfxMuted);
	}
	void OnDestroy()
	{
		instance = null;
	}
	void setAudioToCurrentMute(AudioType _type, bool _muted)
	{
		foreach(AudioSource source in GameObject.FindObjectsOfType<AudioSource>()) {
			if(source.GetComponent<BAudioType>().Type == _type) {
				source.mute = _muted;
			}
		}
	}
	void setup3DSound(AudioSource _source)
	{
		_source.spatialBlend = 1.0f;
		_source.rolloffMode = AudioRolloffMode.Linear;
		_source.maxDistance = 30f;
	}
	public AudioSource CreateAndPlayAudio(AudioClip _clip, bool _loop = false, float _volume = 1.0f, float _pitch = 1, bool _3DSound = false, GameObject _3DTarget = null, AudioType _type = AudioType.Sfx, double _delay = 0)
	{
		var audioGObj = new GameObject(_type + " Audio Object");
		audioGObj.transform.parent = this.gameObject.transform;
		var audioTypeComp = audioGObj.AddComponent<BAudioType>();
		audioTypeComp.Type = _type;
		var audioSource = audioGObj.AddComponent<AudioSource>();
		audioSource.clip = _clip;
		audioSource.volume = _volume;
		audioSource.pitch = _pitch;
		if(_3DSound) {
			if(_3DTarget != null) {
				setup3DSound(audioSource);
				audioGObj.transform.position = _3DTarget.transform.position;
			} else {
				Debug.LogWarning("You must provide a parenting Target for 3D Sounds");
			}
		}
		if((SfxMuted && audioTypeComp.Type == AudioType.Sfx) || (MusicMuted && audioTypeComp.Type == AudioType.Music)) {
			audioSource.mute = true;
		}
		if (_loop) {
			audioSource.loop = true;
			audioSource.PlayScheduled (AudioSettings.dspTime + _delay);
		} else {
			audioSource.Play();
			StartCoroutine(waitForSFXEndThenDestroy(audioSource));
		}
		return audioSource;
	}
	public AudioSource PlayMusic(AudioClip _song, AudioClip _songIntro, float _introPercentToStartLoop, float _volume = 1.0f, int _channel = 0)
	{
		double delay = 0;
		if(_songIntro != null) {
			if(currentSongIntroSource == null) {
				currentSongIntroSource = CreateAndPlayAudio(_songIntro, false, _volume, 1, false, null, AudioType.Music);
			} else {
				currentSongIntroSource.clip = _songIntro;
				currentSongIntroSource.volume = _volume;
				currentSongIntroSource.Play();
			}
			delay = (double)(_songIntro.length * _introPercentToStartLoop);
		} 
		if (musicSources [_channel] == null) {
			musicSources[_channel] = CreateAndPlayAudio (_song, true, _volume, 1, false, null, AudioType.Music, delay);
		} else {
			musicSources[_channel].clip = _song;
			musicSources[_channel].volume = _volume;
			musicSources[_channel].PlayScheduled (AudioSettings.dspTime + delay);
		}
		CurrentMusicDefaultVolume = _volume;
		return musicSources[_channel];
	}

	public void AdjustMusicVolume(float _volume, float _fadeTime, int _channel = 0)
	{
		if(musicSources[_channel] != null) {
			musicSources[_channel].DOFade(_volume, _fadeTime).SetUpdate(true);
		}
		if(currentSongIntroSource != null) {
			currentSongIntroSource.DOFade(_volume, _fadeTime).SetUpdate(true);
		}
	}

	public void ToggleMuteSound(AudioType _type)
	{
		bool muted;
		if(_type == AudioType.Music) {
			MusicMuted = muted = !MusicMuted;
		} else {
			SfxMuted = muted = !SfxMuted;
		}
		setAudioToCurrentMute(_type, muted);
		SaveLoadManager.Save();
	}

	public void ForceMuteForAds(bool _mute)
	{
		if(_mute) {
			setAudioToCurrentMute(AudioType.Music, true);
			setAudioToCurrentMute(AudioType.Sfx, true);
		} else {
			setAudioToCurrentMute(AudioType.Music, MusicMuted);
			setAudioToCurrentMute(AudioType.Sfx, SfxMuted);
		}
	}

	public void TogglePauseMusic(int _channel = 0)
	{
		if(musicSources[_channel] != null) {
			if(musicPaused) {
				musicSources[_channel].Play();
			} else {
				musicSources[_channel].Pause();
			}
		}
		musicPaused = !musicPaused;

	}
	//Stops current music track, if one exists
	public void StopMusic(float _fadeTime = 1.0f, int _channel = 0)
	{
		if(musicSources[_channel] != null) {
			StartCoroutine(fadeOutThenDestroy(musicSources[_channel], _fadeTime, _channel));
		}
	}
	//Stops current music track, if one exists
	public void StopIntroMusic()
	{
		if(currentSongIntroSource != null) {
			Destroy(currentSongIntroSource.gameObject);
			currentSongIntroSource = null;
		} 
	}
	public AudioSource PlayAmbience(AudioClip _amb, float _volume = 1f)
	{
		if(currentAmbienceSource == null) {
			currentAmbienceSource = CreateAndPlayAudio(_amb, true);
		} else {
			currentAmbienceSource.clip = _amb;
			currentAmbienceSource.Play();
		}
		currentAmbienceSource.volume = _volume;
		return currentAmbienceSource;
	}

	//Stops current ambience track, if one exists
	public void StopAmbience()
	{
		if(currentAmbienceSource != null) {
			Destroy(currentAmbienceSource);
		}
	}

	IEnumerator waitForSFXEndThenDestroy(AudioSource _as)
	{
		yield return new WaitForSecondsRealtime(_as.clip.length);
		if(_as != null) {
			Destroy(_as.gameObject);
		}
	}

	IEnumerator fadeOutThenDestroy(AudioSource _as, float _fadeTime, int _channel = 0)
	{
		if(_fadeTime != 0) {
			_as.DOFade(0, _fadeTime).SetUpdate(true);
			yield return new WaitForSecondsRealtime (_fadeTime);
		}
		Destroy (_as.gameObject);
		musicSources[_channel] = null;
	}

	#region helpers specfic to "Ready, Set, Monsters!"
	public void PlayHealSound()
	{
		CreateAndPlayAudio (healSound);
	}
	public AudioSource PlayMenuWhooshSFX(bool _in)
	{
		if(_in) {
			return CreateAndPlayAudio (menuWhooshIn, false, 0.2f);
		} else {
			return CreateAndPlayAudio (menuWhooshOut, false, 0.2f);
		}
	}
	#endregion
}
