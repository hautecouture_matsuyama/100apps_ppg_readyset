﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BBootupFlowManager : MonoBehaviour {

	enum BootupFlowState
	{
		CNVideo,
		CFSplash,
		TMSSplash,
		AdDisclaimer,
		UpfrontSponsorship
	}
	BootupFlowState bfState = BootupFlowState.CNVideo;
	bool loadingAdHasStarted = false;
	bool hasInternet = false;

	public GameObject CFSplash;
	public GameObject TMSSplash;
	public GameObject AdDisclaimer;
	public GameObject SkipButton;

	const float fadeBetweenSplashesTime = 0.5f;
	const float splashHangTime = 2f;
	//Used to force Freewheel bootup ad off always
	const bool forceFreewheelOff = true;

	void Start()
	{
		SkipButton.SetActive(true);
		StartCoroutine (BInternetConnection.IsInternetAvailable((isConnected) => {
			hasInternet = isConnected;
		}));
	}
	public void OnCnVidEnded()
	{
		StartCoroutine(switchToCFSplash());
	}

	public void OnLoadingAdStarted()
	{
		loadingAdHasStarted = true;
	}

	public void OnLoadingAdWatched()
	{
		StopAllCoroutines();
		BScreenFade.StopAllFades();
		BScreenFade.FadeToScene("Story", false);
	}

	public void SkipToNext()
	{
		SkipButton.SetActive(false);
		switch(bfState) {
		case BootupFlowState.CNVideo:
			StopAllCoroutines();
			BScreenFade.StopAllFades();
			BScreenFade.StartFadeToBlack(fadeBetweenSplashesTime, false, () => {
				StartCoroutine(switchToCFSplash());
			});
			break;
		case BootupFlowState.CFSplash:
			StopAllCoroutines();
			BScreenFade.StopAllFades();
			BScreenFade.StartFadeToBlack(fadeBetweenSplashesTime, false, () => {
				StartCoroutine(switchToTMSSplash());
			});
			break;
		case BootupFlowState.TMSSplash:
			StopAllCoroutines();
			BScreenFade.StopAllFades();
			BScreenFade.StartFadeToBlack(fadeBetweenSplashesTime, false, () => {
				StartCoroutine(switchToAdSplash());
			});
			break;
		case BootupFlowState.AdDisclaimer:
			StopAllCoroutines();
			BScreenFade.StopAllFades();
			if(BTitleOverlay.HasSeenAgreement || forceFreewheelOff) {
				if (!hasInternet) {
					BScreenFade.FadeToScene ("Story", false);
				} else if (!DFP.Instance.ShowBanner ()) {
					BScreenFade.FadeToScene ("Story", false);
				} else {
					BScreenFade.FadeToScene ("Story", false);
				}
			} else {
				BScreenFade.StartFadeToBlack(fadeBetweenSplashesTime, false, () => {
					StartCoroutine(switchToSponsorSplash());
				});
			}
			break;
		case BootupFlowState.UpfrontSponsorship:
			StopAllCoroutines();
			BScreenFade.StopAllFades();
			BScreenFade.FadeToScene("banner", false);
			break;
		default:
			break;
		}
	}

	IEnumerator switchToCFSplash()
	{
		CFSplash.SetActive(true);
		bfState = BootupFlowState.CFSplash;
		BScreenFade.StartFadeToClear(fadeBetweenSplashesTime, false, () => {
			StartCoroutine(waitForCFSplash());
		});
		yield return null;
	}

	IEnumerator waitForCFSplash()
	{
		SkipButton.SetActive(true);
		yield return new WaitForSeconds(splashHangTime);
		SkipButton.SetActive(false);
		BScreenFade.StartFadeToBlack(fadeBetweenSplashesTime, false, () => {
			StartCoroutine(switchToTMSSplash());
		});
	}

	IEnumerator switchToTMSSplash()
	{
		CFSplash.SetActive(false);
		TMSSplash.SetActive(true);
		bfState = BootupFlowState.TMSSplash;
		BScreenFade.StartFadeToClear(fadeBetweenSplashesTime, false, () => {
			StartCoroutine(waitForTMSSplash());
		});
		yield return null;
	}

	IEnumerator waitForTMSSplash()
	{
		SkipButton.SetActive(true);
		yield return new WaitForSeconds(splashHangTime);
		SkipButton.SetActive(false);
		BScreenFade.StartFadeToBlack(fadeBetweenSplashesTime, false, () => {
			StartCoroutine(switchToAdSplash());
		});
	}

	IEnumerator switchToAdSplash()
	{
		TMSSplash.SetActive(false);
		AdDisclaimer.SetActive(true);
		bfState = BootupFlowState.AdDisclaimer;
		BScreenFade.StartFadeToClear(fadeBetweenSplashesTime, false, () => {
			StartCoroutine(waitForAdSplash());
		});
		yield return null;
	}

	IEnumerator waitForAdSplash()
	{
		SkipButton.SetActive(true);
		yield return new WaitForSeconds(splashHangTime);
		SkipButton.SetActive(false);
		if(!BTitleOverlay.HasSeenAgreement || forceFreewheelOff) { 
			if (!hasInternet) {
				BScreenFade.FadeToScene ("Story", false);
			} else if (!DFP.Instance.ShowBanner ()) {
				BScreenFade.FadeToScene ("Story", false);
			} else {
				BScreenFade.FadeToScene ("Story", false);
			}
		} else {
			BScreenFade.StartFadeToBlack(fadeBetweenSplashesTime, false, () => {
				StartCoroutine(switchToSponsorSplash());
			});
		}
	}

	IEnumerator switchToSponsorSplash()
	{
		AdDisclaimer.SetActive(false);
		bfState = BootupFlowState.UpfrontSponsorship;
		StartCoroutine(waitForBackupProgressToTitle());
		//FreewheelManager.RequestAd(AdType.Loading, OnLoadingAdWatched, OnLoadingAdStarted);
		ApplovinManager.Instance.ShowReword();

		#if UNITY_EDITOR
		StartCoroutine(Utilities.WaitForFakeAdTime());
		#endif
		yield return null;
	}

	//Skip to title after a timer, in case Freewheel fails
	IEnumerator waitForBackupProgressToTitle()
	{
		yield return new WaitForSeconds(5f);
		if(!loadingAdHasStarted) {
			OnLoadingAdWatched();
		}
	}
}
