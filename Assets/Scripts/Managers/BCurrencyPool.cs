﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class BCurrencyPool : MonoBehaviour
{
	const float goldTweenTime = 0.5f;
	public static GameObject CreateGoldAtLocation(Vector3 _pos, Vector3 _from = default(Vector3))
	{
		var gold = instance.goldPool.Acquire ();
		if (_from != default(Vector3)) {
			var shadow = gold.transform.Find("Shadow").gameObject;
			shadow.SetActive(false);
			gold.transform.position = _from;
			gold.transform.DOJump(_pos, 180, 1, goldTweenTime).SetEase(Ease.OutQuad).OnComplete(()=> {
				shadow.SetActive(true);
			});
			var originalScale = gold.transform.localScale;
			gold.transform.localScale = Vector3.zero;
			gold.transform.DOScale(originalScale, goldTweenTime).SetEase(Ease.OutQuad);
		} else {
			gold.transform.position = _pos;
		}
		gold.GetComponent<BPickup>().PoolHandle = BCurrencyPool.instance.goldPool;
		setOrderInLayer(gold.GetComponentInChildren<tk2dSprite>(), _pos);
		return gold;
	}
	public static GameObject CreateHeartShardAtLocation(Vector3 _pos)
	{
		var heartShard = instance.heartShardPool.Acquire ();
		heartShard.transform.position = _pos;
		heartShard.GetComponent<BPickup>().PoolHandle = instance.heartShardPool;
		setOrderInLayer(heartShard.GetComponentInChildren<tk2dSprite>(), _pos);
		return heartShard;
	}
	static void setOrderInLayer(tk2dSprite _sp, Vector3 _pos)
	{
		float newY = _pos.y;
		if(_pos.y > 576f) {
			newY -= 1152f;
		} else if(_pos.y < -576f) {
			newY += 1152f;
		}
		newY += 196f;
		_sp.SortingOrder = -1 * (int)newY/ 10;
	}
	static BCurrencyPool instance;


	[SerializeField]
	GameObject goldPrefab;
	[SerializeField]
	GameObject heartShardPrefab;

	ObjectPool goldPool;
	ObjectPool heartShardPool;
	void Awake()
	{
		instance = this;
		goldPool = new ObjectPool(goldPrefab, (self, mimic) => {
			self.transform.localScale = mimic.transform.localScale;
		});
		goldPool.Fill(10);

		heartShardPool = new ObjectPool(heartShardPrefab, (self, mimic) => {
			self.transform.localScale = mimic.transform.localScale;
		});
		heartShardPool.Fill(10);
	}
	void OnDestroy()
	{
		instance = null;
	}
}
