﻿using UnityEngine;
using System.Collections;

public class BSwipeManager : MonoBehaviour
{
	public delegate void OnSwipeMotion(Vector2 _amount);
	public static OnSwipeMotion HandleSwipeStarted;
	public static OnSwipeMotion HandleSwipeMoved;
	public static OnSwipeMotion HandleSwipeEnded;

	static BSwipeManager instance;
	public static BSwipeManager Instance
	{
		get {
			if (instance == null) {
				instance = SingletonScriptGenerator.Create<BSwipeManager>();
			}
			return instance;
		}
	}

	static Vector2 positionLastActiveFrame;
	static bool touchActive = false;

	void OnDisable()
	{
		touchActive = false;
	}

	void OnDestroy()
	{
		instance = null;
	}
	void Update ()
	{
		checkInputActive();
		if (touchActive && HandleSwipeMoved != null) {
			var input = inputPos;
			var delX = input.x - positionLastActiveFrame.x;
			var delY = input.y - positionLastActiveFrame.y;
			if (Mathf.Abs(delX) > Mathf.Abs(delY)) {
				HandleSwipeMoved(Vector2.right * delX);
			}
			if (Mathf.Abs(delY) > Mathf.Abs(delX)) {
				HandleSwipeMoved(Vector2.up * delY);
			}
			positionLastActiveFrame = input;
		}
	}
	static void checkInputActive ()
	{
		#if UNITY_EDITOR
		if (Input.GetMouseButton(0)) {
			onTouchStart ();
		} else {
			onTouchEnd ();
		}
		#else
		int fingerCount = 0;
		foreach (var touch in Input.touches) {
			if (touch.phase != TouchPhase.Ended && touch.phase != TouchPhase.Canceled) {
				fingerCount++;
			}
		}
		if (fingerCount == 1) {
			onTouchStart ();
		} else if (fingerCount == 0) {
			onTouchEnd ();
		}
		#endif
	}

	static void onTouchStart ()
	{
		if (!touchActive) {
			touchActive = true;
			positionLastActiveFrame = inputPos;
			if (HandleSwipeStarted != null) {
				HandleSwipeStarted(Vector2.zero);
			}
		}
	}
	static void onTouchEnd ()
	{
		if (touchActive) {
			touchActive = false;
			if (HandleSwipeEnded != null) {
				HandleSwipeEnded(inputPos - positionLastActiveFrame);
			}
		}
	}
	static Vector2 inputPos
	{
		get {
			#if UNITY_EDITOR
			return Input.mousePosition;
			#else
			return Input.touches[0].position;
			#endif
		}
	}
}
