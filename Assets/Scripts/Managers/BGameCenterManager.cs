﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class BGameCenterManager : MonoBehaviour 
{
	//Used for tracking if you have ever completed these achs, get saved to data
	public static bool SurvivedTropical;
	public static bool SurvivedVolcano;
	public static bool SurvivedMonster;
	public static bool HasUsedBlissOnce;

	public static void AuthenticateGCUser()
	{
		#if USES_GAME_SERVICES
		if(NPBinding.GameServices.IsAvailable() && !NPBinding.GameServices.LocalUser.IsAuthenticated) {
			NPBinding.GameServices.LocalUser.Authenticate((bool _success, string _error)=> {
				if (_success) {
					sendAchievementProgressOnBootUp();
					if (!BDebugOverlay.HideSDKLogs) {
						Debug.Log("Sign-In Successfully");
						Debug.Log("Local User Details : " + NPBinding.GameServices.LocalUser.ToString());
					}
				} else {
					Debug.Log("Game Services Sign-In Failed with error " + _error);
				}
			});
		} else if (NPBinding.GameServices.LocalUser.IsAuthenticated) {
			//Resend all ach and lb data on bootup, in case the user did things while offline previously.
			sendAchievementProgressOnBootUp();
			ReportScore();
		}
		#endif
	}
	static void sendAchievementProgressOnBootUp()
	{
		if(SurvivedTropical) {
			ReportProgress(GameCenterConstants.ACH_SURVIVED_TROPICAL, 1);
		}
		if(SurvivedVolcano) {
			ReportProgress(GameCenterConstants.ACH_SURVIVED_VOLCANO, 1);
		}
		if(SurvivedMonster) {
			ReportProgress(GameCenterConstants.ACH_SURVIVED_MONSTER, 1);
		}
		if(HasUsedBlissOnce) {
			ReportProgress(GameCenterConstants.ACH_BLISS_USED, 1);
		}
		CheckGirlUpgraded(PPG.Blossom);
		CheckGirlUpgraded(PPG.Bubbles);
		CheckGirlUpgraded(PPG.Buttercup);
		CheckAllAuras();
		CheckAllFMs();
		CheckAllSupers();
		CheckLoops();
	}

	public static void ReportScore()
	{
		#if USES_GAME_SERVICES
		NPBinding.GameServices.ReportScoreWithGlobalID(GameCenterConstants.LB_HIGH_SCORES, (long)PlayerManager.BestScore, (bool _success, string _error)=>{
			if (_success) {
				Debug.Log(string.Format("Request to report score to leaderboard with GID '{0}' finished successfully. New Score: {1}", GameCenterConstants.LB_HIGH_SCORES, (long)PlayerManager.BestScore));
			} else {
				Debug.Log(string.Format("Request to report score to leaderboard with GID '{0}' failed with error '{1}'", GameCenterConstants.LB_HIGH_SCORES, _error));
			}
		});
		#endif
	}

	public static void ReportProgress(string _achid, int _progressInSteps)
	{
		#if USES_GAME_SERVICES
		int stepsToUnlock = NPBinding.GameServices.GetNoOfStepsForCompletingAchievement(_achid);
		double progressPercentage = ((double)_progressInSteps/stepsToUnlock) * 100d;
		// If its an incremental achievement, make sure you send a incremented cumulative value everytime you call this method
		NPBinding.GameServices.ReportProgressWithGlobalID(_achid, progressPercentage, (bool _status, string _error)=>{
			if (_status && !BDebugOverlay.HideSDKLogs) {
				Debug.Log(string.Format("Request to report progress of achievement with GID '{0}' finished successfully. Percentage complete: {1}", _achid, progressPercentage));
			} else if(!BDebugOverlay.HideSDKLogs) {
				Debug.Log(string.Format("Request to report progress of achievement with GID '{0}' failed with error '{1}'.", _achid, _error));
			}
		});
		#endif
	}

	#region game-specific helpers
	public static void CheckGirlUpgraded(PPG _girl)
	{
		AcquiredStats girlAcquiredStats;
		string achID = "";
		switch(_girl) {
		case PPG.Blossom:
			girlAcquiredStats = PlayerManager.Instance.BlossomAcquiredStats;
			achID = GameCenterConstants.ACH_BLOSSOM_UPGRADED;
			break;
		case PPG.Bubbles:
			girlAcquiredStats = PlayerManager.Instance.BubblesAcquiredStats;
			achID = GameCenterConstants.ACH_BUBBLES_UPGRADED;
			break;
		case PPG.Buttercup:
			girlAcquiredStats = PlayerManager.Instance.ButtercupAcquiredStats;
			achID = GameCenterConstants.ACH_BUTTERCUP_UPGRADED;
			break;
		default:
			girlAcquiredStats = new AcquiredStats();
			Debug.LogWarning("This should never happen! Please pick one of the 3 girls.");
			break;
		}
		int count = 0;
		if(girlAcquiredStats.AuraUnlocked) {
			count += 1;
		}
		if(girlAcquiredStats.X2Unlocked) {
			count += 1;
		}
		if(girlAcquiredStats.X3Unlocked) {
			count += 1;
		}
		count += girlAcquiredStats.Health;
		count += girlAcquiredStats.Attack;
		count += girlAcquiredStats.Shield;
		count += girlAcquiredStats.Stamina;
		if(achID != "") {
			BGameCenterManager.ReportProgress(achID, count);
		}
	}
	public static void CheckAllAuras()
	{
		var remainingAuras = GiftAvailability.RemainingAurasRaw();
		int count = (10 - remainingAuras[PPG.Blossom].Count);
		count += (10 - remainingAuras[PPG.Bubbles].Count);
		count += (10 - remainingAuras[PPG.Buttercup].Count);
		BGameCenterManager.ReportProgress(GameCenterConstants.ACH_ALL_AURAS, count);
	}
	public static void CheckAllFMs()
	{
		var remainingFMs = GiftAvailability.RemainingFriendlyMonsters();
		int count = (6 - remainingFMs[FriendlyMonsterType.Sapper].Count);
		count += (6 - remainingFMs[FriendlyMonsterType.Fighter].Count);
		count += (6 - remainingFMs[FriendlyMonsterType.Guard].Count);
		count += (6 - remainingFMs[FriendlyMonsterType.Medic].Count);
		count += (6 - remainingFMs[FriendlyMonsterType.Patron].Count);
		BGameCenterManager.ReportProgress(GameCenterConstants.ACH_ALL_FMS, count);
	}
	public static void CheckAllSupers()
	{
		int count = 0;
		if(PlayerManager.IsSuperAttackUnlocked(SuperAttack.TornadoTrio)) {
			count++;
		}
		if(PlayerManager.IsSuperAttackUnlocked(SuperAttack.BlissBlitz)) {
			count++;
		}
		BGameCenterManager.ReportProgress(GameCenterConstants.ACH_ALL_SUPERS, count);
	}
	public static void CheckLoops()
	{
		var loops = PlayerManager.MapLoops;
		if(loops > 0) {
			BGameCenterManager.ReportProgress(GameCenterConstants.ACH_LOOPED_2X, loops);
			BGameCenterManager.ReportProgress(GameCenterConstants.ACH_LOOPED_10X, loops);
		}
	}
	#endregion
}
