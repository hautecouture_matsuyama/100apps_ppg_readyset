﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class BSponsorshipManager : MonoBehaviour
{
	[System.Serializable]
	class ConfigFile
	{
		[System.Serializable]
		public class ConfigItem
		{
			[System.Serializable]
			public class Dimension
			{
				public int width = 0;
				public int height = 0;
			}
			public string expirationDate = null;
			public string path = null;
			public Dimension dimensions = null;
		}
		public ConfigItem billboardImage = null;
		public ConfigItem giftbotImage = null;
	}
	public GameObject SponsorContainer;
	public Image SponsorImage;

	public const string billboardKey = "billboard";
	public const string giftbotKey = "giftbot";
	public const int RoomsBetweenSeeingBillboardSponsorship = 30;
	public const int RoomsBetweenSponsorshipBot = 50;
	public const int RoomsTilFirstSponsorshipBot = 20;

	public static Dictionary<string, Sprite> ImageMapping{get; private set;}
	public static int RoomsTilCanSeeBillboardSponsorship = -1;
	public static int RoomsTilCanSeeSponsorshipBot = RoomsTilFirstSponsorshipBot;

	const string baseURL = @"http://i.cdn.turner.com/toon/games/powerpuff-girls/ready-set-monsters/";
	IEnumerator Start()
	{
		yield return new WaitForSeconds(0.1f);

		ImageMapping = new Dictionary<string, Sprite> ();
		ImageMapping.Add (billboardKey, null);
		ImageMapping.Add (giftbotKey , null);

		StartCoroutine (BInternetConnection.IsInternetAvailable ((isConnected) => {
			if (isConnected) {
				StartCoroutine (getConfig ());
			}
		}));
	}

	public static Sprite GetSponsorImage(string _key)
	{
		if(ImageMapping.ContainsKey(_key)) {
			return ImageMapping[_key];
		} else {
			return null;
		}
	}

	public static bool CheckIfActive(string _key)
	{
		return (ImageMapping != null && BSponsorshipManager.ImageMapping.ContainsKey(_key) && BSponsorshipManager.ImageMapping[_key] != null);
	}

	IEnumerator getConfig()
	{
		var www = new WWW((baseURL + "config.json").URLAntiCacheRandomizer());
		yield return www;
		ConfigFile configFile = null;
		if (string.IsNullOrEmpty(www.error)) {
			try {
				configFile = JsonUtility.FromJson<ConfigFile> (www.text);
			} catch {}
			if (configFile != null) {
				if (!string.IsNullOrEmpty(configFile.billboardImage.path)) {
					yield return StartCoroutine(getImage (billboardKey, configFile.billboardImage, 1));
				}
				if (!string.IsNullOrEmpty(configFile.giftbotImage.path)) {
					yield return StartCoroutine(getImage(giftbotKey, configFile.giftbotImage, 1));
				}
			}
		}
	}

	IEnumerator getImage(string _key, ConfigFile.ConfigItem _item, float _ppu)
	{
		var expireTime = DateTime.Parse (_item.expirationDate);
		var currentTime = DateTime.UtcNow;
		if (currentTime <= expireTime) {
			yield return StartCoroutine (downloadImg (_key, _item.dimensions.width, _item.dimensions.height, baseURL + _item.path, _ppu));
		}
	}

	IEnumerator downloadImg (string _key, int _width, int _height, string url, float _ppu)
	{
		#if UNITY_EDITOR || UNITY_ANDROID
		var texture = new Texture2D(_width, _height,TextureFormat.ETC_RGB4, false);
		#else
		var texture = new Texture2D(_width, _height,TextureFormat.DXT1, false);
		#endif
		var www = new WWW(url.URLAntiCacheRandomizer());
		yield return www;
		if (string.IsNullOrEmpty(www.error)) {
			try {
				www.LoadImageIntoTexture(texture);
				ImageMapping[_key] = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f), _ppu);
			} catch {}
		}
	}
}
