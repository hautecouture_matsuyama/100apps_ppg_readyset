﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using UnityEngine.UI;

public class BTutorialManager : MonoBehaviour
{
	const float preventSkipBufferTime = 1f;
	const float fadeTextTime = 0.2f;

	public static bool AllowedToPause{get; private set;}

	public static bool InTutorial = true;
	public static bool ShownCharacterSwap = false;
	public static bool ShownStun = false;
	public static int BattlesSinceStarted = 0;
	public static bool ShownPerfectBlock;
	public static bool DoPerfectBlockTutorial {
		get {
			return BattlesSinceStarted >= 1 && !ShownPerfectBlock;
		}
	}
	public static int RoomsTillFirstHeartShard = 2;
	public static bool ShownChargeAttackTutorial = false;
	public static bool ShownCounterAttackTutorial = false;

	public static BTutorialManager Instance{get; private set;}

	public static void PaueForCounterAttack()
	{
		Instance.StartCoroutine(Instance.doCounterAttackTutorial());
	}

	[SerializeField] tk2dSprite swipeHand;
	[SerializeField] SpriteRenderer pressHand;
	[SerializeField] SpriteRenderer pointHand;
	[SerializeField] TextMeshProUGUI tutorialText;
	[SerializeField] Image tutorialTextBacking;

	[SerializeField] Image attackGlow;
	[SerializeField] Image blockGlow;

	[SerializeField] AudioClip blossomUghVO;
	[SerializeField] AudioClip bubblesUghVO;
	[SerializeField] AudioClip buttercupUghVO;

	int roomsForHand = 2;
	public bool BlockInputIsDown{get; set;}
	public bool ForceDisableBlock{get; private set;}
	public bool ForceDisableAttack{get; private set;}
	public bool ForceKeepBlockButtonHeldDown{get; private set;}
	public bool PauseStaminaFill{get; private set;}
	public bool InputPressed{get; set;}
	public bool AllowPerfectBlockInTutorial{get; private set;}
	public bool InBlockTutorial{get; private set;}

	int freeAttacks = 5;
	bool playerCompletedFirstAttack;

	Coroutine bringSwipeInRoutine;
	Dictionary<SpriteRenderer, Tweener> fadeTweenMapping = new Dictionary<SpriteRenderer, Tweener>();
	Sequence gestureMotionTween;
	bool unregister = false;
	bool useTouchCallbacks = false;
	void Awake()
	{
		Instance = this;
		AllowedToPause = true;
	}
	void Start()
	{
		System.Action fadeOutHands = ()=> {
			swipeHand.DOFade(0, 0.25f);
			fadeGestureOut(pressHand);
			fadeGestureOut(pointHand);
		};

		if (InTutorial) {
			AllowedToPause = false;
			unregister = true;
			BSwipeManager.HandleSwipeStarted += onSwipeStart;
			BSwipeManager.HandleSwipeEnded += onSwipeEnd;
			BWorldMapDragger.HandleEnteredNewRoom += onEnteredNewRoom;
			fadeOutHands();
			StartCoroutine(goThroughTutorial());
		} else {
			AllowedToPause = true;
			Destroy(swipeHand.gameObject);
			Destroy(pressHand.gameObject);
			Destroy(attackGlow.gameObject);

			if (!ShownCharacterSwap && EnvironmentManager.CurrentEnvironment == EnvironmentManager.EnvironmentType.Volcano) {
				StartCoroutine(doCharacterSwapTutorial());
			} else if (ShownCharacterSwap && ShownStun) {
				Destroy(pointHand.gameObject);
				Destroy(tutorialText.gameObject);
				Destroy(tutorialTextBacking.gameObject);
			} else {
				fadeOutHands();
			}
		}
	}
	void OnDestroy()
	{
		if (unregister) {
			BSwipeManager.HandleSwipeStarted -= onSwipeStart;
			BSwipeManager.HandleSwipeEnded -= onSwipeEnd;
			BWorldMapDragger.HandleEnteredNewRoom -= onEnteredNewRoom;
		}
		Instance = null;
	}
	void Update()
	{
		if (BattleManager.InBattle && BattleManager.PlayerPartyLead != null) {
			if (!ShownStun && BattleManager.PlayerPartyLead.IsStunned) {
				ShownStun = true;
				StartCoroutine(doStunTutorial());
			}
			if (!ShownChargeAttackTutorial && PlayerManager.Instance.GetAcquiredStatForGirl(PlayerManager.GetActiveGirl()).AuraUnlocked) {
				ShownChargeAttackTutorial = true;
				StartCoroutine(doChargeAttackTutorial());
			}
		}
	}

	void onSwipeStart(Vector2 _start)
	{
		if (!useTouchCallbacks) {
			return;
		}
	}
	void onSwipeEnd(Vector2 _end)
	{
		if (!useTouchCallbacks) {
			return;
		}
	}
	void onEnteredNewRoom(RoomNode _current, bool _firstTime)
	{
		if (!useTouchCallbacks) {
			return;
		}
		if (ShouldShowSwipeHand) {
			decrementRooms();
		}
		if (bringSwipeInRoutine != null) {
			StopCoroutine(bringSwipeInRoutine);
		}
		stopSwipeGesture();
		if (ShouldShowSwipeHand) {
			startSwipeGesture();
		}
	}
	IEnumerator waitForPlayerToRead()
	{
		Time.timeScale = 0;
		ForceDisableAttack = ForceDisableBlock = true;
		yield return new WaitForSecondsRealtime(preventSkipBufferTime);
		Time.timeScale = 1;
	}

	IEnumerator pointToStamina(string _text, float _feelzDelay = 0.02f, System.Action _postDelayAction = null)
	{
		ForceDisableAttack = ForceDisableBlock = true;
		PauseStaminaFill = true;
		yield return new WaitForSeconds(_feelzDelay);
		if (_postDelayAction != null) {
			_postDelayAction();
		}
		setTutorialTextAndFadeIn (_text);
		fadeGestureInAtLocation(pointHand, BPlayerInput.Instance.StaminaBarPosition());
		startPointOnImage(pointHand);
		yield return StartCoroutine(waitForPlayerToRead());
		Time.timeScale = 0;
		InputPressed = false;
		while(!InputPressed) {
			yield return null;
		}
		stopPointOnImage(pointHand);
		fadeGestureOut(pointHand);
		fadeOutTutorialText();
		Time.timeScale = 1;
		PauseStaminaFill = false;
		ForceDisableAttack = ForceDisableBlock = false;
	}

	IEnumerator goThroughTutorial()
	{
		BPlayerInput.Instance.EnableBlockButton();
		BPlayerInput.Instance.EnableAttackButton();
		BSwipeManager.Instance.enabled = false;
		//The thought here is that we can give the player a "mostly" empty screen to just navigate in
		BPlayerInput.Instance.SwitchToBattleUI();
		BPlayerInput.Instance.SwitchToSuperUI();
		ForceDisableAttack = ForceDisableBlock = true;
		while (BWorldMapDragger.Instance.CurrentRoom == null || PlayerManager.GetActiveGirl() == PPG.None) {yield return null;}
		yield return new WaitForSeconds(2f);//feelz wait
		DialogueBuilder builder = new DialogueBuilder ();
		builder.AddAsCurrentGirl ("うっ！頭がいたい・・・・")
			.Add ("ここはどこ？森の中であのクレイジーモンキーにやられたのかしら？")
			.Add ("とりあえず仲間をさがしたほうが良さそうね。");
		if (PlayerManager.GetActiveGirl() == PPG.Blossom) {
			builder.dialogueStartVO = blossomUghVO;
		} else if (PlayerManager.GetActiveGirl() == PPG.Bubbles) {
			builder.dialogueStartVO = bubblesUghVO;
		} else if (PlayerManager.GetActiveGirl() == PPG.Buttercup) {
			builder.dialogueStartVO = buttercupUghVO;
		}
		builder.Go ();
		while (!BDialogOverlay.Instance.gameObject.activeInHierarchy) {
			yield return null;
		}
		while (BDialogOverlay.Instance.gameObject.activeInHierarchy) {
			yield return null;
		}
		yield return new WaitForSeconds(1f);//feelz wait
		startSwipeGesture ();
		BSwipeManager.Instance.enabled = true;
		useTouchCallbacks = true;
		//wait for the first enemy
		while (ShouldShowSwipeHand) {
			yield return null;
		}
		// ============================= Attack ====================/
		//wait a bit before bringing in the battle ui
		while (BattleManager.CurrentGameState != GameState.PreBattle) {
			yield return null;
		}
		AllowedToPause = true;
		//but since the battle hud is sorta al使えるまで (from above), what we realy do is "un super move" it
		BPlayerInput.Instance.SwitchFromSuperUI();
		var enemyAI = BattleManager.EnemyPartyLead.GetComponent<BTutorialAI>();
		enemyAI.PauseAI = true;
		while (!BattleManager.InBattle) {
			yield return null;
		}
		yield return new WaitForSeconds(0.5f);//arbitrary wait
		//Let player finally attack
		ForceDisableAttack = false;
		playerCompletedFirstAttack = false;
		BattleManager.PlayerPartyLead.HandleSuccessfulAttack += onPlayerCompletedAttack;
		//animate press and hold hand
		const string attackText = "バトル開始！画面の右側をタップして敵を攻撃して！タップ！タップ！";
		setTutorialTextAndFadeIn (attackText);
		startBlinkOnImage(attackGlow);
		while (!playerCompletedFirstAttack) {
			yield return null;
		}
		stopBlinkOnImage(attackGlow);
		//Show that the attack takes stamina
		const string attackTakesStaminaText = "攻撃をするとスタミナが減少するよ！無くならないように気をつけよう！";
		yield return StartCoroutine(pointToStamina(attackTakesStaminaText));
		ForceDisableBlock = true;
		//remove press and hold animation
		fadeOutTutorialText();
		//let the player attack a few more times for the feelz
		while (shouldGivePlayerFreeAttacks) {
			yield return null;
		}
		ForceDisableAttack = true;
		BattleManager.PlayerPartyLead.HandleSuccessfulAttack -= onPlayerCompletedAttack;

		// ============================= Regular Block ====================/
BlockTutorial:
		InBlockTutorial = true;
		yield return new WaitForSeconds(1.5f);//some feelz
		BattleManager.EnemyPartyLead.OnAttackPressed();
		const float timeBeforeBlock = DesignValues.Tutorial.TelegraphTime / 2f;
		yield return new WaitForSeconds(timeBeforeBlock);
		//half way after the enemy attack, allow the player to block
		ForceDisableBlock = false;
		//animate in hold block
		startBlinkOnImage(blockGlow);
		const string blockText = "敵が攻撃してくるので画面の左側をタップし、そのまま押し続けてガードしよう。";
		setTutorialTextAndFadeIn(blockText);
		Time.timeScale = 0f;
		//yield return new WaitForSecondsRealtime(preventSkipBufferTime);
		BlockInputIsDown = false;
		while(!BlockInputIsDown) {yield return null;}
		Time.timeScale = 1f;
		var time = timeBeforeBlock;
		while (time <= DesignValues.Tutorial.TelegraphTime) {
			yield return null;
			time += Time.deltaTime;
		}
		stopBlinkOnImage(blockGlow);
		fadeOutTutorialText();
		var dmg = BattleManager.PlayerPartyLead.MaxHealth - BattleManager.PlayerPartyLead.Health;
		if (dmg > 0) {
			yield return new WaitForSecondsRealtime(fadeTextTime + 1.5f);
			BattleManager.PlayerPartyLead.UpdateHealthAndUpdateUI(dmg);
			goto BlockTutorial;
		}
		ForceKeepBlockButtonHeldDown = true;
		Time.timeScale = 0f;
		//Show that the block takes stamina
		const string blockTakesStaminaText = "ガードし続けるとスタミナの回復が遅くなるので、大丈夫なときはガードを解こう。";
		//would normally call point to stamina here, but need super specific OoO
		setTutorialTextAndFadeIn (blockTakesStaminaText);
		fadeGestureInAtLocation(pointHand, BPlayerInput.Instance.StaminaBarPosition());
		startPointOnImage(pointHand);
		yield return new WaitForSecondsRealtime(3.5f);
		stopPointOnImage(pointHand);
		fadeGestureOut(pointHand);
		fadeOutTutorialText();
		Time.timeScale = 1;
		PauseStaminaFill = false;
		ForceKeepBlockButtonHeldDown = false;
		InBlockTutorial = false;

		// ============================= Perfect Block ====================/
		ForceDisableBlock = true;
		yield return StartCoroutine(doPerfectBlockTutorial());
		//continue battle as regular
		Time.timeScale = 1f;
		enemyAI.PauseAI = false;
		yield return new WaitForSeconds(0.5f);//more feelz
		ForceDisableAttack = ForceDisableBlock = false;
		InTutorial = false;
		SaveLoadManager.Save();

		BSwipeManager.Instance.enabled = false;
		while (BattleManager.CurrentGameState != GameState.Navigation) {
			yield return null;
		}
		yield return new WaitForSeconds(0.1f);
		BSwipeManager.Instance.enabled = true;
		stopSwipeGesture();
		startSwipeGesture ();
		roomsForHand = 1;
		//wait for the first enemy
		while (roomsForHand > 0) {
			yield return null;
		}
	}
	IEnumerator pointToCharacterSelect()
	{
		ForceDisableAttack = true;
		yield return new WaitForSeconds(1.5f);
		Time.timeScale = 0;
		const string text = "左上のキャラアイコンをタップするとガールズを変更できるぞ。試してみよう！";
		setTutorialTextAndFadeIn (text);
		fadeGestureInAtLocation(pointHand, BPlayerInput.Instance.CharacterSwapPosition() );
		startPointOnImage(pointHand, true);

		while(FindObjectOfType<BGirlSelectOverlay>() == null) {
			yield return null;
		}
		stopPointOnImage(pointHand);
		fadeGestureOut(pointHand);
		fadeOutTutorialText();
		Time.timeScale = 1;
		ForceDisableAttack = false;
	}

	IEnumerator doCharacterSwapTutorial()
	{
		pointHand.transform.rotation = Quaternion.identity;
		yield return StartCoroutine(pointToCharacterSelect());
		ShownCharacterSwap = true;
		SaveLoadManager.Save();
	}
	IEnumerator doStunTutorial()
	{
		yield return new WaitForSeconds(0.15f);
		const string text = "なんてこった、スタミナが無くなってしまった！こうなるとスタミナゲージが回復するまで動くことができないぞ！";
		yield return StartCoroutine(pointToStamina(text, 0f, ()=>{
			BattleManager.PlayerPartyLead.FakeZeroOutStamina();
		}));
		SaveLoadManager.Save();
	}
	IEnumerator doChargeAttackTutorial()
	{
		var enemy = BattleManager.EnemyPartyLead;
		var player = BattleManager.PlayerPartyLead;
		BBaseFriendlyMonster fmScript = null;
		if ( BPlayerAgent.FmInstance != null) {
			fmScript = BPlayerAgent.FmInstance.GetComponent<BBaseFriendlyMonster>();
		}
		if (fmScript != null) {
			fmScript.SetPaused(true);
		}
		ForceDisableBlock = true;
		enemy.SetAIPause(true);
ChargeStart:
		ForceDisableAttack = false;
		const string text = "チャージアタックを解除しました。充電を開始するには、攻撃をタップして押し続けます。";
		setTutorialTextAndFadeIn(text);
		while (!player.IsAttacking) {
			yield return null;
		}
		yield return new WaitForSeconds(0.2f);
		if (!player.IsCurrentlyCharging) {
			ForceDisableAttack = true;
			fadeOutTutorialText();
			yield return new WaitForSeconds(1f);
			enemy.UpdateHealthAndUpdateUI(float.MaxValue);
			yield return new WaitForSeconds(1f);
			goto ChargeStart;
		}
		yield return new WaitForSeconds(0.1f);
		Time.timeScale = 0f;
		const string keepHolding = "キャラクターが点滅を開始したら、巨大なダメージのために敵を倒す！あまりにも早くリリースすれば、あなたはまだダメージを受けます。";
		setTutorialTextAndFadeIn(keepHolding);
		yield return new WaitForSecondsRealtime(2f);
		Time.timeScale = 1f;
		while (player.IsAttacking) {
			yield return null;
		}
		ForceDisableBlock = false;
		fadeOutTutorialText();
		enemy.SetAIPause(false);
		if (fmScript != null) {
			fmScript.SetPaused(false);
		}
		SaveLoadManager.Save();
	}
	IEnumerator doCounterAttackTutorial()
	{
		BBaseFriendlyMonster fmScript = null;
		if ( BPlayerAgent.FmInstance != null) {
			fmScript = BPlayerAgent.FmInstance.GetComponent<BBaseFriendlyMonster>();
		}
		if (fmScript != null) {
			fmScript.SetPaused(true);
		}
		ShownCounterAttackTutorial = true;
		yield return null;
		const string text = "反撃時間！パーフェクトブロックの直後に攻撃し、重大なダメージを与えます。";
		setTutorialTextAndFadeIn(text);
		Time.timeScale = 0f;
		yield return new WaitForSecondsRealtime(preventSkipBufferTime);
		InputPressed = false;
		while (!InputPressed) {
			yield return null;
		}
		fadeOutTutorialText();
		Time.timeScale = 1f;
		SaveLoadManager.Save();
		if (fmScript != null) {
			fmScript.SetPaused(true);
		}
	}
	IEnumerator doPerfectBlockTutorial()
	{
		BPlayerInput.Instance.OnBlockReleased();
		ForceDisableAttack = ForceDisableBlock = true;
		yield return new WaitForSeconds(1.5f);
		BattleManager.EnemyPartyLead.OnAttackPressed();
		const float timeBeforeBlock = DesignValues.Tutorial.TelegraphTime - (DesignValues.PlayerBattleFeel.PerfectBlockTimingWindow - 0.02f);
		yield return new WaitForSeconds(timeBeforeBlock);
		ForceDisableBlock = false;
		AllowPerfectBlockInTutorial = true;
		//animate in hold block
		const string text = "敵が攻撃するのと同じように、画面の左側をタップしてパーフェクトブロックを実行し、すべての体力とスタミナのダメージを取り消します。";
		setTutorialTextAndFadeIn(text);
		startBlinkOnImage(blockGlow);
		ForceKeepBlockButtonHeldDown = true;
		var time = timeBeforeBlock;
		while (time <= DesignValues.Tutorial.TelegraphTime) {
			//here we don't let the user continue unless they hold the block button down. 
			Time.timeScale = BlockInputIsDown ? 1f : 0f;
			yield return null;
			time += Time.deltaTime;
		}
		ForceKeepBlockButtonHeldDown = false;
		stopBlinkOnImage(blockGlow);
		fadeOutTutorialText();
	}
	//========= SWIPE HELPERS ==============//
	void startSwipeGesture ()
	{
		var pos = BWorldMapDragger.Instance.CurrentRoom.Handle.position + new Vector3(0.2f * Screen.width, -swipeHand.GetBounds().size.y / 2f, 0f);
		pos.z = 0f;

		swipeHand.transform.position = pos;
		gestureMotionTween = DOTween.Sequence ();
		gestureMotionTween.Append(swipeHand.transform.DOScale(Vector3.one, 0));
		gestureMotionTween.Append(swipeHand.DOFade(1, 0.25f));
		gestureMotionTween.Append(swipeHand.transform.DOScale (Vector3.one * 0.9f, 0.3f).SetDelay(0.25f));
		gestureMotionTween.Append (swipeHand.transform.DOMoveX (pos.x - (Screen.width * 0.25f), 0.75f).SetDelay (0.2f).SetEase (Ease.InOutQuad));
		gestureMotionTween.Append(swipeHand.transform.DOScale (Vector3.one, 0.3f));
		gestureMotionTween.Append(swipeHand.DOFade(0, 0.5f));
		gestureMotionTween.SetLoops (-1, LoopType.Restart);
	}
	void stopSwipeGesture ()
	{
		swipeHand.DOFade(0, 0.25f);
		if (gestureMotionTween != null) {
			gestureMotionTween.Kill();
			gestureMotionTween = null;
		}
	}
	//========= BLINK HELPERS =====================//
	void startBlinkOnImage(Graphic _image)
	{
		_image.DOFade(0, 1).Kill(true);
		_image.DOFade(0.5f, 0.5f)
			.SetEase (Ease.InOutQuad)
			.SetUpdate (true)
			.SetLoops (-1, LoopType.Yoyo);
	}
	void stopBlinkOnImage(Graphic _image)
	{
		_image.DOKill(true);
		_image.DOFade(0, 1).Kill(true);
	}
	//========= PRESS'N'HOLD HELPERS ==============//
	void startPointOnImage(SpriteRenderer _image, bool _doY = false)
	{
		var bounds = _image.bounds;
		Tweener t = null;
		if (_doY) {
			t = _image.transform.DOMoveY (bounds.extents.y, 0.5f);
		} else {
			t = _image.transform.DOMoveX (bounds.extents.x, 0.5f);
		}
		t.SetRelative(true)
			.SetEase (Ease.InOutQuad)
			.SetUpdate (true)
			.SetLoops (-1, LoopType.Yoyo);
	}
	void stopPointOnImage(SpriteRenderer _image)
	{
		_image.DOKill(true);
	}
	void setTutorialTextAndFadeIn (string text)
	{
		tutorialText.text = text;
		tutorialText.color = new Color (1f, 1f, 1f, 0f);
		tutorialText.DOFade (1f, fadeTextTime).SetEase (Ease.Linear).SetUpdate(true);
		tutorialTextBacking.DOFade(0.376f, fadeTextTime).SetEase(Ease.Linear).SetUpdate(true);
	}
	void fadeOutTutorialText()
	{
		tutorialText.DOFade (0f, fadeTextTime).SetEase (Ease.Linear).SetUpdate(true);
		tutorialTextBacking.DOFade(0f, fadeTextTime).SetEase(Ease.Linear).SetUpdate(true);
	}
	void onPlayerCompletedAttack()
	{
		decrementFreeAttacks();
		playerCompletedFirstAttack |= freeAttacks == 3;
	}
	void fadeGestureInAtLocation(SpriteRenderer _gesture, Vector3 _position)
	{
		tweenSpriteAlpha (_gesture, 1f);
		_gesture.transform.position = _position;
	}
	void fadeGestureOut(SpriteRenderer _gesture)
	{
		tweenSpriteAlpha (_gesture, 0f);
	}

	void tweenSpriteAlpha (SpriteRenderer _gesture, float target)
	{
		const float tweenFadeSpeed = 4f;
		var doesContain = false;
		if (fadeTweenMapping.ContainsKey(_gesture)) {
			fadeTweenMapping[_gesture].Kill();
			doesContain = true;
		}
		var time = Mathf.Abs (target - _gesture.color.a) / tweenFadeSpeed;
		var tween = _gesture.DOFade (target, time).SetEase (Ease.Linear).SetUpdate(true);
		if (doesContain) {
			fadeTweenMapping [_gesture] = tween;
		} else {
			fadeTweenMapping.Add (_gesture, tween);
		}
	}
	public int RoomsForHand{get {return roomsForHand;}}
	public bool ShouldShowSwipeHand{get{return InTutorial && roomsForHand > 0;}}
	void decrementRooms() {--roomsForHand;}
	bool shouldGivePlayerFreeAttacks{get{return freeAttacks > 0;}}
	void decrementFreeAttacks() {--freeAttacks;}
}
