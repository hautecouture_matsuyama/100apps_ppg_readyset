﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine;
using Spine.Unity;
using DG.Tweening;

public class BEffectsPool : MonoBehaviour 
{
	public static GameObject CreateNormalHitEffectAtPos(Vector3 _position)
	{
		var effect = instance.normalHitPool.Acquire();
		effect.transform.position = _position;
		var tkSprite = effect.GetComponent<tk2dSprite>();
		if(Camera.main.WorldToScreenPoint(effect.transform.position).x < (Camera.main.pixelWidth / 2)) {
			tkSprite.FlipX = true;
		} else {
			tkSprite.FlipX = false;
		}
		var c = tkSprite.color;
		c.a = 1;
		tkSprite.color = c;
		var tkAnim = effect.GetComponent<tk2dSpriteAnimator>();
		tkAnim.Play("hitEffect" + (Random.Range(0, 3) + 1).ToString());
		tkSprite.DOFade(0, 0.2f).SetEase(Ease.Linear).OnComplete(() => instance.normalHitPool.Release(effect));
		return effect;
	}

	public static GameObject CreateAuraHitEffectAtPos(Vector3 _position)
	{
		var effect = instance.auraHitPool.Acquire();
		effect.transform.position = _position;
		var tkSprite = effect.GetComponent<tk2dSprite>();
		var c = tkSprite.color;
		c.a = 0.8f;
		tkSprite.color = c;
		tkSprite.DOFade(0, 0.25f).SetEase(Ease.Linear).OnComplete(() => instance.auraHitPool.Release(effect));
		return effect;
	}
		
	public static GameObject CreateSuperHitEffectAtPos(Vector3 _position)
	{
		var effect = instance.superHitPool.Acquire();
		effect.transform.position = _position;
		var animator = effect.GetComponent<tk2dSpriteAnimator>();
		animator.Play();
		animator.AnimationCompleted = (a, b) => {
			instance.superHitPool.Release(effect);
			animator.AnimationCompleted = null;
		};
		return effect;
	}
	public static GameObject CreateBlockEffectAtPos(Vector3 _position, bool _playerSide, ref ObjectPool _pool)
	{
		var effect = instance.blockPool.Acquire();
		_pool = instance.blockPool;
		effect.transform.position = _position;
		if(_playerSide) {
			effect.transform.DOScaleX(-1, 0).Kill(true);
		} else {
			effect.transform.DOScaleX(1, 0).Kill(true);
		}
		var animator = effect.GetComponent<SkeletonAnimation>();
		animator.AnimationState.SetAnimation(0, "Regular_Start", false);
		animator.AnimationState.AddAnimation(0, "Regular_Hold", true, 0);
		return effect;
	}
	public static GameObject CreateBlockEffectAtPos(Vector3 _position, BAgent _source)
	{
		ObjectPool pool = null;
		var fx = CreateBlockEffectAtPos(_position,_source is BPlayerAgent, ref pool);
		_source.ShieldPool = pool;
		return fx;
	}

	public static GameObject CreatePerfectBlockEffectAtPos(Vector3 _position)
	{
		var effect = instance.blockPool.Acquire();
		effect.transform.position = _position;
		if(Camera.main.WorldToScreenPoint(effect.transform.position).x < (Camera.main.pixelWidth / 2)) {
			effect.transform.DOScaleX(-1, 0).Kill(true);
		} else {
			effect.transform.DOScaleX(1, 0).Kill(true);
		}
		var animator = effect.GetComponent<SkeletonAnimation>();
		animator.AnimationState.SetAnimation(0, "Perfect_Start", false).TimeScale = 2f;
		animator.AnimationState.AddAnimation(0, "Perfect_TakeHit", false, 0).Complete += delegate {
			instance.blockPool.Release(effect);
		};
		return effect;
	}

	public static GameObject CreateStunEffectAtPos(Vector3 _position, float _uniformScale, float _stunTime)
	{
		var effect = instance.stunPool.Acquire();
		effect.transform.position = _position;
		effect.transform.DOScale(_uniformScale, 0).Kill(true);
		var animator = effect.GetComponent<SkeletonAnimation>();
		var startAnim = animator.AnimationState.SetAnimation(0, "StunStart", false);
		animator.AnimationState.AddAnimation(0, "StunLoop", true, 0);
		animator.AnimationState.AddAnimation(0, "StunEnd", false, _stunTime - (startAnim.animationEnd - startAnim.animationStart)).Complete += delegate {
			instance.stunPool.Release(effect);
		};
		animator.AnimationState.TimeScale = 1f;
		return effect;
	}

	public static void ReleaseStunEffect(GameObject _effect)
	{
		instance.stunPool.Release(_effect);
	}

	public static GameObject CreatePoofEffectAtPos(Vector3 _position)
	{
		var effect = instance.poofPool.Acquire();
		effect.transform.position = _position;
		var animator = effect.GetComponent<tk2dSpriteAnimator>();
		animator.Play();
		animator.AnimationCompleted = (a, b) => {
			instance.poofPool.Release(effect);
			animator.AnimationCompleted = null;
		};
		return effect;
	}

	public static GameObject CreateAuraPopEffectAtPos(Vector3 _position, PPG _currentGirl)
	{
		var effect = instance.auraPopPool.Acquire();
		effect.transform.position = _position;
		var animator = effect.GetComponent<tk2dSpriteAnimator>();
		animator.Play("AuraPop" + _currentGirl.ToString());
		animator.AnimationCompleted = (a, b) => {
			instance.auraPopPool.Release(effect);
			animator.AnimationCompleted = null;
		};
		return effect;
	}

	public static GameObject CreateFlyerWindEffectAtPos(Vector3 _position)
	{
		var effect = instance.flyerWindPool.Acquire();
		effect.transform.position = _position;
		effect.GetComponent<BStaticEffectReleaser>().PoolHandle = instance.flyerWindPool;
		effect.transform.DOScaleY(0.9f, 0.1f).SetEase(Ease.InOutQuad).SetLoops(-1, LoopType.Yoyo);
		return effect;
	}

	public static GameObject CreateHealEffectAtPos(Vector3 _position)
	{
		var effect = instance.healPool.Acquire();
		effect.transform.position = _position;
		var animator = effect.GetComponentInChildren<SkeletonAnimation>();
		animator.AnimationState.AddAnimation(0, "HealLoop", false, 0);
		animator.AnimationState.AddAnimation(0, "HealEnd", false, 0).Complete += delegate {
			instance.healPool.Release(effect);
		};
		return effect;
	}

	public static GameObject CreateFightText()
	{
		var effect = instance.fightTextPool.Acquire();
		effect.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width * 0.48f, Screen.height * 0.37f, 10));
		var animator = effect.GetComponentInChildren<SkeletonAnimation>();
		animator.AnimationState.SetAnimation(0, "animation", false).Complete += delegate {
			instance.fightTextPool.Release(effect);
		};
		return effect;
	}

	public static GameObject CreateHealthChangeEffectAtPos(float _change, Vector3 _position, bool _critical = false)
	{
		var effect = instance.healthChangePool.Acquire();
		var script = effect.GetComponent<BHealthChangePopup>();
		script.PoolHandle = instance.healthChangePool;
		script.Show(_change, _position, _critical);
		return effect;
	}

	public static ParticleSystem CreateCollectEffectAtPos(Vector3 _position)
	{
		return instance.createParticleEffectAtPos(_position, instance.collectPool);
	}

	public static ParticleSystem CreateChargingEffectAtPos(Vector3 _position)
	{
		return instance.createParticleEffectAtPos(_position, instance.chargingPool);
	}

	ParticleSystem createParticleEffectAtPos(Vector3 _position, ObjectPool _pool)
	{
		var effect = _pool.Acquire();
		effect.transform.position = _position;
		effect.GetComponent<CFX_AutoDestructShuriken>().PoolHandle = _pool;
		return effect.GetComponent<ParticleSystem>();
	}


	static BEffectsPool instance;

	[SerializeField] GameObject normalHitEffectPrefab;
	[SerializeField] GameObject auraHitEffectPrefab;
	[SerializeField] GameObject superHitEffectPrefab;
	[SerializeField] GameObject blockEffectPrefab;
	[SerializeField] GameObject stunEffectPrefab;
	[SerializeField] GameObject poofEffectPrefab;
	[SerializeField] GameObject chargingEffectPrefab;
	[SerializeField] GameObject auraPopEffectPrefab;
	[SerializeField] GameObject flyerWindEffectPrefab;
	[SerializeField] GameObject healEffectPrefab;
	[SerializeField] GameObject collectEffectPrefab;
	[SerializeField] GameObject fightTextPrefab;
	[SerializeField] GameObject healthChangePrefab;

	ObjectPool normalHitPool;
	ObjectPool auraHitPool;
	ObjectPool superHitPool;
	ObjectPool blockPool;
	ObjectPool stunPool;
	ObjectPool poofPool;
	ObjectPool chargingPool;
	ObjectPool auraPopPool;
	ObjectPool flyerWindPool;
	ObjectPool healPool;
	ObjectPool collectPool;
	ObjectPool fightTextPool;
	ObjectPool healthChangePool;

	void Awake()
	{
		instance = this;
		normalHitPool = new ObjectPool(normalHitEffectPrefab, (self, mimic) => {
			self.transform.localScale = mimic.transform.localScale;
		});
		normalHitPool.Fill(3);

		auraHitPool = new ObjectPool(auraHitEffectPrefab, (self, mimic) => {
			self.transform.localScale = mimic.transform.localScale;
		});
		auraHitPool.Fill(2);

		superHitPool = new ObjectPool(superHitEffectPrefab, (self, mimic) => {
			self.transform.localScale = mimic.transform.localScale;
		});
		superHitPool.Fill(5);

		blockPool = new ObjectPool(blockEffectPrefab, (self, mimic) => {
			self.transform.localScale = mimic.transform.localScale;
		});
		blockPool.Fill(5);

		stunPool = new ObjectPool(stunEffectPrefab, (self, mimic) => {
			self.transform.localScale = mimic.transform.localScale;
		});
		stunPool.Fill(3);

		poofPool = new ObjectPool(poofEffectPrefab, (self, mimic) => {
			self.transform.localScale = mimic.transform.localScale;
		});
		poofPool.Fill(2);

		chargingPool = new ObjectPool(chargingEffectPrefab, (self, mimic) => {
			self.transform.localScale = mimic.transform.localScale;
		});
		chargingPool.Fill(2);

		auraPopPool = new ObjectPool(auraPopEffectPrefab, (self, mimic) => {
			self.transform.localScale = mimic.transform.localScale;
		});
		auraPopPool.Fill(2);

		flyerWindPool = new ObjectPool(flyerWindEffectPrefab, (self, mimic) => {
			self.transform.localScale = mimic.transform.localScale;
			self.transform.rotation = Quaternion.identity;
		});
		flyerWindPool.Fill(2);

		healPool = new ObjectPool(healEffectPrefab, (self, mimic) => {
		});
		healPool.Fill(2);

		collectPool = new ObjectPool(collectEffectPrefab, (self, mimic) => {
		});
		collectPool.Fill(10);

		fightTextPool = new ObjectPool(fightTextPrefab, (self, mimic) => {
		});
		fightTextPool.Fill(1);

		healthChangePool = new ObjectPool(healthChangePrefab, (self, mimic) => {
			self.transform.localScale = mimic.transform.localScale;
		});
		healthChangePool.Fill(2);
	}
	void OnDestroy()
	{
		instance = null;
	}
		
}
