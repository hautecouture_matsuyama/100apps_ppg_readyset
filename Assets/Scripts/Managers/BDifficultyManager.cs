﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

[System.Serializable]
public class EnemyWeight
{
	[SerializeField] float bruteWeight;
	[SerializeField] float flowerWeight;
	[SerializeField] float flyerWeight;
	[SerializeField] float bigBruteWeight;
	[SerializeField] float bigFlowerWeight;
	[SerializeField] float bigFlyerWeight;
	public float this[EnemyType _t]
	{
		get{
			switch (_t) {
			case EnemyType.Brute:
				return bruteWeight;
			case EnemyType.Flower:
				return flowerWeight;
			case EnemyType.Flyer:
				return flyerWeight;
			case EnemyType.BigBrute:
				return bigBruteWeight;
			case EnemyType.BigFlower:
				return bigFlowerWeight;
			case EnemyType.BigFlyer:
				return bigFlyerWeight;
			default:
				throw new System.ArgumentOutOfRangeException ();
			}
		}
	}
}

public class BDifficultyManager : MonoBehaviour
{
	public static BDifficultyManager Instance{get; private set;}

	[SerializeField] EnemyWeight[] difficultyWeights;
	[SerializeField] AnimationCurve difficultyCurve;
	[SerializeField] float difficultyStep;

	Dictionary<EnemyType, float> weights;
	static float difficultyWeightIndex;
	static float difficultyCurveValue;
	float maxValueInDifficultyCurve;
	void Awake()
	{
		Instance = this;
		maxValueInDifficultyCurve = difficultyCurve.Evaluate(1f);
	}
	void Start()
	{
		weights = new Dictionary<EnemyType, float>()
		{
			{EnemyType.Brute, 0f},
			{EnemyType.Flower, 0f},
			{EnemyType.Flyer, 0f},
			{EnemyType.BigBrute, 0f},
			{EnemyType.BigFlower, 0f},
			{EnemyType.BigFlyer, 0f}
		};
		updateWeightForDifficulty();
	}
	void OnDestroy()
	{
		Instance = null;
	}
	#region Interface
	public void IncrementDifficulty()
	{
		difficultyCurveValue += (difficultyStep * BDebugOverlay.ScoreModifier);
		difficultyCurveValue = Mathf.Clamp01(difficultyCurveValue);

		difficultyWeightIndex += DesignValues.WorldGenerator.DifficultyWeightStep;
		difficultyWeightIndex = Mathf.Clamp(difficultyWeightIndex, 0, difficultyWeights.Length - 1);
		updateWeightForDifficulty();
	}
	public EnemyType EnemyToSpawn
	{
		get{
//			return EnemyType.BigFlyer;
			return Utilities.GetWeightedRandomItem(weights);
		}
	}
	public float DamageModifier
	{
		get {
			return (1f + difficultyCurve.Evaluate(difficultyCurveValue));
		}
	}
	public float HealthModifier
	{
		get {
			return (1f + difficultyCurve.Evaluate(difficultyCurveValue));
		}
	}
	public float StaminaModifier
	{
		get {
			return (1f + (difficultyCurve.Evaluate(difficultyCurveValue) / 2f));
		}
	}
	public float SpeedModifier
	{
		get {
			return (1f + difficultyCurve.Evaluate(difficultyCurveValue) / (2f * maxValueInDifficultyCurve));
		}
	}
	public void Reset()
	{
		difficultyWeightIndex = 0f;
		difficultyCurveValue = 0f;
	}
	#endregion

	#region Helpers
	void updateWeightForDifficulty()
	{
		var current = difficultyWeights[(int)difficultyWeightIndex];
		foreach(var e in Utilities.GetValues<EnemyType>()) {
			weights[e] = current[e];
		}
	}
	#endregion
}