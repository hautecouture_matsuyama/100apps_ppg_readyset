﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class BBaseOverlay : BBaseUI
{
	protected const float tweenTime = 0.3f;
	protected const float tweenDelayTime = 0.25f;
	const float tickDistanceFromCenter = 385f;
	const float degreePerFrame = 5f;

	public Image bg;
	public Transform container;

	protected bool actionPerformed;
	protected Tweener animationTween;
	protected Transform previousOverlay;
	protected bool isOffScreen;

	protected Color bgColor;
	protected Vector3 localScale;

	protected bool allowedToClick;
	bool canCallEscape;
	protected override void Awake()
	{
		base.Awake ();
		bgColor = bg.color;
		localScale = container.transform.localScale;
		allowedToClick = true;
	}
	protected virtual void OnEnable()
	{
		canCallEscape = true;
	}
	protected Tweener animateInScale()
	{
		var toColor = bgColor;
		var toScale = localScale;
		bg.color = Color.clear;
		bg.DOColor(toColor, tweenTime).SetUpdate(true);
		container.localScale = Vector3.zero;
		return container.DOScale(toScale, tweenTime).SetUpdate(true).SetEase(Ease.OutBack);
	}
	protected Tweener animateOutScale()
	{
		container.DOScale(Vector3.zero, tweenTime).SetUpdate(true).SetEase(Ease.InBack);
		return  bg.DOColor(Color.clear, tweenTime).SetUpdate(true).SetDelay(tweenTime / 2).SetEase(Ease.Linear);
	}
	protected void animateOutScaleInstant()
	{
		container.DOScale(Vector3.zero, tweenTime).Kill(true);
		bg.DOColor(Color.clear, tweenTime).Kill(true);
	}

	public void GoToTitle()
	{
		goToScene("Title");
	}
	public void GoToGameplay()
	{
		goToScene("Gameplay");
	}
	public void GoToUpgrade()
	{
		goToScene("Upgrade");
	}
	public void GoToCredits()
	{
		goToScene("Credits");
	}
	protected void goToScene(string _scene)
	{
		if (allowedToClick) {
			allowedToClick = false;
			BEmptySceneDebug.GoToScene(_scene);
			//BScreenFade.FadeToScene (_scene);
		}
	}
	protected void setAllowedToClickTrue()
	{
		allowedToClick = true;
	}

	#region Globe animation
	const float imageOffset = 70;
	protected List<RectTransform> ticks;
	protected void createAllTicks(Transform _initialTick)
	{
		const int degreePerTick = 10;
		ticks = new List<RectTransform>();
		for (int counter = 0; counter <= 360 - degreePerTick; counter += degreePerTick) {
			var rect = _initialTick as RectTransform;
			if (counter > 0) {
				var copy = Instantiate (_initialTick.gameObject, _initialTick.parent);
				rect = copy.GetComponent<RectTransform> ();
			}
			ticks.Add(rect);
			rect.name = string.Format("{0} degrees", counter);
			rect.localRotation = Quaternion.Euler (Vector3.back * (counter - imageOffset));
			rect.localPosition = tickDistanceFromCenter * (rect.localRotation * Vector3.up);
		}
		var parent = _initialTick.parent;
		for(int i = 0; i < ticks.Count; ++i) {
			ticks[i].SetParent(parent, true);
			ticks[i].gameObject.SetActive(false);
		}
	}
	protected void setGlobalProgressMap (Transform _globe, Transform _initialFlag, GameObject _blossom, GameObject _bubbles, GameObject _buttercup, TextMeshProUGUI _loopText, GameObject _loopPfx, float _animDelay)
	{
		var activeGirl = setActiveGirl (_blossom, _bubbles, _buttercup).transform;
		StartCoroutine(animateToCurrentProgress(activeGirl, _globe, _loopText, _loopPfx, _animDelay));
		createFlags(_initialFlag);
	}
	IEnumerator animateToCurrentProgress (Transform _activeGirl, Transform _globe, TextMeshProUGUI _loopText, GameObject _loopPfx, float _animDelay)
	{
		var startingPos = BGameplayInit.StartingProgressValue;
		var targetDegree = PlayerManager.GlobalProgressUnclamped;
		//set base value
		_globe.rotation = Quaternion.Euler(Vector3.forward * (startingPos - imageOffset));
		var clamped = Utilities.ClampValueToCircle(startingPos);
		var percent = clamped / (float)Utilities.FullCircle;
		var secondTickIndex = (int) (percent * ticks.Count);
		for (var tickIndex = 0; tickIndex < secondTickIndex; ++tickIndex) {
			var currentTick = ticks[tickIndex];
			if (!currentTick.gameObject.activeSelf) {
				currentTick.gameObject.SetActive(true);
			}
		}
		var currentLoop = Utilities.RotationCount(startingPos);
		if (currentLoop > 0) {
			_loopText.gameObject.SetActive(true);
			_loopText.text = Utilities.RotationCount(startingPos) + "周達成！";
		} else {
			_loopText.gameObject.SetActive(false);
			_loopText.text = "";
		}
		yield return StartCoroutine(tweenGlobeFromTo(_globe, _loopText, _loopPfx, startingPos, targetDegree, _animDelay));
		for (int i = ticks.Count - 1; i >= 0; --i) {
			var currentTick = ticks[i];
			var tickWorldSpace = currentTick.parent.TransformPoint(currentTick.localPosition);
			var girlWorldSpace = _activeGirl.parent.TransformPoint(_activeGirl.localPosition);
			if (tickWorldSpace.x > girlWorldSpace.x) {
				currentTick.gameObject.SetActive(false);
				break;
			}
		}
	}
	IEnumerator tweenGlobeFromTo(Transform _globe, TextMeshProUGUI _loopText, GameObject _loopPfx, long _start, long _end, float _delay = 0f)
	{
		yield return new WaitForSecondsRealtime(_delay);
		bool reachedEnd = false;//weird wrap around issue with circles
		var counter = _start;
		bool wrappedAround = false;
		do {
			var clamped = Utilities.ClampValueToCircle(counter);
			yield return null;
			_globe.rotation = Quaternion.Euler(Vector3.forward * (counter - imageOffset));
			if (wrappedAround) {
				bool cleanedUpAtLeastOneTick = false;
				for (int i = 0; i < ticks.Count; ++i) {
					if (ticks[i].gameObject.activeSelf) {
						cleanedUpAtLeastOneTick = true;
						ticks[i].gameObject.SetActive(false);
						yield return new WaitForSecondsRealtime(0.025f);
					}
				}
				if (cleanedUpAtLeastOneTick) {
					var futureLoopCount = Utilities.RotationCount(counter);
					if (futureLoopCount > 0) {
						_loopPfx.SetActive(true);
						_loopText.gameObject.SetActive(true);
						_loopText.DOScale(1.2f, 0.3f).SetEase(Ease.InSine).OnComplete(() => {
							_loopText.text = futureLoopCount + "周達成！";
							_loopText.DOScale(1f, 0.2f).SetEase(Ease.OutSine).SetUpdate(true);
						}).SetUpdate(true);
						yield return new WaitForSecondsRealtime(1f);
					}
				}
			}
			var percent = clamped / (float)Utilities.FullCircle;
			var firstTickIndex = (int) (percent * ticks.Count);

			var nextCounter = (long)(counter + degreePerFrame);
			clamped = Utilities.ClampValueToCircle(nextCounter);
			percent = clamped / (float)Utilities.FullCircle;
			var secondTickIndex = (int) (percent * ticks.Count);
			if (secondTickIndex < firstTickIndex) {
				secondTickIndex = ticks.Count; //we wrapped around
				wrappedAround = true;
			} else {
				wrappedAround = false;
			}
			for (var tickIndex = firstTickIndex; tickIndex < secondTickIndex; ++tickIndex) {
				var currentTick = ticks[tickIndex];
				if (!currentTick.gameObject.activeSelf) {
					currentTick.gameObject.SetActive(true);
				}
			}
			if (reachedEnd) {
				break;
			}
			//last thing in while loop
			counter = (long)Mathf.Min((float)counter + degreePerFrame, (float)_end);
			if (counter == _end) {
				reachedEnd = true;
			}
		} while (counter <= _end);
	}
	static void createFlags (Transform _initialFlags)
	{
		var rect = _initialFlags;
		float percent;
		float offset = 1.20f;
		var nextNar = PlayerManager.Objectives.NextNarrativeObjectivePercent;
		if (nextNar != uint.MaxValue) {
			percent = (nextNar - imageOffset);
			if (nextNar == 120) {
				percent -= 5f;
				offset = 1.22f;
			} else if (nextNar == 240) {
				percent -= 10f;
				offset = 1.3f;
			} else if (nextNar == 360) {
				percent -= 7;
				offset = 1.19f;
			}
		} else {
			percent = (Utilities.ClampValueToCircle((long)PlayerManager.Objectives.NextGiftObjectivePercent) - imageOffset);
		}
		rect.localRotation = Quaternion.Euler (Vector3.back * percent);
		rect.localPosition = 355f * offset * (rect.localRotation * Vector3.up);
	}

	static GameObject setActiveGirl (GameObject _blossom, GameObject _bubbles, GameObject _buttercup)
	{
		_blossom.SetActive (false);
		_bubbles.SetActive (false);
		_buttercup.SetActive (false);
		switch (PlayerManager.GetActiveGirl ()) {
		case PPG.Blossom:
			_blossom.SetActive (true);
			_blossom.transform.DOLocalMoveY(25f, 0.75f).SetEase(Ease.InOutQuad).SetLoops(-1, LoopType.Yoyo).SetRelative().SetUpdate(true);
			return _blossom;
		case PPG.Bubbles:
			_bubbles.SetActive (true);
			_bubbles.transform.DOLocalMoveY(25f, 0.75f).SetEase(Ease.InOutQuad).SetLoops(-1, LoopType.Yoyo).SetRelative().SetUpdate(true);
			return _bubbles;
		case PPG.Buttercup:
			_buttercup.SetActive (true);
			_buttercup.transform.DOLocalMoveY(25f, 0.75f).SetEase(Ease.InOutQuad).SetLoops(-1, LoopType.Yoyo).SetRelative().SetUpdate(true);
			return _buttercup;
		}
		return null;
	}
	protected void cleanUpTicks()
	{
		for (int i = 0; i < ticks.Count; ++i) {
			ticks[i].gameObject.SetActive(false);
		}
	}
	#endregion

	protected virtual void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape) && canCallEscape) {
			canCallEscape = false;
			onEscape();
		}
	}
	protected virtual void onEscape()
	{
		throw new System.NotImplementedException(string.Format("{0} needs to implement OnEscape()", this.name));
	}
	protected void setCanCallEscapeTrue()
	{
		canCallEscape = true;
	}
}
