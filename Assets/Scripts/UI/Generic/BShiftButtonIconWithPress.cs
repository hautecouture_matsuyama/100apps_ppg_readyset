﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BShiftButtonIconWithPress : MonoBehaviour 
{
	//Allow other scripts (like BUpgradeConfirm) to disable this shifting if they need to just keep specific icons or texts off
	public bool disableIconShifting;
	public bool disableTextShifting;

	[SerializeField] GameObject unpressedIcon;
	[SerializeField] GameObject pressedIcon;
	[SerializeField] GameObject unpressedText;
	[SerializeField] GameObject pressedText;

	Button btn;
	void Awake()
	{
		btn = GetComponent<Button>();
	}

	public void ShiftIconDown()
	{
		if (btn.interactable) {
			if(!disableIconShifting) { 
				unpressedIcon.SetActive(false);
				pressedIcon.SetActive(true);
			}
			if(unpressedText != null && pressedText != null && !disableTextShifting) { 
				unpressedText.SetActive(false);
				pressedText.SetActive(true);
			}
		}
	}
	public void ShiftIconUp()
	{
		if (btn.interactable) {
			if(!disableIconShifting) { 
				unpressedIcon.SetActive(true);
				pressedIcon.SetActive(false);
			}
			if(unpressedText != null && pressedText != null && !disableTextShifting) {
				unpressedText.SetActive(true);
				pressedText.SetActive(false);
			}
		}
	}
}
