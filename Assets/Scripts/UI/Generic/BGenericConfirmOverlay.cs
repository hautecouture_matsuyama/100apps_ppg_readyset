﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class BGenericConfirmOverlay : BBaseOverlay
{

	static System.Collections.IEnumerator waitForCanvasLoad(ResourceRequest _req, string _header, string _body, string _confirmText, System.Action _onConfirm, System.Action _onCancel)
	{
		yield return _req;
		var prefab = (GameObject)_req.asset;
		var Instance = Instantiate(prefab).GetComponent<BGenericConfirmOverlay>();
		Instance.gameObject.SetActive(true);
		Instance.animateInScale();
		Instance.header.text = _header;
		Instance.body.text = _body;

		Instance.confirm.gameObject.SetActive(true);

		var textComp = Instance.confirm.GetComponentInChildren<Text>();
		if(textComp != null) {
			Instance.confirm.GetComponentInChildren<Text>().text = _confirmText;
		}
		Instance.confirm.onClick.AddListener(() => {
			if (_onConfirm != null) {
				_onConfirm();
			}
			Instance.animateOutScale().OnComplete(() => Destroy(Instance.gameObject));
			Instance.confirm.onClick.RemoveAllListeners();
			Instance.cancel.onClick.RemoveAllListeners();
		});
		Instance.cancel.onClick.AddListener(() => {
			if (_onCancel != null) {
				_onCancel();
			}
			Instance.animateOutScale().OnComplete(() => Destroy(Instance.gameObject));
			Instance.confirm.onClick.RemoveAllListeners();
			Instance.cancel.onClick.RemoveAllListeners();
		});
	}

	public static void SpawnConfirm(string _header, string _body, string _confirmText, System.Action _onConfirm = null, System.Action _onCancel = null)
	{
		var resource = Resources.LoadAsync<GameObject>(@"Prefabs/UI/GenericConfirmOverlay");
		BCoroutine.Instance.StartCoroutine(waitForCanvasLoad(resource, _header, _body, _confirmText, _onConfirm, _onCancel));
	}

	[SerializeField] TextMeshProUGUI header;
	[SerializeField] TextMeshProUGUI body;
	[SerializeField] Button confirm;
	[SerializeField] Button cancel;

	protected override void onEscape ()
	{
		if (cancel.onClick != null) {
			cancel.onClick.Invoke();
		}
	}
}
