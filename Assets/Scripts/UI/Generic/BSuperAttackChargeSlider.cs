﻿using UnityEngine;
using UnityEngine.UI;

public class BSuperAttackChargeSlider : MonoBehaviour
{
	#region Variables
	[SerializeField] protected Slider superMoveSlider;

	protected bool fullyCharged;
	#endregion


	#region Mono Callbacks
	protected virtual void Start()
	{
		
		if ( superAttackCharge == null ) {
			gameObject.SetActive(false);
			return;
		}
	}
	protected virtual void Update()
	{
		if (!fullyCharged) {
			setSliderToPercentComplete();
			checkIfFullyCharged();
		}
	}
	#endregion

	#region Helpers
	protected virtual RechargableRealTimeTimer superAttackCharge
	{
		get {return null;}
	}
	void setSliderToPercentComplete()
	{
		if (superAttackCharge != null) {
			superMoveSlider.value = superAttackCharge.PercentComlete;
		}
	}
	void checkIfFullyCharged()
	{
		if (superAttackCharge != null && Mathf.Approximately(1f, superAttackCharge.PercentComlete)) {
			fullyCharged = true;
		}
	}
	#endregion
}
