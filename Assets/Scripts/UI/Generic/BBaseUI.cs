﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public enum CFUISfx
{
	None,
	ButtonForward,
	ButtonBackward,
	ButtonBigSound
}

public class BBaseUI : MonoBehaviour
{
	static AudioClip buttonFwd;
	static AudioClip buttonBkwd;
	static AudioClip buttonBig;
	static AudioClip menuIn;
	static AudioClip menuOut;
	bool skippedFirstOnSelect;

	protected virtual void Awake () 
	{
		if(buttonFwd == null) {
			buttonFwd = Resources.Load<AudioClip>("Audio/SFX/button_forward");
		}
		if(buttonBkwd == null) {
			buttonBkwd = Resources.Load<AudioClip>("Audio/SFX/button_back");
		}
		if(buttonBig == null) {
			buttonBig = Resources.Load<AudioClip>("Audio/SFX/button_big");
		}
		if(menuIn == null) {
			menuIn = Resources.Load<AudioClip>("Audio/SFX/menu_in");
		}
		if(menuOut == null) {
			menuOut = Resources.Load<AudioClip>("Audio/SFX/menu_out");
		}
	}
	protected virtual void Start (){}

	public void PlayButtonForwardSound()
	{
		BAudioManager.Instance.CreateAndPlayAudio(buttonFwd);
	}
	public void PlayButtonBackwardSound()
	{
		BAudioManager.Instance.CreateAndPlayAudio(buttonBkwd);
	}
	public void PlayButtonBigSound()
	{
		BAudioManager.Instance.CreateAndPlayAudio(buttonBig);
	}
	public void PlayMenuInSound()
	{
		BAudioManager.Instance.CreateAndPlayAudio(menuIn);
	}
	public void PlayMenuOutSound()
	{
		BAudioManager.Instance.CreateAndPlayAudio(menuOut);
	}


	public void OnUnSelect(GameObject _backing)
	{
		var texts = _backing.GetComponentsInChildren<Text>();
		if(texts.Length > 0) {
			foreach(Text text in texts) {
				text.color = Color.black;
			}
		} else{
			_backing.SetActive(false);
		}
	}

	static Button[] allButtons;
	static bool[] states;
	public static void DisableAllButtons()
	{
		if (allButtons != null) {
			EnableAllButtons();
		}
		allButtons = Object.FindObjectsOfType<Button>();
		states = new bool[allButtons.Length];
		for(int i = 0; i < allButtons.Length; ++i) {
			states[i] = allButtons[i].enabled;
			allButtons[i].enabled = false;
		}
	}
	public static void EnableAllButtons()
	{
		if (allButtons != null) {
			for(int i = 0; i < states.Length; ++i) {
				allButtons[i].enabled = states[i];
			}
			allButtons = null;
			states = null;
		}
	}
}