﻿using UnityEngine;
using System.Collections;

public class BCreditsScene : MonoBehaviour
{
	IEnumerator Start ()
	{
		BScreenFade.StartFadeToClear(1f, true, null, 0.2f);
		//StartCoroutine(BAudioManager.Instance.PlayMusic(BAudioManager.Instance.ResultsSong, null, 0f, 0.5f));
		var gobj = gameObject;
		var scroll = gobj.GetComponent<BCreditTextScroll>();
		yield return new WaitForSecondsRealtime(1.5f);
		scroll.AllowScrolling = true;
		Destroy (this);
	}
}
