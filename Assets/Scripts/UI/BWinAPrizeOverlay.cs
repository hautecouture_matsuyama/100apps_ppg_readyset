﻿using UnityEngine;
using DG.Tweening;
using Spine.Unity;
using TMPro;

public class BWinAPrizeOverlay : BBaseOverlay
{
	public static void SpawnWinAPrize()
	{
		var prefab = Resources.Load<GameObject>(@"Prefabs/UI/WinAPrizeOverlay");
		Object.Instantiate(prefab);
	}

	[SerializeField] SkeletonAnimation spineAnim;
	[SerializeField] RectTransform button;
	[SerializeField] RectTransform leaveButton;
	[SerializeField] AudioClip purchaseSound;
	[SerializeField] AudioClip stationOpenSound;
	[SerializeField] TextMeshProUGUI[] priceTexts;

	protected override void Awake()
	{
		base.Awake ();
		var canvas = GetComponent<Canvas> ();
		canvas.worldCamera = GameObject.FindGameObjectWithTag ("UICamera").GetComponent<Camera>();
		canvas.sortingLayerName = "HUD";
		canvas.planeDistance = 100;
	}
	protected override void Start ()
	{
		allowedToClick = true;
		foreach(TextMeshProUGUI text in priceTexts) {
			text.text = DesignValues.Costs.WinAPrizeCost.ToString();
		}
	}
	public void PurchaseButton()
	{
		if (allowedToClick) {
			allowedToClick = false;
			PlayButtonForwardSound();
			BConfirmPrizeMachineOverlay.SpawnConfirm(onConfirmPurchase, onCancelPurchase);
		}
	}

	public void ClosePrizeMachine()
	{
		if (allowedToClick) {
			PlayButtonBackwardSound();
			allowedToClick = false;
			destroyOverlay ();
		}
	}

	void onConfirmPurchase()
	{
		BAudioManager.Instance.CreateAndPlayAudio(purchaseSound);
		BAudioManager.Instance.CreateAndPlayAudio(stationOpenSound);
		PlayerManager.GoldCount -= DesignValues.Costs.WinAPrizeCost;
		button.DOScale(Vector3.zero, 0.5f).SetEase(Ease.InBack).SetUpdate(true);
		leaveButton.DOScale(Vector3.zero, 0.5f).SetEase(Ease.InBack).SetDelay(0.25f).SetUpdate(true);
		spineAnim.AnimationState.SetAnimation(0, "GiftStation_Event", false).Complete += delegate {
			var overlay = BGiftOverlay.SpawnGift(GiftSpawner.GetItemForWinAPrizeGift());
			overlay.OnClose = destroyOverlay;
		};;
	}
	void onCancelPurchase()
	{
		PlayButtonBackwardSound();
		allowedToClick = true;
	}
	void destroyOverlay ()
	{
		BScreenFade.StartFadeToBlack (0.3f, false, () =>  {
			Destroy (button.gameObject);
			Destroy (gameObject);
			BScreenFade.StartFadeToClear (0.3f, false);
		});
	}

	protected override void onEscape ()
	{
		ClosePrizeMachine();
	}
}