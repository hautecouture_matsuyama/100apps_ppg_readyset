﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BFirsttimeLegalOverlay : BBaseOverlay {

	public static BFirsttimeLegalOverlay LaunchFirsttimeLegalOverlay()
	{
		if (instance != null && !instance.gameObject.activeSelf) {
			instance.gameObject.SetActive(true);
		}
		return instance;
	}

	static BFirsttimeLegalOverlay instance;

	public BTitleOverlay TitleOverlay;

	protected override void Awake ()
	{
		instance = this;
		base.Awake ();
		instance.gameObject.SetActive(false);
	}
	protected override void OnEnable()
	{
		base.OnEnable();
		animateInScale();
	}
	void OnDestroy()
	{
		instance = null;
	}
	public void Accept()
	{
		if(allowedToClick) {
			StartCoroutine(waitForAllowedToClick());
			BTitleOverlay.HasSeenAgreement = true;
			SaveLoadManager.Save();
			animateOutScale().OnComplete(() => Destroy(gameObject));
		}
	}
	public void OpenPrivacyPolicy()
	{
		if(allowedToClick) {
			StartCoroutine(waitForAllowedToClick());
			TitleOverlay.OpenPrivacyPolicy();
		}
	}
	public void OpenTermsOfUse()
	{
		if(allowedToClick) {
			StartCoroutine(waitForAllowedToClick());
			TitleOverlay.OpenTermsOfUse ();
		}
	}
	IEnumerator waitForAllowedToClick()
	{
		allowedToClick = false;
		yield return new WaitForSeconds(1f);
		allowedToClick = true;
	}
	protected override void onEscape ()
	{
	}
}
