﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using VoxelBusters.NativePlugins;

public class BOptionsAndSettingsOverlay : BBaseOverlay
{
	public static BOptionsAndSettingsOverlay LaunchOptionsOverlay()
	{
		if (Instance != null && !Instance.gameObject.activeSelf) {
			Instance.gameObject.SetActive(true);
		}
		return Instance;
	}

	public static BOptionsAndSettingsOverlay Instance{get; private set;}
	static Ease panelMotionTween = Ease.InOutBack;

	public BTitleOverlay TitleOverlay;

	[SerializeField] RectTransform optionsOverlay;
	[SerializeField] RectTransform settingsOverlay;

	#region Options Inspector Stuff
	[SerializeField] RectTransform settingsButton;
	[SerializeField] RectTransform rateUsButton;
	[SerializeField] RectTransform achievementButton;
	[SerializeField] RectTransform leaderboardButton;
	[SerializeField] RectTransform creditsButton;
	#endregion

	#region Settings Inspector Stuff
	[SerializeField] Button musicButton;
	[SerializeField] Button sfxButton;

	[SerializeField] Sprite enabledMusic;
	[SerializeField] Sprite enabledSfx;
	[SerializeField] Sprite disabledMusic;
	[SerializeField] Sprite disabledSfx;

	[SerializeField] TextMeshProUGUI[] notificationTexts;
	#endregion

	#region refs
	CanvasScaler canvasScaler;
	BOverlayParallax parallax;
	#endregion

	bool inSettings;

	#region Mono Callbacks
	protected override void Awake ()
	{
		Instance = this;
		base.Awake ();
		allowedToClick = false;
		canvasScaler = GetComponent<CanvasScaler>();
		parallax = GetComponentInChildren<BOverlayParallax>();

		updateSoundButtonForState(musicButton, BAudioManager.MusicMuted ? disabledMusic : enabledMusic);
		updateSoundButtonForState(sfxButton, BAudioManager.SfxMuted ? disabledSfx : enabledSfx);

		Instance.gameObject.SetActive(false);
	}
	protected override void Start ()
	{
		base.Start ();
		bg.DOFade(0, 1f).Kill(true);
		setOverlaysToBaseLocation ();
		if (Application.platform == RuntimePlatform.Android) {
			//move settings down
			var pos = settingsButton.localPosition;
			pos.y = (pos.y + achievementButton.localPosition.y) / 2f;
			settingsButton.localPosition = pos;
			//move rate us down
			pos = rateUsButton.localPosition;
			pos.y = (pos.y + leaderboardButton.localPosition.y) / 2f;
			rateUsButton.localPosition = pos;
			//move credits up
			pos = creditsButton.localPosition;
			pos.y = (pos.y + achievementButton.localPosition.y) / 2f;
			creditsButton.localPosition = pos;
			Destroy(achievementButton.gameObject);
			Destroy(leaderboardButton.gameObject);
		}
	}
	protected override void OnEnable()
	{
		base.OnEnable();
		inSettings = false;
		setOverlaysToBaseLocation();
		updateNotificationText();
		allowedToClick = false;
		bg.DOColor (bgColor, tweenTime).SetEase (Ease.Linear).SetUpdate (true).OnComplete (()=>{
			parallax.StartMotion(tweenTime / 2f);
		});
		tweenEntireOverlay (optionsOverlay, 0, tweenTime * 0.2f, setAllowedToClickTrue);
	}
	void OnDestroy()
	{
		Instance = null;
	}
	#endregion

	#region Options Callbacks
	public void OnSettings()
	{
		if (allowedToClick) {
			allowedToClick = false;
			inSettings = true;
			tweenEntireOverlay (optionsOverlay, -canvasScaler.referenceResolution.x, 0f, setAllowedToClickTrue);
			tweenEntireOverlay (settingsOverlay, 0);
		}
	}
	public void OnRateUs()
	{
		if (allowedToClick) {
			string storeUrl = "https://itunes.apple.com/jp/app/ready-set-monsters/id1355772493?l=ja&ls=1&mt=8";
			//TODO add the amazon macro when deploying amazon builds (starting with beta)
			#if UNITY_ANDROID
			// detect which store installed this app
			if (Application.installerName == "com.amazon.venezia") {
				// app came from Amazon, open Amazon store URL
				storeUrl = "http://www.amazon.com/gp/mas/dl/android?p=com.turner.ppgbliss"; 
			} else { //if (Application.installerName == "com.android.vending") {
				// otherwise default to Google store URL
				storeUrl = "https://play.google.com/store/apps/details?id=com.hautecouture.ppgready"; 
			}
			#elif UNITY_IOS
			storeUrl = "https://itunes.apple.com/us/app/ready-set-monsters-the-powerpuff-girls/id1191886861?ls=1&mt=8";
			#endif
			Application.OpenURL(storeUrl);
		}
	}
	public void OnCheevos()
	{
		#if USES_GAME_SERVICES
		if (allowedToClick) {
			if(!NPBinding.GameServices.LocalUser.IsAuthenticated) {
				#if UNITY_IOS
				BGameCenterManager.AuthenticateGCUser();
				#endif
			} else {
				NPBinding.GameServices.ShowAchievementsUI( (string _error) => {
					if(!BDebugOverlay.HideSDKLogs) {
						Debug.Log("Closed achievement UI.");
					} else if (!string.IsNullOrEmpty(_error)) {
						Debug.Log(string.Format("Error= {0}.", _error));
					}
				});
			}
		}
		#endif
	}
	public void OnLeaderboards()
	{
		#if USES_GAME_SERVICES
		if (allowedToClick) {
			if(!NPBinding.GameServices.LocalUser.IsAuthenticated) {
				#if UNITY_IOS
				BGameCenterManager.AuthenticateGCUser();
				#endif
			} else {
				NPBinding.GameServices.ShowLeaderboardUIWithGlobalID(GameCenterConstants.LB_HIGH_SCORES, eLeaderboardTimeScope.ALL_TIME, (string _error)=> {
					if(!BDebugOverlay.HideSDKLogs) {
						Debug.Log("Closed leaderboard UI.");
					} else if (!string.IsNullOrEmpty(_error)) {
						Debug.Log(string.Format("Error= {0}.", _error));
					}
				});
			}
		}
		#endif
	}
	public void OnCredits()
	{
		goToScene("Credits");
	}
	public void OnTermsOfUse()
	{
		if (allowedToClick && TitleOverlay != null) {
			TitleOverlay.OpenTermsOfUse();
		}
	}
	public void OnPrivacyPolicy()
	{
		if (allowedToClick && TitleOverlay != null) {
			TitleOverlay.OpenPrivacyPolicy();
		}
	}
	public void OnCloseOptions()
	{
		if (allowedToClick) {
			allowedToClick = false;
			SaveLoadManager.Save();
			tweenEntireOverlay (optionsOverlay, canvasScaler.referenceResolution.x);
			parallax.StopMotion(tweenTime);
			bg.DOFade(0f, tweenTime).SetEase(Ease.Linear).SetDelay(tweenTime / 2f).OnComplete(() => {
				setAllowedToClickTrue();
				Instance.gameObject.SetActive(false);
			});

			DFPManager.Instance.Request ();

		}
	}
	public void OnDebug()
	{
		BDebugOverlay.LaunchDebug(false);
	}
	#endregion

	#region Settings Callbacks
	public void OnToggleMusicMute()
	{
		if (allowedToClick) {
			BAudioManager.Instance.ToggleMuteSound(AudioType.Music);
			updateSoundButtonForState(musicButton, BAudioManager.MusicMuted ? disabledMusic : enabledMusic);
		}
	}
	public void OnToggleSfxMute()
	{
		if (allowedToClick) {
			BAudioManager.Instance.ToggleMuteSound(AudioType.Sfx);
			updateSoundButtonForState(sfxButton, BAudioManager.SfxMuted ? disabledSfx : enabledSfx);
		}
	}
	public void OnResetData()
	{
		if (allowedToClick) {
			BGenericConfirmOverlay.SpawnConfirm("セーブデータ", "セーブデータを削除します。よろしいですか？", "削除", () => SaveLoadManager.SaveDefaultValues ());
		}
	}
	public void OnNotifications()
	{
		if (allowedToClick) {
			GameNotifications.AllowNotifications ^= true;
			updateNotificationText();
		}
	}
	public void OnCloseSettings()
	{
		if (allowedToClick) {
			allowedToClick = false;
			inSettings = false;
			SaveLoadManager.Save();
			tweenEntireOverlay (optionsOverlay, 0, 0f, setAllowedToClickTrue);
			tweenEntireOverlay (settingsOverlay, canvasScaler.referenceResolution.x);
		}
	}
	#endregion

	#region Helpers
	void setOverlaysToBaseLocation ()
	{
		optionsOverlay.localPosition = Vector2.right * canvasScaler.referenceResolution.x;
		settingsOverlay.localPosition = Vector2.right * canvasScaler.referenceResolution.x;
	}
	void updateSoundButtonForState(Button _button, Sprite _src)
	{
		for(int i = 0; i < _button.transform.childCount; ++i) {
			var child = _button.transform.GetChild(i).gameObject;
			child.GetComponent<Image>().sprite = _src;
		}
	}

	static void tweenEntireOverlay (Transform _overlay, float _to, float _delay = 0f, System.Action _onComplete = null)
	{
		_overlay.DOLocalMoveX (_to, 0.5f)
			.SetDelay(_delay)
			.SetEase (panelMotionTween)
			.OnComplete (() => {
				if (_onComplete != null) {
					_onComplete();
				}
			});
	}
	void updateNotificationText()
	{
		var str = string.Format("通知: {0}", GameNotifications.AllowNotifications ? "オン" : "オフ");;
		for(int i = 0; i < notificationTexts.Length; ++i) {
			notificationTexts[i].text = str;
		}
	}
	#endregion
	protected override void onEscape ()
	{
		if (inSettings) {	
			OnCloseSettings();
			setCanCallEscapeTrue();
		} else {
			OnCloseOptions();
		}
	}
}
