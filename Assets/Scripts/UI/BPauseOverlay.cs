﻿using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;

public class BPauseOverlay : BBaseOverlay
{
	public static BPauseOverlay Instance{get; private set;}
	public static bool IsPaused {get; private set;}

	[SerializeField] RectTransform rightPanel;
	[SerializeField] RectTransform leftPanel;

	[SerializeField] Button musicButton;
	[SerializeField] Button sfxButton;

	[SerializeField] Sprite enabledMusic;
	[SerializeField] Sprite enabledSfx;
	[SerializeField] Sprite disabledMusic;
	[SerializeField] Sprite disabledSfx;

	[SerializeField] Transform globe;
	[SerializeField] Transform tick;
	[SerializeField] Transform flag;
	[SerializeField] GameObject blossom;
	[SerializeField] GameObject bubbles;
	[SerializeField] GameObject buttercup;

	[SerializeField] TextMeshProUGUI loopMultiplier;
	[SerializeField] GameObject loopBurstPfx;

	const float bgFadeTime = 0.2f;
	const float panelSlideTime = 0.5f;

	float rightPanelOffScreenAnchor;
	float rightPanelOnScreenAnchor;
	float leftPanelOffScreenAnchor;
	float leftPanelOnScreenAnchor;

	BOverlayParallax parallax;
	tk2dSpriteAnimator flagAnim;

	Vector3 blossomOGPosition;
	Vector3 bubblesOGPosition;
	Vector3 buttercupOGPosition;

	#region Mono callbacks
	protected override void Awake ()
	{
		base.Awake ();
		Instance = this;
		parallax = GetComponentInChildren<BOverlayParallax>();

		rightPanelOnScreenAnchor = rightPanel.anchoredPosition.x;
		leftPanelOnScreenAnchor = leftPanel.anchoredPosition.x;

		rightPanelOffScreenAnchor = rightPanel.rect.width * 1.9f;
		leftPanelOffScreenAnchor = -leftPanel.rect.width * 2f;

		Instance = this;
	}
	protected override void Start ()
	{
		base.Start ();
		createAllTicks(tick);
		flagAnim = flag.GetComponentInChildren<tk2dSpriteAnimator>();
		gameObject.SetActive(false);
		blossomOGPosition = blossom.transform.localPosition;
		bubblesOGPosition = bubbles.transform.localPosition;
		buttercupOGPosition = buttercup.transform.localPosition;
	}
	protected override void OnEnable()
	{
		base.OnEnable();
		Time.timeScale = 0f;
		IsPaused = true;
	}
	void OnDisable()
	{
		System.GC.Collect();
		loopBurstPfx.SetActive(false);
		Time.timeScale = 1f;
		IsPaused = false;
	}
	void OnDestroy()
	{
		Instance = null;
	}
	void LateUpdate()
	{
		flagAnim.UpdateAnimation(Time.unscaledDeltaTime);
	}
	#endregion

	#region Inspector Callbacks
	public void LaunchDebug()
	{
		BDebugOverlay.LaunchDebug(true);
	}
	public void Pause()
	{
		gameObject.SetActive(true);
		updateSoundButtonForState(musicButton, BAudioManager.MusicMuted ? disabledMusic : enabledMusic);
		updateSoundButtonForState(sfxButton, BAudioManager.SfxMuted ? disabledSfx : enabledSfx);
		BAudioManager.Instance.AdjustMusicVolume(0.2f, 0.3f);
		BAudioManager.Instance.AdjustMusicVolume(0.2f, 0.3f, 1);
		animateIn();
	}
	public void Resume()
	{
		BAudioManager.Instance.AdjustMusicVolume(0.5f, 0.3f);
		BAudioManager.Instance.AdjustMusicVolume(0.5f, 0.3f, 1);
		animateOut();	
	}
	public void Restart()
	{
		BGenericConfirmOverlay.SpawnConfirm("最初からやりなおす？", "現在のスコアを無効にして最初からやりなおす？","最初からやりなおす？",
			GoToGameplay);
	}
	public void Quit()
	{
		BGenericConfirmOverlay.SpawnConfirm("アップグレード画面にいく？", "現在のスコアを無効にしてアップグレード画面にいく？","最初からやりなおす？",
			GoToUpgrade);
	}
	public void ToggleMusicMute()
	{
		BAudioManager.Instance.ToggleMuteSound(AudioType.Music);
		updateSoundButtonForState(musicButton, BAudioManager.MusicMuted ? disabledMusic : enabledMusic);
	}
	public void ToggleSfxMute()
	{
		BAudioManager.Instance.ToggleMuteSound(AudioType.Sfx);
		updateSoundButtonForState(sfxButton, BAudioManager.SfxMuted ? disabledSfx : enabledSfx);
	}
	#endregion

	#region helpers
	void updateSoundButtonForState(Button _button, Sprite _src)
	{
		for(int i = 0; i < _button.transform.childCount; ++i) {
			var child = _button.transform.GetChild(i).gameObject;
			child.GetComponent<Image>().sprite = _src;
		}
	}

	void animateIn()
	{
		bg.DOColor(Color.clear, 0.1f).Kill(true);
		bg.DOColor(bgColor, bgFadeTime).SetEase(Ease.Linear).SetUpdate(true);
		parallax.StartMotion(bgFadeTime);

		animatePanelFromTo(rightPanel, rightPanelOffScreenAnchor, rightPanelOnScreenAnchor, Ease.OutBack, bgFadeTime);
		const float leftPanelDelay = bgFadeTime + (panelSlideTime / 4f);
		animatePanelFromTo(leftPanel, leftPanelOffScreenAnchor, leftPanelOnScreenAnchor, Ease.OutBack, leftPanelDelay);
		const float animateDelay = leftPanelDelay + panelSlideTime;
		setGlobalProgressMap (globe, flag, blossom, bubbles, buttercup, loopMultiplier, loopBurstPfx, animateDelay);
	}
	void animateOut()
	{
		animatePanelFromTo(leftPanel, leftPanelOnScreenAnchor, leftPanelOffScreenAnchor, Ease.InBack, 0);
		animatePanelFromTo(rightPanel, rightPanelOnScreenAnchor, rightPanelOffScreenAnchor, Ease.InBack, (panelSlideTime / 4f));
		BCoroutine.WaitAndPerformRealtime(() => {
			parallax.StopMotion(bgFadeTime);
			cleanUpTicks();
		}, panelSlideTime);
		bg.DOColor(bgColor, 0.1f).Kill(true);
		bg.DOColor(Color.clear, bgFadeTime).SetEase(Ease.Linear).SetUpdate(true).SetDelay(panelSlideTime * 1.25f).OnComplete(() => {
			blossom.transform.DOKill();
			blossom.transform.localPosition = blossomOGPosition;
			bubbles.transform.DOKill();
			bubbles.transform.localPosition = bubblesOGPosition;
			buttercup.transform.DOKill();
			buttercup.transform.localPosition = buttercupOGPosition;
			gameObject.SetActive (false);
		});
	}

	void animatePanelFromTo(RectTransform _panel, float _from, float _to, Ease _type, float _delay)
	{
		_panel.DOAnchorPosX(_from, panelSlideTime).Kill(true);
		_panel.DOAnchorPosX(_to, panelSlideTime).SetEase(_type).SetDelay(_delay).SetUpdate(true);
	}
	#endregion

	protected override void onEscape ()
	{
		var overlays = FindObjectsOfType<BBaseOverlay>();
		if (overlays.Length == 1 && overlays[0] == this) {
			Resume();
		} else {
			setCanCallEscapeTrue();
		}
	}
}
