﻿using UnityEngine;
using TMPro;
using DG.Tweening;
public class BHealthChangePopup : MonoBehaviour
{
	const float travelTime = 1f;
	const float hangTime = 0.75f;
	const float fadeTime = 0.5f;

	[SerializeField] TextMeshProUGUI text;
	[SerializeField] Color healthDown;
	[SerializeField] Color healthUp;

	public ObjectPool PoolHandle;
	public void Show(float _change, Vector3 _location, bool _critical)
	{
		transform.position = _location + Vector3.right * Random.Range(-30,30);
		text.color = _change < 0 ? healthDown : healthUp;
		var dmgValue = (int)(DesignValues.PlayerBattleFeel.ArbitraryBattleNumbersScalar * _change);
		if (dmgValue <= -9990) {
			dmgValue = -9999;
		}
		text.text = string.Format("{0}{1}", _change >= 0 ? "+": "", dmgValue);

		var originalFontSize = text.fontSize;
		if(_critical) {
			text.fontSize *= 1.5f; 
			transform.DOShakeRotation(travelTime * 0.8f, Vector3.forward * 50, 60);
		}
		text.DOFade(1, 0).Kill(true);
		transform.DOMoveY(_location.y + ((_critical ? 0.2f : 0.12f) * Screen.height), travelTime).SetEase(Ease.OutCirc);
		text.DOFade(0f, fadeTime).SetEase(Ease.Linear).SetDelay(travelTime-fadeTime).OnComplete(() => {
			text.fontSize = originalFontSize;
			PoolHandle.Release (gameObject);
		});
	}
}
