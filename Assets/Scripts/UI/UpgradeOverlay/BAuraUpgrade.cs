﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;
using DG.Tweening;
using TMPro;


public class BAuraUpgrade : BBaseUI
{
	[SerializeField] BUpgradeScrollRect scroller;
	[SerializeField] GameObject equippedPfx;
	[SerializeField] Button[] selectable;
	[SerializeField] Sprite[] auraSprites;
	[SerializeField] string[] auraText;
	[SerializeField] TextMeshProUGUI costText;
	[SerializeField] GirlUpgradeVOData girlVOData;

	protected override void Start()
	{
		//Set cost text
		costText.text =""+DesignValues.Costs.AuraCost;
		Debug.Log (auraSprites [2].name);
		for(int i = 0; i < selectable.Length ; ++i){
			var btn = selectable[i];
			var curInd = i;
			btn.onClick.AddListener(() => onAuraClicked (btn, curInd));
		}
	}

	void onAuraClicked(Button _self, int _selfIndex)
	{
		if (BCharacterSelect.CurrentAcquiredStat == null) {
			return;
		}
		PlayButtonForwardSound ();
		var additionalText = "";
		var forceDisable = false;
		if (!BCharacterSelect.CurrentAcquiredStat[AbiltyType.ChargeAttack]) {
			additionalText = "\n\n<color=\"red\">チャージアタックを覚えよう</color>";
			forceDisable = true;
		}
		var alreadyUnlocked = BCharacterSelect.CurrentAcquiredStat.UnlockedAuras.Get(_selfIndex);
		var currencyType = alreadyUnlocked ? CurrencyType.None : DesignValues.Costs.AuraCurrency;
		uint cost = (uint)(alreadyUnlocked ? 0 : DesignValues.Costs.AuraCost);
		var hideActionButton = forceDisable || BCharacterSelect.CurrentAcquiredStat.EquippedAuraIndex == _selfIndex;
		var auraItemInfos = PlayerEquipsManager.Auras[BCharacterSelect.CurrentGirl];
		var currAuraInfo = auraItemInfos[_selfIndex];

		Sprite sprite = auraSprites[0];

		int idx = 0;
		foreach(var obj in auraText){
			if (obj == currAuraInfo.Name) {
				sprite = auraSprites [idx];
				Debug.Log ("画像名" + sprite.name);
			}
			idx++;
		}


		BUpgradeConfirm.InitBuyOrEquipCanvas(currAuraInfo.Name, string.Format("{0}\n{1}", currAuraInfo.Description, additionalText), sprite, currencyType, cost, ()=> {
			BCharacterSelect.CurrentAcquiredStat.UnlockedAuras.Set(_selfIndex, true);
			BCharacterSelect.CurrentAcquiredStat.EquippedAuraIndex = _selfIndex;
			BGameCenterManager.CheckAllAuras();
			if (BCharacterSelect.CurrentAcquiredStat.EquippedAuraIndex != -1) {
				refreshList();
			}
			if(Random.value <= 0.66f) {
				BAudioManager.Instance.CreateAndPlayAudio(girlVOData.GetRandomClipForGirl(BCharacterSelect.CurrentGirl));
			}
		}, hideActionButton);
	}

	public void OnAuraSelected()
	{
		refreshList();
		scroller.SetToBottom();
	}

	void refreshList()
	{
		if (BCharacterSelect.CurrentAcquiredStat == null) {
			return;
		}
		var auraItemInfos = PlayerEquipsManager.Auras[BCharacterSelect.CurrentGirl];
		for(int i = 0; i < selectable.Length; ++i) {
			var unlocked = BCharacterSelect.CurrentAcquiredStat.UnlockedAuras.Get(i);
			var images = selectable[i].GetComponentsInChildren<Image>(true);
			var selfImage = selectable[i].GetComponent<Image>();
			var currAuraInfo = auraItemInfos[i];
			Sprite sprite = auraSprites[0];

			int idx = 0;
			foreach(var obj in auraText){
				if (obj == currAuraInfo.Name) {
					sprite = auraSprites [idx];
				}
				idx++;
			}

			//var sprite = auraText.Single(arg => arg == currAuraInfo.Name);
			foreach(Image image in images) {
				if(image != selfImage) {
					image.sprite = sprite;
				}
			}
			BUpgradeOverlay.UpdateButtonAppearance(unlocked, i == BCharacterSelect.CurrentAcquiredStat.EquippedAuraIndex, selectable[i].gameObject, equippedPfx);
		}
	}

	void enableScrollIfAuraUnlocked ()
	{
		if (BCharacterSelect.CurrentAcquiredStat != null) {
			scroller.enabled = BCharacterSelect.CurrentAcquiredStat.AuraUnlocked;
		}
	}
	void OnNewGirlActive(PPG _currentGirl)
	{
		scroller.SetToTop (() => {
			refreshList();
			scroller.enabled = true;//enableScrollIfAuraUnlocked();
		});

	}
}
