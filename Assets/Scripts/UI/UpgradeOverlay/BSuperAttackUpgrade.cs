﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;

public class BSuperAttackUpgrade : BBaseUI
{
	public delegate void OnSuperUnlocked(SuperAttack _super);
	public static OnSuperUnlocked HandleSuperUnlocked;

	[SerializeField] Selectable normalSuperAttackButton;
	[SerializeField] Selectable blissSuperAttackButton;

	[SerializeField] Image[] blissImages;
	[SerializeField] TextMeshProUGUI header;

	[SerializeField] GameObject equippedPfx;

	[SerializeField] TextMeshProUGUI normalCostText;
	[SerializeField] TextMeshProUGUI blissCostText;

	protected override void Start()
	{
		if (PlayerManager.Objectives.NextNarrativeObjectivePercent == uint.MaxValue) {
			foreach (Image image in blissImages) {
				image.color = Color.gray;
			}
			header.text = "ブリスティナブリッツ";
			blissSuperAttackButton.interactable = true;
		}
		BUpgradeOverlay.UpdateButtonAppearance (PlayerManager.IsSuperAttackUnlocked (SuperAttack.TornadoTrio), 
			PlayerManager.IsSuperAttackEquipped (SuperAttack.TornadoTrio), 
			normalSuperAttackButton.gameObject,
			equippedPfx,
			true);
		BUpgradeOverlay.UpdateButtonAppearance (PlayerManager.IsSuperAttackUnlocked(SuperAttack.BlissBlitz), 
			PlayerManager.IsSuperAttackEquipped(SuperAttack.BlissBlitz), 
			blissSuperAttackButton.gameObject,
			equippedPfx,
			true);
		if(!PlayerManager.IsSuperAttackUnlocked(SuperAttack.TornadoTrio)) {
			normalCostText.text = DesignValues.Costs.NormalSuperCost.ToString();
			normalCostText.transform.parent.gameObject.SetActive(true);
		}
		if(!PlayerManager.IsSuperAttackUnlocked(SuperAttack.BlissBlitz)) {
			blissCostText.text = DesignValues.Costs.BlissSuperCost.ToString();
			blissCostText.transform.parent.gameObject.SetActive(true);
		}
		if (PlayerManager.Objectives.NextNarrativeObjectivePercent != uint.MaxValue) {
			foreach (Image image in blissImages) {
				image.color = Color.black;
			}
			blissCostText.transform.parent.gameObject.SetActive(false);
		} else {
			header.text = "ブリスティナブリッツ";
			blissSuperAttackButton.interactable = true;
		}
	}

	public void OnNormalSuperAttackClicked()
	{
		var alreadyUnlocked = PlayerManager.IsSuperAttackUnlocked(SuperAttack.TornadoTrio);
		var currencyType = alreadyUnlocked ? CurrencyType.None : DesignValues.Costs.NormalSuperCurrency;
		var cost = (uint)(alreadyUnlocked ? 0 : DesignValues.Costs.NormalSuperCost);
		var hideActiveButton = PlayerManager.IsSuperAttackEquipped(SuperAttack.TornadoTrio);
		var sprite = normalSuperAttackButton.transform.FindChild("icon_unpressed").GetComponent<Image>().sprite;
		PlayButtonForwardSound ();
		BUpgradeConfirm.InitBuyOrEquipCanvas("トルネードトリオ", "ガールズ達が激しい竜巻攻撃で大ダメージをあたえる。", sprite, currencyType, cost, ()=> {
			if (PlayerManager.IsSuperAttackEquipped(SuperAttack.BlissBlitz)) {
				BUpgradeOverlay.UpdateButtonAppearance (true, false, blissSuperAttackButton.gameObject, equippedPfx, true);
			}
			if (!PlayerManager.IsSuperAttackUnlocked(SuperAttack.TornadoTrio)) {
				PlayerManager.UnlockSuperAttack(SuperAttack.TornadoTrio);
				if (HandleSuperUnlocked != null) {
					HandleSuperUnlocked(SuperAttack.TornadoTrio);
				}
			}
			PlayerManager.EquipSuperAttack(SuperAttack.TornadoTrio);
			BUpgradeOverlay.UpdateButtonAppearance (true, true, normalSuperAttackButton.gameObject, equippedPfx, true);
			normalCostText.transform.parent.gameObject.SetActive(false);
		}, hideActiveButton);
	}
	public void OnBlissSuperAttackClicked()
	{
		var alreadyUnlocked = PlayerManager.IsSuperAttackUnlocked(SuperAttack.BlissBlitz);
		var currencyType = alreadyUnlocked ? CurrencyType.None : DesignValues.Costs.BlissSuperCurrency;
		var cost = (uint)(alreadyUnlocked ? 0 : DesignValues.Costs.BlissSuperCost);
		var hideActiveButton = PlayerManager.IsSuperAttackEquipped(SuperAttack.BlissBlitz);
		var sprite = blissSuperAttackButton.transform.FindChild("icon_unpressed").GetComponent<Image>().sprite;
		PlayButtonForwardSound ();
		BUpgradeConfirm.InitBuyOrEquipCanvas("ブリスティナブリッツ", "ブリスの必殺技で最大ダメージをあたえる。", sprite, currencyType, cost, ()=> {
			if (PlayerManager.IsSuperAttackEquipped(SuperAttack.TornadoTrio)) {
				BUpgradeOverlay.UpdateButtonAppearance (true, false, normalSuperAttackButton.gameObject, equippedPfx, true);
			}
			if (!PlayerManager.IsSuperAttackUnlocked(SuperAttack.BlissBlitz)) {
				PlayerManager.UnlockSuperAttack(SuperAttack.BlissBlitz);
				if (HandleSuperUnlocked != null) {
					HandleSuperUnlocked(SuperAttack.BlissBlitz);
				}
			}
			PlayerManager.EquipSuperAttack(SuperAttack.BlissBlitz);
			BUpgradeOverlay.UpdateButtonAppearance (true, true, blissSuperAttackButton.gameObject, equippedPfx, true);
			blissCostText.transform.parent.gameObject.SetActive(false);
		}, hideActiveButton);
	}
}
