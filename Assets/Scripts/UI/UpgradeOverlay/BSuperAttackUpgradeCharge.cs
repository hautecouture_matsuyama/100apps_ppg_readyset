﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BSuperAttackUpgradeCharge : BSuperAttackChargeSlider
{
	[SerializeField] SuperAttack attackType;
	[SerializeField] TextMeshProUGUI readyMessage;

	RechargableRealTimeTimer currentSuperTimer;
	bool tickTimer;

	protected override RechargableRealTimeTimer superAttackCharge
	{
		get {
			return PlayerManager.GetSuperAttackHandle(attackType);
		}
	}
	protected override void Start ()
	{
		base.Start ();
		BSuperAttackUpgrade.HandleSuperUnlocked += onSuperUnlocked;
		if (readyMessage.gameObject.activeSelf) {
			currentSuperTimer = PlayerManager.GetSuperAttackHandle(attackType);
			tickTimer = true;
		}
	}
	protected override void Update()
	{
		base.Update ();
		if (tickTimer && updateTimerTextAndCheckIfDone()) {
			readyMessage.text = "使用可能！";
			tickTimer = false;
		}
	}
	void OnDestroy()
	{
		BSuperAttackUpgrade.HandleSuperUnlocked -= onSuperUnlocked;
	}
	void onSuperUnlocked(SuperAttack _attack)
	{
		if (_attack == attackType) {
			gameObject.SetActive(true);
			currentSuperTimer = PlayerManager.GetSuperAttackHandle(attackType);
			tickTimer = true;
		}
	}
	bool updateTimerTextAndCheckIfDone()
	{
		var timeRemaining = currentSuperTimer.TimeRemaining;
		var hours = timeRemaining.Hours;
		var minutes = timeRemaining.Minutes;
		var seconds = timeRemaining.Seconds;
		if (timeRemaining.TotalSeconds > 0) {
			if (hours > 0) {
				readyMessage.text = string.Format("使えるまで: {0}時間  {1}分", hours, minutes);
			} else {
				readyMessage.text = string.Format("使えるまで: {0}分  {1}秒", minutes, seconds);
			}
		}
		return timeRemaining.TotalSeconds <= 0;
	}
}
