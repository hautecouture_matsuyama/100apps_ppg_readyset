﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BAbilityUpgrade : BBaseUI
{
	[SerializeField] AbiltyType ability;
	[SerializeField] Sprite unlockedButtonSprite;
	[SerializeField] GirlUpgradeVOData girlVOData;

	void updateImageForUnlocked ()
	{
		if (BCharacterSelect.CurrentGirl == PPG.None) {
			return;
		}
		if (BCharacterSelect.CurrentAcquiredStat [ability]) {
			BUpgradeOverlay.UpdateButtonAppearance(true, true, gameObject, null, false, new Color(245f/255f, 74f/255f, 126f/255f));
		} else {
			BUpgradeOverlay.UpdateButtonAppearance(false, false, gameObject, null);
		}
	}

	public void UnlockAbility()
	{
		if (BCharacterSelect.CurrentAcquiredStat != null) {
			string header = "";
			string desc = "アビリティアップグレード情報";
			switch(ability) {
			case AbiltyType.BiggerCombo:
				header = "ロングコンボ";
				desc = "連続攻撃が強化されてより多くのダメージをあたえるようになる。";
				break;
			case AbiltyType.ChargeAttack:
				header = "チャージアタック";
				desc = "画面をタップし続けると力をチャージ！ガールズが点滅をはじめたら指を離して必殺技！指を離すのが早すぎるとダメージが少なくなる。";
				break;
			case AbiltyType.Counter:
				header = "カウンターアタック";
				desc = "パーフェクトガードを成功すると同時に反撃を行うようになる。";
				break;
			default:
				break;
			}
			PlayButtonForwardSound ();
			BUpgradeConfirm.InitBuyOrEquipCanvas(header, desc, null, DesignValues.Costs.AbilityCurrency, DesignValues.Costs.AbilityCost, ()=> {
				BCharacterSelect.CurrentAcquiredStat.UnlockAbility(ability);
				updateImageForUnlocked ();
				if (ability == AbiltyType.ChargeAttack) {
					BCharacterSelect.CurrentAcquiredStat.UnlockAura(0);
					FindObjectOfType<BAuraUpgrade>().OnAuraSelected();
				}
				if(Random.value <= 0.66f) {
					BAudioManager.Instance.CreateAndPlayAudio(girlVOData.GetRandomClipForGirl(BCharacterSelect.CurrentGirl));
				}
			}, BCharacterSelect.CurrentAcquiredStat [ability]);
		}
	}
	void OnNewGirlActive(PPG _currentGirl)
	{
		updateImageForUnlocked ();
	}
}
