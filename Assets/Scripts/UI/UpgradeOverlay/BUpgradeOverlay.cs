﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public enum UpgradeTab
{
	None,
	PPGStats,
	FriendlyMonsters,
	SpecialAttacks
}

public class BUpgradeOverlay : BBaseOverlay
{
	public static void UpdateButtonAppearance(bool _unlocked, bool _equipped, GameObject _buttonObject, GameObject _equippedPfx, bool _isSuperButton = false)
	{
		UpdateButtonAppearance(_unlocked, _equipped, _buttonObject, _equippedPfx, _isSuperButton, Color.white);
	}
	public static void UpdateButtonAppearance(bool _unlocked, bool _equipped, GameObject _buttonObject, GameObject _equippedPfx, bool _isSuperButton, Color _unlockedIconColor)
	{
		var iconImages = _buttonObject.GetComponentsInChildren<Image>(true);
		var buttonImage = _buttonObject.GetComponent<Image>();
		var buttonPressedSpriteState = _buttonObject.GetComponent<Button>().spriteState;
		if(!_unlocked) {
			buttonImage.sprite = _isSuperButton ? instance.superButtonLockedSprite : instance.buttonLockedSprite;
			buttonPressedSpriteState.pressedSprite = _isSuperButton ? instance.superButtonLockedPressedSprite : instance.buttonLockedPressedSprite;
			_buttonObject.GetComponent<Button>().spriteState = buttonPressedSpriteState;
			foreach(Image image in iconImages) {
				if(image != buttonImage) {
					image.color = Color.gray;
					image.DOFade(0.5f, 0).Kill(true);
				}
			}
		} else {
			if(_equipped) {
				buttonImage.sprite = _isSuperButton ? instance.superButtonEquippedSprite : instance.buttonEquippedSprite;
				buttonPressedSpriteState.pressedSprite = _isSuperButton ? instance.superButtonEquippedPressedSprite : instance.buttonEquippedPressedSprite;
				_buttonObject.GetComponent<Button>().spriteState = buttonPressedSpriteState;
				if(_equippedPfx != null) {
					_equippedPfx.transform.SetParent(_buttonObject.transform, false);
					_equippedPfx.transform.localPosition = Vector3.zero;
					_equippedPfx.SetActive(true);
				}
			} else {
				buttonImage.sprite = _isSuperButton ? instance.superButtonUnlockedSprite : instance.buttonUnlockedSprite;
				buttonPressedSpriteState.pressedSprite = _isSuperButton ? instance.superButtonUnlockedPressedSprite : instance.buttonUnlockedPressedSprite;
				_buttonObject.GetComponent<Button>().spriteState = buttonPressedSpriteState;
			}
			foreach(Image image in iconImages) {
				if(image != buttonImage) {
					image.color = _unlockedIconColor;
					image.DOFade(_equipped ? 1f : 0.5f, 0).Kill(true);
				}
			}
		}

	}

	static BUpgradeOverlay instance;

	[SerializeField] Transform ppgTab;
	[SerializeField] GameObject ppgContent;
	[SerializeField] Transform fmTab;
	[SerializeField] GameObject fmContent;
	[SerializeField] Transform saTab;
	[SerializeField] GameObject saContent;
	[SerializeField] AudioClip upgradeMusic;
	[SerializeField] Sprite buttonLockedSprite;
	[SerializeField] Sprite buttonLockedPressedSprite;
	[SerializeField] Sprite buttonUnlockedSprite;
	[SerializeField] Sprite buttonUnlockedPressedSprite;
	[SerializeField] Sprite buttonEquippedSprite;
	[SerializeField] Sprite buttonEquippedPressedSprite;
	[SerializeField] Sprite superButtonLockedSprite;
	[SerializeField] Sprite superButtonLockedPressedSprite;
	[SerializeField] Sprite superButtonUnlockedSprite;
	[SerializeField] Sprite superButtonUnlockedPressedSprite;
	[SerializeField] Sprite superButtonEquippedSprite;
	[SerializeField] Sprite superButtonEquippedPressedSprite;

	bool canPlayTabSwitchSound;

	protected override void Awake ()
	{
		instance = this;
		base.Awake ();
	}
	protected override void Start ()
	{
		SwitchTabs(ppgTab);
		BScreenFade.StartFadeToClear(1f, true, null, 0.25f);
		BAudioManager.Instance.PlayMusic(upgradeMusic, null, 0, 0.5f);
	}
	void OnDestroy()
	{
		instance = null;
	}

	public void SwitchTabs(Transform _selectedTab)
	{
		if(canPlayTabSwitchSound) {
			PlayButtonForwardSound ();
		} else {
			canPlayTabSwitchSound = true;
		}
		_selectedTab.GetComponent<Image>().color = Color.white;
		var others = new List<Transform>(){ppgTab, fmTab, saTab};
		others.Remove(_selectedTab);
		foreach(var o in others) {
			o.GetComponent<Image>().color = Color.gray;
		}
		if (_selectedTab == ppgTab) {
			ppgContent.SetActive(true);
			fmContent.SetActive(false);
			saContent.SetActive(false);
			BroadcastMessage("OnTabChanged", UpgradeTab.PPGStats);
		} else if (_selectedTab == fmTab) {
			ppgContent.SetActive(false);
			fmContent.SetActive(true);
			saContent.SetActive(false);
			BroadcastMessage("OnTabChanged", UpgradeTab.FriendlyMonsters);
		} else if (_selectedTab == saTab) {
			ppgContent.SetActive(false);
			fmContent.SetActive(false);
			saContent.SetActive(true);
			BroadcastMessage("OnTabChanged", UpgradeTab.SpecialAttacks);
		}
	}

	public void GoToSceneOrClose(string _scene)
	{
		var currentScene = SceneManager.GetActiveScene().name;
		if (_scene != currentScene) {
			//use built in methods provided by base UI (in case we need to intercept them)
			if (_scene == "Gameplay") {
				GoToGameplay();
			} else if (_scene == "Title") {
				GoToTitle();
			}
		} else {
			animateOutScale();
		}
	}
	protected override void onEscape ()
	{
		if (BUpgradeConfirm.Instance == null || !BUpgradeConfirm.Instance.gameObject.activeSelf) {
			GoToTitle();
		} else {
			setCanCallEscapeTrue();
		}
	}
}