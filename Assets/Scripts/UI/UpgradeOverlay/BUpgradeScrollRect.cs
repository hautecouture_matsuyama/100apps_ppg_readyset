﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;

public class BUpgradeScrollRect : ScrollRect
{
	protected override void OnEnable()
	{
		base.OnEnable ();
		SetToTop ();
	}

	public bool InMotion
	{
		get
		{
			return velocity.magnitude > 0;
		}
	}

	bool allowedToScroll = true;

	public override void OnInitializePotentialDrag (PointerEventData eventData)
	{
		if (allowedToScroll) {
			base.OnInitializePotentialDrag (eventData);
		}
	}

	public override void OnBeginDrag(PointerEventData eventData)
	{
		if (allowedToScroll) {
			base.OnBeginDrag(eventData);
		}
	}
	public override void OnDrag(PointerEventData eventData)
	{
		if (allowedToScroll) {
			base.OnDrag(eventData);
		}
	}
	public override void OnEndDrag(PointerEventData eventData)
	{
		if (allowedToScroll) {
			base.OnEndDrag(eventData);
		}
	}

	public void SetToTop(System.Action _onComplete = null)
	{
		if (allowedToScroll && verticalNormalizedPosition < 1) {
			StartCoroutine(moveWithVelocity(Vector2.down * Screen.width, _onComplete));
		} else if (_onComplete != null) {
			_onComplete();
		}
	}

	public void SetToBottom(System.Action _onComplete = null)
	{
		if (allowedToScroll && verticalNormalizedPosition > 0) {
			StartCoroutine(moveWithVelocity(Vector2.up * Screen.width, _onComplete));
		} else if (_onComplete != null) {
			_onComplete();
		}
	}

	IEnumerator moveWithVelocity(Vector2 _vel, System.Action _onComplete = null)
	{
		yield return null;
		verticalNormalizedPosition = Mathf.Clamp01(verticalNormalizedPosition);
		//if/elseif block has hack to get shit moving ><
		if (Mathf.Approximately(verticalNormalizedPosition, 0)) {
			verticalNormalizedPosition = 0.01f;
		} else if (Mathf.Approximately(verticalNormalizedPosition, 1)) {
			verticalNormalizedPosition = 0.99f;
		}
		enabled = true;
		velocity = _vel;
		allowedToScroll = false;
		while(velocity.magnitude > 0){
			yield return null;
		}
		allowedToScroll = true;
		if (_onComplete != null) {
			_onComplete();
		}
		verticalNormalizedPosition = Mathf.Clamp01(verticalNormalizedPosition);
	}
}
