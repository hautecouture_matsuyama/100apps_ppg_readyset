﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections.Generic;
using TMPro;

public class BCharacterSelect : BBaseUI
{
	const float tweenTime = 0.5f;

	[SerializeField] BUpgradeScrollRect scroller;
	[SerializeField] Button[] navButtons;

	[SerializeField] GameObject updateSection;

	[SerializeField] TextMeshProUGUI nameTag;
	[SerializeField] Image[] ppgImages;
	[SerializeField] Image[] fmImages;
	[SerializeField] Image saImage;

	public static PPG CurrentGirl{get; private set;}
	public static BaseStats CurrentBaseStat{get; private set;}
	public static AcquiredStats CurrentAcquiredStat{get; private set;}

	int ppgIndex;
	int fmIndex;
	Image current;
	bool allowedToSwitch;
	LinkedList<Tweener> activeTweens = new LinkedList<Tweener>();
	Dictionary<Image, float> originalPositions;
	UpgradeTab currentTab;
	protected override void Awake()
	{
		base.Awake ();
		originalPositions = new Dictionary<Image, float>(ppgImages.Length + fmImages.Length + 1);
		foreach( var p in ppgImages) {
			originalPositions.Add(p, p.transform.localPosition.x);
		}
		foreach( var f in fmImages) {
			if (!originalPositions.ContainsKey(f)) {
				originalPositions.Add(f, f.transform.localPosition.x);
			}
		}
		originalPositions.Add(saImage, saImage.transform.localPosition.x);
		currentTab = UpgradeTab.None;
		switch (PlayerManager.GetActiveGirl ()) {
		case PPG.None:
		case PPG.Blossom:
			ppgIndex = 0;
			break;
		case PPG.Bubbles:
			ppgIndex = 1;
			break;
		case PPG.Buttercup:
			ppgIndex = 2;
			break;
		default:
			throw new System.ArgumentOutOfRangeException ();
		}
		fmIndex = 0;
		setNavButtonsActiveStat(true);
		allowedToSwitch = true;
	}
	#region Inspector Callbacks
	public void MoveRight()
	{
		if (!scroller.InMotion && currentTab == UpgradeTab.PPGStats) {
			PlayButtonForwardSound ();
			updatePPGIndex(1);
		}
	}
	public void MoveLeft()
	{
		if (!scroller.InMotion && currentTab == UpgradeTab.PPGStats) {
			PlayButtonForwardSound ();
			updatePPGIndex(-1);
		}
	}
	#endregion

	#region Message callbacks
	void OnTabChanged(UpgradeTab _tab)
	{
		if (currentTab != _tab) {
			switch (_tab) {
			case UpgradeTab.PPGStats:
				phaseOutToNext(ppgImages[ppgIndex], 1);
				updateActivePPG(ppgIndex);
				setNavButtonsActiveStat(true);
				nameTag.gameObject.SetActive(true);
				break;
			case UpgradeTab.FriendlyMonsters:
				phaseOutToNext(fmImages[fmIndex], 1);
				updateActivePPG(-1);
				setNavButtonsActiveStat(false);	
				nameTag.gameObject.SetActive(false);
				break;
			case UpgradeTab.SpecialAttacks:
				phaseOutToNext(saImage, 1);
				updateActivePPG(-1);
				setNavButtonsActiveStat(false);	
				nameTag.gameObject.SetActive(false);
				break;
			}
			currentTab = _tab;
		}
	}
	#endregion

	#region helpers
	void phaseOutToNext(Image _next, int _dir)
	{
		if (current == _next) {
			return;
		}
		const float delay = 0.2f;
		const Ease easeInTYpe = Ease.OutQuad;
		const Ease easeOutTYpe = Ease.InQuad;
		const float offScreenX = 100f;
		foreach(var t in activeTweens) {
			t.Kill(true);
		}
		activeTweens.Clear();
		if (current != null) {
			var currentOrigX = originalPositions[current];
			activeTweens.AddLast(current.DOFade(0, tweenTime)).Value.SetEase(easeOutTYpe);
			current.transform.DOLocalMoveX(currentOrigX, tweenTime).Kill(true);
			activeTweens.AddLast(current.transform.DOLocalMoveX(_dir * offScreenX, tweenTime));
		}
		allowedToSwitch = false;
		activeTweens.AddLast(_next.DOFade(1, tweenTime).SetEase(easeInTYpe)).Value.SetDelay(delay);
		var nextOrigX = originalPositions[_next];
		_next.transform.DOLocalMoveX(-1 * _dir * offScreenX, tweenTime).Kill(true);
		activeTweens.AddLast(_next.transform.DOLocalMoveX(nextOrigX, tweenTime)).Value.SetEase(easeInTYpe).SetDelay(delay).OnComplete(() => {
			allowedToSwitch = true;
		});
		current = _next;
	}

	void updatePPGIndex(int _dir)
	{
		if (allowedToSwitch) {
			_dir = (int)Mathf.Sign(_dir);
			ppgIndex += _dir;
			ppgIndex = (ppgIndex + ppgImages.Length) % ppgImages.Length;
			var girl = getGirlFromIndex(ppgIndex);
			if (!PlayerManager.IsGirlUnlocked(girl)) {
				updatePPGIndex(_dir);
				return;
			} else if (girl == CurrentGirl) {
				return;
			}
			phaseOutToNext(ppgImages[ppgIndex], _dir);
			updateActivePPG (ppgIndex);
		}
	}
	void updateFMIndex(int _dir)
	{
		if (allowedToSwitch) {
			_dir = (int)Mathf.Sign(_dir);
			fmIndex += _dir;
			fmIndex = (fmIndex+ fmImages.Length) % fmImages.Length;
			phaseOutToNext(fmImages[fmIndex], _dir);
		}
	}
	PPG getGirlFromIndex(int _index)
	{
		switch (_index) {
		case 0:
			return PPG.Blossom;
		case 1:
			return PPG.Bubbles;
		case 2:
			return PPG.Buttercup;
		default:
			return PPG.None;
		}
	}
	void updateActivePPG (int _index)
	{
		var next = getGirlFromIndex(_index);
		if (!PlayerManager.IsGirlUnlocked(next)) {
			return;
		}
		PlayerManager.SetActiveGirl(next);
		SaveLoadManager.Save();
		CurrentGirl = next;
		string currentGirl = "";
		switch (_index) {
			case 0:
				CurrentBaseStat = DesignValues.Blossom.BaseStats;	
				CurrentAcquiredStat = PlayerManager.Instance.BlossomAcquiredStats;
			currentGirl = "ブロッサム";
			break;
			case 1:
				CurrentBaseStat = DesignValues.Bubbles.BaseStats;	
				CurrentAcquiredStat = PlayerManager.Instance.BubblesAcquiredStats;
			currentGirl = "バブルス";

				break;
			case 2:
				CurrentBaseStat = DesignValues.Buttercup.BaseStats;	
				CurrentAcquiredStat = PlayerManager.Instance.ButtercupAcquiredStats;
			currentGirl = "バターカップ";

				break;
			default:
				CurrentBaseStat = null;	
				CurrentAcquiredStat = null;
				break;
		}
		if (CurrentGirl != PPG.None) {
			nameTag.text = currentGirl;
			nameTag.DOFade(0, tweenTime).SetEase(Ease.OutCirc).From();
			nameTag.transform.DOLocalMoveY(nameTag.transform.localPosition.y + 25f, tweenTime).SetEase(Ease.OutCirc).From();
		}
		updateSection.BroadcastMessage("OnNewGirlActive", CurrentGirl);
	}
	void setNavButtonsActiveStat(bool _active)
	{
		_active = _active && PlayerManager.NumberofGirlUnlocked() > 1;
		for(int i = 0; i < navButtons.Length; ++i) {
			navButtons[i].gameObject.SetActive(_active);
		}
	}
	#endregion
}
