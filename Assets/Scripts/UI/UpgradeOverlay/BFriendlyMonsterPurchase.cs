﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using TMPro;

public class BFriendlyMonsterPurchase : BBaseUI
{

	[SerializeField] Sprite[] fmSprites;
	[SerializeField] GameObject equippedPfx;
	[SerializeField] TextMeshProUGUI costText;

	static BitArray getArrayFromColumn (int c)
	{
		BitArray array = null;
		switch (c) {
		case 0:
			array = PlayerManager.Instance.UnlockedFighters;
			break;
		case 1:
			array = PlayerManager.Instance.UnlockedMedics;
			break;
		case 2:
			array = PlayerManager.Instance.UnlockedCoaches;
			break;
		case 3:
			array = PlayerManager.Instance.UnlockedGuards;
			break;
		case 4:
			array = PlayerManager.Instance.UnlockedPatrons;
			break;
		}
		return array;
	}

	static void unlockSkin (int c, int r, Image image, GameObject _equippedPfx)
	{
		var array = getArrayFromColumn (c);
		array.Set (r, true);
		BUpgradeOverlay.UpdateButtonAppearance(true, true, image.gameObject, _equippedPfx);
		BGameCenterManager.CheckAllFMs();
	}
	protected override void Start()
	{
		//Set cost text
		costText.text = ""+DesignValues.Costs.FMCost;
		//These are backwards from what they are visually because of line 70, "current.postion = .... "
		const int rows = 6;
		const int cols = 5;
		var first = transform.GetChild(0);
		var firstRT = first.GetComponent<RectTransform>();
		var width = 1.1f * firstRT.rect.width * first.transform.localScale.x;
		var height = 1.1f * firstRT.rect.height * first.transform.localScale.y;
		var startX = firstRT.anchoredPosition.x;
		var startY = firstRT.anchoredPosition.y;
		var images = new List<Image>();
		for(int c = 0; c < cols; ++c) {
			for(int r = 0;r < rows; ++r) {
				RectTransform current;
				if (r == 0 && c == 0) {
					current = firstRT;
				} else {
					var d = Instantiate(first.gameObject, transform, false);
					current = d.GetComponent<RectTransform>();
				}
				current.anchoredPosition = new Vector2(startX + (r * height), startY - (c * width));
				var selectable = current.GetComponent<Button>();
				var selfImage = current.GetComponent<Image>();
				images.Add(selfImage);

				Sprite fmSprite = null;
				FMShopItem currFMInfo = null;
				if(r < getArrayFromColumn(c).Length) {
					//Set the button icons per FM
					var iconImages = selectable.GetComponentsInChildren<Image>(true);
					var fmItemInfos = PlayerEquipsManager.FriendlyMonsters[Utilities.GetEnum<FriendlyMonsterType>(c)];
					currFMInfo = fmItemInfos[r];
					fmSprite = fmSprites.Single(arg => arg.name == currFMInfo.Name);
					foreach(Image iconImage in iconImages) {
						if(iconImage != selfImage) {
							iconImage.sprite = fmSprite;
						}
					}

					var selectedFM = PlayerManager.Instance.CurrentFriendlyMonster;
					var doesCurrentColContainSelected = selectedFM != null && Utilities.GetIndex((FriendlyMonsterType)selectedFM) == c;
					var isCurrentSelected = r == PlayerManager.Instance.CurrentFriendlyMonsterSkinIndex && doesCurrentColContainSelected;
					var closureC = c;
					var closureR = r;
					BCoroutine.WaitAndPerform(() => {
						if (isCurrentSelected) {
							BUpgradeOverlay.UpdateButtonAppearance(true, true, selfImage.gameObject, equippedPfx);
						} else {
							BUpgradeOverlay.UpdateButtonAppearance(getArrayFromColumn(closureC).Get(closureR), false, selfImage.gameObject, equippedPfx);
						}
					}, 0f);
				} else {
					current.gameObject.SetActive(false);
				}
				int row = r;
				int col = c;

				selectable.onClick.AddListener(() => {
					PlayButtonForwardSound ();
					var fmToSelect = Utilities.GetEnum<FriendlyMonsterType>(col);
					var unlockedAlready = getArrayFromColumn(col).Get(row);
					var currencyType = unlockedAlready ? CurrencyType.None : DesignValues.Costs.FMCurrency;
					var cost = (uint)(unlockedAlready ? 0 : DesignValues.Costs.FMCost);
					var selectedCurrentlyEquipped = PlayerManager.Instance.CurrentFriendlyMonster == fmToSelect &&
						PlayerManager.Instance.CurrentFriendlyMonsterSkinIndex == row;
					System.Action confirmCallback = () => {
						if (!unlockedAlready) {
							unlockSkin (col, row, selfImage, equippedPfx);
						}
						//unselect that previous one
						if (PlayerManager.Instance.CurrentFriendlyMonster != null) {
							var oldC = Utilities.GetIndex((FriendlyMonsterType)PlayerManager.Instance.CurrentFriendlyMonster);
							var ind = (oldC * rows) + PlayerManager.Instance.CurrentFriendlyMonsterSkinIndex;//yes, this is transposed and for good reason. We are looping column-major, but the neum definiion is layed our row-major
							BUpgradeOverlay.UpdateButtonAppearance(true, false, images[ind].gameObject, equippedPfx);
						}
						if (selectedCurrentlyEquipped) {
							PlayerManager.Instance.CurrentFriendlyMonster = null;
							PlayerManager.Instance.CurrentFriendlyMonsterSkinIndex = -1;
							BUpgradeOverlay.UpdateButtonAppearance(true, false, selfImage.gameObject, equippedPfx);
							equippedPfx.SetActive(false);
						} else {
							PlayerManager.Instance.CurrentFriendlyMonster = fmToSelect;
							PlayerManager.Instance.CurrentFriendlyMonsterSkinIndex = row;
							BUpgradeOverlay.UpdateButtonAppearance(true, true, selfImage.gameObject, equippedPfx);
						}
					};
					string desc = string.Format("仲間のモンスターは <color=#FF5B35FF>{0}</color> class. {1}", currFMInfo.Type, currFMInfo.Description);
					BUpgradeConfirm.InitBuyOrEquipCanvas (currFMInfo.Name, desc, fmSprite, currencyType, cost, confirmCallback, false, selectedCurrentlyEquipped);
				});

			}
			startX -= 0.48f * width;
		}
	}
}
