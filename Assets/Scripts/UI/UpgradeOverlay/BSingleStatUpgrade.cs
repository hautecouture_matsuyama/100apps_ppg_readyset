﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;

public class BSingleStatUpgrade : BBaseUI
{
	const int maxStat = 8; //a known number by us humans (lol)

	[SerializeField] RectTransform statRoot;
	[SerializeField] StatType stat;
	[SerializeField] RectTransform statBacking;
	[SerializeField] Button addButton;
	[SerializeField] GirlUpgradeVOData girlVOData;

	bool statChunksCreated = false;

	protected override void Start()
	{
		StartCoroutine (InitStatBars ());
	}
	IEnumerator InitStatBars()
	{
		const float padding = -5;
		var first = statRoot.transform.GetChild(0).gameObject;
		for(int i = 1; i < 8; ++i) {
			var next = Instantiate(first, statRoot.transform, false);
			var prev = statRoot.transform.GetChild(i -1) as RectTransform;
			var rt = next.GetComponent<RectTransform>();
			rt.anchoredPosition = prev.anchoredPosition + (Vector2.right * prev.rect.width) + (Vector2.right * padding);
		}
		yield return null;//wait for BCharacterSelect current stat to be set
		statChunksCreated = true;
		disableButtonIfStatIsFull();
	}
	public void IncreaseStat()
	{
		var modifiedCost = DesignValues.Costs.GetCostForStat(BCharacterSelect.CurrentAcquiredStat);
		string name = "";

		string desc = "アップグレードを開始";
		switch(stat) {
		case StatType.Health:
			name = "体力";
			desc = "HPの最大値を上昇させます。";
			break;
		case StatType.Stamina:
			name = "スタミナ";
			desc = "STの最大値を上昇させます。";
			break;
		case StatType.Attack:
			name = "アタック";
			desc = "攻撃力を上昇させます。";
			break;
		case StatType.Block:
			name = "ブロック";
			desc = "ガードした時のスタミナ減少を抑えます。";
			break;
		default:
			break;
		}
		PlayButtonForwardSound ();
		BUpgradeConfirm.InitBuyOrEquipCanvas(name, desc, null, DesignValues.Costs.StatCurrency, modifiedCost, ()=> {
			//check currency
			BCharacterSelect.CurrentAcquiredStat.IncrementStat(stat);
			var index = (int)BCharacterSelect.CurrentBaseStat[stat] + BCharacterSelect.CurrentAcquiredStat[stat];
			var current = statRoot.transform.GetChild(index - 1).GetComponent<Image>();
			current.color = Color.white;
			disableButtonIfStatIsFull();
			if(Random.value <= 0.66f) {
				BAudioManager.Instance.CreateAndPlayAudio(girlVOData.GetRandomClipForGirl(BCharacterSelect.CurrentGirl));
			}
		});
	}

	void OnNewGirlActive(PPG _currentGirl)
	{
		StartCoroutine(fitStatBarAndAnimateUp());
	}

	void disableButtonIfStatIsFull()
	{
		if (BCharacterSelect.CurrentAcquiredStat != null && BCharacterSelect.CurrentAcquiredStat.IsStatMaxed(stat)) {
			addButton.gameObject.SetActive(false);
		} else {
			addButton.gameObject.SetActive(true);
		}
	}

	IEnumerator fitStatBarAndAnimateUp()
	{
		while(!statChunksCreated) {
			yield return null;
		}
		disableButtonIfStatIsFull();
		if (BCharacterSelect.CurrentGirl != PPG.None) {
			var total = BCharacterSelect.CurrentBaseStat[stat] + 3;
			int offset = 185; //This number is the front padding (needed for the label and the first chunk), plus the back padding (just a wee)
			statBacking.sizeDelta = new Vector2(offset + ((total - 1) * 75), statBacking.sizeDelta.y);
			var acquired = BCharacterSelect.CurrentBaseStat[stat] + BCharacterSelect.CurrentAcquiredStat[stat];
			for(int i = 0; i < maxStat; ++i) {
				var current = statRoot.transform.GetChild(i).GetComponent<Image>();
				if (i < total) {
					current.enabled = true;
					current.color = new Color(0.3f, 0.3f, 0.3f, 1);	
				} else {
					current.enabled = false;
				}
			}
			for(int i = 0; i <= total && i < statRoot.transform.childCount; ++i) {
				yield return new WaitForSeconds(0.1f);
				var current = statRoot.transform.GetChild(i).GetComponent<Image>();
				if (i < acquired) {
					current.color = Color.white;	
				} else {
					break;
				}
			}
		}
	}
}
