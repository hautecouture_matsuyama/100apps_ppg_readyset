﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public enum CurrencyType
{
	None,
	Gold,
	Shard
}

public class BUpgradeConfirm : BBaseOverlay
{
	public static BUpgradeConfirm Instance{get; private set;}

	public static void InitBuyOrEquipCanvas(string _header, string _body, Sprite _itemSprite, CurrencyType _type, uint _cost, System.Action _onConfirm, bool _hideButton = false, bool _setAsUnequip = false)
	{
		Instance.gameObject.SetActive(true);
		Instance.animateInScale();
		if (_itemSprite == null) {
			Instance.noImageHeader.text = _header;
			Instance.noImageBody.text = _body;
			Instance.imageLayoutParent.SetActive (false);
			Instance.noImageLayoutParent.SetActive (true);
		} else {
			Instance.itemImage.sprite = _itemSprite;
			Instance.header.text = _header;
			Instance.body.text = _body;
			Instance.noImageLayoutParent.SetActive (false);
			Instance.imageLayoutParent.SetActive (true);
		}
		if (_type != CurrencyType.None) {
			foreach(Image image in Instance.currencyImages) {
				image.sprite = _type == CurrencyType.Gold ? Instance.goldSprite : Instance.shardSprite;
			}
		} else {
			foreach(Image image in Instance.currencyImages) {
				image.sprite = null;
			}
		}
		Instance.confirm.gameObject.SetActive(true);
		var usesGoldButNotEnough = (_type == CurrencyType.Gold && PlayerManager.GoldCount < _cost);
		var usesShardButNotEnough = (_type == CurrencyType.Shard && PlayerManager.HeartShardCount < _cost);
		if (_hideButton) {
			Instance.confirm.gameObject.SetActive(false);
		} else {
			Instance.confirm.gameObject.SetActive(true);
			var buttonImages = Instance.confirm.GetComponentsInChildren<Image>();
			if ( _cost > 0 && (usesGoldButNotEnough || usesShardButNotEnough) ) {
				Instance.confirm.interactable = false;
				foreach(Image image in buttonImages) {
					image.color = Color.grey;
				}
				foreach(TextMeshProUGUI text in Instance.buttonTexts) {
					text.color = Color.grey;
				}
			} else {
				Instance.confirm.interactable = true;
				foreach(Image image in buttonImages) {
					image.color = Color.white;
				}
				foreach(TextMeshProUGUI text in Instance.buttonTexts) {
					text.color = Color.white;
				}
			}
			if(_cost == 0) {
				Instance.confirm.GetComponent<BShiftButtonIconWithPress>().disableIconShifting = true;
				foreach(TextMeshProUGUI text in Instance.buttonTexts) {
					text.text = _setAsUnequip ? "装備解除！" : "装備";
				}
				foreach(Image image in Instance.currencyImages) {
						image.gameObject.SetActive(false);
				} 
			} else {
				Instance.confirm.GetComponent<BShiftButtonIconWithPress>().disableIconShifting = false;
				foreach(TextMeshProUGUI text in Instance.buttonTexts) {
					text.text = Utilities.FormatTextForCount(_cost, 0, false);
				}
				Instance.currencyImages[0].gameObject.SetActive(true);
			}

			Instance.confirm.onClick.AddListener(() => {
				if (_cost > 0) {
					if (_type == CurrencyType.Gold) {
						PlayerManager.GoldCount -= _cost;
						BAudioManager.Instance.CreateAndPlayAudio(goldPurchase);
					} else if (_type == CurrencyType.Shard) {
						PlayerManager.HeartShardCount -= _cost;
						BAudioManager.Instance.CreateAndPlayAudio(heartPurchase);
					}
				} else {
					Instance.PlayButtonForwardSound();
				}
				_onConfirm();
				SaveLoadManager.Save();
				Instance.animateOutScale().OnComplete(() => Instance.gameObject.SetActive (false));
				Instance.confirm.onClick.RemoveAllListeners();
				Instance.cancel.onClick.RemoveAllListeners();
			});
		}

		Instance.cancel.onClick.AddListener(() => {
			Instance.animateOutScale().OnComplete(() => Instance.gameObject.SetActive (false));
			Instance.PlayButtonBackwardSound();
			Instance.confirm.onClick.RemoveAllListeners();
			Instance.cancel.onClick.RemoveAllListeners();
		});
	}

	[SerializeField] GameObject imageLayoutParent;
	[SerializeField] TextMeshProUGUI header;
	[SerializeField] TextMeshProUGUI body;
	[SerializeField] Image itemImage;
	[SerializeField] GameObject noImageLayoutParent;
	[SerializeField] TextMeshProUGUI noImageHeader;
	[SerializeField] TextMeshProUGUI noImageBody;
	[SerializeField] Image[] currencyImages;
	[SerializeField] TextMeshProUGUI[] buttonTexts;
	[SerializeField] Button confirm;
	[SerializeField] Button cancel;

	[SerializeField]
	Sprite goldSprite;
	[SerializeField]
	Sprite shardSprite;


	static AudioClip goldPurchase;
	static AudioClip heartPurchase;

	protected override void Awake ()
	{
		if(goldPurchase == null) {
			goldPurchase = Resources.Load<AudioClip>("Audio/SFX/gold_purchase");
		}
		if(heartPurchase == null) {
			heartPurchase = Resources.Load<AudioClip>("Audio/SFX/heart_purchase");
		}
		base.Awake ();
		Instance = this;
		gameObject.SetActive(false);
	}

	void OnDestroy()
	{
		Instance = null;
	}
	protected override void onEscape ()
	{
		if (cancel.onClick != null) {
			cancel.onClick.Invoke();
		}
	}
}
