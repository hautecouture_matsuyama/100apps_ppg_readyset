﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class BTitleOverlay : BBaseOverlay
{
	public static bool HasSeenAgreement = false;

	[SerializeField] AudioClip titleMusic;
	[SerializeField] GameObject blackBGForWebview;
	[SerializeField] GameObject agreementPrefab;
	[SerializeField] GameObject inputBlocker;

	//Items for tweens and anims
	[SerializeField] RectTransform blossom;
	[SerializeField] RectTransform buttercup;
	[SerializeField] RectTransform bubbles;
	[SerializeField] Image gameLogo;
	[SerializeField] Image ppgLogo;
	[SerializeField] GameObject playButton;
	[SerializeField] RectTransform settingsButton;
	[SerializeField] RectTransform moreGamesButton;

	UniWebView webView;
	string backupURL = "privacypolicy.html#textBox_priv_a.html";

	protected override void Start ()
	{
		base.Start ();
		BAudioManager.Instance.PlayMusic(titleMusic, null, 0, 0.7f);
		//Block all input if the player hasn't accepted pp yet, so they can't skip past title while anims are happening
		if (!HasSeenAgreement) {
			inputBlocker.SetActive(true);
		}
		//Kickoff the anims
		StartCoroutine(doTitleAnimations());
		DFPManager.Instance.Request ();
	}

	void OnDestroy()
	{
		destroyWebView();	
	}

	void OnApplicationPause( bool lostFocus )
	{
		if(lostFocus && webView != null && !disablingWebView) {
			disablingWebView = true;
			webView.Hide (false, UniWebViewTransitionEdge.Bottom, 0.3f);
			blackBGForWebview.SetActive (false);
			destroyWebView();
			disablingWebView = false;
		}
	}

	IEnumerator doTitleAnimations()
	{
		//Start fade in after a delay, because of the way the music is
		yield return new WaitForSeconds(0.5f);
		BScreenFade.StartFadeToClear(1.5f, true, null, 0.25f);
		//Set object init states
		gameLogo.DOFade(0,0).Kill(true);
		ppgLogo.DOFade(0,0).Kill(true);
		blossom.DOShakeAnchorPos(10f, 1, 15, 90, false, false).SetLoops(-1, LoopType.Incremental);
		buttercup.DOAnchorPosY(15, 0.8f).SetRelative().SetEase(Ease.InOutQuad).SetLoops(-1, LoopType.Yoyo);
		bubbles.DOAnchorPosY(-5, 0.5f).SetRelative().SetEase(Ease.InOutQuad).SetLoops(-1, LoopType.Yoyo);
		playButton.transform.DOScale(0,0).Kill(true);
		settingsButton.gameObject.SetActive(false);
		moreGamesButton.gameObject.SetActive(false);

		//Wait then fade in logos and pop play button
		yield return new WaitForSeconds(2.25f);
		gameLogo.DOFade(1.25f, 1f).SetEase(Ease.Linear);
		ppgLogo.DOFade(1.25f, 1f).SetEase(Ease.Linear).SetDelay(0.3f);
		playButton.transform.DOScale(1f, 0.4f).SetEase(Ease.OutBack).SetDelay(0.75f).OnComplete(() => {
			playButton.transform.DOScale(0.95f, 1.1f).SetEase(Ease.InOutQuad).SetLoops(-1, LoopType.Yoyo);
		});
		settingsButton.DOAnchorPosX(-settingsButton.rect.width, 0.25f).SetEase(Ease.OutBack).SetDelay(1f).From().SetRelative().OnStart(() => {
			settingsButton.gameObject.SetActive(true);
		});
		moreGamesButton.DOAnchorPosX(moreGamesButton.rect.width, 0.25f).SetEase(Ease.OutBack).SetDelay(1.2f).From().SetRelative().OnStart(() => {
			moreGamesButton.gameObject.SetActive(true);
		});

		if (!HasSeenAgreement) {
			yield return new WaitForSeconds(2f); 
			BFirsttimeLegalOverlay.LaunchFirsttimeLegalOverlay().TitleOverlay = this;
			//Turn off the input blocker that is on if player hasn't accepted pp yet
			inputBlocker.SetActive(false);
		}
	}

	#region Button Callbacks
	public void Play()
	{
		DFPManager.Instance.HideBanner ();

		if(!BTutorialManager.InTutorial) {
			GoToUpgrade();
		} else {
			SaveLoadManager.Save();
			GoToGameplay();
		}
	}

	public void MoreGames()
	{
		Application.OpenURL("http://www.cartoonnetwork.com/mobile");
	}

	public void Options()
	{
		BOptionsAndSettingsOverlay.LaunchOptionsOverlay().TitleOverlay = this;
		DFPManager.Instance.HideBanner ();
	}

	public void OpenPrivacyPolicy()
	{
		if (webView != null) {
			destroyWebView ();
		}
		webView = createWebView();
		backupURL = "privacypolicy.html#textBox_priv_a";
		#if UNITY_EDITOR
		webView.url = "https://crystalfishgames.com/privacypolicy/index.html";
		#else
		webView.url = "https://www.cartoonnetwork.com/legal/priv_tou.html#textBox_priv_a";
		#endif
		webView.CleanCache();
		webView.Load (); 
		webView.Show (false, UniWebViewTransitionEdge.None, 0f);
		StartCoroutine (waitForSlideIn ());
	}
	public void OpenTermsOfUse()
	{
		if (webView != null) {
			destroyWebView ();
		}
		webView = createWebView();
		backupURL = "privacypolicy.html#textBox_tou_a";
		#if UNITY_EDITOR
		webView.url = "https://crystalfishgames.com";
		#else
		webView.url = "https://www.cartoonnetwork.com/legal/priv_tou.html#textBox_tou_a";
		#endif
		webView.CleanCache();
		webView.Load (); 
		webView.Show (false, UniWebViewTransitionEdge.None, 0f);
		StartCoroutine (waitForSlideIn ());
	}
	#endregion

	#region WebView Helpers
	bool disablingWebView = false;
	public void DismissWebView()
	{
		if (!disablingWebView) {
			disablingWebView = true;
			webView.Hide (false, UniWebViewTransitionEdge.Bottom, 0.3f);
			blackBGForWebview.SetActive (false);
			StartCoroutine(waitForSlideOut());
		}
	}

	void OnLoadComplete(UniWebView _webView, bool _success, string _errorMessage) {
		if (_success) {
			//Do nothing, yay!
		} else {
			//GetComponent<BWidgetLauncher>().LaunchWidget();
			loadFromFile();
			Debug.Log("Something wrong in web view loading: " + _errorMessage);
		}
	}

	void loadFromFile()
	{
		destroyWebView();
		webView = createWebView();
		webView.url = UniWebViewHelper.streamingAssetURLForPath(backupURL);
		webView.Load();
		webView.Show(false, UniWebViewTransitionEdge.None, 0f);
	}

	UniWebView createWebView() 
	{
		var webViewGameObject = GameObject.Find("WebView");
		if (webViewGameObject == null) {
			webViewGameObject = new GameObject("WebView");
		}

		var newWebView = webViewGameObject.AddComponent<UniWebView>();

		int topInset = (int)(Screen.height * 0.13f);
		newWebView.insets = new UniWebViewEdgeInsets(topInset,0,0,0);
		newWebView.SetShowSpinnerWhenLoading(true);
		newWebView.OnLoadComplete += OnLoadComplete;

		return newWebView;
	}

	void destroyWebView()
	{
		if(webView != null){
			webView.OnLoadComplete -= OnLoadComplete;
			Destroy (webView);
		}
	}
	IEnumerator waitForSlideIn()
	{
		yield return new WaitForSeconds (0.3f);
		blackBGForWebview.SetActive (true);
	}
	IEnumerator waitForSlideOut()
	{
		yield return new WaitForSeconds (0.3f);
		destroyWebView();
		disablingWebView = false;
	}
	#endregion

	protected override void onEscape ()
	{
		if (BOptionsAndSettingsOverlay.Instance == null || !BOptionsAndSettingsOverlay.Instance.gameObject.activeSelf) {
			BGenericConfirmOverlay.SpawnConfirm("Close Game?", "Are you sure you want to leave?", "close", () => {
				#if UNITY_EDITOR
				UnityEditor.EditorApplication.isPlaying = false;
				#elif UNITY_ANDROID
				Application.Quit();
				#endif
			}, setCanCallEscapeTrue);
		} else {
			setCanCallEscapeTrue();
		}
	}
}
