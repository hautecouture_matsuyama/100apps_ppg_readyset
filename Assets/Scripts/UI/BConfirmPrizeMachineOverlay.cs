﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class BConfirmPrizeMachineOverlay : BBaseOverlay
{
	public static void SpawnConfirm(System.Action _onConfirm, System.Action _onCancel)
	{
		if (PlayerManager.GoldCount < DesignValues.Costs.WinAPrizeCost) {
			return;
		}

		var prefab = Resources.Load<GameObject>(@"Prefabs/UI/ConfirmPrizeMachineOverlay");
		var Instance = Instantiate(prefab).GetComponent<BConfirmPrizeMachineOverlay>();
		Instance.costText.text = string.Format("{0}", DesignValues.Costs.WinAPrizeCost);
		Instance.gameObject.SetActive(true);
		Instance.animateInScale();

		Instance.confirm.gameObject.SetActive(true);

		Instance.confirm.onClick.AddListener(() => {
			if (_onConfirm != null) {
				_onConfirm();
			}
			Instance.animateOutScale().OnComplete(() => Destroy (Instance.gameObject));
			Instance.confirm.onClick.RemoveAllListeners();
			Instance.cancel.onClick.RemoveAllListeners();
		});
		Instance.cancel.onClick.AddListener(() => {
			if (_onCancel != null) {
				_onCancel();
			}
			Instance.animateOutScale().OnComplete(() => Destroy(Instance.gameObject));
			Instance.confirm.onClick.RemoveAllListeners();
			Instance.cancel.onClick.RemoveAllListeners();
		});
	}
	[SerializeField] Button confirm;
	[SerializeField] Button cancel;
	[SerializeField] TextMeshProUGUI costText;

	protected override void onEscape ()
	{
		if (cancel.onClick != null) {
			cancel.onClick.Invoke();
		}
	}
}