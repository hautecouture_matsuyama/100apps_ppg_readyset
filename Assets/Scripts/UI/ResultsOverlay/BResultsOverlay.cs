﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;

public class BResultsOverlay : BBaseOverlay
{
	public static BResultsOverlay Instance{get; private set;}

	[SerializeField] RectTransform rightPanel;
	[SerializeField] RectTransform scoreText;
	[SerializeField] RectTransform progressSection;

	[SerializeField] RectTransform watchAd;
	[SerializeField] GameObject watchAdCurrencyImage;
	[SerializeField] GameObject watchAdGiftImage;
	[SerializeField] RectTransform freeGift;
	[SerializeField] TextMeshProUGUI freeGiftText;
	[SerializeField] GameObject freeGiftIcon;
	[SerializeField] RectTransform winAPrize;
	[SerializeField] TextMeshProUGUI winAPrizeText;
	[SerializeField] GameObject winAPrizeCurrencyImage;
	[SerializeField] GameObject winAPrizeGiftImage;
	[SerializeField] GameObject winAPrizeButton;

	[SerializeField] RectTransform homeButton;
	[SerializeField] RectTransform upgradeButton;
	[SerializeField] RectTransform playButton;

	[SerializeField] Transform globe;
	[SerializeField] Transform tick;
	[SerializeField] Transform flag;
	[SerializeField] GameObject blossom;
	[SerializeField] GameObject bubbles;
	[SerializeField] GameObject buttercup;
	[SerializeField] TextMeshProUGUI loopMultiplier;
	[SerializeField] GameObject loopBurstPfx;
	[SerializeField] BScoreTick bestScoreTicker;

	public static bool FreezeBackButton = true;

	bool earnCoins;
	bool justGaveAdReward;
	bool tickFreeGiftTimer;
	uint previousGoldCount;
	protected override void Awake ()
	{
		base.Awake();
		Instance = this;
		gameObject.SetActive(false);
		var canvas = GetComponent<Canvas> ();
		canvas.worldCamera = GameObject.FindGameObjectWithTag ("UICamera").GetComponent<Camera>();
		canvas.sortingLayerName = "HUD";
		canvas.planeDistance = 100;

	}

	public static int gameCnt = 0;

	protected override void Start()
	{
		createAllTicks(tick);
		bestScoreTicker.OldBestScore = (int)PlayerManager.BestScore;
		if (PlayerManager.BestScore < PlayerManager.CurrentScore) {
			PlayerManager.BestScore = PlayerManager.CurrentScore;
		}
		BGameCenterManager.ReportScore();
		animateContentsIn ();
		//Make sure to set this to empty string so that Update can do FUN things wtih the timer
		if (BDebugOverlay.ForceFreeGiftOnNextDeath) {
			enableFreeGiftBanner();
		} else if (System.DateTime.UtcNow - GiftSpawner.Instance.TimeSinceLastFreeGift < GiftSpawner.HoursTillFreeGift[GiftSpawner.Instance.FreeGiftWaitTier]) {
			StartCoroutine(disableFreeGiftBanner (false));
		}
		earnCoins = true;
		watchAdCurrencyImage.SetActive(earnCoins);
		watchAdGiftImage.SetActive(!earnCoins);
		StartCoroutine(BInternetConnection.IsInternetAvailable((isConnected) => {
			//if(!isConnected || GiftSpawner.DecrementRoundsSinceLastEarnOpportunity() > 0) {
			if(!isConnected || Random > 0) {
				disableEarnBanner ();
				watchAd.gameObject.SetActive(false);
			} else {
				//Decide whether Earn opp will be giving away Coins or unlockables
				if(!GiftAvailability.AnyAurasRemaining() && !GiftAvailability.AnyFriendlyMonsterRemaining() ) {
					earnCoins = true;
				} else {
					earnCoins = Random.Range(0f,1f) <= 0.8f ? true : false;
				}
				watchAdCurrencyImage.SetActive(earnCoins);
				watchAdGiftImage.SetActive(!earnCoins);
			}
		}));
	}
	void OnDestroy()
	{
		Instance = null;
	}
	protected override void Update()
	{
		base.Update();
		if (tickFreeGiftTimer && updateTimerTextAndCheckIfDone()) {
			enableFreeGiftBanner();
		}
		if (previousGoldCount != PlayerManager.GoldCount) {
			const int minGoldCount = 200;
			previousGoldCount = PlayerManager.GoldCount;
			if (previousGoldCount < minGoldCount) {
				int score = 200 - (int)PlayerManager.GoldCount;
				winAPrizeText.text = score+"不足";
				winAPrizeButton.GetComponent<Button>().interactable = false;
				winAPrizeCurrencyImage.SetActive(true);
				winAPrizeGiftImage.SetActive(false);
			} else {
				winAPrizeText.text = "";
				winAPrizeButton.GetComponent<Button>().interactable = true;
				winAPrizeCurrencyImage.SetActive(false);
				winAPrizeGiftImage.SetActive(true);
			}
		}
	}

	void positionScoreText (float delayCount)
	{
		var child1 = scoreText.GetChild (0) as RectTransform;
		var child2 = child1.GetChild (0) as RectTransform;
		var bound1 = RectTransformUtility.CalculateRelativeRectTransformBounds (child1);
		var bound2 = RectTransformUtility.CalculateRelativeRectTransformBounds (child2);
		var parentBound = RectTransformUtility.CalculateRelativeRectTransformBounds(scoreText);

		var combined = new Bounds();
		combined.Encapsulate(bound1);
		combined.Encapsulate(bound2);
		combined.Encapsulate(parentBound);

		scoreText.DOAnchorPosY (combined.size.y, tweenTime).From ().SetDelay (tweenDelayTime * delayCount).SetEase (Ease.OutBack).SetUpdate (true);
	}

	void animateContentsIn ()
	{
		checkIfAllThingsUnlocked();
		FreezeBackButton = true;
		float delayCount = 0f;
		//BG fade
		bg.DOFade(0, tweenTime).From ().SetDelay (tweenDelayTime * delayCount).SetEase (Ease.Linear).SetUpdate (true);
		delayCount += 2f;
		//Panels and groupings
		rightPanel.DOAnchorPosX (1.7f * rightPanel.rect.width, tweenTime).From ().SetDelay (tweenDelayTime * delayCount).SetEase (Ease.OutSine).SetUpdate (true);
		++delayCount;
		positionScoreText (delayCount);
		++delayCount;
		var progressSectionDelay = tweenDelayTime * delayCount;
		progressSection.DOAnchorPosX (-(progressSection.rect.width + globe.GetComponent<RectTransform> ().rect.width), tweenTime).From ().SetDelay (progressSectionDelay).SetEase (Ease.OutBack).SetUpdate (true);
		var animateDelay = progressSectionDelay + tweenTime;
		setGlobalProgressMap(globe, flag, blossom, bubbles, buttercup, loopMultiplier, loopBurstPfx, animateDelay);
		++delayCount;
		//Banners
		if (watchAd.gameObject.activeInHierarchy) {
			watchAd.DOAnchorPosX (watchAd.rect.width * 1.31f, tweenTime).From ().SetDelay (tweenDelayTime * delayCount).SetEase (Ease.OutBack).SetUpdate (true);
			++delayCount;
		}
		freeGift.DOAnchorPosX (watchAd.rect.width * 1.48f, tweenTime).From ().SetDelay (tweenDelayTime * delayCount).SetEase (Ease.OutBack).SetUpdate (true);
		++delayCount;
		if (winAPrize.gameObject.activeInHierarchy) { 
			winAPrize.DOAnchorPosX (watchAd.rect.width * 1.65f, tweenTime).From ().SetDelay (tweenDelayTime * delayCount).SetEase (Ease.OutBack).SetUpdate (true);
			++delayCount;
		}
		//Lower Buttons
		float buttonTweenDelay = 0.15f;
		homeButton.DOScale (0, tweenTime).From ().SetDelay (buttonTweenDelay * delayCount).SetEase (Ease.OutBack).SetUpdate (true);
		++delayCount;
		playButton.DOScale (0, tweenTime).From ().SetDelay (buttonTweenDelay * delayCount).SetEase (Ease.OutBack).SetUpdate (true);
		++delayCount;
		upgradeButton.DOScale (0, tweenTime).From ().SetDelay (buttonTweenDelay * delayCount).SetEase (Ease.OutBack).SetUpdate (true).OnComplete(() => {
			FreezeBackButton = false;
		});
	}

	bool updateTimerTextAndCheckIfDone()
	{
		var timeRemaining = GiftSpawner.HoursTillFreeGift[GiftSpawner.Instance.FreeGiftWaitTier] - (System.DateTime.UtcNow - GiftSpawner.Instance.TimeSinceLastFreeGift);
		var hours = timeRemaining.Hours;
		var minutes = timeRemaining.Minutes;
		var seconds = timeRemaining.Seconds;
		if (timeRemaining.TotalSeconds > 0) {
			if (hours > 0) {
				freeGiftText.text = string.Format("次のプレゼントまで {0}時間  {1}分", hours, minutes);
			} else if (minutes > 0) {
				freeGiftText.text = string.Format("次のプレゼントまで {0}分  {1}秒", minutes, seconds);
			} else {
				freeGiftText.text = string.Format("次のプレゼントまで {0}秒", seconds);
			}
		}
		return timeRemaining.TotalSeconds <= 0;
	}

	public void PlayEarnAd()
	{
		AppLovin.SetUnityAdListener (this.gameObject.name);

		FreezeBackButton = true;
		GiftSpawner.ResetRoundsSinceLastEarnOpportunity();
		disableEarnBanner();
			Utilities.DisableRenderingForAds();
			BAudioManager.Instance.ForceMuteForAds(true);
			DisableAllButtons();
		if(AppLovin.IsIncentInterstitialReady()){
			AppLovin.ShowRewardedInterstitial ();
		}
			
			//FreewheelManager.RequestAd(AdType.Preroll, OnAdWatched);

			#if UNITY_EDITOR
			StartCoroutine(Utilities.WaitForFakeAdTime());
			#endif
	}

	void onAppLovinEventReceived(string ev) {
		if(ev.Contains("REWARDAPPROVEDINFO")) {
			Debug.Log ("よばれた");
			// 動画が再生された
		} else if(ev.Contains("LOADEDREWARDED")) {
			// 読み込み完了
		} else if(ev.Contains("LOADREWARDEDFAILED")) {  
			// 読み込み失敗
		} else if(ev.Contains("HIDDENREWARDED")) {
			//動画の表示し終わり、非表示にされた
			Debug.Log ("をわｒ");

			OnAdWatched ();
			// 次の動画の準備
			AppLovin.LoadRewardedInterstitial();
		}
	}


	public void OnAdWatched()
	{
			FreezeBackButton = false;
			BAudioManager.Instance.ForceMuteForAds (false);
			Utilities.EnableRenderingForAds();
			justGaveAdReward = true;
			EnableAllButtons ();
			BGiftOverlay.SpawnGift(GiftSpawner.GetItemForEarnResults(earnCoins));
	}

	public void PlayFreeGift()
	{
		FreezeBackButton = true;
		if (BDebugOverlay.ForceFreeGiftOnNextDeath) {
			BDebugOverlay.ForceFreeGiftOnNextDeath = false;
		} else {
			GiftSpawner.ResetTimeSinceLastFreeGift();
		}
		StartCoroutine(disableFreeGiftBanner(true));
		BGiftOverlay.SpawnGift(GiftSpawner.GetItemForFreeGift());
	}

	public void PlayWinAPrize()
	{
		BScreenFade.StartFadeToBlack(0.3f, false, ()=> {
			BWinAPrizeOverlay.SpawnWinAPrize();
			BScreenFade.StartFadeToClear(0.3f, false);
		});
	}
	void enableFreeGiftBanner ()
	{
		tickFreeGiftTimer = false;
		freeGiftIcon.SetActive(true);
		foreach (var ch in freeGift.GetComponentsInChildren<UnityEngine.UI.Selectable> ()) {
			ch.interactable = true;
		}
		freeGiftText.text = "プレゼント!";
	}
	IEnumerator disableFreeGiftBanner (bool _waitForGiftSpawn)
	{
		tickFreeGiftTimer = true;
		foreach (var ch in freeGift.GetComponentsInChildren<Selectable> ()) {
			ch.interactable = false;
		}
		yield return new WaitForSeconds(_waitForGiftSpawn ? 1.0f : 0f);
		updateTimerTextAndCheckIfDone();
		freeGiftIcon.SetActive(false);
	}
	void disableEarnBanner ()
	{
		foreach (var ch in watchAd.GetComponentsInChildren<Selectable> ()) {
			ch.interactable = false;
		}
		watchAd.DOAnchorPosX (watchAd.rect.width * 1.31f, tweenTime).SetEase (Ease.InBack).SetUpdate (true);
	}
	protected override void onEscape ()
	{
		if(!FreezeBackButton) {
			GoToTitle();
		}
	}

	void checkIfAllThingsUnlocked()
	{
		var allFMs = !GiftAvailability.AnyFriendlyMonsterRemaining();
		var allSupers = PlayerManager.IsSuperAttackUnlocked(SuperAttack.BlissBlitz) && PlayerManager.IsSuperAttackUnlocked(SuperAttack.TornadoTrio);
		var allStats = true;
		var girls = new []{PPG.Blossom, PPG.Bubbles, PPG.Buttercup};
		foreach(var girl in girls) {
			var stat = PlayerManager.Instance.GetAcquiredStatForGirl(girl);
			allStats &= stat.AllAurasUnlocked() && stat.AuraUnlocked && stat.X2Unlocked && stat.X3Unlocked;
			if (!allStats) {
				break;
			}
		}
		winAPrize.gameObject.SetActive(!(allFMs && allStats && allSupers));
	}
}
