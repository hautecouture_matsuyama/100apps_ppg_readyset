﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class BOverlayParallax : MonoBehaviour
{
	[System.Serializable]
	public class Layer
	{
		public int Direction;
		public int Count;
		public RandomRangedValue SpeedRange;
		public RandomRangedValue SizeRange;
	}

	[SerializeField] Image baseImage;
	[SerializeField] Layer[] parallaxLayer;

	CanvasScaler parentScalar;
	Dictionary<Transform, Tweener> tweenMapping;
	bool doneInit = false;

	#region Mono callbacks
	void Awake()
	{
		parentScalar = GetComponentInParent<CanvasScaler>();
	}
	void Start()
	{

		if (parallaxLayer.Length > 0) {
			initOverlay ();
		} else {
			Destroy(gameObject);
		}
	}
	void OnDestroy()
	{
		if (tweenMapping != null) {
			foreach(var kv in tweenMapping){
				kv.Value.Kill();
			}
			tweenMapping.Clear();
		}
	}
	#endregion

	#region Public
	public void StopMotion(float _time)
	{
		foreach(var kv in tweenMapping) {
			var graphic = kv.Key.GetComponent<Graphic>();
			if (graphic != null) {
				var tween = graphic.DOFade(0f, _time).SetEase(Ease.Linear).SetUpdate(true).OnComplete(() => {
					kv.Value.Pause();
					kv.Key.gameObject.SetActive(false);
				});
				if(Mathf.Approximately(_time, 0f)) {
					tween.Kill(true);
				}
			}
		}
		enabled = false;
	}
	public void StartMotion(float _time)
	{
		initOverlay();
		enabled = true;
		foreach(var kv in tweenMapping) {
			kv.Value.Play();
			kv.Key.gameObject.SetActive(true);
			var graphic = kv.Key.GetComponent<Graphic>();
			if (graphic != null) {
				graphic.DOFade(1f, _time).SetEase(Ease.Linear).SetUpdate(true);
			}
		}
	}
	#endregion

	#region Helpers

	void initOverlay ()
	{
		if (doneInit) {
			return;
		} 
		doneInit = true;
		tweenMapping = new Dictionary<Transform, Tweener> ();
		var dim = parentScalar.referenceResolution;
		for (int i = 0; i < parallaxLayer.Length; ++i) {
			var curLayer = parallaxLayer [i];
			for (int j = 0; j < curLayer.Count; ++j) {
				RectTransform rect = null;
				if (i == 0 && j == 0) {
					rect = baseImage.rectTransform;
				}
				else {
					var copy = Object.Instantiate (baseImage.gameObject, baseImage.transform.parent);
					rect = copy.GetComponent<RectTransform> ();
				}
				tweenMapping.Add (rect, null);
				initItemAtLocation (rect, curLayer, Random.Range (-0.5f, 0.5f) * dim.x);
			}
		}
		StopMotion (0);
	}
	void tweenFromOneSideToAnother(RectTransform _transform, Layer _layerInfo)
	{
		var res = parentScalar.referenceResolution;
		var dim = _transform.rect.size;
		var curPos = _transform.localPosition;
		var targetPosX = Mathf.Sign(_layerInfo.Direction) * ((res.x / 2f) + (dim.x * 0.5f * _transform.localScale.x));
		var speed = _layerInfo.SpeedRange.floatValue;
		var time = Mathf.Abs(targetPosX - curPos.x) / speed;
		tweenMapping[_transform] = _transform.DOLocalMoveX(targetPosX, time).SetEase(Ease.Linear).SetUpdate(true).OnComplete(() => {
			var x = -targetPosX;
			initItemAtLocation (_transform, _layerInfo, x);
		});
	}

	void initItemAtLocation (RectTransform _transform, Layer _layerInfo, float x)
	{
		var res = parentScalar.referenceResolution;
		var pos = Vector3.up * Random.Range(-0.5f, 0.5f) * res.y;
		pos.x = x;
		_transform.localPosition = pos;
		_transform.localScale = Vector2.one * _layerInfo.SizeRange.floatValue;
		tweenFromOneSideToAnother (_transform, _layerInfo);
	}
	#endregion
}
