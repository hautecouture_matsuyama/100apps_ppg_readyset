﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class BNarrativeGiftOverlay : BBaseOverlay
{
	public static BNarrativeGiftOverlay SpawnGirlReward(PPG _girlToUnlock)
	{
		var prefab = Resources.Load<GameObject>(@"Prefabs/UI/NarrativeGiftOverlay");
		var inst = Object.Instantiate(prefab);
		var script = inst.GetComponent<BNarrativeGiftOverlay>();
		script.girlToUnlock = _girlToUnlock;
		return script;
	}

	public static BNarrativeGiftOverlay SpawnBliss()
	{
		var prefab = Resources.Load<GameObject>(@"Prefabs/UI/NarrativeGiftOverlay");
		var inst = Object.Instantiate(prefab);
		var script = inst.GetComponent<BNarrativeGiftOverlay>();
		script.girlToUnlock = PPG.None;
		return script;
	}

	[SerializeField] Sprite blossomSprite;
	[SerializeField] Sprite bubblesSprite;
	[SerializeField] Sprite buttercupSprite;
	[SerializeField] Sprite blissSprite;

	[SerializeField] Image itemImage;
	[SerializeField] TextMeshProUGUI itemDisplayName;
	[SerializeField] RectTransform continueButton;

	public System.Action OnClose;

	PPG girlToUnlock;
	public void OnContinue()
	{
		animateOutScale().OnComplete(() => {
			if (OnClose != null) {
				OnClose();
			}
			Destroy(gameObject);
		});
	}
	protected override void onEscape ()
	{
		OnContinue();
	}
	protected override void Start ()
	{
		itemImage.sprite = getImageFromGirl();
		if (girlToUnlock == PPG.None) {
			itemDisplayName.text = "ブリスティナ";
		} else {
			itemDisplayName.text = girlToUnlock.ToString ();
		}
		animateInScale();
	}

	Sprite getImageFromGirl()
	{
		switch (girlToUnlock) {
		case PPG.Blossom:
			return blossomSprite;
		case PPG.Bubbles:
			return bubblesSprite;
		case PPG.Buttercup:
			return buttercupSprite;
		case PPG.None:
			return blissSprite;
		}
		return null;
	}
}
