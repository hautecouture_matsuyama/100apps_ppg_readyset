﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;
using System.Collections;
using DG.Tweening;
using TMPro;
using Spine.Unity;

public class BGiftOverlay : BBaseOverlay
{
	public static bool IsGiftOverlayOpen{get; private set;}

	public static BGiftOverlay SpawnGift(GiftSpawner.GiftItem _toYield)
	{
		if (BPlayerInput.Instance != null) {
			BCoroutine.Instance.StartCoroutine(BPlayerInput.Instance.DisableHUDForGiftOverlay (true));
		}

		var prefab = Resources.Load<GameObject>(@"Prefabs/UI/GiftOverlay");
		var inst = Object.Instantiate(prefab);
		var script = inst.GetComponent<BGiftOverlay>();
		script.itemToYield = _toYield;
		script.itemDisplayName.GetComponent<TextMeshProUGUI>().text = _toYield.ToString();
		return script;
	}

	[SerializeField] SkeletonAnimation starsSpineAnim;
	[SerializeField] SkeletonAnimation chestSpineAnim;
	[SerializeField] Button openChestButton;
	[SerializeField] GameObject goldRewardGroup;
	[SerializeField] TextMeshProUGUI goldAmountText;
	[SerializeField] GameObject goldBackingEffect;
	[SerializeField] GameObject itemRewardGroup;
	[SerializeField] TextMeshProUGUI itemDisplayName;
	[SerializeField] TextMeshProUGUI itemSubtitle;
	[SerializeField] TextMeshProUGUI itemDescription;
	[SerializeField] Image itemImage;
	[SerializeField] GameObject itemBackingEffect;
	[SerializeField] GameObject purchasePfx;
	[SerializeField] RectTransform continueButton;
	[SerializeField] GameObject equipButton;
	[SerializeField] Sprite[] fmSprites;
	[SerializeField] Sprite[] auraSprites;
	[SerializeField] AudioClip giftDropSound;
	[SerializeField] AudioClip giftOpenSound;

	public System.Action OnGiftOpenedExternalCallback;
	public System.Action OnClose;

	GiftSpawner.GiftItem itemToYield;
	System.Action equipNowAction;
	public void OnGiftOpened()
	{
		if (!animatedIn) {
			return;
		}
		openChestButton.interactable = false;
		if (OnGiftOpenedExternalCallback != null) {
			OnGiftOpenedExternalCallback();
		}
		goldRewardGroup.transform.DOScale(Vector3.one, tweenTime).SetEase(Ease.OutBack).SetUpdate(true).SetDelay(1.2f);
		itemRewardGroup.transform.DOScale(Vector3.one, tweenTime).SetEase(Ease.OutBack).SetUpdate(true).SetDelay(1.2f).OnStart(()=>{
			giveGift();
			purchasePfx.SetActive(true);
		}).OnComplete(() => {
			goldRewardGroup.transform.DOScale(Vector3.one * 1.05f, 0.7f).SetEase(Ease.InOutQuad).SetUpdate(true).SetLoops(-1,LoopType.Yoyo);
			itemImage.transform.DOScale(Vector3.one * 1.05f, 0.7f).SetEase(Ease.InOutQuad).SetUpdate(true).SetLoops(-1,LoopType.Yoyo);
		});
		BAudioManager.Instance.CreateAndPlayAudio(giftOpenSound);
		chestSpineAnim.AnimationState.SetAnimation (0, "Click", false).Complete += delegate {
			continueButton.gameObject.SetActive(true);
			continueButton.DOScale(Vector2.one, tweenTime).SetEase(Ease.OutBack).SetUpdate(true);
		};
		chestSpineAnim.AnimationState.AddAnimation (0, "OpenIdle", true, 0);
	}
	public void OnEquipNow()
	{
		if (equipNowAction != null) {
			equipNowAction();
			SaveLoadManager.Save();
		}
		OnContinue();
	}
	public void OnContinue()
	{
		if(allowedToClick) {
			allowedToClick = false;
			PlayButtonBackwardSound();
			animateContentsOut().OnComplete(() => {
				if (BPlayerInput.Instance != null) {
					BCoroutine.Instance.StartCoroutine(BPlayerInput.Instance.DisableHUDForGiftOverlay (false, 0.3f));
				}
				Destroy (gameObject);
				BResultsOverlay.FreezeBackButton = false;
				if (OnClose != null) {
					OnClose();
				}
			});
		}
	}
	protected override void onEscape ()
	{
		if (!openChestButton.interactable) { //meaning gift is opened
			OnContinue();
		} else {
			setCanCallEscapeTrue();
		}
	}
	protected override void Awake()
	{
		IsGiftOverlayOpen = true;
		base.Awake ();
		var canvas = GetComponent<Canvas> ();
		canvas.worldCamera = GameObject.FindGameObjectWithTag ("UICamera").GetComponent<Camera>();
		canvas.sortingLayerName = "HUD";
		canvas.planeDistance = 100;
	}
	protected override void Start ()
	{
		base.Start ();
		StartCoroutine(animateContentsIn ());
	}
	void OnDestroy()
	{
		IsGiftOverlayOpen = false;
	}
	bool animatedIn;
	IEnumerator animateContentsIn ()
	{
		goldRewardGroup.transform.DOScale(0, 1).Kill(true);
		continueButton.DOScale(0f, 1).Kill(true);
		equipButton.transform.DOScale(0f, 1).Kill(true);
		itemRewardGroup.transform.DOScale(0f, 1).Kill(true);
		yield return new WaitForSeconds (0.5f);
		BAudioManager.Instance.CreateAndPlayAudio(giftDropSound);
		chestSpineAnim.gameObject.SetActive (true);
		chestSpineAnim.AnimationState.AddAnimation (0, "ClosedIdle", true, 0);
		animatedIn = true;
	}
	Tweener animateContentsOut()
	{
		continueButton.DOScale(0f, tweenTime).SetEase(Ease.InBack).SetUpdate(true);
		equipButton.transform.DOScale(0f, tweenTime).SetEase(Ease.InBack).SetUpdate(true);
		goldRewardGroup.transform.DOScale(0f, tweenTime).SetEase(Ease.InBack).SetUpdate(true);
		itemRewardGroup.transform.DOScale(0f, tweenTime).SetEase(Ease.InBack).SetUpdate(true);

		starsSpineAnim.AnimationState.SetAnimation (0, "Reverse", false);
		chestSpineAnim.transform.DOScale (Vector3.zero, tweenTime).SetEase (Ease.InBack);
		return bg.DOFade(0f, tweenTime).SetEase(Ease.Linear);
	}
	void giveGift()
	{

		System.Action<int> coinAction = _x => {
			PlayerManager.GoldCount += (uint)_x;
			SaveLoadManager.Save();
			goldRewardGroup.SetActive(true);
			goldAmountText.text = _x.ToString();
			//goldBackingEffect.GetComponent<Spineanim>();
		};
		System.Action<System.Action> unlockAction = _giftGiver => {
			_giftGiver();
			SaveLoadManager.Save();
		};
		//DEBUG toggle next line for easier testing in construction-yard
		//itemToYield = GiftSpawner.GiftItem.FriendlyMonster;
		switch (itemToYield) {
		case GiftSpawner.GiftItem.Coin50:
			coinAction (50);
			break;
		case GiftSpawner.GiftItem.Coin100:
			coinAction (100);
			break;
		case GiftSpawner.GiftItem.Coin200:
			coinAction (200);
			break;
		case GiftSpawner.GiftItem.HeartShard:
			PlayerManager.HeartShardCount++;
			itemRewardGroup.SetActive(true);
			itemDisplayName.text = "ハートのカケラ";
			//heartBackingEffect.SetActive(true);
			break;
		case GiftSpawner.GiftItem.Aura:
			{
				var unlocked = GiftAvailability.UnlockRandomAura();
				unlockAction (() => {
					var auraItemInfos = PlayerEquipsManager.Auras[unlocked.Key];
					var currAuraInfo = auraItemInfos[unlocked.Value];
					var currAuraSprite = auraSprites.Single(arg => arg.name == currAuraInfo.Name);
					itemImage.sprite = currAuraSprite;
					itemDisplayName.text = string.Format("New {0} Aura!", unlocked.Key);
					itemSubtitle.text = currAuraInfo.Name;
					itemSubtitle.gameObject.SetActive(true);
					PlayerManager.Instance.GetAcquiredStatForGirl(unlocked.Key).UnlockAura(unlocked.Value);
					itemRewardGroup.SetActive(true);
				});
				continueButton.GetComponent<RectTransform>().anchoredPosition += Vector2.right * 300;
				equipButton.transform.gameObject.SetActive(true);
				equipButton.transform.DOScale(Vector2.one, tweenTime).SetEase(Ease.OutBack).SetUpdate(true).SetDelay(0.3f);
				equipNowAction = () => PlayerManager.Instance.GetAcquiredStatForGirl(unlocked.Key).EquippedAuraIndex = unlocked.Value;
				break;
			}
		case GiftSpawner.GiftItem.FriendlyMonster:
			{
				var unlocked = GiftAvailability.UnlockRandomFriendlyMonster();
				unlockAction (() => {
					var fmItemInfos = PlayerEquipsManager.FriendlyMonsters[unlocked.Key];
					var currFMInfo = fmItemInfos[unlocked.Value];
					var currFMSprite = fmSprites.Single(arg => arg.name == currFMInfo.Name);
					itemImage.sprite = currFMSprite;
					itemDisplayName.text = "新しい仲間モンスター";
					itemSubtitle.text = string.Format("Name: {0}\tClass: {1}", currFMInfo.Name, currFMInfo.Type);
					itemSubtitle.gameObject.SetActive(true);
					itemDescription.text = currFMInfo.Description;
					itemDescription.gameObject.SetActive(true);
					PlayerManager.Instance.GetUnlockedFriendlyMonsterArray(unlocked.Key).Set(unlocked.Value, true);
					itemRewardGroup.SetActive(true);
					BGameCenterManager.CheckAllFMs();
				});
				continueButton.GetComponent<RectTransform>().anchoredPosition += Vector2.right * 300;
				equipButton.transform.gameObject.SetActive(true);
				equipButton.transform.DOScale(Vector2.one, tweenTime).SetEase(Ease.OutBack).SetUpdate(true).SetDelay(0.3f);
				equipNowAction = () => {
					PlayerManager.Instance.CurrentFriendlyMonster = unlocked.Key;
					PlayerManager.Instance.CurrentFriendlyMonsterSkinIndex = unlocked.Value;
					var player = FindObjectOfType<BPlayerAgent>();
					player.UpdateFM();
				};

				break;
			}
		}
	}
}
