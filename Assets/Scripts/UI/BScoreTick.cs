﻿using UnityEngine;
using System.Collections;
using TMPro;
using DG.Tweening;

public class BScoreTick : MonoBehaviour
{
	const float tickTime = 1f;
	[SerializeField] bool useBest;
	[SerializeField] string baseText;
	[SerializeField] float delay;
	[SerializeField] AudioClip newBestVO;

	public int OldBestScore;

	TextMeshProUGUI scoreText;

	void Awake()
	{
		scoreText = GetComponent<TextMeshProUGUI>();
	}
	void OnEnable()
	{
		if(useBest) {
			scoreText.text = string.Format("{0}: {1}", "ベストスコア", PlayerManager.BestScore);
			if(OldBestScore != -1) {
				StartCoroutine(waitForBestSlam());
			}
		} else {
			StartCoroutine(tick());
		}
	}
	IEnumerator tick()
	{
		if (delay > 0) {
			yield return new WaitForSecondsRealtime(delay);
		}
		float timer = 0;
		var target = useBest ? PlayerManager.BestScore : PlayerManager.CurrentScore;
		while(timer <= tickTime) {
			var lerp = Mathf.Lerp(0f, (float)target, timer / tickTime);
			scoreText.text = string.Format("{0}: {1}", baseText, Utilities.FormatTextForCount((uint)lerp, 0, false));
			yield return null;
			timer += Time.unscaledDeltaTime;
		}
		scoreText.text = string.Format("{0}: {1}", baseText, Utilities.FormatTextForCount(target, 0, false));
	}
	IEnumerator waitForBestSlam()
	{
		yield return new WaitForSecondsRealtime(delay + tickTime);
		if(PlayerManager.BestScore > OldBestScore) {
			scoreText.text = string.Format("{0}: {1}", baseText, PlayerManager.BestScore);
			float tweenTime = 0.5f;
			transform.DOScale(Vector3.one * 4, tweenTime).From().SetEase(Ease.InCirc).OnComplete(() => {
				transform.DOShakePosition(0.5f, 8);
				transform.DOShakeRotation(0.5f, Vector3.forward * 10, 50);
				if(newBestVO != null) {
					BAudioManager.Instance.CreateAndPlayAudio(newBestVO);
				}
			});
			scoreText.DOFade(0, tweenTime).From().SetDelay(0).SetEase(Ease.InCirc);
		}
	}
}
