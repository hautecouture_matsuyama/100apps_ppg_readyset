﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using DG.Tweening;

public class BSuperAttackHUD : MonoBehaviour
{
	#region Variables
	[SerializeField] Selectable button;
	[SerializeField] Image[] glows;

	bool fullyCharged;
	bool isOut;
	RectTransform rt;
	#endregion

	#region Mono Callbacks
	void Awake()
	{
		rt = button.GetComponent<RectTransform>();
	}
	void Start()
	{
		button.interactable = false;
		BattleManager.HandleStateSwitch += onStateChange;
	}
	void OnDestroy()
	{
		BattleManager.HandleStateSwitch -= onStateChange;
	}
	void Update()
	{
		if (!fullyCharged) {
			checkIfFullyCharged();
		}
		if(fullyCharged && !isOut && BattleManager.InBattle) {
			slideIn();
		}
	}
	#endregion

	#region UI Callbacks
	public void PerformSuper()
	{
		if (fullyCharged && BattleManager.InBattle) {
			slideOut();
			fullyCharged = false;
			button.interactable = false;
			PlayerManager.CurrentSuperAttack.ResetTimer();
			SaveLoadManager.Save();
			BattleManager.PerformSuperAttack( PlayerManager.IsSuperAttackEquipped(SuperAttack.TornadoTrio) ?
				SuperAttack.TornadoTrio : SuperAttack.BlissBlitz );
		}
	}
	#endregion

	#region Helpers
	RechargableRealTimeTimer superAttackCharge
	{
		get {
			return PlayerManager.CurrentSuperAttack;
		}
	}
	void onStateChange(GameState _state)
	{
		if (!BattleManager.InBattle && isOut) {
			slideOut();
		}
	}
	void checkIfFullyCharged()
	{
		if (superAttackCharge != null && Mathf.Approximately(1f, superAttackCharge.PercentComlete)) {
			fullyCharged = true;
		}
	}
	void slideOut()
	{
		isOut = false;
		button.interactable = false;
		rt.DOAnchorPosX(600, 0.3f).SetEase(Ease.InBack).OnComplete(() => {
			button.gameObject.SetActive(false);
			rt.DOAnchorPosX(105, 0).Kill(true);
			for(int i=0;i<glows.Length;i++) {
				glows[i].DOKill();
				glows[i].DOFade(1, 0).Kill(true);
			}
		});
	}
	void slideIn()
	{
		isOut = true;
		for(int i=0;i<glows.Length;i++) {
			glows[i].DOFade(0.6f, 0.1f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
		}
		rt.DOAnchorPosX(600, 0.3f).SetEase(Ease.OutBack).From().OnStart(() => {
			button.gameObject.SetActive(true);		
		}).OnComplete(() => {
			button.interactable = true;
		});
	}
	#endregion
}
