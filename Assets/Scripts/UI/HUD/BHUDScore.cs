﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class BHUDScore : MonoBehaviour
{
	[SerializeField]
	TextMeshProUGUI scoreText;

	void Start ()
	{
		scoreText.text = PlayerManager.CurrentScore.ToString();
		PlayerManager.HandleScoreChanged += onScoreChanged;
	}

	void OnDestroy()
	{
		PlayerManager.HandleScoreChanged -= onScoreChanged;
	}

	void onScoreChanged(uint _old, uint _new)
	{
		//scoreText.text = string.Format("Score: {0}", _new);
		scoreText.text = _new.ToString();
		scoreText.transform.DOPunchScale(0.2f * Vector2.one, 0.2f);
	}
}
