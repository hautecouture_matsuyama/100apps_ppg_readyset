﻿using UnityEngine;
using TMPro;
using DG.Tweening;

public class BGoldHUD : MonoBehaviour
{
	[SerializeField]
	TextMeshProUGUI textCount;
	void Start()
	{
		onGoldCollected(0, PlayerManager.GoldCount);
		PlayerManager.HandleGoldCollected += onGoldCollected;
	}
	void OnDestroy()
	{
		PlayerManager.HandleGoldCollected -= onGoldCollected;
	}

	void onGoldCollected(uint _old, uint _new)
	{
		textCount.DOScale(1.1f, 0.1f).OnComplete(() => {
			textCount.text = Utilities.FormatTextForCount(_new, 0, false);
			textCount.DOScale(1f, 0.1f).SetEase(Ease.OutSine).SetUpdate(true);
		}).SetEase(Ease.InSine).SetUpdate(true);
	}
}
