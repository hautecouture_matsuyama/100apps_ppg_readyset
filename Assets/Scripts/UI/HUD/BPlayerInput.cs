﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class BPlayerInput : MonoBehaviour
{
	public static BPlayerInput Instance{get; private set;}
	[SerializeField] Transform navOnlyHUD;
	[SerializeField] Transform battleOnlyHUD;
	[SerializeField] RectTransform topButtonGroup;
	[SerializeField] RectTransform bottomButonGroup;
	[SerializeField] Button blockButton;
	[SerializeField] Button attackButton;
	[SerializeField] TextMeshProUGUI attackText;
	[SerializeField] TextMeshProUGUI blockText;
	[SerializeField] GameObject blockGlow;
	[SerializeField] GameObject attackGlow;
	[SerializeField] GameObject staminaPointerLocation;
	[SerializeField] GameObject characterSwapPointerLocation;

	Tweener textTween;
	Tweener blockShakeTween;
	Dictionary<Graphic, float> uiAlphaMapping = new Dictionary<Graphic, float>();
	public void LaunchGirlSelectOverlay()
	{
		BGirlSelectOverlay.SpawnGirlSelect(false, false);
	}

	#region Battle Callbacks
	public void OnAttackPressed()
	{
		BTutorialManager.Instance.InputPressed = true;
		if (BattleManager.InBattle && !BTutorialManager.Instance.ForceDisableAttack) {
			BattleManager.PlayerPartyLead.OnAttackPressed();
		}
	}
	public void OnAttackReleased()
	{
		if (BattleManager.InBattle) {
			BattleManager.PlayerPartyLead.OnAttackReleased();
		}
	}

	public void OnBlockPressed()
	{
		BTutorialManager.Instance.InputPressed = true;
		if (BattleManager.InBattle && !BTutorialManager.Instance.ForceDisableBlock) {
			BTutorialManager.Instance.BlockInputIsDown = true;
			BattleManager.PlayerPartyLead.OnBlockPressed();
		}
	}
	public void OnBlockReleased()
	{
		if (BattleManager.InBattle) {
			BTutorialManager.Instance.BlockInputIsDown = false;
			BattleManager.PlayerPartyLead.OnBlockReleased();
		}
	}
	#endregion

	#region UI Animations
	public void SwitchToBattleUI(bool _now = false)
	{
		moveButtonGroupTo(navOnlyHUD, navOnlyHUD.GetComponent<RectTransform>().rect.height, Ease.InBack, _now);
		moveButtonGroupTo(battleOnlyHUD, 0, Ease.OutBack, _now);
	}
	public void SwitchToNavUI(bool _now = false)
	{
		moveButtonGroupTo(navOnlyHUD, 0, Ease.OutBack, _now);
		var child = (RectTransform)battleOnlyHUD.GetChild(0);
		moveButtonGroupTo(battleOnlyHUD, -child.rect.height, Ease.InBack, _now);
	}

	public void SwitchToSuperUI()
	{
		moveButtonGroupTo(topButtonGroup, topButtonGroup.rect.height, Ease.InBack, false);
		moveButtonGroupTo(bottomButonGroup, -bottomButonGroup.rect.height, Ease.InBack, false);
		if (BattleManager.EnemyPartyLead != null) {
			var child  = BattleManager.EnemyPartyLead.transform.FindChild("Health Canvas");
			if (child != null) {
				var uiImages = child.GetComponentsInChildren<Graphic>();
				foreach(var a in uiImages) {
					uiAlphaMapping.Add(a, a.color.a);
					a.DOFade(0, 0.2f).SetEase(Ease.Linear);
				}
			}
		}
	}
	public void SwitchFromSuperUI()
	{
		moveButtonGroupTo(topButtonGroup, 0, Ease.OutBack, false);
		moveButtonGroupTo(bottomButonGroup, 0, Ease.OutBack, false);
		if (uiAlphaMapping != null) {
			foreach(var kv in uiAlphaMapping) {
				kv.Key.DOFade(kv.Value, 0.2f).SetEase(Ease.Linear);
			}
			uiAlphaMapping.Clear();
		}
	}
	public IEnumerator DisableHUDForGiftOverlay(bool _disable, float _delay = 0)
	{
		yield return new WaitForSeconds (_delay);
		navOnlyHUD.gameObject.SetActive (!_disable);
		battleOnlyHUD.gameObject.SetActive (!_disable);
		topButtonGroup.gameObject.SetActive (!_disable);
	}
	public void DisableAttackButton()
	{
		attackButton.interactable = false;
	}
	public void EnableAttackButton()
	{
		attackButton.interactable = true;
	}
	public void DisableBlockButton()
	{
		blockButton.interactable = false;
	}
	public void EnableBlockButton()
	{
		blockButton.interactable = true;
	}

	public void SetAttackTextForAura()
	{
		setAttackText("Aura!", true);
	}

	public Vector3 StaminaBarPosition()
	{
		return staminaPointerLocation.transform.position;
	}
	public Vector3 CharacterSwapPosition()
	{
		return characterSwapPointerLocation.transform.position;
	}
	#endregion

	#region Helpers
	void setAttackText(string _text, bool _shake) {
		attackGlow.SetActive(false);
		attackText.text = _text;
		if (textTween != null) {
			textTween.Kill(true);
		}
		if (_shake) {
			attackGlow.SetActive(true);
			textTween = attackText.transform.DOScale(1.05f * Vector3.one, 0.05f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo).OnComplete(() => {
				textTween = null;
				attackText.transform.localScale = Vector3.one;
				attackGlow.SetActive(false);
			});
		}
	}

	void moveButtonGroupTo(Transform _group, float _yPos, Ease _type,  bool _now)
	{
		var t = _group.GetComponent<RectTransform>().DOAnchorPosY(_yPos, 0.2f);
		t.SetEase(_type);
		if (_now) {
			t.Kill(true);
		}
	}
	#endregion

	#region Mono Overrides
	void Awake()
	{
		Instance = this;
		var canvas = GetComponent<Canvas> ();
		canvas.worldCamera = GameObject.FindGameObjectWithTag ("UICamera").GetComponent<Camera>();
		canvas.sortingLayerName = "HUD";
		canvas.planeDistance = 100;
	}
	void Start()
	{
		SwitchToNavUI(true);
	}
	void OnDestroy()
	{
		Instance = null;
	}
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape) &&
			!BPauseOverlay.IsPaused &&
			BPauseOverlay.Instance != null &&
			BTutorialManager.AllowedToPause) {
			var overlays = FindObjectsOfType<BBaseOverlay>();
			if (overlays.Length == 0) {
				BPauseOverlay.Instance.Pause();
			}
		}

		#if UNITY_EDITOR
		if (Input.GetKeyDown(KeyCode.Q)) {
			BGiftOverlay.SpawnGift(GiftSpawner.GetItemForFreeGift());
		}

		if (BattleManager.CurrentGameState != GameState.Battle) return;
		if (Input.GetKeyDown(KeyCode.A)) {
			BattleManager.PlayerPartyLead.OnAttackPressed();
		} else if (Input.GetKeyDown(KeyCode.D)) {
			BattleManager.PlayerPartyLead.OnBlockPressed();
		}

		if (Input.GetKeyUp(KeyCode.A)) {
			BattleManager.PlayerPartyLead.OnAttackReleased();
		} else if (Input.GetKeyUp(KeyCode.D)) {
			BattleManager.PlayerPartyLead.OnBlockReleased();
		}
		#endif
	}
	#endregion
}
