﻿using UnityEngine;
using System.Text;
using TMPro;
using DG.Tweening;

public class BHeartShardHUD : MonoBehaviour {

	[SerializeField]
	TextMeshProUGUI textCount;
	void Start()
	{
		onHeartShardCollected(0, PlayerManager.HeartShardCount);
		PlayerManager.HandleHeartShardCollected += onHeartShardCollected;
	}
	void OnDestroy()
	{
		PlayerManager.HandleHeartShardCollected -= onHeartShardCollected;
	}

	void onHeartShardCollected(uint _old, uint _new)
	{
		textCount.DOScale(1.1f, 0.1f).OnComplete(() => {
			textCount.text = Utilities.FormatTextForCount(_new, 0, false);
			textCount.DOScale(1f, 0.1f).SetEase(Ease.OutSine).SetUpdate(true);
		}).SetEase(Ease.InSine).SetUpdate(true);
	}
}
