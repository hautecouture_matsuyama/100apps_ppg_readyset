﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BGirlPortraitHUD : MonoBehaviour 
{
	[SerializeField]
	GameObject BlossomPortraitButton;
	[SerializeField]
	GameObject BubblesPortraitButton;
	[SerializeField]
	GameObject ButtercupPortraitButton;

	void Start () 
	{
		PlayerManager.HandleCharacterChange += onCharacterChanged;
		BattleManager.HandleStateSwitch += onStateChange;
		switch(PlayerManager.GetActiveGirl()) {
		case PPG.Blossom:
			BlossomPortraitButton.SetActive(true);
			BubblesPortraitButton.SetActive(false);
			ButtercupPortraitButton.SetActive(false);
			break;
		case PPG.Bubbles:
			BlossomPortraitButton.SetActive(false);
			BubblesPortraitButton.SetActive(true);
			ButtercupPortraitButton.SetActive(false);
			break;
		case PPG.Buttercup:
			BlossomPortraitButton.SetActive(false);
			BubblesPortraitButton.SetActive(false);
			ButtercupPortraitButton.SetActive(true);
			break;
		}
	}

	void OnDestroy()
	{
		PlayerManager.HandleCharacterChange -= onCharacterChanged;
		BattleManager.HandleStateSwitch -= onStateChange;
	}
	void onStateChange(GameState _state)
	{
		setButtonInteractable(_state == GameState.Navigation);
	}

	void onCharacterChanged(PPG _girl)
	{
		switch(_girl) {
		case PPG.Blossom:
			BlossomPortraitButton.SetActive(true);
			BubblesPortraitButton.SetActive(false);
			ButtercupPortraitButton.SetActive(false);
			break;
		case PPG.Bubbles:
			BlossomPortraitButton.SetActive(false);
			BubblesPortraitButton.SetActive(true);
			ButtercupPortraitButton.SetActive(false);
			break;
		case PPG.Buttercup:
			BlossomPortraitButton.SetActive(false);
			BubblesPortraitButton.SetActive(false);
			ButtercupPortraitButton.SetActive(true);
			break;
		default:
			throw new System.ArgumentOutOfRangeException(_girl + " in onCharacterChanged of BGirlPortraitHUD was not an expected value!!");
		}
	}

	void setButtonInteractable(bool _active)
	{
		_active &= PlayerManager.NumberofGirlUnlocked() > 1;
		BlossomPortraitButton.GetComponent<Button>().interactable = _active;
		BlossomPortraitButton.GetComponent<UnityEngine.EventSystems.EventTrigger>().enabled = _active;
		BubblesPortraitButton.GetComponent<Button>().interactable = _active;
		BubblesPortraitButton.GetComponent<UnityEngine.EventSystems.EventTrigger>().enabled = _active;
		ButtercupPortraitButton.GetComponent<Button>().interactable = _active;
		ButtercupPortraitButton.GetComponent<UnityEngine.EventSystems.EventTrigger>().enabled = _active;
	}
}
