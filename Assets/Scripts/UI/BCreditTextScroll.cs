﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class BCreditTextScroll : BBaseOverlay
{
	public const float yPadding = 10f;
	enum CreditItem
	{
		None,
		Heading1,
		Heading2,
		Normal,
		GameImage,
		DevImage,
		CNImage
	}

	[SerializeField] GameObject header1Prefab;
	[SerializeField] GameObject header2Prefab;
	[SerializeField] GameObject normalTextPrefab;
	[SerializeField] GameObject gameLogoPrefab;
	[SerializeField] GameObject devLogosPrefab;
	[SerializeField] GameObject cnLogoPrefab;
	[SerializeField] float scrollSpeed;
	[SerializeField] AudioClip creditsMusic;

	public bool AllowScrolling{ get; set; }
	public const float baseScreenResolution = 712f;


	LinkedList<GameObject> toScroll = new LinkedList<GameObject>();
	LinkedList<string> lines;
	LinkedListNode<string> currentLine;

	CreditItem itemAboveNewest;

	protected override void Start ()
	{
		var file = Resources.Load(@"Prefabs/UI/credits") as TextAsset;
		lines = new LinkedList<string> (file.text.Split ('\n'));
		currentLine = lines.First;
		BAudioManager.Instance.PlayMusic(creditsMusic, null, 0, 0.3f);
	}

	// Update is called once per frame
	protected override void Update ()
	{
		base.Update();
		if (AllowScrolling) {
			if ((toScroll.Count == 0 || toScroll.Last.Value.transform.localPosition.y > -halfScreenHeight) &&
				currentLine != null) {
				convertLine (currentLine.Value);
				currentLine = currentLine.Next;
			}
			foreach (var go in toScroll) {
				go.transform.position += Vector3.up * Time.deltaTime * (scrollSpeed * (Screen.height / baseScreenResolution));
			}
		}
		if (toScroll.Count > 0) {
			var top = toScroll.First.Value;
			var topY = top.transform.localPosition.y;
			var rectTransform = top.GetComponent<RectTransform> ();
			if ((topY - rectTransform.rect.height / 2) > halfScreenHeight) {
				toScroll.RemoveFirst ();
				Destroy (top.gameObject);
				if (toScroll.Count == 0) {
					currentLine = lines.First;
				}
			}
		}
	}
	float getSpacingForPair(CreditItem _above, CreditItem _below)
	{
		if (_above == CreditItem.None) {
			return 0;
		}
		if (_above == CreditItem.Heading1) {
			if(_below == CreditItem.DevImage) {
				return 50;
			} else {
				return 100;
			}
		} else if (_above == CreditItem.GameImage) {
			return 500;
		} else if (_above == CreditItem.DevImage) {
			return 400;
		} else if (_above == CreditItem.CNImage) {
			return 300;
		} else if (_above == CreditItem.Heading2) {
			return 30;
		} else if (_above == CreditItem.Normal) {
			if (_below == CreditItem.Heading1) {
				return 150;
			} else if (_below == CreditItem.Heading2) {
				return 70;
			} else {
				return 20;
			}
		}
		return 0;
	}
	void convertLine(string _line)
	{
		const int typeIndex = 0;
		const int valueIndex = 1;
		var info = _line.Split ('\t');
		var type = info [typeIndex];
		var value = info [valueIndex];
		GameObject gObj = null;
		RectTransform rectTransform = null;
		var previous = itemAboveNewest;
		if (type.Contains ("_image")) {
			if(type.Contains("_game")) {
				gObj = Instantiate (gameLogoPrefab);
				itemAboveNewest = CreditItem.GameImage;
			} else if (type.Contains("_dev")) {
				gObj = Instantiate (devLogosPrefab);
				itemAboveNewest = CreditItem.DevImage;
			} else if (type.Contains("_cn")) {
				gObj = Instantiate (cnLogoPrefab);
				itemAboveNewest = CreditItem.CNImage;
			}
			rectTransform = gObj.GetComponent<RectTransform>();
		} else {
			if (type.Contains("_h1")){
				gObj = Instantiate (header1Prefab);
				itemAboveNewest = CreditItem.Heading1;
			} else if (type.Contains("_h2")){
				gObj = Instantiate (header2Prefab);
				itemAboveNewest = CreditItem.Heading2;
			} else {
				gObj = Instantiate (normalTextPrefab);
				itemAboveNewest = CreditItem.Normal;
			}
			var textObj = gObj.GetComponent<TextMeshProUGUI> ();
			textObj.text = value;
			var dim = textObj.GetPreferredValues();
			rectTransform = textObj.GetComponent<RectTransform>();
			dim.y += (2f * yPadding);
			rectTransform.sizeDelta = dim;
		}
		var spacing = getSpacingForPair (previous, itemAboveNewest);
		var firstOffScreenLoc = -halfScreenHeight;
		var lastPos = toScroll.Count == 0 ?
			new Vector3 (0, firstOffScreenLoc) : 
			toScroll.Last.Value.transform.localPosition;
		gObj.transform.position = new Vector3 (0,
			lastPos.y - rectTransform.rect.height / 2 - spacing,
			lastPos.z);
		rectTransform.SetParent (transform, false);
		toScroll.AddLast (gObj);
	}
	static float halfScreenHeight
	{
		get{ return (float)(1152 / 2f);}
	}
	protected override void onEscape ()
	{
		GoToTitle();
	}
}
