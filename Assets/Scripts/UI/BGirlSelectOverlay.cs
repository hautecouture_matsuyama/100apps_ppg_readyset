﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using System.Collections.Generic;

public class BGirlSelectOverlay : BBaseOverlay
{
	public static BGirlSelectOverlay Instance {get; private set;}

	public static void SpawnGirlSelect(bool _firstTime, bool _hideCloseButton)
	{
		BattleManager.StartCharacterSwap();
		Instance.firstTime = _firstTime;
		Instance.hideCloseButton = _hideCloseButton;
		if (_firstTime) {
			BCoroutine.WaitAndPerformRealtime(() => Instance.gameObject.SetActive (true), -1);
		} else {
			Instance.gameObject.SetActive(true);
			Instance.PlayMenuInSound();
		}
	}

	[SerializeField] TextMeshProUGUI header;
	[SerializeField] Button blossom;
	[SerializeField] Button bubbles;
	[SerializeField] Button buttercup;
	[SerializeField] GameObject closeButton;
	[SerializeField] Image blossomImage;
	[SerializeField] Image bubblesImage;
	[SerializeField] Image buttercupImage;
	[SerializeField] AudioClip blossomSelectedVO;
	[SerializeField] AudioClip bubblesSelectedVO;
	[SerializeField] AudioClip buttercupSelectedVO;

	Canvas canvas;

	bool firstTime;
	bool hideCloseButton;
	public bool CallerWillCallSetActiveGirl;
	public bool CallerWillCloseOverlay;
	public PPG Selected{get; private set;}

	Vector3 blossomScale;
	Vector3 bubblesScale;
	Vector3 buttercupScale;

	Dictionary<Object, Vector3> originalPositions = new Dictionary<Object, Vector3>();

	BOverlayParallax parallax;
	protected override void Awake ()
	{
		base.Awake ();
		Instance = this;
		blossomScale = blossom.transform.localScale;
		bubblesScale = bubbles.transform.localScale;
		buttercupScale = buttercup.transform.localScale;

		originalPositions.Add(header, header.rectTransform.anchoredPosition);
		originalPositions.Add(blossom, blossom.GetComponent<RectTransform>().anchoredPosition );
		originalPositions.Add(bubbles, bubbles.GetComponent<RectTransform>().anchoredPosition );
		originalPositions.Add(buttercup, buttercup.GetComponent<RectTransform>().anchoredPosition );

		canvas = GetComponent<Canvas>();
		parallax = GetComponentInChildren<BOverlayParallax>();
		animateOutScaleInstant();
		gameObject.SetActive (false);
	}
	protected override void Start()
	{
		base.Start();
		blossom.image.alphaHitTestMinimumThreshold = 0.1f;
		bubbles.image.alphaHitTestMinimumThreshold = 0.1f;
		buttercup.image.alphaHitTestMinimumThreshold = 0.1f;
	}
	void OnDestroy()
	{
		Instance = null;
		CallerWillCallSetActiveGirl = false;
		CallerWillCloseOverlay = false;
	}
	protected override void OnEnable ()
	{
		base.OnEnable();
		Selected = PPG.None;
		if (!firstTime) {
			blossom.interactable = PlayerManager.IsGirlUnlocked(PPG.Blossom) && PlayerManager.BlossomHealth > 0;
			blossomImage.color = blossom.interactable ? Color.white : Color.black;
			bubbles.interactable = PlayerManager.IsGirlUnlocked(PPG.Bubbles) && PlayerManager.BubblesHealth > 0;
			bubblesImage.color = bubbles.interactable ? Color.white : Color.black;
			buttercup.interactable = PlayerManager.IsGirlUnlocked(PPG.Buttercup) && PlayerManager.ButtercupHealth > 0;
			buttercupImage.color = buttercup.interactable ? Color.white : Color.black;

			switch (PlayerManager.GetActiveGirl ()) {
			case PPG.Blossom:
				blossom.transform.DOScale (new Vector3 (0.95f, 0.95f, 1), 0.7f).SetEase (Ease.InOutQuad).SetLoops (-1, LoopType.Yoyo);
				blossom.interactable = false;
				break;
			case PPG.Bubbles:
				bubbles.transform.DOScale (new Vector3 (0.95f, 0.95f, 1), 0.7f).SetEase (Ease.InOutQuad).SetLoops (-1, LoopType.Yoyo);
				bubbles.interactable = false;
				break;
			case PPG.Buttercup:
				buttercup.transform.DOScale (new Vector3 (0.95f, 0.95f, 1), 0.7f).SetEase (Ease.InOutQuad).SetLoops (-1, LoopType.Yoyo);
				buttercup.interactable = false;
				break;
			}
			canvas.sortingOrder = 100;
			header.text = "あなたのガールズを選ぼう";
		} else {
			canvas.sortingOrder = 201;
			header.text = "最初のガールズを選ぼう！";
		}
		closeButton.SetActive(!(firstTime || hideCloseButton));
		animateIn();
	}
	void OnDisable()
	{
		blossom.interactable = true;
		bubbles.interactable = true;
		buttercup.interactable = true;

		blossom.transform.localScale = blossomScale;
		bubbles.transform.localScale = bubblesScale;
		buttercup.transform.localScale = buttercupScale;
	}
	public void OnSelectBlossom()
	{
		onSelectGirl(PPG.Blossom);
		if(BattleManager.CurrentGameState != GameState.Limbo) {
			BAudioManager.Instance.CreateAndPlayAudio(blossomSelectedVO);
		}
	}
	public void OnSelectBubbles()
	{
		onSelectGirl(PPG.Bubbles);
		if(BattleManager.CurrentGameState != GameState.Limbo) {
			BAudioManager.Instance.CreateAndPlayAudio(bubblesSelectedVO);
		}
	}
	public void OnSelectButtercup()
	{
		onSelectGirl(PPG.Buttercup);
		if(BattleManager.CurrentGameState != GameState.Limbo) {
			BAudioManager.Instance.CreateAndPlayAudio(buttercupSelectedVO);
		}
	}
	void onSelectGirl(PPG _girl)
	{
		var cur = PlayerManager.GetActiveGirl();
		if (cur == PPG.None || cur != _girl) {
			if (firstTime) {
				PlayerManager.UnlockGirl(_girl);
				PlayerManager.HealAllGirls();
			}
			if (!CallerWillCallSetActiveGirl) {
				PlayerManager.SetActiveGirl(_girl);
				SaveLoadManager.Save();
			} else {
				CallerWillCallSetActiveGirl = false;
			}
		}
		PlayButtonBigSound();
		Selected = _girl;
		blossom.transform.DOKill (true);
		bubbles.transform.DOKill (true);
		buttercup.transform.DOKill (true);
		blossom.interactable = false;
		bubbles.interactable = false;
		buttercup.interactable = false;
		const float punchScale = -0.1f;
		const float punchTime = 0.75f;
		const float elasticity = 1;
		Tweener punchTween = null;
		switch (_girl) {
		case PPG.Blossom:
			punchTween = blossom.transform.DOPunchScale (new Vector3 (punchScale, punchScale, 1), punchTime, 10, elasticity).SetRelative(false);
			break;
		case PPG.Bubbles:
			punchTween = bubbles.transform.DOPunchScale (new Vector3 (punchScale, punchScale, 1), punchTime, 10, elasticity).SetRelative(false);
			break;
		case PPG.Buttercup:
			punchTween = buttercup.transform.DOPunchScale (new Vector3 (punchScale, punchScale, 1), punchTime, 10, elasticity).SetRelative(false);
			break;
		}
		if (punchTween != null && !CallerWillCloseOverlay) {
			punchTween.OnComplete(() => Close());
		}
	}
	public void Close(bool _playMenuOut = false)
	{
		parallax.StopMotion(tweenTime);
		animateOut().OnComplete(() => {
			if (!firstTime) {
				BattleManager.CurrentGameState = GameState.Navigation;
			}
			gameObject.SetActive(false);
		});
		if(_playMenuOut) {
			PlayMenuOutSound();
		}
	}
	Tweener tweenToOffScreenRight (Component component)
	{
		var screenHalfWidth = tk2dCamera.Instance.nativeResolutionWidth / 2f;
		var r = component.GetComponent<RectTransform> ();
		r.DOAnchorPosX(originalPositions[component].x,1f).Kill(true);
		return r.DOAnchorPosX (screenHalfWidth + (r.rect.width / 2f) , tweenTime)
			.SetUpdate(true);
	}
	Tweener tweenFromOffScreenLeft (Component component)
	{
		var screenHalfWidth = tk2dCamera.Instance.nativeResolutionWidth / 2f;
		var r = component.GetComponent<RectTransform> ();
		r.DOAnchorPosX(-(screenHalfWidth + (r.rect.width / 2f)),1f).Kill(true);
		return r.DOAnchorPosX (originalPositions[component].x , tweenTime)
			.SetUpdate(true);
	}

	Tweener animateIn()
	{
		bg.DOFade(0f, 1f).Kill(true);
		bg.DOFade(bgColor.a, tweenTime)
			.SetEase(Ease.Linear)
			.SetUpdate(true);
		container.DOScale(1f, 0.02f)
			.SetDelay(tweenTime)
			.SetUpdate(true);
		parallax.StartMotion(tweenTime);

		tweenFromOffScreenLeft(header)
			.SetDelay(tweenTime)
			.SetEase(Ease.OutBack);
		closeButton.transform.DOScale(0f, 1f).Kill(true);
		closeButton.transform.DOScale(1f, tweenTime)
			.SetDelay (tweenTime)
			.SetEase (Ease.OutBack)
			.SetUpdate (true);
		var buttons = new[]{blossom, bubbles, buttercup};
		Tweener lastTween = null;
		foreach(var b in buttons) {
			lastTween = tweenFromOffScreenLeft (b)
				.SetDelay (tweenTime * 1.5f)
				.SetEase (Ease.OutBack);
		}
		return lastTween;
	}
	Tweener animateOut()
	{
		tweenToOffScreenRight(header)
			.SetEase(Ease.InBack);
		closeButton.transform.DOScale(1f, 1f).Kill(true);
		closeButton.transform.DOScale(0f, tweenTime)
			.SetEase (Ease.InBack)
			.SetUpdate (true);
		var buttons = new[]{blossom, bubbles, buttercup};
		foreach(var b in buttons) {
			tweenToOffScreenRight (b)
				.SetDelay (tweenTime / 2f)
				.SetEase (Ease.InBack);
		}

		parallax.StopMotion(1.5f * tweenTime);
		bg.DOFade(bgColor.a, 1f).Kill(true);
		container.DOScale(0f, 0.02f)
			.SetDelay(1.5f * tweenTime)
			.SetUpdate(true);
		return bg.DOFade(0f, tweenTime)
			.SetDelay(1.5f * tweenTime)
			.SetEase(Ease.Linear)
			.SetUpdate(true);
	}
	protected override void onEscape ()
	{
		if (PlayerManager.GetActiveGirl() != PPG.None) {
			Close();
		}
	}
}
