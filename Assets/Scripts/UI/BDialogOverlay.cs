﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public enum DialogueSpeaker
{
	Blossom,
	Bubbles,
	Buttercup,
	Bliss,
	HeartShard
}

public class DialogueBuilder
{
	public float Delay{get; private set;}
	bool built;
	public System.Action OnCompleteCallback{get; private set;}
	public AudioClip dialogueStartVO;
	public readonly LinkedList<KeyValuePair<DialogueSpeaker, string>> Conversation = new LinkedList<KeyValuePair<DialogueSpeaker, string>> ();
	public DialogueBuilder Add(DialogueSpeaker _speaker, string _text)
	{
		if (built) {
			throw new System.Exception ("Can't add after being built");
		}
		Conversation.AddLast (new KeyValuePair<DialogueSpeaker, string> (_speaker, _text));
		return this;
	}
	public DialogueBuilder Add(string _text)
	{
		return Add (Conversation.Last.Value.Key, _text);
	}
	public DialogueBuilder AddAsCurrentGirl(string _text)
	{
		var cur = PlayerManager.GetActiveGirl ();
		switch (cur) {
		case PPG.Blossom:
			return Add (DialogueSpeaker.Blossom, _text);
		case PPG.Bubbles:
			return Add (DialogueSpeaker.Bubbles, _text);
		case PPG.Buttercup:
			return Add (DialogueSpeaker.Buttercup, _text);
		}
		throw new System.ArgumentOutOfRangeException(cur + " cannot speak");
	}
	public DialogueBuilder OnComplete(System.Action _onComplete)
	{
		OnCompleteCallback = _onComplete;
		return this;
	}
	public DialogueBuilder SetDelay(float _delay)
	{
		Delay = _delay;
		return this;
	}
	public void Go()
	{
		built = true;
		BDialogOverlay.LaunchDialog (this);
	}
}

public class BDialogOverlay : BBaseUI
{
	const float tweenTime = 0.3f;

	public static BDialogOverlay Instance {get; private set;}
	public static void LaunchDialog(DialogueBuilder _bulider)
	{
		Instance.currentDialog = _bulider.Conversation.First;
		Instance.setSpeakerImage ();
		Instance.dialog.text = Instance.currentDialogText;
		Instance.onComplete = _bulider.OnCompleteCallback;
		Instance.dialogueStartVO = _bulider.dialogueStartVO;

		BCoroutine.WaitAndPerformRealtime(() => {
			Instance.gameObject.SetActive (true);
			Instance.animateIn ();
		}, _bulider.Delay);
	}

	[SerializeField] Sprite blossomHead;
	[SerializeField] Sprite bubblesHead;
	[SerializeField] Sprite buttercupHead;
	[SerializeField] Sprite blissHead;
	[SerializeField] Sprite heartShard;

	[SerializeField] Image bg;
	[SerializeField] TextMeshProUGUI dialog;
	[SerializeField] Image speaker;
	[SerializeField] RectTransform speakerBacking;
	[SerializeField] RectTransform closeButton;

	Color bgColor;
	Dictionary<Object, Vector3> anchoredPos = new Dictionary<Object, Vector3>();
	LinkedListNode<KeyValuePair<DialogueSpeaker, string>> currentDialog;
	System.Action onComplete;
	AudioClip dialogueStartVO;

	int restrictionCount = 0;
	bool canMoveOn {
		get {
			return restrictionCount == 0;
		}
	}
	protected override void Awake ()
	{
		base.Awake ();
		Instance = this;
		speaker.preserveAspect = true;
		bgColor = bg.color;
		anchoredPos.Add (speakerBacking, speakerBacking.anchoredPosition);
		gameObject.SetActive (false);
	}
	void OnDestroy()
	{
		Instance = null;
	}

	DialogueSpeaker currentSpeaker
	{
		get {
			return currentDialog.Value.Key;
		}
	}

	string currentDialogText
	{
		get {
			return currentDialog.Value.Value;
		}
	}

	void setSpeakerImage()
	{
		switch (currentSpeaker) {
		case DialogueSpeaker.Blossom:
			speaker.sprite = Instance.blossomHead;
			break;
		case DialogueSpeaker.Bubbles:
			speaker.sprite = Instance.bubblesHead;
			break;
		case DialogueSpeaker.Buttercup:
			speaker.sprite = Instance.buttercupHead;
			break;
		case DialogueSpeaker.Bliss:
			speaker.sprite = Instance.blissHead;
			break;
		case DialogueSpeaker.HeartShard:
			speaker.sprite = Instance.heartShard;
			break;
		default:
			throw new System.ArgumentOutOfRangeException ();
		}
	}
	void moveOn(LinkedListNode<KeyValuePair<DialogueSpeaker, string>> _next)
	{
		var prevSpeaker = currentDialog.Value.Key;
		currentDialog = currentDialog.Next;
		updateDialogVisual ();
		if (prevSpeaker != currentDialog.Value.Key) {
			updateSpeakerImage ();
		}
	}

	public void Close()
	{
		if (canMoveOn) {
			if (currentDialog.Next != null) {
				moveOn (currentDialog);
			} else {
				animateOut ().OnComplete (() => {
					Instance.gameObject.SetActive (false);
					if (onComplete != null) {
						onComplete();
					}
				});
			}
		}
	}

	void animateIn()
	{
		restrictionCount = int.MaxValue;

		BAudioManager.Instance.AdjustMusicVolume(0.3f, 0.5f);

		bg.DOFade (0f, 1f).Kill (true);
		bg.DOFade (bgColor.a, tweenTime)
			.SetEase (Ease.Linear)
			.SetUpdate (true);

		var offScrenLeft = -tk2dCamera.Instance.nativeResolutionWidth / 2f;
		speakerBacking.DOAnchorPosX (offScrenLeft - (speakerBacking.rect.width / 2f), 1f).Kill (true);
		speakerBacking.DOAnchorPosX (anchoredPos [speakerBacking].x, tweenTime)
			.SetEase (Ease.OutBack)
			.SetDelay(tweenTime)
			.SetUpdate (true);

		dialog.DOFade (0f, 1f).Kill (true);
		dialog.DOFade (1f, tweenTime)
			.SetDelay (2f * tweenTime)
			.SetEase (Ease.Linear)
			.SetUpdate (true)
			.OnComplete(() => {
				restrictionCount = 0;
				if(Instance.dialogueStartVO != null) {
					BAudioManager.Instance.CreateAndPlayAudio(Instance.dialogueStartVO);
				}
			});

//		closeButton.DOScale (0f, 1f).Kill (true);
//		closeButton.DOScale (1f, tweenTime)
//			.SetDelay (2f)
//			.SetEase (Ease.OutBack)
//			.SetUpdate (true);
	}
	Tweener animateOut()
	{
		restrictionCount = int.MaxValue;
		var offScrenLeft = -tk2dCamera.Instance.nativeResolutionWidth / 2f;

		BAudioManager.Instance.AdjustMusicVolume(0.5f, 0.75f);

//		closeButton.DOScale (1f, 1f).Kill (true);
//		closeButton.DOScale (0f, tweenTime)
//			.SetEase (Ease.InBack)
//			.SetUpdate (true);

		dialog.DOFade (1f, 1f).Kill (true);
		dialog.DOFade (0f, tweenTime)
			.SetDelay (tweenTime)
			.SetEase (Ease.Linear)
			.SetUpdate (true);

		speakerBacking.DOAnchorPosX (anchoredPos [speakerBacking].x, 1f).Kill (true);
		speakerBacking.DOAnchorPosX (offScrenLeft - (speakerBacking.rect.width / 2f), tweenTime)
			.SetEase (Ease.InBack)
			.SetDelay(2f * tweenTime)
			.SetUpdate (true);

		bg.DOFade (bgColor.a, 1f).Kill (true);
		return bg.DOFade (0f, tweenTime)
			.SetDelay (2f * tweenTime)
			.SetEase (Ease.Linear)
			.SetUpdate (true);
	}

	void updateDialogVisual()
	{
		++restrictionCount;
		dialog.DOFade (0f, tweenTime)
			.SetEase (Ease.Linear)
			.SetUpdate (true)
			.OnComplete (() => {
				dialog.text = currentDialogText;
				dialog.DOFade (1f, tweenTime)
					.SetEase (Ease.Linear)
					.SetUpdate (true)
					.OnComplete(() => --restrictionCount);
			});
		
	}
	void updateSpeakerImage()
	{
		++restrictionCount;
		var offScrenLeft = -tk2dCamera.Instance.nativeResolutionWidth / 2f;
		speakerBacking.DOAnchorPosX (anchoredPos [speakerBacking].x, 1f).Kill (true);
		speakerBacking.DOAnchorPosX (offScrenLeft - (speakerBacking.rect.width / 2f), tweenTime)
			.SetEase (Ease.InBack)
			.SetUpdate (true)
			.OnComplete (() => {
				setSpeakerImage();
				speakerBacking.DOAnchorPosX (anchoredPos [speakerBacking].x, tweenTime)
					.SetEase (Ease.OutBack)
					.SetUpdate (true)
					.OnComplete(() => --restrictionCount);
			});
	}
}
