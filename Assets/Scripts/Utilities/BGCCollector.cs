﻿using UnityEngine;
using System.Collections;

public class BGCCollector : MonoBehaviour
{
	public int FrameCount = 30;
	void Update()
	{
		if (Time.frameCount % FrameCount == 0) {
			System.GC.Collect();
		}
	}

}
