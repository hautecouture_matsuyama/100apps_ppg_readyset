﻿using UnityEngine;
using System.Collections;

public class BCoroutine : MonoBehaviour
{
	static BCoroutine instance;
	public static BCoroutine Instance
	{
		get {
			if (instance == null) {
				instance = SingletonScriptGenerator.Create<BCoroutine>();
			}
			return instance;
		}
	}

	void OnDestroy()
	{
		instance = null;
	}

	public static void WaitAndPerformRealtime(System.Action _action, float _delay)
	{
		if (Instance != null) {
			Instance.StartCoroutine(waitAndPerformRealtime(_action, _delay));
		}
	}
	static IEnumerator waitAndPerformRealtime(System.Action _action, float _delay)
	{
		if (_delay < 0) {
			yield return null;
		} else { 
			yield return new WaitForSecondsRealtime(_delay);
		}
		_action();
	}
	public static void WaitAndPerform(System.Action _action, float _delay)
	{
		if (Instance != null) {
			Instance.StartCoroutine(waitAndPerform(_action, _delay));
		}
	}
	static IEnumerator waitAndPerform(System.Action _action, float _delay)
	{
		if (_delay < 0) {
			yield return null;
		} else { 
			yield return new WaitForSeconds(_delay);
		}
		_action();
	}
	public static void PerformInternetCheck(System.Action _isConnectedAction, System.Action _isNotConnectedAction)
	{
		Instance.StartCoroutine (BInternetConnection.IsInternetAvailable ((isConnected) => {
			if (isConnected) {
				_isConnectedAction ();
			} else {
				_isNotConnectedAction ();
			}
		}));
	}
}
