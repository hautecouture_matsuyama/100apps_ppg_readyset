﻿using UnityEngine;

public class BAppEventListener : MonoBehaviour 
{
	void OnApplicationPause( bool lostFocus )
	{
		if (lostFocus) {
			//Create local notifications if app lost focus
			GameNotifications.CreateNotifications();
			//Pause the game if in gameplay
			if(BPauseOverlay.Instance != null &&
				!FreewheelManager.AdIsPlaying &&
				PlayerManager.NumberofGirlUnlocked() > 0 &&
				BattleManager.CurrentGameState != GameState.GameOver &&
				BattleManager.CurrentGameState != GameState.Limbo) {
				BPauseOverlay.Instance.Pause();
			}
		} else {
			//Clear local notifications if returning to app
			GameNotifications.ClearNotifications();
			#if UNITY_IOS
			//Attempt GC login
			BGameCenterManager.AuthenticateGCUser();
			#endif
		}
	}
}
