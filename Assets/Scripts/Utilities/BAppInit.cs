﻿using UnityEngine;
using System.Collections;

public class BAppInit : MonoBehaviour {

	void Start () 
	{
		//We discovered during SSB that this fixes some lag/fps issues on iOS
		Application.targetFrameRate = 60;
	}
}
