﻿using UnityEngine;
using System.Collections;

public class BInternetConnection
{

	private const bool allowCarrierDataNetwork = true;
	private const string pingAddress = "8.8.8.8"; // Google Public DNS server
	private const float waitingTime = 1.0f;

	private static Ping ping;
	private static float pingStartTime;

	public static IEnumerator IsInternetAvailable(System.Action<bool> action)
	{
		bool internetPossiblyAvailable;
		switch (Application.internetReachability) {
			case NetworkReachability.ReachableViaLocalAreaNetwork:
				internetPossiblyAvailable = true;
				break;
			case NetworkReachability.ReachableViaCarrierDataNetwork:
				internetPossiblyAvailable = allowCarrierDataNetwork;
				break;
			default:
				internetPossiblyAvailable = false;
				break;
		}
		if (!internetPossiblyAvailable) {
			action(false);
		} else {
			ping = new Ping(pingAddress);
			pingStartTime = Time.time;
			while (ping != null) {
				bool stopCheck = true;
				if (ping.isDone) {
					if (ping.time >= 0) {
						action(true);
					} else {
						action(false);
					}
				} else if (Time.time - pingStartTime < waitingTime) {
					stopCheck = false;
				} else {
					action(false);
				}
				if (stopCheck) {
					ping = null;
				}
				yield return null;
			}
		}
	}
}
