﻿using UnityEngine;
using System.Collections;

public class BDelayInvoker : MonoBehaviour
{

	private static BDelayInvoker instance;

	public static BDelayInvoker Instance
	{
		get {
			if (instance == null) {
				instance = SingletonScriptGenerator.Create<BDelayInvoker>();
			}
			return instance;
		}

	}

	void OnDestroy()
	{
		instance = null;
	}

	public void InvokeAfterDelay(System.Action _fn, float _time)
	{
		StartCoroutine(delay(_fn, _time));
	}

	IEnumerator delay(System.Action _fn, float _time)
	{
		yield return new WaitForSeconds(_time);
		_fn();
	}
}
