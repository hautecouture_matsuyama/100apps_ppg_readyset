﻿using UnityEngine;
using System.Collections;

public class BPersistentGameObject : MonoBehaviour {

	private bool isPrimary = false;

	void Awake()
	{
		int count = 0;
		foreach(var gObj in GameObject.FindObjectsOfType<BPersistentGameObject>()) {
			if(gObj.name == this.gameObject.name) ++count;
		}
		if(count == 1) {
			isPrimary = true;
			DontDestroyOnLoad(this);
		}
	}

	void Start()
	{
		if(!isPrimary) Destroy(this.gameObject);
	}
}
