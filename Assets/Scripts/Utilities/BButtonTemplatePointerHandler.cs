﻿
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class BButtonTemplatePointerHandler : MonoBehaviour, IPointerExitHandler, IPointerUpHandler, IPointerDownHandler
{
	[SerializeField] EventTrigger.TriggerEvent pointerExit;
	[SerializeField] EventTrigger.TriggerEvent pointerDown;
	[SerializeField] EventTrigger.TriggerEvent pointerUp;

	#region IPointerExitHandler implementation

	public void OnPointerExit (PointerEventData eventData)
	{
		if (pointerExit != null) {
			pointerExit.Invoke(eventData);
		}
	}

	#endregion

	#region IPointerUpHandler implementation

	public void OnPointerUp (PointerEventData eventData)
	{
		if (pointerUp != null) {
			pointerUp.Invoke(eventData);
		}
	}

	#endregion

	#region IPointerDownHandler implementation

	public void OnPointerDown (PointerEventData eventData)
	{
		if (pointerDown != null) {
			pointerDown.Invoke(eventData);
		}
	}

	#endregion


}
