﻿using UnityEngine;
using System.Collections;

public class BGameplaySleep : MonoBehaviour
{
	static BGameplaySleep instance;
	static Coroutine sleepRoutine;
	public static void Sleep(float _seconds, float _delay = 0)
	{
		if(instance == null) {
			instance = SingletonScriptGenerator.Create<BGameplaySleep>();
		}
		if (sleepRoutine == null) {
			sleepRoutine = instance.StartCoroutine(instance.waitForSleep(_seconds, _delay));
		}
	}
	void OnDestroy()
	{
		instance = null;
		sleepRoutine = null;
	}
	IEnumerator waitForSleep(float _seconds, float _delay)
	{
		if (BPauseOverlay.IsPaused) { yield break;}
		yield return new WaitForSeconds(_delay);
		if (BPauseOverlay.IsPaused) { yield break;}
		Time.timeScale = 0f;
		yield return new WaitForSecondsRealtime(_seconds);
		if (BPauseOverlay.IsPaused) { yield break;}
		Time.timeScale = 1.0f;
		sleepRoutine = null;
	}

}
