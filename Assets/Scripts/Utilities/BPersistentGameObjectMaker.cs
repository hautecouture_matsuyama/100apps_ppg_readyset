﻿using UnityEngine;
using System.Collections;

public class BPersistentGameObjectMaker : MonoBehaviour {

	public string NameToCheck;

	void Awake () 
	{
		int count = 0;
		string combinedNameToCheck = NameToCheck + "(Clone)";
		foreach(var gObj in GameObject.FindObjectsOfType<BPersistentGameObject>()) {
			if(gObj.name == combinedNameToCheck) ++count;
		}
		if(count == 0) {
			var go = Resources.Load<GameObject>("Prefabs/" + NameToCheck);
			Instantiate(go);
		}
		Destroy(gameObject);
	}
}
