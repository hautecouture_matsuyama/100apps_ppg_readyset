/*
 *	This class must be initialized before using. Initialize with FreeWheel.Init(Game object name, callback function) 
 *  The object that calls initialization will need a callback function that will be used for FreeWheel messaging 
 *  
 *  Server properties must also be assigned for ads to play. Set server properties with Freewheel.SetServerInfo(url, network, profile, section, id)  
 * 
 * 
 */

using UnityEngine;
using System.Runtime.InteropServices;

public static class FreeWheel
{
	// These should be enum values, not strings.  
	// Unity does not allow SendMessage() from Java to pass ints, only strings, 
	public const string kRequestStarted = "REQUEST_STARTED";
	public const string kRequestEnded = "REQUEST_ENDED";
	public const string kRequestFailed = "REQUEST_FAILED";
	public const string kShowStarted = "SHOW_STARTED";
	public const string kSlotLoaded = "SLOT_LOADED";
	public const string kShowEnded = "SHOW_ENDED";
	public const string kError = "ERROR";

	#if UNITY_IPHONE && !UNITY_EDITOR
	
	[DllImport("__Internal")]
	private static extern void _FWInit(string objName, string msg);

	[DllImport("__Internal")]
	private static extern void _FWShowAds();

	[DllImport("__Internal")]
	private static extern void _FWRequestAds();

	[DllImport("__Internal")]
	private static extern bool _FWIsReadyToPlayAds();
	
	[DllImport("__Internal")]
	private static extern void _FWSetServerInfo(string serverURL, string networkID, string profile, string siteSection, string videoAssetID);

	public static void Init(string objName, string msg)
	{
		Debug.Log("FW: Calling Init from unity");
		_FWInit(objName, msg);
	}

	public static void SetServerInfo(string serverURL, int networkID, string profile, string siteSection, string videoAssetID) 
	{
		_FWSetServerInfo(serverURL, networkID.ToString(), profile, siteSection, videoAssetID);
		Debug.Log (string.Format("FW: Calling setServerInfo from Unity ({0}. {1}, {2}, {3}, {4})", serverURL, networkID, profile, siteSection, videoAssetID));
	}

	public static void RequestAds(string requestAdsCompleteCallback = null)	{
		_FWRequestAds();
	}

	public static void ShowAds(string showAdsCompleteCallback = null) {
		_FWShowAds();
	}

	public static bool IsReadyToPlayAds() {
		return _FWIsReadyToPlayAds();
	}
	#elif UNITY_ANDROID && !UNITY_EDITOR

	private static AndroidJavaClass freeWheel;
	private static AndroidJavaObject unityActivity;


	public static void Init(string freeWheelMessageObject = "", string freeWheelMessageCallback = "") {

		// this will force unity to include the internet permission
		// Is this necessary?  Can we force it via the ui?
		WWW www = new WWW("http://cartoonnetwork.com");


		// Get the current activity and save it
		AndroidJavaClass unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		unityActivity = unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity");

		//Give the activity instance to my own static method to launch an activity from there
		Debug.Log ("FW: Calling Init from Unity");
		freeWheel = new AndroidJavaClass("com.cartoonnetwork.services.FreeWheelController");
		freeWheel.CallStatic("init", unityActivity, freeWheelMessageObject, freeWheelMessageCallback);
		//freeWheel = fw.Call<AndroidJavaObject>("initFreeWheel");
	}
	 
	public static void RequestAds()
	{
		Debug.Log ("FW: Calling RequestAds from Unity");
		freeWheel.CallStatic("requestAds");
	}
	
	public static void ShowAds()
	{
		Debug.Log ("FW: Calling ShowAds from Unity");
		freeWheel.CallStatic("showAds");
	}
	
	public static bool IsReadyToPlayAds()
	{

		return freeWheel.CallStatic<bool>("isReadyToPlayAds");
	}

	public static void SetServerInfo(string serverURL, int networkID, string profile, string siteSection, string videoAssetID) 
	{
		Debug.Log (string.Format("FW: Calling setServerInfo from Unity ({0}. {1}, {2}, {3}, {4})", serverURL, networkID, profile, siteSection, videoAssetID));
		freeWheel.CallStatic("setServerInfo", serverURL, networkID, profile, siteSection, videoAssetID);
	}

	#else

	public static void Init(string objName, string msg) 
	{
		if(!BDebugOverlay.HideSDKLogs) Debug.Log("FW: Called Init/Reset stub: " + objName + ": " + msg );
	}

	public static void RequestAds()
	{
		if(!BDebugOverlay.HideSDKLogs) Debug.Log("FW: Called RequestAds() stub");
	}

	public static void ShowAds()
	{
		if(!BDebugOverlay.HideSDKLogs) Debug.Log("FW: Called ShowAds() stub");
	}

	public static bool IsReadyToPlayAds()
	{
		if(!BDebugOverlay.HideSDKLogs) Debug.Log("FW: Called IsReadyToPlayAds() stub");
		return true;
	}
	
	public static void SetServerInfo(string serverURL, int networkID, string profile, string siteSection, string videoAssetID) 
	{
		if(!BDebugOverlay.HideSDKLogs) Debug.Log("FW: Called SetServerInfo() stub");
	}
	#endif
}

