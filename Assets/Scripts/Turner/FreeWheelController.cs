﻿using UnityEngine;
using System.Collections;

public class FreeWheelController : MonoBehaviour 
{
	#if (UNITY_IOS || UNITY_ANDROID)

	public static FreeWheelController instance = null;
	public enum FreeWheelServer
	{
		DEMO,
		PRODUCTION,
	}
	private enum FreeWheelAdType
	{
		PREROLL,
		LOADING,
	}

	public FreeWheelServer server;
	public string iOSProductionPrerollID, iOSProductionLoadingID, androidProductionPrerollID, androidProductionLoadingID;
	private string fw_preroll, fw_loading;

	#if UNITY_ANDROID
	private const string FW_PROD_URL = "bea4.v.fwmrm.net";
	private const int FW_PROD_NETWORK_ID = 48804;
	private const string FW_PROD_PROFILE = "turner_android_fw512";
	private const string FW_PROD_ASSET_ID = "toon-dummy-ros";
	private const string FW_DEMO_URL = "http://demo.v.fwmrm.net";
	private const int FW_DEMO_NETWORK_ID = 42015;
	private const string FW_DEMO_PROFILE = "fw_tutorial_android";
	private const string FW_DEMO_SITE_SECTION_PREROLL = "fw_tutorial_android";
	private const string FW_DEMO_SITE_SECTION_LOADING = "fw_tutorial_android";
	private const string FW_DEMO_ASSET_ID = "fw_simple_tutorial_asset";
	#elif UNITY_IOS
	private const string FW_PROD_URL = "https://bea4.v.fwmrm.net";
	private const int FW_PROD_NETWORK_ID = 48804;
	private const string FW_PROD_PROFILE = "turner_ios_fw69";
	private const string FW_PROD_ASSET_ID = "toon-dummy-ros";
	private const string FW_DEMO_URL = "https://bea4.v.fwmrm.net";
	private const int FW_DEMO_NETWORK_ID = 42448;
	private const string FW_DEMO_PROFILE = "turner_ios_fw69";
	private const string FW_DEMO_SITE_SECTION_PREROLL = "cn.com_mobile_unity_testapp";
	private const string FW_DEMO_SITE_SECTION_LOADING = "cn.com_mobile_unity_testapp";
	private const string FW_DEMO_ASSET_ID = "toon-dummy-ros";
	#endif

	void Awake()
	{
		if(instance == null)
		{
			instance = this;
		}
		else if(instance != this)
		{
			Destroy(this.gameObject);
		}

		SetProfileIDs();

		FreeWheel.Init(this.gameObject.name, "OnFreeWheelMessage");
		if(server.Equals(FreeWheelServer.DEMO))
		{
			FreeWheel.SetServerInfo(FW_DEMO_URL, FW_DEMO_NETWORK_ID, FW_DEMO_PROFILE, FW_DEMO_SITE_SECTION_PREROLL, FW_DEMO_ASSET_ID); 
		}
		else if(server.Equals(FreeWheelServer.PRODUCTION))
		{
			FreeWheel.SetServerInfo(FW_PROD_URL, FW_PROD_NETWORK_ID, FW_PROD_PROFILE, fw_preroll, FW_PROD_ASSET_ID);
		}
	}


	/* 
	 * Any custom behavior (such as pausing or changing audio) should listen to these callbacks.
	 */ 
	 
	public void OnFreeWheelMessage(string msg)
	{
		if(!BDebugOverlay.HideSDKLogs) Debug.Log("FW: callback: " + msg);

		switch (msg)
		{
		case FreeWheel.kShowStarted:
			if(FreewheelManager.OnStart != null && (FreewheelManager.OnStart.Method.IsStatic || FreewheelManager.OnStart.Target != null)) {
				FreewheelManager.OnStart();
				FreewheelManager.OnStart = null;
			}
			break;
		case FreeWheel.kShowEnded:
			if(FreewheelManager.OnEnd != null && (FreewheelManager.OnEnd.Method.IsStatic || FreewheelManager.OnEnd.Target != null)) {
				FreewheelManager.OnEnd();
				FreewheelManager.OnEnd = null;
			}
			break;
		case FreeWheel.kRequestStarted:
			break;
		case FreeWheel.kRequestEnded:
			break;
		case FreeWheel.kRequestFailed:
			if(FreewheelManager.OnEnd != null && (FreewheelManager.OnEnd.Method.IsStatic || FreewheelManager.OnEnd.Target != null)) {
				FreewheelManager.OnEnd();
				FreewheelManager.OnEnd = null;
			}
			break;
		case FreeWheel.kSlotLoaded:
			FreeWheel.ShowAds();
			break;
		case FreeWheel.kError:
			if(FreewheelManager.OnEnd != null && (FreewheelManager.OnEnd.Method.IsStatic || FreewheelManager.OnEnd.Target != null)) {
				FreewheelManager.OnEnd();
				FreewheelManager.OnEnd = null;
			}
			break;
		default:
			Debug.LogError("Unknown FreeWheel callback: " + msg);
			break;
		}
	}

	public void RequestLoadingAd()
	{
		#if UNITY_ANDROID
		if(FreeWheel.IsReadyToPlayAds())
		{
			FreeWheel.ShowAds();
		}
		else
		{
			if(server.Equals(FreeWheelServer.DEMO))
			{
				FreeWheel.SetServerInfo(FW_DEMO_URL, FW_DEMO_NETWORK_ID, FW_DEMO_PROFILE, FW_DEMO_SITE_SECTION_LOADING, FW_DEMO_ASSET_ID); 
			}
			else if(server.Equals(FreeWheelServer.PRODUCTION))
			{
				FreeWheel.SetServerInfo(FW_PROD_URL, FW_PROD_NETWORK_ID, FW_PROD_PROFILE, fw_loading, FW_PROD_ASSET_ID);
			}

			FreeWheel.RequestAds();
		}
		#elif UNITY_IOS
		FreeWheel.Init(this.gameObject.name, "OnFreeWheelMessage");
		if(server.Equals(FreeWheelServer.DEMO))
		{
			FreeWheel.SetServerInfo(FW_DEMO_URL, FW_DEMO_NETWORK_ID, FW_DEMO_PROFILE, FW_DEMO_SITE_SECTION_LOADING, FW_DEMO_ASSET_ID); 
		}
		else if(server.Equals(FreeWheelServer.PRODUCTION))
		{
			FreeWheel.SetServerInfo(FW_PROD_URL, FW_PROD_NETWORK_ID, FW_PROD_PROFILE, fw_loading, FW_PROD_ASSET_ID);
		}

		FreeWheel.RequestAds();
		#endif
	}

	public void RequestPrerollAd()
	{
		#if UNITY_ANDROID
		if(FreeWheel.IsReadyToPlayAds())
		{
			FreeWheel.ShowAds();
		}
		else
		{
			if(server.Equals(FreeWheelServer.DEMO))
			{
				FreeWheel.SetServerInfo(FW_DEMO_URL, FW_DEMO_NETWORK_ID, FW_DEMO_PROFILE, FW_DEMO_SITE_SECTION_PREROLL, FW_DEMO_ASSET_ID); 
			}
			else if(server.Equals(FreeWheelServer.PRODUCTION))
			{
				FreeWheel.SetServerInfo(FW_PROD_URL, FW_PROD_NETWORK_ID, FW_PROD_PROFILE, fw_preroll, FW_PROD_ASSET_ID);
			}

			FreeWheel.RequestAds();
		}
		#elif UNITY_IOS
		FreeWheel.Init(this.gameObject.name, "OnFreeWheelMessage");
		if(server.Equals(FreeWheelServer.DEMO))
		{
			FreeWheel.SetServerInfo(FW_DEMO_URL, FW_DEMO_NETWORK_ID, FW_DEMO_PROFILE, FW_DEMO_SITE_SECTION_PREROLL, FW_DEMO_ASSET_ID); 
		}
		else if(server.Equals(FreeWheelServer.PRODUCTION))
		{
			FreeWheel.SetServerInfo(FW_PROD_URL, FW_PROD_NETWORK_ID, FW_PROD_PROFILE, fw_preroll, FW_PROD_ASSET_ID);
		}		

		FreeWheel.RequestAds();
		#endif
	}

	private void SetProfileIDs()
	{
		#if UNITY_ANDROID
		fw_preroll = androidProductionPrerollID;
		fw_loading = androidProductionLoadingID;
		#elif UNITY_IOS
		fw_preroll = iOSProductionPrerollID;
		fw_loading = iOSProductionLoadingID;
		#endif
	}

	#endif
				
}
