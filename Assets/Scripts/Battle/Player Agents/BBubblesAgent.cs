﻿using UnityEngine;
using System.Collections;

public class BBubblesAgent : BPlayerAgent
{
	protected override float baseHealthStat  {get {return DesignValues.Bubbles.BaseStats.Health;}}
	protected override float baseStaminaStat {get {return DesignValues.Bubbles.BaseStats.Stamina;}}
	protected override float baseAttackStat  {get {return DesignValues.Bubbles.BaseStats.Attack;}}
	protected override float baseShieldStat  {get {return DesignValues.Bubbles.BaseStats.Shield;}}

	protected override float attack1Power{get{return DesignValues.Bubbles.Attack1Power;}}
	protected override float attackCooldownTime{get{return DesignValues.Bubbles.ComboCooldownTime;}}
	protected override ComboData[] comboData{get{return DesignValues.Bubbles.ComboData;}}

	protected override float staminaForAttack{get{return DesignValues.Bubbles.StaminaData.Attack;}}
//	protected override float staminaFillCooldownTime{get{return DesignValues.Bubbles.StaminaData.FillCooldownTime;}}
//	protected override float staminaFillRatePerSecond{get{return DesignValues.Bubbles.StaminaData.FillRatePerSecond;}}
//	protected override float staminaFillRateWhileBlockingPerSecond{get{return DesignValues.Bubbles.StaminaData.FillRateWhileBlocking;}}

	protected override int comboCount
	{
		get{
			return PlayerManager.Instance.BubblesAcquiredStats.X2Unlocked ?
				comboData.Length : 2;
		}
	}

	public override float Health
	{
		get {
			return PlayerManager.BubblesHealth;
		}
		protected set {
			PlayerManager.BubblesHealth = value;
		}
	}

	protected override void Awake()
	{
		base.Awake();
		acquiredStats = PlayerManager.Instance.BubblesAcquiredStats;
	}
}
