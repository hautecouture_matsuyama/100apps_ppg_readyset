﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using Spine.Unity;

public class BPlayerAgent : BAgent
{
	public delegate void OnSuccessfulAttack();
	public event OnSuccessfulAttack HandleSuccessfulAttack;

	public static GameObject FmInstance{get; private set;}
	public static void CleanFMInstanceHandle()
	{
		FmInstance = null;
	}

	[SerializeField] GameObject AuraParent;

	protected override float telegraphTime{get{return 0;}}

	public override float ImageHeight{get{return 300f;}}
	public bool IsCurrentlyCharging{get; private set;}

	Vector3 originalAuraScale;
	protected tk2dSprite sprite;
	int comboIndex;//used for combo
	Tweener staminaBarTween;
	protected virtual ComboData[] comboData{get{throw new System.NotImplementedException();}}
	protected virtual int comboCount{get{throw new System.NotImplementedException();}}

	protected override string counterAttackAnimation{get {return "CounterAttack";}}
	protected override bool canCounterAttack
	{
		get {
			return acquiredStats.X3Unlocked && base.canCounterAttack;
		}
	}


	protected override void Awake ()
	{
		base.Awake();
		tkAnim = GetComponent<tk2dSpriteAnimator>();
		sprite = GetComponent<tk2dSprite>();

	}
	protected override void Start ()
	{
		base.Start ();
		var sliderParent = GameObject.Find("Health UI").transform;
		healthSlider = sliderParent.GetChild(0).gameObject.GetComponent<Slider>();
		var healthRT = healthSlider.GetComponent<RectTransform> ();
		var healthSizeDelta = healthRT.sizeDelta;
		healthSizeDelta = new Vector2(MaxHealth * 10, healthSizeDelta.y);
		healthRT.sizeDelta = healthSizeDelta;
		healthSliderBacking = healthSlider.transform.GetChild(1).GetComponent<Slider>();

		staminaSlider = sliderParent.GetChild(1).gameObject.GetComponent<Slider>();
		var staminaRT = staminaSlider.GetComponent<RectTransform> ();
		var staminaSizeDelta = staminaRT.sizeDelta;
		staminaSizeDelta = new Vector2(maxStamina * 10, staminaSizeDelta.y);
		staminaRT.sizeDelta = staminaSizeDelta;
		staminaSliderBacking = staminaSlider.transform.GetChild(1).GetComponent<Slider>();

		comboIndex = 0;
		makeFriendlyMonster ();
		UpdateHealthUI();
	}
	public override void SetTakeDamagePacket(AttackPacket _packet)
	{
		if (!BGuardFriendlyMonster.IsDefending) {
			base.SetTakeDamagePacket(_packet);
		}
	}
	public void Revive()
	{
		IsDead = false;
		Health = MaxHealth;
		tweenHealthUIUp(0, Health);
		Stamina = maxStamina;
		onStaminaChanged(Stamina);
		BAudioManager.Instance.PlayHealSound ();
		BEffectsPool.CreateHealEffectAtPos(transform.position + Vector3.up * ImageHeight * 0.7f);
		safePlayClip("Idle");
		shadowObject.SetActive(true);
		shadowObject.transform.DOTogglePause();
		makeFriendlyMonster ();
	}
	public void UpdateHealthUI()
	{
		if (enabled) {
			tweenHealthUIUp(0, Health, 0.5f);
		}
	}
	public override void UpdateHealthAndUpdateUI(float _amount)
	{
		base.UpdateHealthAndUpdateUI(_amount);
		sprite.DOColor(Color.green, 0.2f).SetEase(Ease.Linear).OnComplete(() => {
			sprite.DOColor(Color.white, 0.2f).SetEase(Ease.Linear);
		});
	}
	public void FakeZeroOutStamina()
	{
		onStaminaChanged(0);
	}
	public void UpdateFM()
	{
		if (FmInstance != null) {
			Destroy(FmInstance);
			FmInstance = null;
		}
		makeFriendlyMonster();
	}
	public void PrepareForSuper()
	{
		if (IsBlocking) {
			OnBlockReleased();
		}
		if (IsTakingHit) {
			this.SafeStopCoroutine(ref takeHitRoutine);
		}
		if (IsStunned) {
			this.SafeStopCoroutine(ref stunRoutine);
			Stamina = maxStamina;
			onStaminaChanged(maxStamina);
			var animator = stunEffect.GetComponent<SkeletonAnimation>();
			animator.AnimationState.TimeScale = 1000f;
		}
		canAttack = true;
		safePlayClip("Idle");
	}
	public void OnBattleWon()
	{
		safePlayAudio(effectsData.VictoryVOClips);
		healthSliderBacking.DOValue(healthSlider.value, 0.2f).SetEase(Ease.Linear);
	}
	public void OnBattleStart()
	{
		safePlayAudio(effectsData.BattleStartVOClips);
	}
	#region Internal Callbacks
	protected override void onDeath()
	{
		base.onDeath();
		if (PlayerManager.AreAllGirlsDead()) {
			BAudioManager.Instance.StopMusic(0, 1);
			BAudioManager.Instance.StopIntroMusic();
			StartCoroutine(waitForPlayGameOverSting());
		}
		//The above 2 lines must happen first, in that order
		if(shadowObject != null) {
			shadowObject.SetActive(false);
			shadowObject.transform.DOTogglePause();
		}
		if (BattleManager.PlayerPartyLead == this) {
			BattleManager.OnCurrentPlayerLeadDied();
		}
		safePlayAudio(effectsData.FaintVO);
	}
	protected override void onTelegraphInterrupted()
	{
		base.onTelegraphInterrupted();
		comboIndex = 0;
	}
	protected override IEnumerator doTelegraph ()
	{
		if (canCounterAttack) {
			StartCoroutine(doCounterAttack());
		} else {
			safePlayAudio(effectsData.TelegraphSound);
			StartCoroutine(doAttack());
			yield return null;
		}
	}
	protected override void onPerfectBlock ()
	{
		base.onPerfectBlock ();
		if (canCounterAttack && !BTutorialManager.ShownCounterAttackTutorial) {
			BTutorialManager.PaueForCounterAttack();
		}
	}
	protected override void onStaminaChanged(float _to)
	{
		base.onStaminaChanged(_to);
		if(_to < 10 && staminaBarTween == null) {
			staminaBarTween = staminaSlider.transform.DOScale(1.05f, 0.2f).SetEase(Ease.InOutQuad).SetLoops(-1, LoopType.Yoyo);
		} else if (_to >= 10 && staminaBarTween != null) {
			staminaSlider.transform.DOKill();
			staminaBarTween = null;		
			staminaSlider.transform.DOScale(1, 0).Kill(true);
		}
	}
	#endregion

	#region Helpers

	void makeFriendlyMonster ()
	{
		if (PlayerManager.Instance.CurrentFriendlyMonster != null && FmInstance == null) {
			var fm = (FriendlyMonsterType)PlayerManager.Instance.CurrentFriendlyMonster;
			var fmArray = PlayerEquipsManager.FriendlyMonsters [fm];
			var fmPrefab = Resources.Load<GameObject> (@"Prefabs/FriendlyMonsters/" + fmArray [PlayerManager.Instance.CurrentFriendlyMonsterSkinIndex].Name);
			FmInstance = Object.Instantiate (fmPrefab, transform.parent, true);
			FmInstance.transform.localPosition = Vector3.left * 170;
			FmInstance.transform.localPosition += Vector3.down * 50;
		}
	}
	protected override bool onTakeDamage()
	{
		if (BattleManager.EnemyPartyLead != null && !BGuardFriendlyMonster.IsDefending) {
			var tookDamage = base.onTakeDamage();
			if (!didPerfectBlock) {
				if (IsBlocking) {
					if(shieldEffect != null) {
						var comp = shieldEffect.GetComponent<SkeletonAnimation>();
						comp.AnimationState.SetAnimation(0, "Regular_TakeHit", false);
						comp.AnimationState.AddAnimation(0, "Regular_Hold", true, 0);
					}
					BCameraShake.Instance.SmallShake();
				} else {
					BEffectsPool.CreateNormalHitEffectAtPos(sprite.transform.position + (Vector3.up * sprite.GetBounds().size.y * 0.8f));
					BCameraShake.Instance.MediumShake();
				}
			}
			return tookDamage;
		}
		takeDamagePacket = null;
		return false;
	}
	protected override bool hasClip(string _clip)
	{
		return tkAnim.HasClip(_clip);
	}
	protected override float getTimeForClip(string _clip)
	{
		return tkAnim.GetTimeForClip(_clip);
	}
	protected override void safePlayClip(string _name, string _next = "", float _speedScalar = 1)
	{
		if (IsDead) {
			return;
		}
		tkAnim.AnimationCompleted = null;
		if(!string.IsNullOrEmpty(_name) && tkAnim.GetClipByName(_name) != null) {
			tkAnim.Play(_name);
			if (!string.IsNullOrEmpty(_next)) {
				tkAnim.AnimationCompleted  = (a, b) => {
					safePlayClip(_next, "");
					tkAnim.AnimationCompleted = null;
				};
			}
		} else if(!string.IsNullOrEmpty(_next)) {
			safePlayClip(_next, "");
		}
	}

	void updateComboIndex ()
	{
		++comboIndex;
		comboIndex = ((comboIndex + comboCount) % comboCount);
	}

	protected override IEnumerator doAttack()
	{
		if (!IsDead) {
			onStartAttack();
			var currentComboData = comboData[comboIndex];
			var attackAnimName = string.Format("Attack{0}", currentComboData.ComboAttackAnimNum);
			safePlayClip(attackAnimName);
			var baseTime = getTimeForClip (attackAnimName);
			var time = baseTime + currentComboData.HoldAttackDuration;
			var earlyThresholdTime = time * (1 - currentComboData.EarlyTimingPercent);
			var lateThresholdTime = time + currentComboData.LateTimingDuration;
			var reallowBlockingTime = time * (1 - currentComboData.ReallowBlockingPercent);
			var frameForAttack = currentComboData.AttackLandFrame;
			var timeTillLandAttack = frameForAttack / tkAnim.ClipFps;
			const float waitForCheckCharging = 0.15f;
			var checkedForCharge = false;

			var tookDamageDuringAttack = false;
			var nextAttackQueued = false;
			var performedAttack = false;
			var timer = 0f;
			while (timer < time) {
				if (tkAnim.CurrentFrame < frameForAttack) {
					sendTelegraphUpdate(timer / timeTillLandAttack, timeTillLandAttack);
				}
				yield return null;
				timer += Time.deltaTime;
				//Check if the player meant to do a chargeattack
				if (timer >= waitForCheckCharging && !performedAttack && !checkedForCharge) {
					checkedForCharge = true;
					if(acquiredStats.AuraUnlocked && comboIndex == 0 && holdingAttack && (!tookDamageDuringAttack || superArmor)) {
						StartCoroutine(doChargeAttack());
						yield break;
					}
				}
				//check if the hit landed
				if (tkAnim.CurrentFrame == frameForAttack && !performedAttack) {
					performedAttack = true;
					if (!BattleManager.EnemyPartyLead.IsDodging) {
						safePlayAudio(effectsData.AttackSounds);
						if(Utilities.FiftyFifty) {
							safePlayAudio(effectsData.AttackVOClips);
						}
					}
					//Take the combo modifier into account, but don't modify the damage by the Attack stat again
					var doCritical = BattleManager.EnemyPartyLead.IsStunned;
					var modifiedPacket = new AttackPacket(this, rawAttackStat, attack1Power * currentComboData.DamageModifier, doCritical);
					BattleManager.AttackOpponent(modifiedPacket);
					decreaseStaminaByValue(staminaForAttack);
					if (HandleSuccessfulAttack != null) {
						HandleSuccessfulAttack();
					}
					if (IsStunned) {
						yield return new WaitForSeconds(baseTime - timer);
						goto ExitEarly;
					}
				}
				//if the last hit killed the enemy, let the original animation time finish before returning to idle and what not
				if (!BattleManager.InBattle) {
					yield return new WaitForSeconds(baseTime - timer);
					goto ExitEarly;
				}
				//then check if damage was taken
				if (takeDamagePacket != null) {
					onTakeDamage();
					tookDamageDuringAttack = true;
					if (!superArmor) {
						goto ExitEarly;
					}
				}
				//then check for a combo
				if (timer >= earlyThresholdTime) {
					if(!canQueue) {
						canQueue = true;
					}
					if (inputQueued && !nextAttackQueued) {
						nextAttackQueued = true;
					}
				}
				//then check for a reallow blocking window
				if (timer >= reallowBlockingTime) {
					if(!nextAttackQueued && holdingBlock) {
						goto ExitEarly;
					}
				}
			}
			//if we're still in battle and haven't already queued up an attack and
			//haven't flinched from taking damage, check the hang-time for another queued attack
			if (BattleManager.InBattle && !nextAttackQueued && (!tookDamageDuringAttack || superArmor)) {
				while (timer <= lateThresholdTime) {
					yield return null;
					timer += Time.deltaTime;
					if (takeDamagePacket != null) {
						onTakeDamage();
						tookDamageDuringAttack = true;
						if (!superArmor) {
							goto ExitEarly;
						}
					}
					if (inputQueued && !nextAttackQueued) {
						nextAttackQueued = true;
						break;
					}
				}
			}
			if (nextAttackQueued && (!tookDamageDuringAttack || superArmor)) {
				updateComboIndex ();
				if (comboIndex > 0 ) {
					StartCoroutine(doAttack());
					yield break;
				}
			} 
			//Check for charge attack again, for Bubbles (since her attacks are so fast)
//			else if (!checkedForCharge && acquiredStats.AuraUnlocked && comboIndex == 0 && holdingAttack && (!tookDamageDuringAttack || superArmor)) {
//				checkedForCharge = true;
//				StartCoroutine(doChargeAttack());
//				yield break;
//			}
			if (tookDamageDuringAttack && superArmor) {
				goto ExitEarly;
			} else {
				StartCoroutine(doCooldown());
			}
			goto CleanupAttackState;
ExitEarly:
			canAttack = true;
CleanupAttackState:
			if(!tookDamageDuringAttack || (tookDamageDuringAttack && superArmor)) {
				safePlayClip("Idle");
			}
			onEndAttack();
			comboIndex = 0;
		}
	}
	protected override IEnumerator doStun ()
	{
		BPlayerInput.Instance.DisableBlockButton();
		BPlayerInput.Instance.DisableAttackButton();
		yield return StartCoroutine(base.doStun ());
		BPlayerInput.Instance.EnableBlockButton();
		BPlayerInput.Instance.EnableAttackButton();
	}

	IEnumerator waitForPlayGameOverSting()
	{
		yield return new WaitForSeconds(0.5f);
		BAudioManager.Instance.CreateAndPlayAudio(effectsData.GameOverSting, false);
		yield return new WaitForSeconds(effectsData.GameOverSting.length * 0.3f);
		BAudioManager.Instance.TogglePauseMusic();
		BAudioManager.Instance.AdjustMusicVolume(0.2f, 0.3f);
	}
	IEnumerator doChargeAttack()
	{
		safePlayClip("ChargeStart");
		const float chargeTime = 1f;
		const float maxModifier = 3.5f;
		var timer = 0f;
		var scaledModifier = 0.5f;
		var tookDamage = false;
		var originalPos = transform.position;
		var tweener = transform.DOShakePosition(100, 10f, 20).SetLoops(-1,LoopType.Incremental).OnKill(()=>{
			transform.position = originalPos;
		});
		BCharacterBlinker blinker = null;//BCharacterBlinker.AddBlinkForDuration(gameObject, -1, 0.5f);
		var pfx = BEffectsPool.CreateChargingEffectAtPos(transform.position + Vector3.up * ImageHeight * 0.5f);
		bool isFullyCharged = false;
		IsCurrentlyCharging = true;
		while (holdingAttack) {
			scaledModifier = Mathf.Lerp(1f, maxModifier, timer / chargeTime);
			if(timer > chargeTime && !isFullyCharged) {
				isFullyCharged = true;
				blinker = BCharacterBlinker.AddBlinkForDuration(gameObject, -1, 0.17f);
			}
			yield return null;
			timer += Time.deltaTime;
			if (takeDamagePacket != null) {
				tookDamage = true;
				onTakeDamage();
				if (!superArmor) {
					goto Exit;
				}
			}
		}
		IsCurrentlyCharging = false;
		if(blinker != null) {
			blinker.Stop();
		}
		if(tweener != null) {
			tweener.Kill();
		}
		pfx.Stop();
		if(isFullyCharged) {
			StartCoroutine(doAuraAnimation(1f));
			safePlayAudio(effectsData.CounterAttackSound);
			yield return new WaitForSeconds(0.5f);
		}
		safePlayClip("ChargeRelease");
		var animTime = getTimeForClip("ChargeRelease");
		sendTelegraphUpdate(1, 1f);
		yield return null;
		var modifiedPacket = new AuraAttackPacket(this, rawAttackStat, scaledModifier, isFullyCharged);
		BattleManager.AttackOpponent(modifiedPacket);
		decreaseStaminaByValue(staminaForAttack * scaledModifier * 0.7f);
		if (HandleSuccessfulAttack != null) {
			HandleSuccessfulAttack();
		}
	
		timer = 0f;
		while (timer < animTime) {
			yield return null;
			timer += Time.deltaTime;
			if (takeDamagePacket != null) {
				tookDamage = true;
				onTakeDamage();
				if (!superArmor) {
					goto Exit;
				}
			}
		}
Exit:
		if(blinker != null) {
			blinker.Stop();
		}
		if(tweener != null) {
			tweener.Kill();
		}
		pfx.Stop();
		if (!IsStunned && (!tookDamage || superArmor)) {
			safePlayClip("Idle");
		}
		StartCoroutine(doCooldown());
		onEndAttack();
		comboIndex = 0;
	}
	IEnumerator doAuraAnimation(float _time)
	{
		var effect = BEffectsPool.CreateAuraPopEffectAtPos(transform.position + Vector3.up * ImageHeight * 0.7f, PlayerManager.GetActiveGirl());
		var tkAnimator = effect.GetComponent<tk2dSpriteAnimator>();
		var time = tkAnimator.GetTimeForClip(tkAnimator.CurrentClip.name);

		var shopItems = PlayerEquipsManager.Auras[PlayerManager.GetActiveGirl()];
		var currentEquippedIndex = PlayerManager.Instance.GetAcquiredStatForGirl(PlayerManager.GetActiveGirl()).EquippedAuraIndex;

		//GameObject currentAura = AuraParent.transform.Find(PlayerEquipsManager.AurasText[currentEquippedIndex]).gameObject;
		GameObject currentAura = AuraParent.transform.Find(PlayerEquipsManager.AurasText[currentEquippedIndex]).gameObject;
		currentAura.SetActive(true);

		if(currentAura.transform.localScale != Vector3.zero) {
			originalAuraScale = currentAura.transform.localScale;
		}
		currentAura.transform.DOScale(0f, 0f).Kill(true);
		currentAura.transform.DOScale(originalAuraScale, time / 2f).SetDelay(time / 2f).SetEase(Ease.OutBack);

		//Wait an arbitrary percent of the anim, then to a little punch move on the aura object
		var timer = 0f;
		var timeTillPunchEffect = _time * 0.6f;
		while (timer < timeTillPunchEffect) {
			yield return null;
			timer += Time.deltaTime;
			if (takeDamagePacket != null) {
				onTakeDamage();
				if (!superArmor) {
					goto ExitAura;
				}
			}
		}
		currentAura.transform.DOPunchPosition(new Vector3(200, 0, 0), 0.4f);
		if(Utilities.FiftyFifty) {
			safePlayAudio(effectsData.ChargeAttackVOClips);
		}
		//Wait the remainder of the anim and then close up shop
		while (timer < _time) {
			yield return null;
			timer += Time.deltaTime;
			if (takeDamagePacket != null) {
				onTakeDamage();
				if (!superArmor) {
					goto ExitAura;
				}
			}
		}
ExitAura:
		BEffectsPool.CreateAuraPopEffectAtPos(transform.position + Vector3.up * ImageHeight * 0.7f, PlayerManager.GetActiveGirl());
		currentAura.transform.DOScale(0, time / 2f).SetDelay(time / 2f).SetEase(Ease.InBack).OnComplete(() => {
			currentAura.SetActive(false);
		});
	}
	#endregion
}
