﻿using UnityEngine;
using System.Collections;

public class BBlossomAgent : BPlayerAgent
{
	protected override float baseHealthStat  {get {return DesignValues.Blossom.BaseStats.Health;}}
	protected override float baseStaminaStat {get {return DesignValues.Blossom.BaseStats.Stamina;}}
	protected override float baseAttackStat  {get {return DesignValues.Blossom.BaseStats.Attack;}}
	protected override float baseShieldStat  {get {return DesignValues.Blossom.BaseStats.Shield;}}

	protected override float attack1Power{get{return DesignValues.Blossom.Attack1Power;}}
	protected override float attackCooldownTime{get{return DesignValues.Blossom.ComboCooldownTime;}}
	protected override ComboData[] comboData{get{return DesignValues.Blossom.ComboData;}}

	protected override float staminaForAttack{get{return DesignValues.Blossom.StaminaData.Attack;}}
//	protected override float staminaFillCooldownTime{get{return DesignValues.Blossom.StaminaData.FillCooldownTime;}}
//	protected override float staminaFillRatePerSecond{get{return DesignValues.Blossom.StaminaData.FillRatePerSecond;}}
//	protected override float staminaFillRateWhileBlockingPerSecond{get{return DesignValues.Blossom.StaminaData.FillRateWhileBlocking;}}
//
	protected override int comboCount
	{
		get{
			return PlayerManager.Instance.BlossomAcquiredStats.X2Unlocked ?
				comboData.Length : 2;
		}
	}

	public override float Health
	{
		get {
			return PlayerManager.BlossomHealth;
		}
		protected set {
			PlayerManager.BlossomHealth = value;
		}
	}
	protected override void Awake()
	{
		base.Awake();
		acquiredStats = PlayerManager.Instance.BlossomAcquiredStats;
	}
}
