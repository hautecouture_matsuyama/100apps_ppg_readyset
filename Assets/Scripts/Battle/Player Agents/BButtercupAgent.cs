﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BButtercupAgent : BPlayerAgent
{
	protected override float baseHealthStat  {get {return DesignValues.Buttercup.BaseStats.Health;}}
	protected override float baseStaminaStat {get {return DesignValues.Buttercup.BaseStats.Stamina;}}
	protected override float baseAttackStat  {get {return DesignValues.Buttercup.BaseStats.Attack;}}
	protected override float baseShieldStat  {get {return DesignValues.Buttercup.BaseStats.Shield;}}

	protected override float attack1Power{get{return DesignValues.Buttercup.Attack1Power;}} 
	protected override float attackCooldownTime{get{return DesignValues.Buttercup.ComboCooldownTime;}}
	protected override ComboData[] comboData{get{return DesignValues.Buttercup.ComboData;}}

	protected override float staminaForAttack{get{return DesignValues.Buttercup.StaminaData.Attack;}}
//	protected override float staminaFillCooldownTime{get{return DesignValues.Buttercup.StaminaData.FillCooldownTime;}}
//	protected override float staminaFillRatePerSecond{get{return DesignValues.Buttercup.StaminaData.FillRatePerSecond;}}
//	protected override float staminaFillRateWhileBlockingPerSecond{get{return DesignValues.Buttercup.StaminaData.FillRateWhileBlocking;}}

	protected override int comboCount
	{
		get{
			return PlayerManager.Instance.ButtercupAcquiredStats.X2Unlocked ?
				comboData.Length : 2;
		}
	}

	public override float Health
	{
		get {
			return PlayerManager.ButtercupHealth;
		}
		protected set {
			PlayerManager.ButtercupHealth = value;
		}
	}

	protected override void Awake()
	{
		base.Awake();
		acquiredStats = PlayerManager.Instance.ButtercupAcquiredStats;
	}
}
