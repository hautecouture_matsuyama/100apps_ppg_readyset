﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using Spine.Unity;

public class AttackPacket
{
	public AttackPacket(BAgent _source, float _attackStat, float _attackPower, bool _critical)
	{
		Source = _source;
		var treatAttackPowerAsRawDaamge = Mathf.Approximately(_attackStat, -1);
		var attackStatModifier = treatAttackPowerAsRawDaamge ? 1f : (1f + (_attackStat / DesignValues.BaseAgentStats.AttackDamageDivisor));
		Damage = attackStatModifier * _attackPower;
		Critical = _critical;
		if (!treatAttackPowerAsRawDaamge && _critical) {
			Damage *= DesignValues.PlayerBattleFeel.CriticalDamageModifier;
		}
		DamageLandTime = Time.time;
	}
	public readonly BAgent Source;
	public readonly float Damage;
	public readonly bool Critical;

	public float DamageLandTime;
	public override string ToString ()
	{
		return string.Format ("Attack packet source, {0}, with damage, {1}", Source, Damage);
	}
}
public class StaminaDamagePacket : AttackPacket
{
	public StaminaDamagePacket(BAgent _source, float _attackStat, float _attackPower)
		:base(_source, _attackStat, _attackPower, false)
	{

	}
}
public class AuraAttackPacket : AttackPacket
{
	public AuraAttackPacket(BAgent _source, float _attackStat, float _attackPower, bool _critical)
		:base(_source, _attackStat, _attackPower, _critical)
	{
		
	}
}

public class FMAttackPacket : AttackPacket
{
	public FMAttackPacket(BAgent _source, float _attackStat, float _attackPower, bool _critical)
		:base(_source, _attackStat, _attackPower, _critical)
	{
	}
}


public class BAgent : MonoBehaviour
{
	const float sliderBackingHangTime = 0.5f;

	public delegate void OnTelegraphUpdate(float _percent, float _totalTime);
	public event OnTelegraphUpdate HandleTelegraphUpdate;

	[SerializeField] protected GameObject shadowObject;
	[SerializeField] protected Slider healthSlider;
	[SerializeField] protected Slider staminaSlider;
	[SerializeField] protected AgentEffectsData effectsData;

	public virtual float Health{get; protected set;}
	public virtual float ImageHeight{get{return 0f;}}
	public float Stamina{get; protected set;}
	public bool IsAttacking{get; protected set;}
	public bool IsBlocking{get{return blockStartTime >= 0;}}
	public bool IsStunned{get {return stunRoutine != null;}}
	public bool IsDead{get; protected set;}
	public bool IsTakingHit {get {return takeHitRoutine != null;}}
	public ObjectPool ShieldPool{get; set;}

	protected Slider healthSliderBacking;
	protected Slider staminaSliderBacking;
	protected virtual string counterAttackAnimation{get {return "Attack";}}
	protected virtual bool canCounterAttack{get {return counterAttackWindowRoutine != null;}}
	protected bool canAttack = true;
	protected bool canQueue;

	protected GameObject shieldEffect;
	protected tk2dSpriteAnimator tkAnim;
	protected AcquiredStats acquiredStats = new AcquiredStats();
	protected tk2dCamera tkCam;
	Coroutine blockRoutine;
	protected Coroutine stunRoutine;
	protected Coroutine staminaFillDelayRoutine;
	Coroutine counterAttackWindowRoutine;
	protected Coroutine takeHitRoutine;

	protected bool invulnerable;
	protected float blockStartTime;
	protected GameObject stunEffect;
	protected AttackPacket takeDamagePacket;
	protected bool didPerfectBlock;
	protected bool inputQueued;
	protected bool holdingAttack;
	protected bool holdingBlock;
	bool releasingShield;

	protected virtual void Awake()
	{
		tkCam = FindObjectOfType<tk2dCamera>();
	}
	protected virtual void Start()
	{
		Stamina = maxStamina;
		blockStartTime = -1f;
	}
	protected virtual void Update()
	{
		if (!IsStunned && Stamina < maxStamina && staminaFillDelayRoutine == null) {
			var oldStamina = Stamina;
			var rate = 100f;
			if (BattleManager.InBattle) {
				rate = IsBlocking ? staminaFillRateWhileBlockingPerSecond : staminaFillRatePerSecond;
			}
			Stamina += rate * Time.deltaTime;
			clampStamina();
			if (!Mathf.Approximately(oldStamina, Stamina)) {
				onStaminaChanged(Stamina);
			}
		}
		if (!(BTutorialManager.Instance != null && BTutorialManager.Instance.PauseStaminaFill) && !IsBlocking && shieldEffect != null && !releasingShield) {
			releasingShield = true;
			shieldEffect.GetComponent<SkeletonAnimation>().AnimationState.AddAnimation(0, "Regular_End", false, 0).Complete += delegate {
				safeReleaseShieldEffect();
				releasingShield = false;
			};
		}
	}
	protected virtual void OnDestroy()
	{
	}
	#region Upgradable Stats
	protected virtual float baseHealthStat{get {throw new System.NotImplementedException();}}
	protected virtual float baseStaminaStat{get {throw new System.NotImplementedException();}}
	protected virtual float baseAttackStat{get {throw new System.NotImplementedException();}}
	protected virtual float baseShieldStat{get {throw new System.NotImplementedException();}}
	#endregion
	/// <summary>
	/// A flag indicating whether the agent flinches during a take hit
	/// </summary>
	protected bool superArmor;

	#region Calculated Battle values
	/// <summary>
	/// Gets the max health.
	/// </summary>
	/// <value>The health.</value>
	public virtual float MaxHealth
	{
		get{
			return BattleManager.CalculateHealth(baseHealthStat, acquiredStats.Health);
		}
	}
	/// <summary>
	/// Gets the max stamina.
	/// </summary>
	/// <value>The stamina.</value>
	protected virtual float maxStamina
	{
		get{
			return DesignValues.BaseAgentStats.BaseAgentStamina + ((acquiredStats.Stamina + baseStaminaStat) * DesignValues.BaseAgentStats.MaxStaminaModifier);
		}
	}
	/// <summary>
	/// Gets the attack damage.
	/// </summary>
	/// <value>The attack damage.</value>
	protected virtual float rawAttackStat
	{
		get{
			return (acquiredStats.Attack + baseAttackStat);
		}
	}
	/// <summary>
	/// Gets the shield reduction value.
	/// </summary>
	/// <value>The shield reduction.</value>
	protected float shieldReductionPercentage
	{
		get{
			return 1f - ((acquiredStats.Shield + baseShieldStat) / DesignValues.BaseAgentStats.ShieldReductionDivisor);
		}
	}
	#endregion

	#region Hidden Stats
	protected virtual float attack1Power{get{throw new System.NotImplementedException();}} 
	protected virtual float attack2Power{get{throw new System.NotImplementedException();}} 
	protected virtual float attackCooldownTime{get{throw new System.NotImplementedException();}}
	protected virtual float telegraphTime{get{throw new System.NotImplementedException();}}
	protected virtual float staminaForAttack{get{return DesignValues.BaseAgentStats.StaminaData.Attack;}}
	protected virtual float staminaFillCooldownTime{get{return DesignValues.BaseAgentStats.StaminaData.FillCooldownTime;}}
	protected virtual float staminaFillRatePerSecond{get{return DesignValues.BaseAgentStats.StaminaData.FillRatePerSecond;}}
	protected virtual float staminaFillRateWhileBlockingPerSecond{get{return DesignValues.BaseAgentStats.StaminaData.FillRateWhileBlocking;}}
	protected virtual float stunnedTime{get{return DesignValues.BaseAgentStats.StunnedTime;}}
	protected virtual float stunEffectSize{get{return 0.5f;}}
	protected virtual float blockStartSpeedScalar{get{return 1f;}}
	#endregion

	#region Internal Callbacks
	protected virtual void onStartAttack()
	{
		canAttack = false;
		IsAttacking = true;
		inputQueued = false;
		canQueue = false;
	}
	protected virtual void onTelegraphInterrupted()
	{
		IsAttacking = false;
		canAttack = true;
	}
	protected virtual void onEndAttack()
	{
		IsAttacking = false;
		inputQueued = false;
		canQueue = false;
	}
	protected virtual void onPerfectBlock()
	{
		safePlayAudio(effectsData.PerfectBlockSound);
		safePlayClip("BlockLoop");
		BGameplaySleep.Sleep(DesignValues.PlayerBattleFeel.PerfectBlockSleepTime, 0.1f);
		BEffectsPool.CreatePerfectBlockEffectAtPos(transform.position);
		BCoroutine.WaitAndPerform(() => {
			didPerfectBlock = false;
		}, -1f);
		counterAttackWindowRoutine = StartCoroutine(waitForCounterAttackWindow());
	}
	protected virtual void onStaminaChanged(float _to)
	{
		var target = Mathf.Clamp01(_to / maxStamina);
		if (target > staminaSlider.value) {
			staminaSliderBacking.value = target;
		} else {
			staminaSliderBacking.DOValue(target, staminaFillCooldownTime / 2f).SetEase(Ease.Linear).SetDelay(staminaFillCooldownTime / 2f);
		}
		staminaSlider.value = target;
	}
	protected virtual bool onTakeDamage()
	{
		if (takeDamagePacket.Critical) {
			BreakSuperArmor();
		}

		var packet = takeDamagePacket;//because we have multiple exists and C# is dumpy, we can create a temp copy that will get destructed later
		takeDamagePacket = null;//remove reference now
		var deltaTime = packet.DamageLandTime - blockStartTime;
		didPerfectBlock = (!BTutorialManager.InTutorial || BTutorialManager.Instance.AllowPerfectBlockInTutorial) && deltaTime > 0 && deltaTime < DesignValues.PlayerBattleFeel.PerfectBlockTimingWindow;
		if (didPerfectBlock) {
			onPerfectBlock();
			return false;
		} else {
			var oldHealth = Health;
			var totalDamage = packet.Damage;
			var isStaminaDamageOnly = packet is StaminaDamagePacket;
			var isFM = !isStaminaDamageOnly && (packet is FMAttackPacket);
			if (IsBlocking || isStaminaDamageOnly) {
				if (isStaminaDamageOnly) {
					decreaseStaminaByValue(IsBlocking ? packet.Damage : Stamina);
				} else {
					decreaseStaminaByValue(packet.Damage * (IsBlocking ? shieldReductionPercentage : 1));
				}
				totalDamage = 0;
			} else {
				Health -= totalDamage;
				clampHealth();
			}
			var damageTaken = !Mathf.Approximately (oldHealth, Health);
			var willBeDead = Mathf.Approximately(Health, 0);
			if (damageTaken) {
				var numberEffect = BEffectsPool.CreateHealthChangeEffectAtPos(-1f * totalDamage, transform.position + (Vector3.up * ImageHeight), packet.Critical);
				if (isFM) {
					numberEffect.transform.localScale = Vector2.one * 0.7f;
				}
				tweenHealthUIDown(oldHealth, Health, totalDamage);
			}
			float sleepTime = DesignValues.PlayerBattleFeel.NormalHitSleepTime;
			if (!willBeDead && superArmor && IsAttacking) {
				sleepTime = DesignValues.PlayerBattleFeel.SuperArmorHitSleepTime;
			}
			if (!BTutorialManager.Instance.InBlockTutorial) {
				BGameplaySleep.Sleep(sleepTime);
			}
			if (willBeDead) {
				if(stunEffect != null) {
					BEffectsPool.ReleaseStunEffect(stunEffect);
					stunEffect = null;
				}
				onDeath();
			} else if (damageTaken || IsBlocking) {
				onDamageTaken();
			}
			return damageTaken;
		}
	}
	public void PlayTakeHitBlockSound()
	{
		safePlayAudio(effectsData.TakehitBlockSound);
	}
	void onDamageTaken()
	{
		if (IsBlocking) {
			PlayTakeHitBlockSound();
		} else {
			safePlayAudio(effectsData.TakehitNormalSounds);
			safePlayAudio(effectsData.TakeHitVOClips);
			BCharacterBlinker.AddBlinkForDuration(gameObject, 0.3f, 0.15f);
		}
		if (!superArmor) {
			takeHitRoutine = StartCoroutine(doTakeHitAnimation());
		}
	}
	protected virtual void onDeath()
	{
		OnBlockReleased();
		safePlayClip("Faint");
		safePlayAudio(effectsData.DeathSound);
		IsDead = true;
	}
	#endregion

	#region Action Methods
	public void OnAttackPressed()
	{
		if (!enabled || IsTakingHit) {
			return;
		}
		if ((canAttack || canCounterAttack) && !IsStunned && !IsTakingHit) {
			holdingAttack = true;
			stopBlocking();
			onStartAttack();
			startBlocking();
			StartCoroutine(doTelegraph());
		} else if(canQueue) {
			inputQueued = true;
		}
	}
	public void OnAttackReleased()
	{
		holdingAttack = false;
	}
	public void OnBlockPressed()
	{
		holdingBlock = true;
		startBlocking();
	}
	public void OnBlockReleased()
	{
		holdingBlock = false;
		stopBlocking();
	}
	public virtual void SetTakeDamagePacket(AttackPacket _packet)
	{
		if (invulnerable) {
			return;
		}
		if (takeDamagePacket == null) {
			takeDamagePacket = _packet;
			if (!IsAttacking && !IsBlocking) {
				onTakeDamage();
			}
		}
	}
	public void BreakSuperArmor()
	{
		if (superArmor) {
			superArmor = false;
			BCoroutine.WaitAndPerform(() => {
				superArmor = true;
			},0);
		}
	}
	public virtual void UpdateHealthAndUpdateUI(float _amount)
	{
		_amount = Mathf.Min(_amount, MaxHealth - Health);
		if (_amount > 0) {
			BEffectsPool.CreateHealthChangeEffectAtPos(_amount, transform.position + (Vector3.up * ImageHeight));
			var oldHealth = Health;
			Health = Mathf.Clamp(Health + _amount, 0, MaxHealth);
			tweenHealthUIUp (oldHealth, Health);
		}
	}
	public void OnBattleEnd()
	{
		stopBlocking();
	}
	#endregion

	#region Helpers

	protected void safeReleaseShieldEffect ()
	{
		if (shieldEffect != null && ShieldPool != null) {
			ShieldPool.Release (shieldEffect);
			shieldEffect = null;
			ShieldPool = null;
		}
	}

	void startBlocking()
	{
		if (enabled && !IsBlocking) {
			blockRoutine = StartCoroutine(doBlock());
		}
	}
	void stopBlocking()
	{
		if (enabled && IsBlocking && (BTutorialManager.Instance != null && !BTutorialManager.Instance.ForceKeepBlockButtonHeldDown)) {
			blockStartTime = -1f;
			releasingShield = true;
			shieldEffect.GetComponent<SkeletonAnimation>().AnimationState.AddAnimation(0, "Regular_End", false, 0).Complete += delegate {
				safeReleaseShieldEffect();
				releasingShield = false;
			};
		}
	}
	void checkIfStunned ()
	{
		if (Mathf.Approximately(0, Stamina)) {
			stopBlocking();
			stunRoutine = StartCoroutine(doStun());
			startBlocking();
		}
	}
	void clampStamina ()
	{
		Stamina = Mathf.Clamp (Stamina, 0, maxStamina);
	}
	void clampHealth ()
	{
		Health = Mathf.Clamp (Health, 0, MaxHealth);
		if (Health > 0 && Health < 1f) {
			Health = 1f;
		}
	}
	protected virtual bool hasClip(string _clip){return false;}
	protected virtual float getTimeForClip(string _clip){return 0;}
	protected virtual void safePlayClip(string _name, string _next = "", float _speedScalar = 1){}
	protected void safePlayAudio(AudioClip _clip)
	{
		if(_clip != null) {
			BAudioManager.Instance.CreateAndPlayAudio(_clip);
		}
	}
	protected void safePlayAudio(AudioClip[] _clips)
	{
		if(_clips != null && _clips.Length > 0) {
			safePlayAudio(_clips[Random.Range(0, _clips.Length)]);
		}
	}
	void tweenHealthUIDown(float _from, float _to, float _totalDamage) {
		var targetPercent = Mathf.Clamp01(_to / MaxHealth);
		var actualDamagePercent = _totalDamage / MaxHealth;

		healthSlider.value = targetPercent;
		var backingValue = Mathf.Max(healthSliderBacking.value, Mathf.Clamp01(targetPercent + actualDamagePercent));
		healthSliderBacking.value = backingValue;
		BCoroutine.WaitAndPerform(() => {
			healthSliderBacking.DOValue(Mathf.Clamp01(_to / MaxHealth), 0.2f);
		}, sliderBackingHangTime);
	}
	protected void tweenHealthUIUp(float _from, float _to, float _delay = 0f)
	{
		BCoroutine.WaitAndPerform(() => {
			var start = Mathf.Clamp01(_from / MaxHealth);
			var end = Mathf.Clamp01(_to / MaxHealth);
			healthSlider.value = start;
			healthSlider.DOValue(end, 0.2f);
			healthSliderBacking.value = start;
			healthSliderBacking.DOValue(end, 0.2f);
		}, _delay);
	}
	protected void decreaseStaminaByValue (float _deltaStamina)
	{
		if (!BattleManager.InBattle || IsStunned) {
			return;
		}
		var oldStamina = Stamina;
		Stamina -= _deltaStamina;
		clampStamina ();
		if (!Mathf.Approximately (oldStamina, Stamina)) {
			onStaminaChanged (Stamina);
		}
		if (staminaFillDelayRoutine != null) {
			StopCoroutine(staminaFillDelayRoutine);
		}
		staminaFillDelayRoutine = StartCoroutine(waitForStaminaFillDelayRoutine());
		checkIfStunned ();
	}

	protected void sendTelegraphUpdate (float _percent, float _fullTime)
	{
		if (HandleTelegraphUpdate != null) {
			HandleTelegraphUpdate (_percent, _fullTime);
		}
	}

	#region Statemachine-like coroutines
	protected virtual IEnumerator doTelegraph()
	{
		if (!BattleManager.InBattle) {
			onTelegraphInterrupted();
			yield break;
		}
		safePlayClip("TelegraphStart", "TelegraphLoop");
		safePlayAudio(effectsData.TelegraphSound);
		StartCoroutine(telegraphTillPerfectBlockWindow());
		var timeTillRelease = telegraphTime - getTimeForClip("TelegraphRelease");
		var switchedToRelease = false;
		var time = 0f;
		while (time < telegraphTime) {
			if (time >= timeTillRelease && !switchedToRelease) {
				switchedToRelease = true;
				safePlayClip("TelegraphRelease");
			}
			yield return null;
			time += Time.deltaTime;
			if (takeDamagePacket != null) {
				onTakeDamage();
				if (!superArmor) {
					onTelegraphInterrupted();
					yield break;
				}
			}
			if (!BattleManager.InBattle) {
				onTelegraphInterrupted();
				yield break;
			}
		}
		if (!IsDead) {
			StartCoroutine(doAttack());
		}
	}
	protected IEnumerator telegraphTillPerfectBlockWindow()
	{
		var time = telegraphTime - DesignValues.PlayerBattleFeel.PerfectBlockTimingWindow;
		var timer = 0f;
		while (timer < time && !IsDead) {
			sendTelegraphUpdate (timer / telegraphTime, telegraphTime);
			yield return null;
			timer += Time.deltaTime;
		}
		if (!IsDead) {
			sendTelegraphUpdate (1f, telegraphTime);
		}
	}
	protected virtual IEnumerator doAttack()
	{
		yield return null;
	}
	protected IEnumerator doCooldown()
	{
		yield return new WaitForSeconds(attackCooldownTime);
		canAttack = true;
	}

	IEnumerator doTakeHitAnimation()
	{
		string anim = IsBlocking ? "TakeHitBlock" : "TakeHitNormal";
		safePlayClip(anim);
		var time = getTimeForClip(anim);
		yield return new WaitForSeconds(time);
		if (IsBlocking) {
			safePlayClip("BlockLoop");
		} else if (IsStunned) {
			safePlayClip("StunStart", "StunLoop");
		} else {
			var hasBlockEnd = hasClip("TakeHitEnd");
			var first = hasBlockEnd ? "TakeHitEnd" : "Idle";
			var second = hasBlockEnd ? "Idle" : "";
			safePlayClip(first, second);
		}
		takeHitRoutine = null;
	}
	IEnumerator doBlock()
	{
		while(IsAttacking || IsTakingHit || IsStunned) {
			if(!holdingBlock || !BattleManager.InBattle) {
				yield break;
			}
			yield return null;
		}
		if(!holdingBlock || !BattleManager.InBattle) {
			yield break;
		}
		var ogBlockTIme = Time.time;
		blockStartTime = Time.time;
		safePlayClip("BlockStart", "BlockLoop", blockStartSpeedScalar);
		safePlayAudio(effectsData.BlockStartSound);
		safeReleaseShieldEffect();
		shieldEffect = BEffectsPool.CreateBlockEffectAtPos(transform.position, this);
		while(IsBlocking && !IsStunned && BattleManager.InBattle) {
			if (takeDamagePacket != null) {
				onTakeDamage();
			}
			yield return null;
		}
		if (takeDamagePacket != null) {
			blockStartTime = ogBlockTIme;
			onTakeDamage();
			stopBlocking();
		}
		if (!IsStunned && !IsAttacking) {
			safePlayClip("BlockEnd", "Idle");
		}
		blockRoutine = null;
	}
	IEnumerator waitForStaminaFillDelayRoutine()
	{
		yield return new WaitForSeconds(staminaFillCooldownTime);
		staminaFillDelayRoutine = null;
	}
	protected virtual IEnumerator doStun()
	{
		//finish up attack anim if the attack caused stun
		while (IsAttacking) {
			yield return null;
		}
		safePlayAudio(effectsData.StunnedVO);
		safePlayClip("StunStart","StunLoop");
		var extraEndTime = getTimeForClip("StunEnd");
		var baseTime = stunnedTime - extraEndTime;
		safePlayAudio(effectsData.ShieldBreakSound);
		stunEffect = BEffectsPool.CreateStunEffectAtPos(transform.FindChild("StunPos").position, stunEffectSize, baseTime);
		var timer = 0f;
		yield return new WaitForSeconds(staminaFillCooldownTime);
		while (timer <= baseTime) {
			if (!BTutorialManager.Instance.PauseStaminaFill) {
				Stamina = Mathf.Lerp(0f, maxStamina, timer / baseTime);
				onStaminaChanged(Stamina);
			}
			yield return null;
			timer += Time.deltaTime;
		}
		stunEffect = null;
		safePlayClip("StunEnd", "Idle");
		var initialStamina = Stamina;
		while (timer <= stunnedTime) {
			Stamina = Mathf.Lerp(initialStamina, maxStamina, timer / stunnedTime);
			onStaminaChanged(Stamina);
			yield return null;
			timer += Time.deltaTime;
		}
		stunRoutine = null;
	}
	protected IEnumerator waitForCounterAttackWindow()
	{
		yield return new WaitForSeconds(DesignValues.PlayerBattleFeel.CounterAttackWindow);
		counterAttackWindowRoutine = null;
	}
	protected IEnumerator doCounterAttack(float _delay = 0f)
	{
		this.SafeStopCoroutine(ref counterAttackWindowRoutine);
		onStartAttack();

		yield return new WaitForSeconds(_delay);
		const float counterAttackPower = 2f;
		this.SafeStopCoroutine(ref blockRoutine);
		safePlayClip(counterAttackAnimation, "Idle");
		while (Time.timeScale < 1f) { yield return null;}
		BGameplaySleep.Sleep(0.3f);
		yield return new WaitForSecondsRealtime(0.3f);
		safePlayAudio(effectsData.AttackSounds);
		var modifiedPacket = new AttackPacket(this, rawAttackStat, counterAttackPower, true);
		modifiedPacket.DamageLandTime = float.MaxValue;
		BattleManager.AttackOpponent(modifiedPacket);

		yield return new WaitForSeconds(getTimeForClip(counterAttackAnimation));
		canAttack = true;
		onEndAttack();
		takeDamagePacket = null;
	}
	#endregion
	#endregion
}
