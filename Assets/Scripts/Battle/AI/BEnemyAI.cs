﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BEnemyAI<T> : MonoBehaviour where T : BEnemyAgent
{
	#region member vairables
	protected Coroutine idleRoutine;
	protected Coroutine attackRoutine;
	protected Coroutine blockRoutine;
	protected Coroutine anticipateBlockRoutine;
	protected Coroutine blockCooldownRoutine;

	protected BPlayerAgent playerHandle;
	protected T agent;
	protected virtual bool listenToPlayerTelegraph{get{return false;}}
	protected virtual bool allowBlocking{get{return true;}}
	protected virtual bool allowAttacking{get{return true;}}
	protected virtual int maxAnticipateCount{get {return 0;}}

	protected int anticipateCount = 0;
	#endregion

	public bool PauseAI{get; set;}

	#region Mono Callbacks
	void Awake()
	{
		agent = GetComponent<T>();
	}
	protected virtual IEnumerator Start()
	{
		while(BattleManager.PlayerPartyLead == null) {
			yield return null;
		}
		playerHandle = BattleManager.PlayerPartyLead;
		if (listenToPlayerTelegraph) {
			playerHandle.HandleTelegraphUpdate += onTelegraphProgressChanged;
		}
	}
	void OnDisable()
	{
		safeStopAllCoroutines();
	}
	void OnDestroy()
	{
		if (listenToPlayerTelegraph) {
			playerHandle.HandleTelegraphUpdate -= onTelegraphProgressChanged;
		}
	}
	protected virtual void Update ()
	{
		if (PauseAI || !BattleManager.InBattle || agent.IsDead) {
			return;
		}
//		if (Input.GetKeyDown(KeyCode.L)) {
//			agent.OnBlockPressed();
//		} else if (Input.GetKeyUp(KeyCode.L)) {
//			agent.OnBlockReleased();
//		} else if (Input.GetKeyDown(KeyCode.J)) {
//			agent.OnAttackPressed();
//		}
		if (!inState && !agent.IsStunned) {
			idleRoutine = StartCoroutine(idleThenPerform());
		}
	}
	#endregion
	protected IEnumerator waitForTakeHitToFinish()
	{
		while (agent.IsTakingHit) {
			yield return null;
		}
	}
	#region Statemachine-esque routines
	protected virtual IEnumerator idleThenPerform()
	{
		yield return new WaitForSeconds(agent.ModifiedIdleTime());
		if (agent.IsDead) {
			yield break;
		}
		do {
			yield return null;
		} while (!BattleManager.InBattle || BattleManager.PlayerPartyLead == null || BattleManager.PlayerPartyLead.IsDead || agent.IsDodging);
		idleRoutine = null;

		yield return StartCoroutine(waitForTakeHitToFinish());

		var total = agent.DoNothingWeight + agent.AttackWeight + agent.BlockWeight;
		var chance = Random.value;
		if (allowAttacking && chance <= agent.AttackWeight / total) {
			attackRoutine = StartCoroutine(doAttack());
			yield break;
		}
		chance -= agent.AttackWeight / total;
		if (allowBlocking && chance <= agent.BlockWeight && canBlock) {
			var blockTime = Random.Range(agent.MinBlockTime, agent.MaxBlockTime);
			blockRoutine = StartCoroutine(doBlock(blockTime));
			yield break;
		}
	}
	protected virtual IEnumerator doAttack()
	{
		if (agent.IsDead) {
			yield break;
		}
		agent.OnAttackPressed();
		do {
			yield return null;
		} while (agent.IsAttacking);
		attackRoutine = null;
		anticipateCount = 0;
	}
	protected IEnumerator doBlock(float _blockTime)
	{
		if (agent.IsDead) {
			yield break;
		}
		agent.OnBlockPressed();
		var timer = 0f;
		while (timer < _blockTime) {
			if (agent.IsStunned) {
				break;
			}
			yield return null;
			timer += Time.deltaTime;
		}
		agent.OnBlockReleased();
		blockCooldownRoutine = StartCoroutine(doBlockCooldown());
		blockRoutine = null;
	}

	protected IEnumerator doBlockCooldown()
	{
		yield return new WaitForSeconds(agent.BlockCooldownTime / BDifficultyManager.Instance.SpeedModifier);
		blockCooldownRoutine = null;
	}

	protected virtual IEnumerator doAnticipate(float _enemyTelegraphDuration)
	{
		if (canBlock) {
			yield return null;
			blockRoutine = StartCoroutine(doBlock(_enemyTelegraphDuration * 1.1f / BDifficultyManager.Instance.SpeedModifier));
		}
		anticipateBlockRoutine = null;
	}
	#endregion

	#region helpers
	protected bool inState
	{
		get{
			return idleRoutine != null || attackRoutine != null||
				anticipateBlockRoutine != null || blockRoutine != null;
		}
	}
	protected bool canBlock
	{
		get {
			return blockCooldownRoutine == null && blockRoutine == null;
		}
	}
	protected void safeStopAllCoroutines()
	{
		this.SafeStopCoroutine(ref idleRoutine);
		this.SafeStopCoroutine(ref attackRoutine);
		this.SafeStopCoroutine(ref blockRoutine);
	}
	#endregion

	#region event handlers
	protected virtual void onTelegraphProgressChanged(float _progress, float _totalDuration)
	{
		if (Mathf.Approximately(0, _progress) &&
			anticipateBlockRoutine == null && attackRoutine == null && blockRoutine == null &&
			!agent.IsBlocking &&!agent.IsStunned &&
			anticipateCount < maxAnticipateCount) {
			++anticipateCount;
			safeStopAllCoroutines();
			anticipateBlockRoutine = StartCoroutine(doAnticipate(_totalDuration));
		}
	}
	#endregion
}
