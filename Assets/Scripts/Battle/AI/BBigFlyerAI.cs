﻿using System.Collections;
using UnityEngine;

public class BBigFlyerAI : BEnemyAI<BBigFlyerAgent>
{
	protected override bool listenToPlayerTelegraph{get{return true;}}
	protected override int maxAnticipateCount {
		get {
			return DesignValues.BigFlyer.MaxAnticipateCount;
		}
	}
	protected override bool allowBlocking{get{return false;}}
	protected Coroutine dodgeCooldownRoutine;

	protected override void onTelegraphProgressChanged(float _progress, float _totalDuration)
	{
		var chance = Random.value;
		if (chance <= DesignValues.Flyer.ChanceToDodge && dodgeCooldownRoutine == null && _progress >= 0.9f && !agent.IsAttacking && !agent.IsStunned) {
			agent.DodgeAttack();
			dodgeCooldownRoutine = StartCoroutine(waitForDodgeCooldown());
		}
	}
	#region Statemachine-esque routines
	protected override IEnumerator idleThenPerform()
	{
		yield return new WaitForSeconds(agent.ModifiedIdleTime());
		if (agent.IsDead) {
			yield break;
		}
		do {
			yield return null;
		} while (!BattleManager.InBattle || BattleManager.PlayerPartyLead == null || BattleManager.PlayerPartyLead.IsDead || agent.IsDodging);
		idleRoutine = null;

		yield return StartCoroutine(waitForTakeHitToFinish());

		var total = agent.DoNothingWeight + agent.AttackWeight + agent.Attack2Weight;
		var chance = Random.value;
		if (allowAttacking && chance <= agent.AttackWeight / total) {
			yield return StartCoroutine(agent.DoBasicAttack(Random.Range(1, 4)) );
			yield break;
		}
		chance -= agent.AttackWeight / total;
		if (allowAttacking && chance <= agent.Attack2Weight / total && !BattleManager.PlayerPartyLead.IsStunned) {
			yield return StartCoroutine(agent.DoGroundPound() );
			yield break;
		}
	}
	IEnumerator waitForDodgeCooldown()
	{
		yield return new WaitForSeconds(DesignValues.BigFlyer.DodgeCooldown);
		dodgeCooldownRoutine = null;
	}
	#endregion
}

