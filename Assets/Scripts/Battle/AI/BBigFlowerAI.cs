﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BBigFlowerAI : BEnemyAI<BBigFlowerAgent>
{
	protected override bool listenToPlayerTelegraph{get{return true;}}
	protected override int maxAnticipateCount {
		get {
			return DesignValues.BigFlower.MaxAnticipateCount;
		}
	}
	#region Statemachine-esque routines
	protected override IEnumerator doAttack()
	{
		var doDouble = Random.value <= agent.DoubleAttackChance;
		var time = agent.TimeBetweenAttacks.floatValue;
		agent.ForceSkipCooldown();
		agent.OnAttackPressed();
		if (doDouble) {
			while (agent.IsAttacking) {
				yield return null;
			}
			yield return new WaitForSeconds(time);
			agent.ForceNextTelegraphTime(agent.SecondAttackTelegraphTime.floatValue);
			agent.OnAttackPressed();
		}
		while (agent.IsAttacking) {
			yield return null;
		}
		attackRoutine = null;
		anticipateCount = 0;
	}
	protected override IEnumerator doAnticipate(float _enemyTelegraphDuration)
	{
		const float buffer = 0.05f;
		var timeTillPerfectBlock = _enemyTelegraphDuration - DesignValues.PlayerBattleFeel.PerfectBlockTimingWindow + buffer;
		if (canBlock) {
			yield return new WaitForSeconds(timeTillPerfectBlock);
			blockRoutine = StartCoroutine(doBlock(agent.MinBlockTime));
		}
		anticipateBlockRoutine = null;
	}
	#endregion
}
