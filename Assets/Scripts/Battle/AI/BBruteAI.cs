﻿/// <summary>
/// Brute AI. Simplest of AIs and does nothing special. This class is just needed to fill out the <T> in BEnemyAI
/// </summary>
public class BBruteAI : BEnemyAI<BBruteAgent>
{

}
