﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BFlowerAI : BEnemyAI<BFlowerAgent>
{
	protected override bool listenToPlayerTelegraph{get{return true;}}
	protected override int maxAnticipateCount {
		get {
			return DesignValues.Flower.MaxAnticipateCount;
		}
	}
}
