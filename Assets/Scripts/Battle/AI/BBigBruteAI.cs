﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BBigBruteAI : BEnemyAI<BBigBruteAgent>
{
	int attackCount;
	protected override IEnumerator doAttack ()
	{
		var thisAttackIsRage = agent.InRage;
		yield return StartCoroutine(base.doAttack());
		if (!thisAttackIsRage) {
			++attackCount;
			if (attackCount == agent.AttacksTillRage) {
				agent.TurnOnRage();
			}
			attackCount = (attackCount + agent.AttacksTillRage) % agent.AttacksTillRage;
		}
	}
}
