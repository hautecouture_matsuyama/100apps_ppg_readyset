﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BBigFlowerAgent : BEnemyAgent
{
	public override float ImageHeight{get{return 450f;}}

	#region Base Stats
	protected override float baseHealthStat  {get {return DesignValues.BigFlower.Stats.Health;}}
	protected override float baseStaminaStat {get {return DesignValues.BigFlower.Stats.Stamina;}}
	protected override float baseAttackStat  {get {return DesignValues.BigFlower.Stats.Attack;}}
	protected override float baseShieldStat  {get {return DesignValues.BigFlower.Stats.Shield;}}
	#endregion

	#region Hidden Stats
	protected override float attack1Power{get{return DesignValues.BigFlower.Attack1Power;}}
	protected override float attackCooldownTime{get{return DesignValues.BigFlower.HiddenStats.AttackCooldownTime;}}
	protected override RandomRangedValue telegraphRangeTime{get{return DesignValues.BigFlower.HiddenStats.TelegraphTime;}}
	public override float BlockCooldownTime {get{return DesignValues.BigFlower.HiddenStats.BlockCooldownTime;}}
	public override float MinBlockTime{get{return DesignValues.BigFlower.MinBlockTime / BDifficultyManager.Instance.SpeedModifier;}}
	public override float MaxBlockTime{get{return DesignValues.BigFlower.MaxBlockTime / BDifficultyManager.Instance.SpeedModifier;}}
	protected override float stunEffectSize{get{return 0.7f;}}
	#endregion

	protected override float idleTime{get {return DesignValues.BigFlower.IdleTime;}}
	public override float AttackWeight{get {return DesignValues.BigFlower.Weights[AgentActions.Attack];}}
	public override float BlockWeight{get {return DesignValues.BigFlower.Weights[AgentActions.Block];}}
	public override float DoNothingWeight{get {return DesignValues.BigFlower.Weights[AgentActions.DoNothing];}}

	public float DoubleAttackChance{get {return DesignValues.BigFlower.DoubleAttackChance;}}
	public RandomRangedValue TimeBetweenAttacks{get {return DesignValues.BigFlower.TimeBetweenAttacks;}}
	public RandomRangedValue SecondAttackTelegraphTime{get {return DesignValues.BigFlower.SecondAttackTelegraphTime;}}

	protected override void onPerfectBlock()
	{
		base.onPerfectBlock();
		if (Random.value <= DesignValues.BigFlower.CounterAttackChance && canCounterAttack) {
			StartCoroutine(doCounterAttack(0.75f));
		}
	}
}
