﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using Spine.Unity;

public class BBigBruteAgent : BEnemyAgent
{
	public override float ImageHeight{get{return 450f;}}

	#region Base Stats
	protected override float baseHealthStat  {get {return DesignValues.BigBrute.Stats.Health;}}
	protected override float baseStaminaStat {get {return DesignValues.BigBrute.Stats.Stamina;}}
	protected override float baseAttackStat  {get {return InRage ? DesignValues.BigBrute.RageAttack : DesignValues.BigBrute.Stats.Attack;}}
	protected override float baseShieldStat  {get {return DesignValues.BigBrute.Stats.Shield;}}
	#endregion

	#region Hidden Stats
	protected override float attack1Power{get{return DesignValues.BigBrute.Attack1Power;}}
	protected override float attackCooldownTime{get{return DesignValues.BigBrute.HiddenStats.AttackCooldownTime;}}
	protected override RandomRangedValue telegraphRangeTime{get{return DesignValues.BigBrute.HiddenStats.TelegraphTime;}}
	public override float BlockCooldownTime {get{return DesignValues.BigBrute.HiddenStats.BlockCooldownTime;}}
	protected override float stunEffectSize{get{return 0.7f;}}
	protected override float blockStartSpeedScalar{get{return 2f;}}
	public override float MinBlockTime{get{return DesignValues.BigBrute.MinBlockTime / BDifficultyManager.Instance.SpeedModifier;}}
	public override float MaxBlockTime{get{return DesignValues.BigBrute.MaxBlockTime / BDifficultyManager.Instance.SpeedModifier;}}
	#endregion

	public int AttacksTillRage{get{return DesignValues.BigBrute.AttacksTillRage;}}

	protected override float idleTime{get {return DesignValues.BigBrute.IdleTime;}}
	public override float AttackWeight{get {return InRage ? DesignValues.BigBrute.Weights[AgentActions.RageAttack] : DesignValues.BigBrute.Weights[AgentActions.Attack];}}
	public override float BlockWeight{get {return DesignValues.BigBrute.Weights[AgentActions.Block];}}
	public override float DoNothingWeight{get {return InRage ? DesignValues.BigBrute.Weights[AgentActions.RageDoNothing] : DesignValues.BigBrute.Weights[AgentActions.DoNothing];}}

	Tweener rageTween;
	public bool InRage{get{return rageTween != null;}}
	public void TurnOnRage()
	{
		if (rageTween == null) {
			var percent = 0f;
			rageTween = DOTween.To(() => percent, (float _p) => {
				percent = _p;
				var color = Color.Lerp(Color.white, Color.red, _p);
				spineAnim.skeleton.SetColor(color);
			}, 1f, 0.2f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
		}
	}
	protected override void Update ()
	{
		base.Update ();
		if (InRage) {
			spineAnim.timeScale *= 2f;
		}
	}
	void turnOffRage()
	{
		if (rageTween != null) {
			rageTween.Kill();
			rageTween = null;
			spineAnim.skeleton.SetColor(Color.white);
		}
	}
		
	protected override IEnumerator doAttack ()
	{
		turnOffRage();
		yield return StartCoroutine(base.doAttack ());
	}
}
