﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BFlowerAgent : BEnemyAgent
{
	public override float ImageHeight{get{return 320f;}}

	#region Base Stats
	protected override float baseHealthStat  {get {return DesignValues.Flower.Stats.Health;}}
	protected override float baseStaminaStat {get {return DesignValues.Flower.Stats.Stamina;}}
	protected override float baseAttackStat  {get {return DesignValues.Flower.Stats.Attack;}}
	protected override float baseShieldStat  {get {return DesignValues.Flower.Stats.Shield;}}
	#endregion

	#region Hidden Stats
	protected override float attack1Power{get{return DesignValues.Flower.Attack1Power;}}
	protected override float attackCooldownTime{get{return DesignValues.Flower.HiddenStats.AttackCooldownTime;}}
	protected override RandomRangedValue telegraphRangeTime{get{return DesignValues.Flower.HiddenStats.TelegraphTime;}}
	public override float BlockCooldownTime {get{return DesignValues.Flower.HiddenStats.BlockCooldownTime;}}
	public override float MinBlockTime{get{return DesignValues.Flower.MinBlockTime / BDifficultyManager.Instance.SpeedModifier;}}
	public override float MaxBlockTime{get{return DesignValues.Flower.MaxBlockTime / BDifficultyManager.Instance.SpeedModifier;}}
	#endregion

	protected override float idleTime{get {return DesignValues.Flower.IdleTime;}}
	public override float AttackWeight{get {return DesignValues.Flower.Weights[AgentActions.Attack];}}
	public override float BlockWeight{get {return DesignValues.Flower.Weights[AgentActions.Block];}}
	public override float DoNothingWeight{get {return DesignValues.Flower.Weights[AgentActions.DoNothing];}}

	public float DoubleAttackChance{get {return DesignValues.Flower.DoubleAttackChance;}}
	public RangedValue TimeBetweenAttacks{get {return DesignValues.Flower.TimeBetweenAttacks;}}
	public RangedValue SecondAttackTelegraphTime{get {return DesignValues.Flower.SecondAttackTelegraphTime;}}

}
