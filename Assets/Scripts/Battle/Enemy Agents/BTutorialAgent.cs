﻿using UnityEngine;
using System.Collections;

public class BTutorialAgent : BEnemyAgent
{
	public override float ImageHeight{get{return 300f;}}

	#region Base Stats
	protected override float baseHealthStat  {get {return DesignValues.Tutorial.Stats.Health;}}
	protected override float baseStaminaStat {get {return DesignValues.Tutorial.Stats.Stamina;}}
	protected override float baseAttackStat  {get {return DesignValues.Tutorial.Stats.Attack;}}
	protected override float baseShieldStat  {get {return DesignValues.Tutorial.Stats.Shield;}}
	#endregion

	#region Hidden Stats
	protected override float attack1Power{get{return DesignValues.Tutorial.Attack1Power;}}
	protected override float attackCooldownTime{get{return DesignValues.Tutorial.HiddenStats.AttackCooldownTime;}}
	protected override RandomRangedValue telegraphRangeTime{get{return DesignValues.Tutorial.HiddenStats.TelegraphTime;}}
	public override float BlockCooldownTime {get{return DesignValues.Tutorial.HiddenStats.BlockCooldownTime;}}
	#endregion

	protected override float idleTime{get {return DesignValues.Tutorial.IdleTime;}}
	public override float AttackWeight{get {return DesignValues.Tutorial.Weights[AgentActions.Attack];}}
	public override float BlockWeight{get {return DesignValues.Tutorial.Weights[AgentActions.Block];}}
	public override float DoNothingWeight{get {return DesignValues.Tutorial.Weights[AgentActions.DoNothing];}}
}

