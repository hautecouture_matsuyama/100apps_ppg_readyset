﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using System.Linq;
using Spine.Unity;

public enum EnemyType
{
	Brute,
	Flower,
	Flyer,
	BigBrute,
	BigFlower,
	BigFlyer
}

public class BEnemyAgent : BAgent
{
	static readonly HashSet <string> loopedAnims = new HashSet<string> () 
	{
		{"BlockLoop"},
		{"Idle"},
		{"StunLoop"},
		{"TelegraphLoop"}
	};
	public virtual float MinBlockTime{get{return 1f / BDifficultyManager.Instance.SpeedModifier;}}
	public virtual float MaxBlockTime{get{return 2f / BDifficultyManager.Instance.SpeedModifier;}}

	public virtual float BlockCooldownTime{get {throw new System.NotImplementedException();}}

	protected virtual float idleTime{get {throw new System.NotImplementedException();}}
	public virtual float AttackWeight{get {throw new System.NotImplementedException();}}
	public virtual float BlockWeight{get {throw new System.NotImplementedException();}}
	public virtual float DoNothingWeight{get {throw new System.NotImplementedException();}}

	[SerializeField]
	GameObject healthCanvas;

	protected SkeletonAnimation spineAnim;
	protected MeshRenderer rend;
	protected virtual RandomRangedValue telegraphRangeTime
	{
		get {
			throw new System.NotImplementedException();
		}
	}
	bool forceSkipCooldown = false;
	float forceTelegraphTime = -1f;
	protected override void Start ()
	{
		Health = MaxHealth;
		base.Start ();
		superArmor = true;
		//Spine warns that this component is not guranteed to exist in Awake, and to do this in Start
		spineAnim = GetComponentInChildren<SkeletonAnimation>();
		rend = GetComponentInChildren<MeshRenderer>();
	
		healthSliderBacking = healthSlider.transform.GetChild(1).GetComponent<Slider>();
		staminaSliderBacking = staminaSlider.transform.GetChild(1).GetComponent<Slider>();
	}
	/// <inheritdoc/>
	protected override float maxStamina
	{
		get {
			return base.maxStamina * BDifficultyManager.Instance.StaminaModifier;
		}
	}
	/// <inheritdoc/>
	protected override float rawAttackStat
	{
		get {
			return base.rawAttackStat * BDifficultyManager.Instance.DamageModifier;
		}
	}
	/// <inheritdoc/>
	public override float MaxHealth {
		get {
			return base.MaxHealth * BDifficultyManager.Instance.HealthModifier;
		}
	}

	float variableTelegraphTime;
	/// <inheritdoc/>
	protected override float telegraphTime {
		get {
			return variableTelegraphTime;
		}
	}
	protected override void Update()
	{
		base.Update();
		spineAnim.timeScale = BDifficultyManager.Instance.SpeedModifier;
	}
	public void ForceSkipCooldown()
	{
		forceSkipCooldown = true;
	}
	public void ForceNextTelegraphTime(float _time)
	{
		forceTelegraphTime = _time;
	}
	public virtual int GetGoldYield()
	{
		return Random.Range(1, 3);
	}
	public void SetAIPause(bool _value)
	{
		if (this is BBruteAgent) {
			GetComponent<BBruteAI>().PauseAI = _value;
		} else if (this is BBigBruteAgent) {
			GetComponent<BBigBruteAI>().PauseAI = _value;
		} else if (this is BFlowerAgent) {
			GetComponent<BFlowerAI>().PauseAI = _value;
		} else if (this is BBigFlowerAgent) {
			GetComponent<BBigFlowerAI>().PauseAI = _value;
		} else if (this is BFlyerAgent) {
			GetComponent<BFlyerAI>().PauseAI = _value;
		} else if (this is BBigFlyerAgent) {
			GetComponent<BBigFlyerAI>().PauseAI = _value;
		}
	}
	public void PrepareForSuper()
	{
		onEndAttack();
		blockStartTime = - 1f;
		this.SafeStopCoroutine(ref stunRoutine);
		safeReleaseShieldEffect ();
		StopAllCoroutines ();
		safePlayClip ("Idle");
	}
	public void DoFakeTakeHitForSuper()
	{
		safePlayClip("TakeHitNormal", "TakeHitEnd");
		var meshRend = GetComponentInChildren<MeshRenderer>();
		BEffectsPool.CreateNormalHitEffectAtPos(meshRend.transform.position + (Vector3.up * meshRend.bounds.size.y * 0.5f));
		safePlayAudio(effectsData.TakehitNormalSounds);
	}
	public bool IsDodging{get; private set;}
	public void DodgeAttack()
	{
		if (IsDodging) {
			return;
		}
		IsDodging = true;
		const string dodgeStart = "Dodge Start";
		const string dodgeEnd = "Dodge End";
		float startTime = 0.3f / BDifficultyManager.Instance.SpeedModifier;
		float endTIme = 0.3f / BDifficultyManager.Instance.SpeedModifier;
		safePlayAudio (effectsData.DodgeSound);
		safePlayClip(dodgeStart);
		var curPos = transform.position;
		var center = BWorldMapDragger.Instance.CurrentRoom.Handle.position;
		invulnerable = true;
		var targetTransform = spineAnim.transform;
		targetTransform .DOMoveY(center.y + (Screen.height / 2), startTime).SetEase(Ease.InBack).OnComplete(() => {
			targetTransform.position += Vector3.right * Screen.width * 0.5f;
		});
		targetTransform .DOMove(curPos, endTIme).SetDelay(startTime).SetEase(Ease.Linear).OnComplete(() => {
			invulnerable = false;
			IsDodging = false;
		}).OnStart(() => {
			safePlayClip(dodgeEnd, "Idle");
		});
	}
	public IEnumerator DoGroundPound()
	{
		const string anim = "Attack 2";
		if (canAttack) {
			onStartAttack();
			safePlayAudio (effectsData.GroundPoundSound);
			safePlayClip(anim);
			var totalTime = getTimeForClip(anim);
			var timer = 0f;
			var dealtDamage = false;
			const float percentToDealDamage = 0.5f;
			var timeTillDamageForGuardFM = (percentToDealDamage / 2f) * totalTime; //NEED TO HAVE FRACTION FOR GUARD TO WORK - WHY DO I HAVE TO HALF THIS TIME?!?!!?
			while (timer < totalTime) {
				sendTelegraphUpdate(timer / timeTillDamageForGuardFM , timeTillDamageForGuardFM );
				if (timer >= (totalTime * percentToDealDamage) && !dealtDamage) {
					dealtDamage = true;
					var attackPacket = new StaminaDamagePacket(this, rawAttackStat, attack2Power);
					BattleManager.AttackOpponent(attackPacket);
					decreaseStaminaByValue(staminaForAttack);
				}
				yield return null;
				timer += Time.deltaTime;
				if (takeDamagePacket != null) {
					onTakeDamage();
					if (!superArmor) {
						break;
					}
				}
			}
			onEndAttack();
			canAttack = true;
		}
	}
	/// <inheritdoc/>
	protected override bool onTakeDamage()
	{
		if (BattleManager.PlayerPartyLead != null) {
			var _packet = takeDamagePacket;
			var tookDamage = base.onTakeDamage();
			if (!didPerfectBlock) {
				if (IsBlocking) {
					if(shieldEffect != null) {
						var comp = shieldEffect.GetComponent<SkeletonAnimation>();
						comp.AnimationState.SetAnimation(0, "Regular_TakeHit", false);
						comp.AnimationState.AddAnimation(0, "Regular_Hold", true, 0);
					}
					BCameraShake.Instance.SmallShake();
				} else {
					var fromFM = _packet is FMAttackPacket;
					if(_packet is AuraAttackPacket) {
						BEffectsPool.CreateAuraHitEffectAtPos(rend.transform.position + (Vector3.up * rend.bounds.size.y * 0.5f));
					} else {
						var effect = BEffectsPool.CreateNormalHitEffectAtPos(rend.transform.position + (Vector3.up * rend.bounds.size.y * 0.5f));
						if (fromFM) {
							effect.transform.localScale = Vector2.one * 0.6f;
						}
					}
					if (fromFM) {
						BCameraShake.Instance.SmallShake();
					} else {
						BCameraShake.Instance.MediumShake();
					}
				}
			}
			if (!IsStunned && tookDamage) {
				var coach = FindObjectOfType<BCoachFriendlyMonster>();
				if (coach != null) {
					var percent = coach.StaminaDamagePercent;
					if (percent > 0) {
						decreaseStaminaByValue(_packet.Damage * percent);
						coach.ShakeForSapping();
					}
				}
			}
			return tookDamage;
		}
		takeDamagePacket = null;
		return false;
	}

	/// <inheritdoc/>
	protected override void onDeath()
	{
		base.onDeath();
		safePlayAudio (effectsData.VictorySting);
		if (BattleManager.EnemyPartyLead == this) {
			BattleManager.OnCurrentEnemyLeadDied();
		}
		healthCanvas.SetActive(false);
		const float totalDeathEffectTime = 2f;
		shadowObject.GetComponent<tk2dSprite>().DOFade(0,totalDeathEffectTime).SetEase(Ease.Linear).OnComplete(() => Destroy (gameObject));
		//transform.DOShakePosition(totalDeathEffectTime, new Vector3(20,20,0), 100).SetEase(Ease.InCirc);
	}
	#region Helpers
	protected override bool hasClip(string _clip)
	{
		return spineAnim.HasClip(_clip);
	}
	/// <inheritdoc/>
	protected override float getTimeForClip(string _clip)
	{
		return spineAnim.GetTimeForClip(_clip) / BDifficultyManager.Instance.SpeedModifier;
	}
	protected override void safePlayClip(string _name, string _next = "", float _speedScalar = 1)
	{
		if (IsDead) {
			return;
		}
		if(!string.IsNullOrEmpty(_name)) {
			spineAnim.AnimationState.SetAnimation(0, _name, loopedAnims.Contains(_name)).timeScale = _speedScalar;
			if (!string.IsNullOrEmpty(_next)) {
				bool loop = loopedAnims.Contains(_next);
				spineAnim.AnimationState.AddAnimation(0, _next, loop, 0);
				if(!loop){
					spineAnim.AnimationState.AddAnimation(0, "Idle", true, 0);
				}
			}
		} else if(!string.IsNullOrEmpty(_next)) {
			safePlayClip(_next, "");
		}
	}
	protected override IEnumerator doTelegraph ()
	{
		variableTelegraphTime = forceTelegraphTime >= 0 ? forceTelegraphTime : telegraphRangeTime.floatValue;
		variableTelegraphTime /= spineAnim.timeScale;
		forceTelegraphTime = -1f;
		yield return StartCoroutine(base.doTelegraph());
	}
	protected override IEnumerator doAttack()
	{
		const string attackAnimName = "Attack";
		if (!IsDead && BattleManager.InBattle) {
			safePlayClip(attackAnimName, "Idle");
			safePlayAudio(effectsData.AttackSounds);
			var doCritical = BattleManager.PlayerPartyLead.IsStunned;
			var attackPacket = new AttackPacket(this, rawAttackStat, attack1Power, doCritical);
			//must come before attack
			decreaseStaminaByValue(staminaForAttack);
			BattleManager.AttackOpponent(attackPacket);
			var time = getTimeForClip(attackAnimName);
			var timer = 0f;
			var tookDamageDuringAttack = false;
			while (timer < time) {
				yield return null;
				timer += Time.deltaTime;
				if (takeDamagePacket != null) {
					onTakeDamage();
					tookDamageDuringAttack = true;
					if (!superArmor) {
						break;
					}
				}
			}
			onEndAttack();
			if (forceSkipCooldown || (tookDamageDuringAttack && superArmor)) {
				canAttack = true;
				forceSkipCooldown = false;
			} else {
				StartCoroutine(doCooldown());
			}
		}
	}
	protected override void onEndAttack()
	{
		base.onEndAttack();
		if (!IsTakingHit) {
			if (hasClip("AttackEnd")) {
				safePlayClip("AttackEnd", "Idle");
			} else {
				safePlayClip("Idle");
			}
		}
	}
	public float ModifiedIdleTime()
	{
		var randomizedIdleTime = Random.Range(idleTime * 0.9f, idleTime * 1.1f);
		return randomizedIdleTime / BDifficultyManager.Instance.SpeedModifier;
	}
	#endregion
}
