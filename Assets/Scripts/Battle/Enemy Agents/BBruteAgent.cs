﻿using UnityEngine;
using System.Collections;

public class BBruteAgent : BEnemyAgent
{
	public override float ImageHeight{get{return 350f;}}

	#region Base Stats
	protected override float baseHealthStat  {get {return DesignValues.Brute.Stats.Health;}}
	protected override float baseStaminaStat {get {return DesignValues.Brute.Stats.Stamina;}}
	protected override float baseAttackStat  {get {return DesignValues.Brute.Stats.Attack;}}
	protected override float baseShieldStat  {get {return DesignValues.Brute.Stats.Shield;}}
	#endregion

	#region Hidden Stats
	protected override float attack1Power{get{return DesignValues.Brute.Attack1Power;}}
	protected override float attackCooldownTime{get{return DesignValues.Brute.HiddenStats.AttackCooldownTime;}}
	protected override RandomRangedValue telegraphRangeTime{get{return DesignValues.Brute.HiddenStats.TelegraphTime;}}
	public override float BlockCooldownTime {get{return DesignValues.Brute.HiddenStats.BlockCooldownTime;}}
	protected override float blockStartSpeedScalar{get{return 2f;}}
	public override float MinBlockTime{get{return DesignValues.Brute.MinBlockTime / BDifficultyManager.Instance.SpeedModifier;}}
	public override float MaxBlockTime{get{return DesignValues.Brute.MaxBlockTime / BDifficultyManager.Instance.SpeedModifier;}}
	#endregion

	protected override float idleTime{get {return DesignValues.Brute.IdleTime;}}
	public override float AttackWeight{get {return DesignValues.Brute.Weights[AgentActions.Attack];}}
	public override float BlockWeight{get {return DesignValues.Brute.Weights[AgentActions.Block];}}
	public override float DoNothingWeight{get {return DesignValues.Brute.Weights[AgentActions.DoNothing];}}
}

