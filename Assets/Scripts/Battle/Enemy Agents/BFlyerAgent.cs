﻿using UnityEngine;
using DG.Tweening;


public class BFlyerAgent : BEnemyAgent
{
	public override float ImageHeight{get{return 200f;}}

	[SerializeField] Transform shockwaveSpawnPoint;

	#region Base Stats
	protected override float baseHealthStat  {get {return DesignValues.Flyer.Stats.Health;}}
	protected override float baseStaminaStat {get {return DesignValues.Flyer.Stats.Stamina;}}
	protected override float baseAttackStat  {get {return DesignValues.Flyer.Stats.Attack;}}
	protected override float baseShieldStat  {get {return DesignValues.Flyer.Stats.Shield;}}
	#endregion

	#region Hidden Stats
	protected override float attack1Power{get{return DesignValues.Flyer.Attack1Power;}} 
	protected override float attack2Power{get{return DesignValues.Flyer.RawStaminaDamage;}} 
	protected override float attackCooldownTime{get{return DesignValues.Flyer.HiddenStats.AttackCooldownTime;}}
	protected override float staminaForAttack{get{return DesignValues.Flyer.HiddenStats.StaminaForAttack;}}
	protected override RandomRangedValue telegraphRangeTime{get{return DesignValues.Flyer.HiddenStats.TelegraphTime;}}
	public override float BlockCooldownTime {get{return DesignValues.Flyer.HiddenStats.BlockCooldownTime;}}
	#endregion

	protected override float idleTime{
		get {
			return BattleManager.PlayerPartyLead.IsStunned ? DesignValues.Flyer.IdleTimeWhenPlayerStunned : DesignValues.Flyer.IdleTime;
		}
	}
	public override float AttackWeight{
		get {
			return BattleManager.PlayerPartyLead.IsStunned ? DesignValues.Flyer.WeightsWhenPlayerStunned [AgentActions.Attack] : DesignValues.Flyer.Weights [AgentActions.Attack];
		}
	}
	public float Attack2Weight{
		get {
			return BattleManager.PlayerPartyLead.IsStunned ? DesignValues.Flyer.WeightsWhenPlayerStunned [AgentActions.Attack2] : DesignValues.Flyer.Weights [AgentActions.Attack2];
		}
	}
	public override float DoNothingWeight{
		get {
			return BattleManager.PlayerPartyLead.IsStunned ? DesignValues.Flyer.WeightsWhenPlayerStunned [AgentActions.DoNothing] : DesignValues.Flyer.Weights [AgentActions.DoNothing];
		}
	}
	public static T DecorateTweenWithEarlyCutoff<T>(T _tween, BAgent _agent, float _attackStat, float _attackPower, GameObject _fx, float _regularPercent, float _guardPercent) where T : Tween
	{
		_tween.OnUpdate(() => {
			if (_agent.IsDead || BattleManager.PlayerPartyLead == null || BattleManager.PlayerPartyLead.IsDead) {
				_tween.Kill();
			} else {
				var percent = _tween.ElapsedPercentage();
				if (BattleManager.PlayerPartyLead != null && BattleManager.PlayerPartyLead.IsBlocking && percent > _regularPercent) {
					var attackPacket = new AttackPacket (_agent, _attackStat, _attackPower, false);
					BattleManager.AttackOpponent (attackPacket);
					_tween.Kill();
				} else if (BGuardFriendlyMonster.IsDefending && percent > _guardPercent) {
					var attackPacket = new AttackPacket (_agent, _attackStat, _attackPower, false);
					BattleManager.AttackOpponent (attackPacket);
					_tween.Kill();
				}
			}
		}).OnKill(() => _fx.GetComponent<BStaticEffectReleaser> ().Release ());
		return _tween;
	}
	public static void PatternVariation1 (BAgent _agent, float _attackStat, float _attackPower, Transform _shockwaveSpawnPoint, float _shockwaveTravelTime)
	{
		_shockwaveTravelTime /= BDifficultyManager.Instance.SpeedModifier;
		var fx = BEffectsPool.CreateFlyerWindEffectAtPos (_shockwaveSpawnPoint.position);
		var target = BattleManager.PlayerPartyLead.transform.position + (BattleManager.PlayerPartyLead.ImageHeight * 0.5f * Vector3.up);
		var tween = fx.transform.DOMove (target, _shockwaveTravelTime).SetEase (Ease.InCirc).OnComplete (() =>  {
			var attackPacket = new AttackPacket (_agent, _attackStat, _attackPower, false);
			BattleManager.AttackOpponent (attackPacket);
		});
		DecorateTweenWithEarlyCutoff(tween, _agent, _attackStat, _attackPower, fx, 0.9f, 0.375f);
	}

	public System.Collections.IEnumerator DoBasicAttack()
	{
		const string anim = "Attack 1";
		if (canAttack) {
			onStartAttack();
			safePlayAudio (effectsData.AttackSounds[0]);
			safePlayClip(anim);
			var totalTime = getTimeForClip(anim);
			var timer = 0f;
			var dealtDamage = false;
			const float percentToDealDamage = 0.45f;
			var timeTillDamageForGuardFM = percentToDealDamage * totalTime;
			while (timer < totalTime && !IsDead) {
				if (BattleManager.PlayerPartyLead == null || BattleManager.PlayerPartyLead.IsDead) {
					break;
				}
				sendTelegraphUpdate(timer / timeTillDamageForGuardFM , timeTillDamageForGuardFM );
				if (timer >= (totalTime * percentToDealDamage) && !dealtDamage) {
					PatternVariation1(this, rawAttackStat, attack1Power, shockwaveSpawnPoint, DesignValues.Flyer.Pattern1TravelTime.floatValue);
					dealtDamage = true;
					decreaseStaminaByValue(staminaForAttack);
				}
				yield return null;
				timer += Time.deltaTime;
				if (takeDamagePacket != null) {
					onTakeDamage();
					if (!superArmor) {
						break;
					}
				}
			}
			onEndAttack();
			canAttack = true;
		}
	}
}
