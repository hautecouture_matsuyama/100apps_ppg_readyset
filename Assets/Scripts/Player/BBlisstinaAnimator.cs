﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Spine;
using Spine.Unity;

public class BBlisstinaAnimator : MonoBehaviour
{
	//SFX
	[SerializeField] AudioClip[] punchSounds;
	[SerializeField] AudioClip inSound;
	[SerializeField] AudioClip outSound;
	//VO
	[SerializeField] AudioClip[] startVO;
	[SerializeField] AudioClip[] attackVO;
	[SerializeField] AudioClip finalAttackVO;

	SkeletonAnimation spineAnim;

	void Awake()
	{
		spineAnim = GetComponentInChildren<SkeletonAnimation>();
	}
	public IEnumerator PerformAnimation ()
	{
		const string start = "Bliss_Start";
		BAudioManager.Instance.CreateAndPlayAudio(inSound);
		BAudioManager.Instance.CreateAndPlayAudio(startVO[Random.Range(0, startVO.Length)], false, 1, 1 ,false, null, AudioType.Sfx, 0.2f);
		float startAnimTime = spineAnim.GetTimeForClip (start);
		var centerX = (BattleManager.PlayerPartyLead.transform.position.x + BattleManager.EnemyPartyLead.transform.position.x) / 2f;
		var centerLoc = BattleManager.PlayerPartyLead.transform.position;
		centerLoc.x = centerX;
		centerLoc.y -= 150f;
		transform.position = centerLoc;
		yield return new WaitForSeconds(startAnimTime);
		var anims = new []{"Bliss_Attack1", "Bliss_Attack2", "Bliss_Attack3"};
		var prevLoc = Vector3.left;
		for (int i = 0; i < anims.Length; ++i) {
			yield return new WaitForSeconds(DesignValues.BlissSuperMove.HangTime);
			prevLoc *= -1;
			prevLoc = Quaternion.AngleAxis(Random.Range(-DesignValues.BlissSuperMove.AngleVariation / 2, DesignValues.BlissSuperMove.AngleVariation / 2), Vector3.forward) * prevLoc;
			var target = (prevLoc * DesignValues.BlissSuperMove.SpawnDistance.floatValue) + BattleManager.EnemyPartyLead.transform.position + (Vector3.up * (i==2 ? 150 : -100));
			transform.localPosition = target;
			spineAnim.AnimationState.SetAnimation (0, anims [i], false);
			float animTime = spineAnim.GetTimeForClip(anims[i]);
			BattleManager.EnemyPartyLead.DoFakeTakeHitForSuper();
			BAudioManager.Instance.CreateAndPlayAudio(punchSounds[Random.Range(0, punchSounds.Length)]);
			if(i == anims.Length - 1) {
				BAudioManager.Instance.CreateAndPlayAudio(finalAttackVO);
			} else {
				BAudioManager.Instance.CreateAndPlayAudio(attackVO[Random.Range(0, attackVO.Length)]);
			}
			yield return new WaitForSeconds(animTime / spineAnim.timeScale);
		}
		transform.position = centerLoc;
		spineAnim.AnimationState.SetAnimation (0, start, false);
		BAudioManager.Instance.CreateAndPlayAudio(outSound);
		//yield return new WaitForSeconds(startAnimTime);
	}
}
