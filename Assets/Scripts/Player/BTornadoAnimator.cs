﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BTornadoAnimator : MonoBehaviour {

	[SerializeField]
	tk2dSpriteAnimator blossomAnimation;
	[SerializeField]
	tk2dSpriteAnimator bubblesAnimation;
	[SerializeField]
	tk2dSpriteAnimator buttercupAnimation;
	[SerializeField]
	tk2dSpriteAnimator effectLinesAnimation;
	[SerializeField]
	tk2dSpriteAnimator tornadoAnimation;
	[SerializeField]
	tk2dSpriteAnimator tornado70PercentOpaque;
	[SerializeField]
	GameObject blossomFlyIn;
	[SerializeField]
	GameObject bubblesFlyIn;
	[SerializeField]
	GameObject buttercupFlyIn;
	[SerializeField]
	AudioClip tornadoActivateSound;
	[SerializeField]
	AudioClip tornadoWhirlSound;

	tk2dSpriteAnimator activeGirlAnimation;
	string activeGirlName;
	Vector3 flyInParentStartPos;

	IEnumerator Start () 
	{
		switch (PlayerManager.GetActiveGirl())
		{
		case PPG.Blossom:
			blossomAnimation.gameObject.SetActive(true);
			activeGirlAnimation = blossomAnimation;
			activeGirlName = "Blossom";
			break;
		case PPG.Bubbles:
			bubblesAnimation.gameObject.SetActive(true);
			activeGirlAnimation = bubblesAnimation;
			activeGirlName = "Bubbles";
			break;
		case PPG.Buttercup:
			buttercupAnimation.gameObject.SetActive(true);
			activeGirlAnimation = buttercupAnimation;
			activeGirlName = "Buttercup";
			break;
		default:
			throw new System.ArgumentOutOfRangeException(PlayerManager.GetActiveGirl() + " in Start of BTornadoAttack was not an expected value!!");	
		}
		activeGirlAnimation.Play(activeGirlName + "Start");
		activeGirlAnimation.AnimationCompleted = onGirlStartAnimComplete;
		BAudioManager.Instance.CreateAndPlayAudio(tornadoActivateSound);
		BAudioManager.Instance.CreateAndPlayAudio(tornadoWhirlSound);
		yield return new WaitForSeconds(activeGirlAnimation.GetTimeForClip(activeGirlName + "Start") * 0.8f);
		setUnlockedGirlsActiveForFlyInOut(true);
		flyInParentStartPos = bubblesFlyIn.transform.parent.localPosition;
		bubblesFlyIn.transform.parent.DOLocalMove(Vector3.zero, activeGirlAnimation.GetTimeForClip(activeGirlName + "Start") * 0.2f).SetEase(Ease.Linear);
	}

	#region Helpers
	void setUnlockedGirlsActiveForFlyInOut(bool _active)
	{
		if(_active) {
			if(PlayerManager.IsGirlUnlocked(PPG.Blossom) && PlayerManager.GetActiveGirl() != PPG.Blossom) {
				blossomFlyIn.SetActive(true);
			} 
			if(PlayerManager.IsGirlUnlocked(PPG.Bubbles) && PlayerManager.GetActiveGirl() != PPG.Bubbles) {
				bubblesFlyIn.SetActive(true);
			}
			if(PlayerManager.IsGirlUnlocked(PPG.Buttercup) && PlayerManager.GetActiveGirl() != PPG.Buttercup) {
				buttercupFlyIn.SetActive(true);
			}
		} else {
			blossomFlyIn.SetActive(false);
			bubblesFlyIn.SetActive(false);
			buttercupFlyIn.SetActive(false);
		}
	}
	#endregion

	#region Anim Callbacks
	void onGirlStartAnimComplete(tk2dSpriteAnimator _anim, tk2dSpriteAnimationClip _clip)
	{
		if(PlayerManager.IsGirlUnlocked(PPG.Blossom)) {
			blossomAnimation.gameObject.SetActive(true);
			blossomAnimation.Play("BlossomLoop");
		} 
		if(PlayerManager.IsGirlUnlocked(PPG.Bubbles)) {
			bubblesAnimation.gameObject.SetActive(true);
			bubblesAnimation.Play("BubblesLoop");
		}
		if(PlayerManager.IsGirlUnlocked(PPG.Buttercup)) {
			buttercupAnimation.gameObject.SetActive(true);
			buttercupAnimation.Play("ButtercupLoop");
		}
		setUnlockedGirlsActiveForFlyInOut(false);
		effectLinesAnimation.gameObject.SetActive(false);
		tornadoAnimation.gameObject.SetActive(true);
		tornado70PercentOpaque.gameObject.SetActive(true);
		StartCoroutine(waitForLoopAndTween());
	}

	void onGirlEndAnimComplete(tk2dSpriteAnimator _anim, tk2dSpriteAnimationClip _clip)
	{
		activeGirlAnimation.gameObject.SetActive(false);
	}

	void onTornadoEndAnimComplete(tk2dSpriteAnimator _anim, tk2dSpriteAnimationClip _clip)
	{
		tornadoAnimation.gameObject.SetActive(false);
		tornado70PercentOpaque.gameObject.SetActive(false);
	}
	#endregion

	#region Coroutines
	IEnumerator waitForLoopAndTween()
	{
		const float tweenTime = DesignValues.NormalSuperMove.Duration / (DesignValues.NormalSuperMove.Steps + 1f);
		var prevLoc = Vector3.left;
		for (int i = 0; i < DesignValues.NormalSuperMove.Steps; ++i) {
			prevLoc *= -1;
			prevLoc = Quaternion.AngleAxis(Random.Range(-DesignValues.NormalSuperMove.AngleVariation / 2, DesignValues.NormalSuperMove.AngleVariation / 2), Vector3.forward) * prevLoc;
			var target = (prevLoc * DesignValues.NormalSuperMove.SpawnDistance.floatValue) + BattleManager.EnemyPartyLead.transform.position + new Vector3(0, DesignValues.NormalSuperMove.LoopYOffset, 0);
			tornadoAnimation.transform.parent.transform.DOLocalMove(target, tweenTime).SetEase(Ease.InOutSine);
			yield return new WaitForSeconds(tweenTime);
			BEffectsPool.CreateSuperHitEffectAtPos(BattleManager.EnemyPartyLead.transform.position + new Vector3(Random.Range(-100, 100), Random.Range(50, 200)));
			BattleManager.EnemyPartyLead.DoFakeTakeHitForSuper();
		}
		tornadoAnimation.transform.parent.transform.DOMove(BattleManager.PlayerPartyLead.transform.position + new Vector3(0, DesignValues.NormalSuperMove.SpawnYOffset, 0), tweenTime).SetEase(Ease.InOutSine);
		yield return new WaitForSeconds(tweenTime);
		blossomAnimation.gameObject.SetActive(false);
		buttercupAnimation.gameObject.SetActive(false);
		bubblesAnimation.gameObject.SetActive(false);
		activeGirlAnimation.gameObject.SetActive(true);
		activeGirlAnimation.Play(activeGirlName + "End");
		activeGirlAnimation.AnimationCompleted = onGirlEndAnimComplete;
		tornadoAnimation.Play("TornadoEnd");
		tornado70PercentOpaque.Play("TornadoEnd");
		activeGirlAnimation.AnimationCompleted = onTornadoEndAnimComplete;
		bubblesFlyIn.transform.parent.localScale = new Vector3(-1,1,1);
		setUnlockedGirlsActiveForFlyInOut(true);
		bubblesFlyIn.transform.parent.DOLocalMove(flyInParentStartPos, activeGirlAnimation.GetTimeForClip(activeGirlName + "Start") * 0.2f).SetEase(Ease.Linear).OnComplete(() => {
			setUnlockedGirlsActiveForFlyInOut(false);
			bubblesFlyIn.transform.parent.localScale = Vector3.one;
		});
	}
	#endregion
}
