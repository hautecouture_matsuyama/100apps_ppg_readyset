﻿using UnityEngine;
using System.Collections;
using DG.Tweening;


public enum PPG
{
	None,
	Blossom,
	Bubbles,
	Buttercup
}

public class BPlayerNavParty : MonoBehaviour
{
	public static BPlayerNavParty Instance{get; private set;}

	[SerializeField] AudioClip tropicalIntroMusic;
	[SerializeField] AudioClip tropicalLoopMusic;
	[SerializeField] AudioClip volcanoIntroMusic;
	[SerializeField] AudioClip volcanoLoopMusic;
	[SerializeField] AudioClip monsterIntroMusic;
	[SerializeField] AudioClip monsterLoopMusic;
	[SerializeField] AudioClip battleIntroMusic;
	[SerializeField] AudioClip battleLoopMusic;
	[SerializeField] NavEnvSoundEffectsData effectsData;

	const float tropicalIntroPercent = 0.78125f;
	const float volcanoIntroPercent = 0.7576f;
	const float monsterIntroPercent = 0.8f;
	const float battleIntroPercent = 0.7648f;
	AudioClip navMusicIntro;
	AudioClip navMusicLoop;
	Transform currentGirl;
	void Awake()
	{
		Instance = this;;
	}
	IEnumerator Start ()
	{
		float introPercent = 0f;
		switch(EnvironmentManager.CurrentEnvironment) {
		case EnvironmentManager.EnvironmentType.Tropical:
			navMusicIntro = tropicalIntroMusic; 
			navMusicLoop = tropicalLoopMusic;
			introPercent = tropicalIntroPercent;
			monsterIntroMusic.UnloadAudioData();
			monsterLoopMusic.UnloadAudioData();
			volcanoIntroMusic.UnloadAudioData();
			volcanoLoopMusic.UnloadAudioData();
			break;
		case EnvironmentManager.EnvironmentType.Volcano:
			navMusicIntro = volcanoIntroMusic;
			navMusicLoop = volcanoLoopMusic;
			introPercent = volcanoIntroPercent;
			monsterIntroMusic.UnloadAudioData();
			monsterLoopMusic.UnloadAudioData();
			tropicalIntroMusic.UnloadAudioData();
			tropicalLoopMusic.UnloadAudioData();
			break;
		case EnvironmentManager.EnvironmentType.Monster:
			navMusicIntro = monsterIntroMusic;
			navMusicLoop = monsterLoopMusic;
			introPercent = monsterIntroPercent;
			volcanoIntroMusic.UnloadAudioData();
			volcanoLoopMusic.UnloadAudioData();
			tropicalIntroMusic.UnloadAudioData();
			tropicalLoopMusic.UnloadAudioData();
			break;
		default:
			Debug.LogWarning("Something went wrong! No nav music will play.");
			break;
		}
		while(!BWorldMapGenerator.WorldGenerated) {
			yield return null;
		}
		if (PlayerManager.NumberofGirlUnlocked() > 0) {
			onCharacterChanged((PPG)PlayerManager.GetActiveGirl());
		}
		PlayerManager.HandleCharacterChange += onCharacterChanged;

		if(navMusicIntro != null && navMusicLoop != null) {
			BAudioManager.Instance.PlayMusic (navMusicLoop, navMusicIntro, introPercent, 0.5f);
		}
	}

	void OnDestroy()
	{
		PlayerManager.HandleCharacterChange -= onCharacterChanged;
		Instance = null;
	}

	IEnumerator waitforBringInGirl (PPG _girl)
	{
		const float tweenTime = 0.2f;
		const float tweenDelay = 0.5f;
		var dragger = BWorldMapDragger.Instance;
		var center = dragger.CurrentRoom.Handle.position;
		var offScreenX = center.x - (tk2dCamera.Instance.nativeResolutionWidth / 2) - 300;

		var doingASwap = currentGirl != null;
		float y = 0f;

		if (doingASwap) {
			y = currentGirl.transform.position.y;
			currentGirl.DOMoveX (offScreenX, tweenTime).SetUpdate (true).SetDelay (0.5f).SetEase (Ease.InOutQuad);
			yield return new WaitForSecondsRealtime(tweenTime + tweenDelay);
			Destroy (currentGirl.gameObject);
			currentGirl = null;
			Resources.UnloadUnusedAssets();
		}
		var resource = Resources.LoadAsync<GameObject> ("Prefabs/Characters/Players/" + _girl.ToString ());
		yield return resource;
		var girlPrefab = resource.asset as GameObject; //Resources.Load<GameObject> ("Prefabs/Characters/Players/" + _girl.ToString ());
		var instance = Object.Instantiate (girlPrefab, transform);
		currentGirl = instance.transform;

		var selecter = BGirlSelectOverlay.Instance;
		if (selecter.gameObject.activeSelf && selecter.CallerWillCloseOverlay) {
			selecter.Close();
			selecter.CallerWillCloseOverlay = false;
		}

		var midstOfBattle = BattleManager.CurrentGameState == GameState.Limbo;

		if (doingASwap) {
			currentGirl.Translate(Vector3.right * offScreenX);
			var pos = currentGirl.transform.position;
			pos.y = y;
			currentGirl.transform.position = pos;
			var offset = midstOfBattle ? BWorldMapGenerator.AgentOffset : 0;
			currentGirl.DOMoveX (center.x - offset, tweenTime).SetEase (Ease.InOutQuad).SetUpdate (true);
			yield return new WaitForSecondsRealtime(tweenTime);
		}
		if (!midstOfBattle) {
			BattleManager.StartNavigation ();
			if (BWorldMapDragger.PickingGirlForFirstTime) {
				const float amount = 110f;
				currentGirl.Translate (Vector3.down * amount);
				currentGirl.DOLocalMoveY (amount, 0.1f).SetRelative (true);
			}
		}
	}

	void onCharacterChanged(PPG _girl)
	{
		StartCoroutine(waitforBringInGirl (_girl));
	}

	public void MoveGirlToCenter()
	{
		BattleManager.CurrentGameState = GameState.PostBattle;
		var dragger = BWorldMapDragger.Instance;
		var pos = dragger.CurrentRoom.Handle.position;
		transform.DOMove(pos, 1f).SetEase(Ease.InOutQuad).OnComplete(() => {
			BattleManager.CurrentGameState = GameState.Navigation;
		});
		BAudioManager.Instance.StopMusic(0.3f, 1);
		BAudioManager.Instance.StopIntroMusic();
		BAudioManager.Instance.TogglePauseMusic ();
	}

	public void MoveGirlsToNewRoom(RoomNode _target, bool _notReadjustMove, float _time,float _delay, System.Action _onComplete)
	{
		if (!(BattleManager.CurrentGameState == GameState.Navigation || BattleManager.CurrentGameState == GameState.ObjectiveEvent)) {
			return;
		}
		if (_notReadjustMove) {
			StartCoroutine(playWhooshSoundAfterDelay(0.5f));
		}
		var enemiesInRoom = _target.Enemies.Count > 0;
		var narrativeObjective = GameObject.FindGameObjectWithTag("NarrativeObjective") != null;
		var pos = _target.Handle.position;
		pos -= (enemiesInRoom || narrativeObjective) ? (Vector3.right * BWorldMapGenerator.AgentOffset) : Vector3.zero;
		if (pos.x < transform.position.x) {
			var scale = transform.localScale;
			scale.x = -1 * Mathf.Abs(scale.x);
			transform.localScale = scale;
		} else if (pos.x > transform.position.x) {
			var scale = transform.localScale;
			scale.x = Mathf.Abs(scale.x);
			transform.localScale = scale;
		}
		transform.DOMove(pos, _time).SetEase(Ease.InOutQuad).SetDelay(_delay).OnComplete(() => {
			if (_onComplete != null) {
				_onComplete();
			}
			if (enemiesInRoom) {
				BAudioManager.Instance.TogglePauseMusic();
				BAudioManager.Instance.StopIntroMusic();
				Instance.PlayBattleMusic();
				BattleManager.StartBattle();
			}
		});
	}

	public void PlayBattleMusic()
	{
		BAudioManager.Instance.PlayMusic(battleLoopMusic, battleIntroMusic, battleIntroPercent, 0.5f, 1);
	}

	IEnumerator playWhooshSoundAfterDelay(float _delay)
	{
		yield return new WaitForSeconds(_delay);
		BAudioManager.Instance.CreateAndPlayAudio(effectsData.MoveWhooshSounds[Random.Range(0,effectsData.MoveWhooshSounds.Length)], false, 0.3f);
	}
}
