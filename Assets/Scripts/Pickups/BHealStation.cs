﻿using UnityEngine;

public class BHealStation : BStation
{
	void Start()
	{
		popupHeaderText = "HP全回復";
		popupBodyText = "広告を見て今戦っているガールズのHPを全回復させる？";
		popupConfirmText = "見る";
	}
	protected override void onWatchedAd()
	{
		Utilities.EnableRenderingForAds();
		BAudioManager.Instance.ForceMuteForAds(false);
		PlayerManager.HealActiveGirl();
		BPlayerAgent player = GameObject.FindWithTag("Player").GetComponent<BPlayerAgent>();
		BAudioManager.Instance.PlayHealSound ();
		BEffectsPool.CreateHealEffectAtPos(player.transform.position + Vector3.up * player.ImageHeight * 0.7f);
	}
}
