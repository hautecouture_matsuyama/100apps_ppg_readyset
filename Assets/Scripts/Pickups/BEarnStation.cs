﻿using UnityEngine;

public class BEarnStation : BStation
{
	void Start()
	{
		popupHeaderText = "プレゼント獲得チャンス！";
		popupBodyText = "広告を見てプレゼントをもらう？";
		popupConfirmText = "見る";
	}
	protected override void onWatchedAd()
	{
		Utilities.EnableRenderingForAds();
		BAudioManager.Instance.ForceMuteForAds(false);
		BGiftOverlay.SpawnGift(GiftSpawner.GetItemForEarnStation());
	}
}
