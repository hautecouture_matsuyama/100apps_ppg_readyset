﻿using UnityEngine;
using DG.Tweening;

public class BGoldCoinPickup : BPickup
{
	void Start()
	{
		hud = FindObjectOfType<BGoldHUD>().transform;
	}

	protected override void OnCollect()
	{
		safePlayAudio(effectsData.CoinPickupSound);
		var worldSpace = Camera.main.ScreenToWorldPoint(hud.position);
		transform.DOMove(new Vector3(worldSpace.x, worldSpace.y, transform.position.z), tweenTime)
			.SetEase(Ease.InCirc)
			.OnComplete(() => {
				++PlayerManager.GoldCount;
				CleanUp();
			});
		transform.DOScale(0f, tweenTime)
			.SetEase(Ease.InCirc);
		base.OnCollect();
	}
}
