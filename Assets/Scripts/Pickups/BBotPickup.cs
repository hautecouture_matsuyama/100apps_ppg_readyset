﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class BBotPickup : BPickup
{
	const float shakeTime = 0.5f;
	const float scaleOutTime = 0.2f;

	Vector3 origPos;
	Collider2D coll;
	GameState prevState;
	[SerializeField] GameObject botParent;
	[SerializeField] SpriteRenderer sponsorhip;
	void Awake()
	{
		coll = GetComponent<Collider2D>();
	}
	void Start()
	{
		if(BSponsorshipManager.ImageMapping != null) {
			if(!BSponsorshipManager.ImageMapping.ContainsKey(BSponsorshipManager.giftbotKey)) {
				CleanUp();
			}
			sponsorhip.sprite = BSponsorshipManager.ImageMapping[BSponsorshipManager.giftbotKey];
		}
	}

	public void StartMovingAround(Vector3 _pos)
	{
		StartCoroutine(moveAround(_pos));
	}

	IEnumerator moveAround(Vector3 _pos)
	{
		var prevLoc = Vector3.right;
		while (true) {
			prevLoc *= -1;
			prevLoc = Quaternion.AngleAxis(Random.Range(-DesignValues.SponsorshipBot.AngleVariation / 2, DesignValues.SponsorshipBot.AngleVariation / 2), Vector3.forward) * prevLoc;
			var target = (prevLoc * DesignValues.SponsorshipBot.SpawnDistance.floatValue) + _pos;
			transform.DOLocalMove(target, DesignValues.SponsorshipBot.TravelTime).SetEase(Ease.InOutSine);
			yield return new WaitForSeconds(DesignValues.SponsorshipBot.TravelTime + DesignValues.SponsorshipBot.HangTime);//adding extra time 
		}
	}
	IEnumerator waitForSpawnGift()
	{
		yield return new WaitForSeconds(shakeTime + scaleOutTime);
		Destroy(gameObject);
		var g = BGiftOverlay.SpawnGift(GiftSpawner.GetItemForEarnStation());
		g.OnClose = () => {
			BattleManager.CurrentGameState = prevState;
		};
	}
	protected override void OnCollect ()
	{
		safePlayAudio(effectsData.GiftBotTapSound);
		coll.enabled = false;
		StopAllCoroutines();
		DOTween.Kill(gameObject);
		prevState = BattleManager.CurrentGameState;
		BattleManager.CurrentGameState = GameState.ObjectiveEvent;
		DOTween.Kill(botParent);
		DOTween.Kill(shadowObject);
		botParent.transform.DOShakeRotation(shakeTime, Vector3.forward * 60, 30, 20);
		transform.DOScale(0f, scaleOutTime).SetDelay(shakeTime).SetEase(Ease.InBack);
		StartCoroutine(waitForSpawnGift());
	}
}
