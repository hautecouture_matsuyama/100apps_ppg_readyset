﻿using UnityEngine;
using UnityEngine.UI;

public class BGiftPickup : BPickup
{
	public System.Action OnCollectAction;

	[SerializeField] tk2dSpriteAnimator tkAnim;
	protected override void OnCollect()
	{
		safePlayAudio(effectsData.GiftPickupSound);
		BEffectsPool.CreateCollectEffectAtPos(transform.position);
		OnCollectAction();
		Destroy(gameObject);
	}
}
