﻿using UnityEngine;
using DG.Tweening;

public class BHeartShardPickup : BPickup
{
	void Start()
	{
		hud = FindObjectOfType<BHeartShardHUD>().transform;
	}

	protected override void OnCollect()
	{
		if (BattleManager.CurrentGameState == GameState.Navigation) {
			safePlayAudio(effectsData.HeartShardPickupSound);
			var worldSpace = Camera.main.ScreenToWorldPoint(hud.position);
			transform.DOMove(new Vector3(worldSpace.x, worldSpace.y, transform.position.z), tweenTime)
				.SetEase(Ease.InCirc)
				.OnComplete(() => {
					++PlayerManager.HeartShardCount;
					CleanUp();
				});
			transform.DOScale(0f, tweenTime)
				.SetEase(Ease.InCirc);
			base.OnCollect();
		}
	}
}
