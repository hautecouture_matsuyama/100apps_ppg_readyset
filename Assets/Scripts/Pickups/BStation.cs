﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StationType
{
	Earn,
	Heal,
	Gift
}

public class BStation : BPickup
{

	[SerializeField] string onSpriteName;
	[SerializeField] string offSpriteName;

	protected string popupHeaderText;
	protected string popupBodyText;
	protected string popupConfirmText;

	tk2dSprite sp;
	Collider2D coll;
	void Awake()
	{
		sp = GetComponent<tk2dSprite>();
		coll = GetComponent<Collider2D>();

	}
	protected override void OnCollect ()
	{
		if (BattleManager.CurrentGameState == GameState.Navigation) {
			safePlayAudio(effectsData.StationActivateSound);
			coll.enabled = false;
			sp.SetSprite(offSpriteName);
			StartCoroutine(waitForSpawnConfirm());
		}
	}
	void playAd()
	{
		AppLovin.SetUnityAdListener (this.gameObject.name);

		#if UNITY_EDITOR
		StartCoroutine(Utilities.WaitForFakeAdTime());
		#elif !UNITY_EDITOR
			Utilities.DisableRenderingForAds();
			BAudioManager.Instance.ForceMuteForAds(true);
		if(AppLovin.IsIncentInterstitialReady()){
			AppLovin.ShowRewardedInterstitial ();
		}	

		#endif
			

	}

	void onAppLovinEventReceived(string ev) {
		if(ev.Contains("REWARDAPPROVEDINFO")) {
			Debug.Log ("よばれた");
			// 動画が再生された
		} else if(ev.Contains("LOADEDREWARDED")) {
			// 読み込み完了
		} else if(ev.Contains("LOADREWARDEDFAILED")) {  
			// 読み込み失敗
		} else if(ev.Contains("HIDDENREWARDED")) {
			//動画の表示し終わり、非表示にされた
			Debug.Log ("をわｒ");

			onWatchedAd ();

			// 次の動画の準備
			AppLovin.LoadRewardedInterstitial();
		}
	}


	protected virtual void onWatchedAd(){}

	IEnumerator waitForSpawnConfirm()
	{
		BattleManager.CurrentGameState = GameState.ObjectiveEvent;
		yield return new WaitForSeconds(0.25f);
		BGenericConfirmOverlay.SpawnConfirm(popupHeaderText, popupBodyText, popupConfirmText, () => {
			playAd();
			BattleManager.CurrentGameState = GameState.Navigation;
		}, () => {
			coll.enabled = true;
			sp.SetSprite(onSpriteName);
			BattleManager.CurrentGameState = GameState.Navigation;
		});
	}
}
