﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class BPickup : MonoBehaviour
{
	protected const float tweenTime = 0.25f;

	public ObjectPool PoolHandle;

	[SerializeField] protected GameObject shadowObject;
	[SerializeField] protected NavEnvSoundEffectsData effectsData;

	protected Transform hud;
	void OnEnable()
	{
		if (shadowObject) {
			shadowObject.transform.DOScale(Vector3.one, 1).Kill(true);
		}
		var tweens = GetComponentsInChildren<DOTweenAnimation>();
		foreach(var t in tweens) {
			t.DORestart(true);
		}
	}
	void OnMouseDown()
	{
		OnCollect();
	}

	public void CleanUp()
	{
		if (PoolHandle != null) {
			PoolHandle.Release(gameObject);
		} else {
			Destroy(gameObject);
		}
	}

	protected virtual void OnCollect()
	{
		//kill any top level tweens
		var tweens = DOTween.TweensByTarget(gameObject);
		if (tweens != null && tweens.Count > 0 ) {
			DOTween.Kill(gameObject);
		}
		if(shadowObject != null) {
			DOTween.Pause(shadowObject);
			shadowObject.transform.DOScale(Vector3.zero, 0.15f);
		}
		//Pickup pfx
		BEffectsPool.CreateCollectEffectAtPos(transform.position + Vector3.up * 200f);
	}

	protected void safePlayAudio(AudioClip _clip)
	{
		if(_clip != null) {
			BAudioManager.Instance.CreateAndPlayAudio(_clip);
		}
	}
	protected void safePlayAudio(AudioClip[] _clips)
	{
		if(_clips.Length > 0) {
			safePlayAudio(_clips[Random.Range(0, _clips.Length)]);
		}
	}
}
