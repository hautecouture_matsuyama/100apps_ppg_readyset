﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class BStoryMoviePlayerCallback : MobileMoviePlayer 
{
	protected override void onMovieEnded ()
	{
		Resources.UnloadUnusedAssets();
		SceneManager.LoadScene("Title");
	}
}
