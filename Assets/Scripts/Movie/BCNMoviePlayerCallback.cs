﻿using UnityEngine;
using System.Collections;

public class BCNMoviePlayerCallback : MobileMoviePlayer 
{

	public BBootupFlowManager BFManager;

	protected override void onMovieEnded ()
	{
		BFManager.OnCnVidEnded();
	}
}
