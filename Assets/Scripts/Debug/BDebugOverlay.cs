﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using DG.Tweening;
using System.Linq;

public class BDebugOverlay : BBaseOverlay
{
	static BDebugOverlay()
	{
		ScoreModifier = 1;
		SkipAds = false;
	}
	public static BDebugOverlay LaunchDebug(bool _inGame)
	{
		var prefabName = _inGame ? "InGame" : "Settings";
		var prefab = Resources.Load<GameObject>(@"Prefabs\UI\" + prefabName + "DebugOverlay");
		var instance = Instantiate(prefab);
		return instance.GetComponent<BDebugOverlay>();
	}

	[SerializeField] Dropdown enemyDropDown;
	[SerializeField] Dropdown stationDropDown;
	[SerializeField] Toggle scoreButton;
	[SerializeField] Toggle skipToggle;
	[SerializeField] Toggle reviveToggle;
	[SerializeField] Toggle giftToggle;

	protected override void Start ()
	{
		base.Start ();
		animateInScale();
		if (enemyDropDown) {
			var enEnums = Utilities.GetValues<EnemyType>().Select(s => s.ToString()).ToList();
			enEnums.Insert(0, "None");
			enEnums.Add("");
			enemyDropDown.ClearOptions();
			enemyDropDown.AddOptions(enEnums);
			enemyDropDown.value = debugForceEnemy == null ? 0 : Utilities.GetIndex((EnemyType)debugForceEnemy) + 1;
		}
		if (stationDropDown) {
			var stEnums = Utilities.GetValues<StationType>().Select(s => s.ToString()).ToList();
			stEnums.Insert(0, "None");
			stEnums.Add("");
			stationDropDown.ClearOptions();
			stationDropDown.AddOptions(stEnums);
			stationDropDown.value = debugForceStation == null ? 0 : Utilities.GetIndex((StationType)debugForceStation) + 1;
			stationDropDown.enabled = debugForceEnemy == null;
		}
		if (scoreButton != null) {
			scoreButton.isOn = ScoreModifier != 1;
		}
		if (skipToggle != null) {
			skipToggle.isOn = SkipAds;
		}
		if (reviveToggle != null) {
			reviveToggle.isOn = ForceReviveOnNextDeath;
		}
		if (giftToggle != null) {
			giftToggle.isOn = ForceFreeGiftOnNextDeath;
		}
	}

	public void Close()
	{
		animateOutScale().OnComplete(() => Destroy (gameObject));
	}

	public void DebugGiveGold(int _count)
	{
		PlayerManager.GoldCount += (uint)_count;
	}
	public void DebugGiveHeartShard(int _count)
	{
		PlayerManager.HeartShardCount += (uint)_count;
	}

	public static EnemyType? debugForceEnemy;
	public static bool ShouldForceEnemy{get {return debugForceEnemy != null;}}
	public static EnemyType DebugForceEnemy{
		get {
			var ret = (EnemyType)debugForceEnemy;
			debugForceEnemy = null;
			return ret;
		}
	}
	public void DebugForceNextEnemy(int _enemy)
	{
		if (_enemy != 0) {
			var offset = _enemy - 1;
			if (offset == Utilities.GetValues<EnemyType>().Count()) {
				offset--;
			}
			debugForceEnemy = Utilities.GetEnum<EnemyType>(offset);

			debugForceStation = null;
			stationDropDown.value = 0;
		} else {
			debugForceEnemy = null;
		}
	}

	public static uint ScoreModifier {get; private set;}
	public void OnToggleScoreModifier(bool _xTen)
	{
		ScoreModifier = (uint)(_xTen ? 100 : 1);
	}

	public static bool SkipAds{get; private set;}
	public void OnToggleSkipAds(bool _skip)
	{
		SkipAds = _skip;
	}

	public static bool ForceReviveOnNextDeath;
	public void OnForceReviveOnNextDeath(bool _force)
	{
		ForceReviveOnNextDeath = _force;
	}

	public static bool ForceFreeGiftOnNextDeath{get; set;}
	public void OnForceFreeGiftOnNextDeath(bool _force)
	{
		ForceFreeGiftOnNextDeath = _force;
	}
	public static StationType? debugForceStation;
	public static bool ShouldForceStation{get {return debugForceStation != null;}}
	public static StationType DebugForceStation {
		get {
			var ret = (StationType)debugForceStation;
			debugForceStation = null;
			return ret;
		}
	}
	public void DebugForceNextStation(int _station)
	{
		if (_station != 0) {
			var offset = _station - 1;
			if (offset == Utilities.GetValues<StationType>().Count()) {
				offset--;
			}
			debugForceStation = Utilities.GetEnum<StationType>(offset);

			debugForceEnemy = null;
			enemyDropDown.value = 0;
		} else {
			debugForceStation = null;
		}
	}

	public static bool HideSDKLogs = true;

	protected override void onEscape ()
	{
		Close();
	}
}
