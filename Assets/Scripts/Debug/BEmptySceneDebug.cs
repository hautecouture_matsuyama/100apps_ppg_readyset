﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BEmptySceneDebug : MonoBehaviour
{
	static string nextScene;

	public static void GoToScene(string _scene)
	{
		nextScene = _scene;
		BScreenFade.FadeToScene ("empty-debug", true);
	}
	void Start()
	{
		BPlayerAgent.CleanFMInstanceHandle();
		Resources.UnloadUnusedAssets();
		System.GC.Collect();
		SceneManager.LoadScene (nextScene);
		nextScene = string.Empty;
	}
//	void Update()
//	{
//		#if UNITY_EDITOR
//		if (Input.GetKeyDown(KeyCode.Q)) {
//			Debug.Log("------------- cleaning -------------");
//			Resources.UnloadUnusedAssets();
//			System.GC.Collect();
//		}
//		if (Input.GetKeyDown(KeyCode.Space)) {
//			Debug.Log("------------- switching -------------");
//			SceneManager.LoadScene (nextScene);
//			nextScene = string.Empty;
//		}
//		#else
//		if (Input.touchCount == 1) {
//			Debug.Log("------------- cleaning -------------");
//			Resources.UnloadUnusedAssets();
//			System.GC.Collect();
//		}
//		if (Input.touchCount == 2) {
//			Debug.Log("------------- switching -------------");
//			SceneManager.LoadScene (nextScene);
//			nextScene = string.Empty;
//		}
//		#endif
//	}
}
