﻿using UnityEngine;
using DG.Tweening;

public class BCameraShake : MonoBehaviour
{
	public static BCameraShake Instance{get; private set;}
	float smallMag, mediumMag, bigMag;
	float smallTime, mediumTime, bigTime;

	Vector3 positionBeforeShake;
	void Awake()
	{
		Instance = this;
		var min = Mathf.Min(Screen.width, Screen.height);
		smallMag = 0.03f * min;
		mediumMag = 0.07f * min;
		bigMag = 0.5f * min;

		smallTime = 0.15f;
		mediumTime = 0.25f;
		bigTime = 0.6f;
	}
	void OnDestroy()
	{
		Instance = null;
	}

	#region Shakes
	public void SmallShake ()
	{
		Vector3 shakeAmount = new Vector3(smallMag, smallMag, 0);
		CustomShake(shakeAmount, smallTime);
	}
	public void MediumShake ()
	{
		Vector3 shakeAmount = new Vector3(mediumMag, mediumMag, 0);
		CustomShake(shakeAmount, mediumTime);
	}
	public void BigShake ()
	{
		Vector3 shakeAmount = new Vector3(bigMag, bigMag, 0);
		CustomShake(shakeAmount, bigTime);
	}
	public void CustomShake (Vector3 _shakeAmount, float _time)
	{
		transform.DOKill(true);
		positionBeforeShake = transform.position;
		transform.DOShakePosition(_time, _shakeAmount, 30).OnComplete(resetPosition);
	}
	void resetPosition()
	{
		transform.position = positionBeforeShake;
	}
	#endregion
}
