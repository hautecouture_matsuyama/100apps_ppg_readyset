﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BHiddenPFXCoinManager : MonoBehaviour
{
	public readonly List<GameObject> Coins = new List<GameObject>();

	[SerializeField] ParticleSystem pfx;
	bool setToDestroy;
	void Update()
	{
		if (setToDestroy) {
			return;
		}
		if (Coins.Count > 0) {
			for (int i = 0; i < Coins.Count; ++i) {
				if (Coins [i].activeSelf) {
					return;
				}
			}
			pfx.Stop (true);
			Destroy (gameObject, 0.1f);
			setToDestroy = true;
		}
	}
}
