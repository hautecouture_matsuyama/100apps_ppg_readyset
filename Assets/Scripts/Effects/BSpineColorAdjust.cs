﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine;
using Spine.Unity;

public class BSpineColorAdjust : MonoBehaviour 
{

	[SerializeField] SkeletonAnimation spineAnim;
	[SerializeField] Color color;

	void Start () 
	{
		spineAnim.skeleton.SetColor(color);
	}
}
