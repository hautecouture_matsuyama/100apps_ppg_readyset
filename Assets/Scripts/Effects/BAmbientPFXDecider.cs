﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BAmbientPFXDecider : MonoBehaviour
{
	[SerializeField] GameObject tropicalPfx;
	[SerializeField] GameObject volcanoPfx;
	[SerializeField] GameObject monsterPfx;
	// Use this for initialization
	void Start ()
	{
		if (EnvironmentManager.CurrentEnvironment == EnvironmentManager.EnvironmentType.Tropical) {
			if (tropicalPfx != null) {
				tropicalPfx.SetActive(true);
			}
		} else if (EnvironmentManager.CurrentEnvironment == EnvironmentManager.EnvironmentType.Volcano) {
			if (volcanoPfx != null) {
				volcanoPfx.SetActive(true);
			}
		} else if (EnvironmentManager.CurrentEnvironment == EnvironmentManager.EnvironmentType.Monster) {
			if (monsterPfx != null) {
				monsterPfx.SetActive(true);
			}
		}
	}
}
