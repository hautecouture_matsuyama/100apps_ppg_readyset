﻿using UnityEngine;
using TMPro;
using DG.Tweening;

public class BBattleTextDisplay : MonoBehaviour
{
	void Awake ()
	{
		BattleManager.HandleStateSwitch += onBattleStateChange;
	}
	void OnDestroy()
	{
		BattleManager.HandleStateSwitch -= onBattleStateChange;
	}
	void onBattleStateChange(GameState _state)
	{
		if (_state == GameState.PreBattle) {
			BEffectsPool.CreateFightText();
		}
	}
}
