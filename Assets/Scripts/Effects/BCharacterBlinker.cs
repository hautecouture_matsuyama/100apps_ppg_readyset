﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BCharacterBlinker : MonoBehaviour
{
	public static BCharacterBlinker AddBlinkForDuration(GameObject _gObj, float _duration, float _periodicity)
	{
		var mr = _gObj.GetComponentInChildren<MeshRenderer>();
		var current = mr.GetComponent<BCharacterBlinker>();
		if (current != null) {
			current.Stop();
		} else {
			current = mr.gameObject.AddComponent<BCharacterBlinker>();
		}
		current.duration = _duration;
		current.periodicity = _periodicity;
		current.startBlink();
		return current;
	}


	MeshRenderer mr;
	const string propName = "_Blink";
	float periodicity;
	float duration;
	float timer;

	void Awake () 
	{
		mr = GetComponentInChildren<MeshRenderer>();
	}
	void OnDestroy()
	{
		resetColor ();
	}
	void startBlink()
	{
		if (mr != null ) {
			InvokeRepeating("doBlink",0, periodicity / 2f);
		}
		timer = 0f;
	}
	void doBlink ()
	{
		if (mr.sharedMaterial.HasProperty(propName)) {
			var value = mr.sharedMaterial.GetFloat(propName);
			mr.sharedMaterial.SetFloat(propName, Mathf.Approximately(0, value) ? 1f : 0f);
			timer += periodicity / 2f;
			if (timer >= duration && !Mathf.Approximately(duration, -1)) {
				Stop();
			}
		}
	}
	public void Stop()
	{
		CancelInvoke("doBlink");
		resetColor();
	}

	void resetColor ()
	{
		if (mr != null && mr.sharedMaterial != null) {
			mr.sharedMaterial.SetFloat (propName, 0);
		}
	}
}
