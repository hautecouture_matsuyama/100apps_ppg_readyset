﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BStaticEffectReleaser : MonoBehaviour {

	public ObjectPool PoolHandle{get; set;}

	Tweener wobbleTween;

	public void Release()
	{
		transform.DOKill();
		PoolHandle.Release(gameObject);
		PoolHandle = null;
	}
}
