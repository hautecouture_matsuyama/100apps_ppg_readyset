﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class BScreenFade : MonoBehaviour
{
	public static BScreenFade Instance{get; private set;}

	[SerializeField] Image sp;
	[SerializeField] TextMeshProUGUI loadingText;

	void Awake()
	{
		Instance = this;
	}
	void OnDestroy()
	{
		Instance = null;
	}
	static BScreenFade createInstance()
	{
		return Object.Instantiate((GameObject)Resources.Load ("Prefabs/UI/FadeCanvas")).GetComponentInChildren<BScreenFade>();;
	}
	public static void FadeToScene (string _scene, bool _showText)
	{
		BAudioManager.Instance.StopMusic (0.9f);
		BAudioManager.Instance.StopMusic (0.9f, 1);
		float fadeTime = 1f;
		if(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "Title") {
			fadeTime = 1.5f;
		}
		BScreenFade.StartFadeToBlack (fadeTime, _showText, () => SceneManager.LoadScene (_scene));
	}
	public static Coroutine StartFadeToBlack(float _time, bool _showText, System.Action _onEnd = null, float _percentCall = 1f)
	{
		if (Instance == null) {
			Instance = createInstance();
		} else {
			var tween = Instance.GetComponent<DOTweenAnimation>();
			tween.DOKill(true);
			Instance.StopAllCoroutines();
		}
		return Instance.StartCoroutine(Instance.fadeHelper(0, 1, 0, _time, _onEnd, _percentCall, false, _showText));
	}
	public static Coroutine StartFadeToClear(float _time, bool _showText, System.Action _onEnd = null, float _percentCall = 1f)
	{
		var destroyAfter = true;
		if (Instance == null) {
			Instance = createInstance();
		} else {
			destroyAfter = false;//we didn't create it, it already existed so don't touch it,
			//because it will somehow remember it's z order and wont show up properly on fadttoblack
		}
		System.Action endWrapper = () => {
			Instance.sp.raycastTarget = false;
			if (_onEnd != null) {
				_onEnd();
			}
		};
		const float delay = 0.5f;
		return Instance.StartCoroutine(Instance.fadeHelper(1, 0, delay, _time, endWrapper, _percentCall, destroyAfter, _showText));
	}
	public static void StopAllFades()
	{
		if(Instance != null) {
			Instance.StopAllCoroutines();
		}
	}
	IEnumerator textFadeHelper(float _from, float _to) 
	{
		loadingText.DOFade(_from, 0f).SetUpdate(true).OnComplete(() => {
			loadingText.DOFade(_to, 0.2f).SetUpdate(true);
		}).Kill(true);
		yield return new WaitForSecondsRealtime(0.2f);
	}
	IEnumerator fadeHelper(float _from, float _to, float _delay, float _time, System.Action _onEnd, float _percentCall, bool _destroy, bool _showText)
	{
		if(!_showText) {
			loadingText.DOFade(0, 0f).SetUpdate(true).Kill(true);
		}
		_percentCall = Mathf.Clamp01(_percentCall);
		sp.raycastTarget = true;
		var c = sp.color;
		c.a = _from;
		sp.color = c;
		yield return new WaitForSecondsRealtime(_delay);
		if(_to == 0 && _showText) {
			yield return StartCoroutine(textFadeHelper(1, 0));
		}
		var currentTween = sp.DOFade(_to, _time).SetUpdate(true).SetEase(Ease.Linear);
		yield return new WaitForSecondsRealtime(_time * _percentCall);
		if(_to == 1 && _showText) {
			yield return StartCoroutine(textFadeHelper(0, 1));
		}
		if (_onEnd != null) {
			_onEnd();
		}
		yield return StartCoroutine(finishFade(currentTween, _time, _percentCall, _destroy));
	}
	IEnumerator finishFade(Tweener _currentTween, float _time, float _percentCall, bool _destroy)
	{
		if (!Mathf.Approximately(1, _percentCall)) {
			yield return new WaitForSecondsRealtime(_time * (1 - _percentCall));
		}
		_currentTween.Kill(true);
		if (_destroy) {
			Destroy(transform.parent.gameObject);
		}
	}
}
