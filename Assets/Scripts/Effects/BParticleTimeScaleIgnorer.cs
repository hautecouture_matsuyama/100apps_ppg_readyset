﻿using System.Collections;
using UnityEngine;

public class BParticleTimeScaleIgnorer : MonoBehaviour 
{
	ParticleSystem pfx;

	void Awake()
	{
		pfx = GetComponent<ParticleSystem>();
	}
	void Update()
	{
		if (pfx != null && Time.timeScale < 0.01f) {
			pfx.Simulate(Time.unscaledDeltaTime, true, false);
		}
	}
}
