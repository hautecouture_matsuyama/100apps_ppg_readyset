﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BShadowPicker : MonoBehaviour
{
	void Start () 
	{
		var shadowSprite = GetComponent<tk2dSprite>();
		if(shadowSprite != null) {
			switch(EnvironmentManager.CurrentEnvironment) 
			{
				case EnvironmentManager.EnvironmentType.Tropical:
					shadowSprite.color = new Color(0.11f, 0.251f, 0.329f, 0.25f);
					break;
				case EnvironmentManager.EnvironmentType.Volcano:
					shadowSprite.color = new Color(0.212f, 0.067f, 0.133f, 0.75f);
					break;
				case EnvironmentManager.EnvironmentType.Monster:
					shadowSprite.color = new Color(0.212f, 0.067f, 0.133f, 0.35f);
					break;
			}
		}
		Destroy(this);
	}
}
