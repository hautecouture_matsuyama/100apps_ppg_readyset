﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class BCameraBattleZoom : MonoBehaviour
{
	tk2dCamera cam;
	float baseZoomFactor;
	void Awake ()
	{
		cam = GetComponent<tk2dCamera>();
		baseZoomFactor = cam.ZoomFactor;
		BattleManager.HandleStateSwitch += onBattleStateChange;
	}
	void OnDestroy()
	{
		BattleManager.HandleStateSwitch -= onBattleStateChange;
	}
	void onBattleStateChange(GameState _state)
	{
		if (_state == GameState.PreBattle) {
			DOTween.To(() => cam.ZoomFactor, a => cam.ZoomFactor = a, 1.1f * baseZoomFactor, 0.4f).SetEase(Ease.OutCirc);
		} else if (_state == GameState.PostBattle) {
			DOTween.To(() => cam.ZoomFactor, a => cam.ZoomFactor = a, baseZoomFactor, 1f)
				.SetEase(Ease.InOutQuad)
				.SetDelay(DesignValues.PostDeathWaitTime);
		}
	}
}
