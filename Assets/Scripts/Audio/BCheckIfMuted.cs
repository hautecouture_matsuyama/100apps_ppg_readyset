﻿using UnityEngine;
using System.Collections;

public class BCheckIfMuted : MonoBehaviour {

	AudioType type;
	AudioSource source;

	void Awake()
	{
		type = GetComponent<BAudioType>().Type;
		source = GetComponent<AudioSource>();
	}

	void Start () 
	{
		if((BAudioManager.SfxMuted && type == AudioType.Sfx) || (BAudioManager.MusicMuted && type == AudioType.Music)) {	
			source.mute = true;
		}
	}
}
