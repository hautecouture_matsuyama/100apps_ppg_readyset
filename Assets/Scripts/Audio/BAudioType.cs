﻿using UnityEngine;
using System.Collections;

public enum AudioType 
{
	Music,
	Sfx
}
public class BAudioType : MonoBehaviour {
	public AudioType Type;
}
