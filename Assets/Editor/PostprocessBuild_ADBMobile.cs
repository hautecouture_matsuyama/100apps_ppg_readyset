﻿#if UNITY_IOS
using UnityEditor.Callbacks;
using UnityEditor;
using System.IO;
using UnityEditor.iOS.Xcode;
using UnityEngine;

public class PostprocessBuild_ADBMobile : UnityEngine.MonoBehaviour
{
	[PostProcessBuild]
	public static void OnPostprocessBuild(BuildTarget buildTarget, string path)
	{
		if (buildTarget == BuildTarget.iOS)
		{
			string projPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";

			//Copy config vars
			string configPathUnity = "Plugins/iOS/ADBMobile/ADBMobileConfig.json";
			string configPathUnityFull = Application.dataPath + "/" + configPathUnity;
			string configPathXcode = "Libraries/Plugins/iOS/ADBMobile/ADBMobileConfig.json";
			string configPathXcodeFull = path + "/" + configPathXcode;
			configPathXcodeFull = configPathXcodeFull.Replace("//", "/");
			string XcodeProjectPath = "Libraries/Plugins/iOS/ADBMobile";

			PBXProject proj = new PBXProject();
			proj.ReadFromString(File.ReadAllText(projPath));

			string target = proj.TargetGuidByName("Unity-iPhone");
			proj.AddFileToBuild(target, proj.AddFile("usr/lib/libsqlite3.tbd", "Frameworks/libsqlite3.tbd", PBXSourceTree.Sdk));
			proj.SetBuildProperty(target, "ENABLE_BITCODE", "false");

			//Copy file from Unity to Xcode
			if(File.Exists(configPathUnityFull))
			{
				File.Copy(configPathUnityFull, configPathXcodeFull, true);
			}
				
			if(File.Exists(configPathXcodeFull))
			{
				if(Directory.Exists(path + "/" + XcodeProjectPath))
				{
					proj.AddFileToBuild(target, proj.AddFile(configPathXcode, configPathXcode));
				}
				else
				{
					Debug.LogError("Attempting to move ADBMobileConfig.json to an invalid directory: " + path + "/" + XcodeProjectPath);
				}
			}

			File.WriteAllText(projPath, proj.WriteToString());
		}
	}
}
#endif
