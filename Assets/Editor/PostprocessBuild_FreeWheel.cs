﻿#if UNITY_IOS
using UnityEditor.Callbacks;
using UnityEditor;
using System.IO;
using UnityEditor.iOS.Xcode;
using UnityEngine;

public class PostprocessBuild_FreeWheel : UnityEngine.MonoBehaviour
{
	[PostProcessBuild]
	public static void OnPostprocessBuild(BuildTarget buildTarget, string path)
	{
		if (buildTarget == BuildTarget.iOS)
		{
			string projPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";
			PBXProject proj = new PBXProject();
			proj.ReadFromString(File.ReadAllText(projPath));

			string target = proj.TargetGuidByName("Unity-iPhone");
			proj.AddFileToBuild(target, proj.AddFile("usr/lib/libxml2.tbd", "Frameworks/libxml2.tbd", PBXSourceTree.Sdk));

			File.WriteAllText(projPath, proj.WriteToString());
		}
	}
}
#endif
