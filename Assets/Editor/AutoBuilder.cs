using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;

public class AutoBuilder : MonoBehaviour
{
    // init build params
    static string buildPath = "builds";
    static string appName = GetAppName(PlayerSettings.bundleIdentifier);
    static string[] scenes;
    static string ksPass = "p0werpuff";

    // Menu Items
    //[MenuItem("Build/Build Web")]
    static void BuildWeb()
    {
        BuildProject("web");
    }

    [MenuItem("Build/Build iOS")]
    static void BuildIOS()
    {
        BuildProject("ios");
    }

    [MenuItem("Build/Build Android")]
    static void BuildAndroid()
    {
        PrepAndroid(ksPass, ksPass, false);

        BuildProject("android");
    }

    [MenuItem("Build/Build Android Split")]
    static void BuildAndroidSplit()
    {
        PrepAndroid(ksPass, ksPass, true);

        BuildProject("android");
    }

    [MenuItem("Build/Build Amazon")]
    static void BuildAmazon()
    {
        PrepAndroid(ksPass, ksPass, false);

        BuildProject("amazon");
    }

    // Build Logic
    private static void BuildProject(string platform)
    {
        // set vals
        scenes = GetScenes();
        BuildTarget target = BuildTarget.StandaloneOSXUniversal;
        string deployPath = buildPath + "/" + platform + "/";
        // remove previous builds / directory
        ClearBuildPath(deployPath);

        // setup build target
        switch (platform)
        {
            case "ios":
                target = BuildTarget.iOS;
                break;
            case "android":
            case "amazon":
                target = BuildTarget.Android;
                deployPath += appName + ".apk";
                break;
            case "web":
            case "default":
                //target = BuildTarget.WebPlayerStreamed;
                break;
        }

        // build
        BuildPipeline.BuildPlayer(scenes, deployPath, target, BuildOptions.None);
    }

    private static string[] GetScenes()
    {
        List<string> s = new List<string>();

        for (int i = 0; i < EditorBuildSettings.scenes.Length; i++)
        {
            if (EditorBuildSettings.scenes[i].enabled)
            {
                s.Add(EditorBuildSettings.scenes[i].path);
            }
        }

        return s.ToArray();
    }
    
    private static string GetAppName(string s)
    {
        int position = s.LastIndexOf('.');
        if (position > -1)
            s = s.Substring(position + 1);
        return s;
    }

    private static void ClearBuildPath(string path)
    {
        if (Directory.Exists(path))
        {
            Directory.Delete(path, true);
        }

        Directory.CreateDirectory(path);
    }

    private static void PrepAndroid(string keyaliasP, string keystoreP, bool useObb)
    {
        PlayerSettings.keyaliasPass = keyaliasP;
        PlayerSettings.keystorePass = keystoreP;
        PlayerSettings.Android.useAPKExpansionFiles = useObb;
    }
}