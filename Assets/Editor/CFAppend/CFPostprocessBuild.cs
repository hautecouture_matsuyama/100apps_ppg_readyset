﻿#if UNITY_IOS
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System;
using System.Collections.Generic;
using System.Xml;
using System.Linq;
using System.IO;
using System.Diagnostics;
using UnityEditor.iOS.Xcode;

public class CFPostprocessBuild : MonoBehaviour {

	[PostProcessBuild]
	public static void ChangeXcodePlist(BuildTarget buildTarget, string pathToBuiltProject) {

		if (buildTarget == BuildTarget.iOS) {

			// Get plist
			string plistPath = pathToBuiltProject + "/Info.plist";
			PlistDocument plist = new PlistDocument();
			plist.ReadFromString(File.ReadAllText(plistPath));

			// Get root
			PlistElementDict rootDict = plist.root;

			// Status Bar
			var statusKey = "UIViewControllerBasedStatusBarAppearance";
			rootDict.SetBoolean(statusKey, false);
			// Calendar access desc
			var calAccessDesc = "NSCalendarsUsageDescription";
			rootDict.SetString(calAccessDesc, "The app needs access for ads to work properly.");
			// Export Compliance
			var exportCompliance = "ITSAppUsesNonExemptEncryption";
			rootDict.SetBoolean(exportCompliance, false);

			// Write to file
			File.WriteAllText(plistPath, plist.WriteToString());

			UnityEngine.Debug.Log( "CF Append post processor completed successfully" );
		}
	}
}
#endif