﻿using UnityEngine;
using UnityEditor;
using System.Linq;
using DG.Tweening;

public class FMReplacer : EditorWindow
{
	[MenuItem("Examples/Update FM")]
	static void UpdateFM()
	{
		var fms = PlayerEquipsManager.FriendlyMonsters;
		foreach(var fm in fms) {
			foreach(var item in fm.Value) {
				var path = @"Prefabs/FriendlyMonsters/" + item.Name;
				var fmPrefab = Resources.Load<GameObject> (path);
				var copy = Instantiate(fmPrefab);
				var fmImage = Instantiate(copy);
				fmImage.name = fmImage.name.Replace("(Clone)","");
				Object.DestroyImmediate(fmImage.transform.GetChild(0).gameObject);
				fmImage.transform.SetParent(copy.transform, false);
				fmImage.transform.SetAsFirstSibling();
				DestroyImmediate(copy.GetComponent<SpriteRenderer>());
				DestroyImmediate(copy.GetComponent<DOTweenAnimation>());
				DestroyImmediate(fmImage.GetComponent<BBaseFriendlyMonster>());
				PrefabUtility.CreatePrefab(string.Format("Assets/Resources/{0}.prefab", path), copy);
				DestroyImmediate(copy);
			}
		}
	}
}