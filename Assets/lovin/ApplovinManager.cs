﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ApplovinManager : MonoBehaviour {

	public static ApplovinManager Instance
	{
		get
		{
			return instance;    
		}
	}

	private static ApplovinManager instance;

	void Awake()
	{
		if (instance == null) {
			instance = this;
			DontDestroyOnLoad (this);

		} 
		else{
			Destroy (this.gameObject);
		}
			

		Initialise ();
	}

	private void Initialise()
	{
		AppLovin.InitializeSdk ();
		AppLovin.SetSdkKey ("RDmulElkN2DASsd9dhi9QWRuiEQ4qKaGTK9HTu0xxD5N3kWhPErrmGiSU7XJS6Z0RiI-bIvWV_vZiQF7e5zOLf");

		AppLovin.LoadRewardedInterstitial ();
	}

	public void ShowReword()
	{
		AppLovin.SetUnityAdListener (this.gameObject.name);

		if(AppLovin.IsIncentInterstitialReady()){
			AppLovin.ShowRewardedInterstitial ();
		}
	}


	void onAppLovinEventReceived(string ev) {
		if(ev.Contains("REWARDAPPROVEDINFO")) {
			Debug.Log ("よばれた");
			// 動画が再生された
		} else if(ev.Contains("LOADEDREWARDED")) {
			// 読み込み完了
		} else if(ev.Contains("LOADREWARDEDFAILED")) {  
			// 読み込み失敗
		} else if(ev.Contains("HIDDENREWARDED")) {
			//動画の表示し終わり、非表示にされた
			Debug.Log ("をわｒ");

			BattleManager.OnReviveAdWatched();
			// 次の動画の準備
			AppLovin.LoadRewardedInterstitial();
		}
	}
				
	public void Debugs(){

		Debug.Log ("ヨバレイa");
	}


}
