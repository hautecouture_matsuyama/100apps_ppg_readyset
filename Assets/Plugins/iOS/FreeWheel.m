#import "FreeWheel.h"

@interface FreeWheel() { }

- (void) initAdManager;
- (void) createVideoSuperview;
- (void) handleAdsRequestCompleted:(NSNotification *)notification;
- (void) handleSlotPreload:(NSNotification *)notification;
- (void) handleSlotEnd:(NSNotification *)notification;
- (void) handleAdError:(NSNotification *)notification;
- (void) playPrerolls;

@end

@implementation FreeWheel

@synthesize adManager;
@synthesize adContext;
@synthesize moviePlayerSuperview;
@synthesize videoContainer;
@synthesize moviePlayerController;
@synthesize temporalSlots;

@synthesize fwCallbackGameObjectName;
@synthesize fwCallbackMethod;
@synthesize fwServerURL;
@synthesize fwNetworkID;
@synthesize fwProfile;
@synthesize fwSiteSection;
@synthesize fwVideoAssetID;

@synthesize kRequestStarted;
@synthesize kRequestEnded;
@synthesize kRequestFailed;
@synthesize kShowStarted;
@synthesize kShowEnded;
@synthesize kError;
@synthesize kSlotLoaded;

static FreeWheel *singleton = nil;

+ (FreeWheel*) createInstance {
    singleton = [[self alloc] init];
    return singleton;
}

+ (FreeWheel*) instance {
    return singleton;
}

- (FreeWheel*) init {
    if (self = [super init]) {
        self.temporalSlots = [NSMutableArray array];
        waitingRequestResult = false;
        [self createVideoSuperview];
        fwCallbackGameObjectName = NULL;
        fwCallbackMethod = NULL;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleSlotPreload:)
                                                     name:FWSlotPreloadedNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleAdError:)
                                                     name:FWAdErrorEvent
                                                   object:nil];
        
        
        kRequestStarted = "REQUEST_STARTED";
        kRequestEnded = "REQUEST_ENDED";
        kRequestFailed = "REQUEST_FAILED";
        kSlotLoaded = "SLOT_LOADED";
        kShowStarted = "SHOW_STARTED";
        kShowEnded = "SHOW_ENDED";
        kError = "ERROR";
    }
    
    return self;
}

- (void) initAdManager {
    adManager = newAdManager();
    // contact FreeWheel Integration Support Engineer about configuration of ad network/serverUrl/...
    //set networkId of the customer MRM network
    [adManager setNetworkId:[fwNetworkID integerValue]];
    //set the ad server URL to which all ad requests will go
    [adManager setServerUrl:fwServerURL];
}

- (void) createVideoSuperview {
    // self.view.backgroundColor = [UIColor whiteColor];
    // self.view.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    self.moviePlayerSuperview = [[UIView alloc] initWithFrame:FWPLAYER_MOVIE_PLAYER_SUPERVIEW_FRAME];
    moviePlayerSuperview.backgroundColor = [UIColor blackColor];
    // [self.view addSubview:moviePlayerSuperview];
}

- (void)dealloc {
    if (fwCallbackMethod != NULL) {
        free(*fwCallbackMethod);
        fwCallbackMethod = NULL;
    }
    if (fwCallbackGameObjectName != NULL) {
        free(*fwCallbackGameObjectName);
        fwCallbackGameObjectName = NULL;
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super dealloc];
}

- (void) requestAds {
    UnitySendMessage(fwCallbackGameObjectName, fwCallbackMethod, kRequestStarted);
    if (!waitingRequestResult) {
        NSLog(@"isnt waiting; will make ad request");
        //Set current viewController object to AdManager if you are also using AdMob Renderer or Millennial Renderer.
        //[adManager setCurrentViewController:self];
        
        //create new adContext for each playback; the adContext object is used to gather info for ad request, send ad request to ad server,
        //parse ad info from ad response and take controll of all ads play
        adContext = [adManager newContext];
        
        //contact FreeWheel Integration Support Engineer about configuration of profiles/slots/keyvalue for your integration.
        [adContext setPlayerProfile:fwProfile
         defaultTemporalSlotProfile:nil
      defaultVideoPlayerSlotProfile:nil
      defaultSiteSectionSlotProfile:nil];
        
        [adContext setSiteSectionId:fwSiteSection
                             idType:FWIdTypeCustom
                     pageViewRandom:0
                          networkId:0
                         fallbackId:0];
        
        [adContext setVideoAssetId:fwVideoAssetID
                            idType:FWIdTypeCustom
                          duration:160
                      durationType:FWVideoAssetDurationTypeExact
                          location:nil
                      autoPlayType:false // setting this to true generated a infinite loop somewhere, somehow
                   videoPlayRandom:0
                         networkId:0
                        fallbackId:0];
        
        //tell the ad context on which base view object to render video ads
        //[adContext setVideoDisplayBase:moviePlayerSuperview];
        [adContext setVideoDisplayBase:UnityGetGLView()];
        
        //regist callback handler for ad request complete event
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleAdsRequestCompleted:) name:FWRequestCompleteNotification object:adContext];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleSlotEnd:) name:FWSlotEndedNotification object:adContext];
        //submit ad request to the Freewheel ad server
        waitingRequestResult = true;
        [adContext submitRequestWithTimeout:FWPLAYER_AD_REQUEST_TIMEOUT];
    }
}

- (void)handleAdsRequestCompleted:(NSNotification *)notification {
    NSLog(@"FreeWheel: handle requests");
    UnitySendMessage(fwCallbackGameObjectName, fwCallbackMethod, kRequestEnded);
    BOOL requestWasSuccesfull = ![[notification userInfo] objectForKey:FWInfoKeyError];
    
    if (requestWasSuccesfull) {
        NSLog(@"FreeWheel: request succesfull");
        preloadedSlotCount = 0;
        
        [temporalSlots addObjectsFromArray:[adContext getSlotsByTimePositionClass:FWTimePositionClassPreroll]];
        
        for (id<FWSlot> slot in temporalSlots) {
            [slot preload];
        }
        NSLog(@"slot count %lu", (unsigned long)[temporalSlots count]);
    } else {
        waitingRequestResult = false;
        NSLog(@"FreeWheel: request ad failed: %@", [[notification userInfo] objectForKey:FWInfoKeyError]);
        UnitySendMessage(fwCallbackGameObjectName, fwCallbackMethod, kRequestFailed);
        //[self cleanup];
    }
}

- (void) handleAdError:(NSNotification *)notification {
    NSLog(@"FreeWheel: Error occurred, aborting!");
    waitingRequestResult = false;
    UnitySendMessage(fwCallbackGameObjectName, fwCallbackMethod, kShowEnded);
    [self cleanup];
}

- (void) handleSlotPreload:(NSNotification *)notification {
    ++preloadedSlotCount;
    waitingRequestResult = false;
    if (preloadedSlotCount == 1) {
        UnitySendMessage(fwCallbackGameObjectName, fwCallbackMethod, kSlotLoaded);
    }
}

- (void) handleSlotEnd:(NSNotification *)notification {
    [temporalSlots removeObjectAtIndex:0];
    --preloadedSlotCount;
    
    if (notification == nil) {
        NSLog(@"invalid notification object, no userInfo found.");
        UnitySendMessage(fwCallbackGameObjectName, fwCallbackMethod, kError);
    }
    if (FWTimePositionClassPreroll == [[adContext getSlotByCustomId:[[notification userInfo] objectForKey:FWInfoKeySlotCustomId]] timePositionClass]) {
        // When the preroll slot ends, play content video.
        [self playPrerolls];
    } else {
        NSLog(@"not a preroll!");
    }
}

- (void) showAds {
    NSLog(@"FreeWheel: will show ads");
    /*
     * Old logic would create a hardcoded frame & movie player and then add as a subview to the Unity view
     * Removed logic, ad is set at the end of "requestads()"
     */
    
    [self playPrerolls];
}

- (void) playPrerolls {
    UnitySendMessage(fwCallbackGameObjectName, fwCallbackMethod, kShowStarted);
    if (temporalSlots.count > 0) {
        NSLog(@"Playing preroll");
        id<FWSlot> slot = [temporalSlots objectAtIndex:0];
        [slot setParameter:FWParameterEnableCountdownTimer withValue:@"YES"];
        [slot play];
    } else {
        UnitySendMessage(fwCallbackGameObjectName, fwCallbackMethod, kShowEnded);
        [self cleanup];
    }
}

- (void) cleanup {
    if (moviePlayerController) {
        [moviePlayerController stop];
        [[moviePlayerController view] removeFromSuperview];
        // [[NSNotificationCenter defaultCenter] removeObserver:self name:nil object:moviePlayerController]; //this is for video content to tell the context that user paused or played
        [self setMoviePlayerController:nil];
    }
    
    [adManager setCurrentViewController:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:nil object:adContext];
    [self setAdContext: nil];
}

- (void) setServerInfo:(NSString *)url :(NSString *)networkID :(NSString *)profile :(NSString *)siteSection :(NSString *)assetID {
    fwServerURL = url;
    fwNetworkID = networkID;
    fwProfile = profile;
    fwSiteSection = siteSection;
    fwVideoAssetID = assetID;
    [self initAdManager];
}

-(void) setCallbackData:(const char *)objName :(const char *)objMsg{
    fwCallbackGameObjectName = cStringCopy(objName);
    fwCallbackMethod = cStringCopy(objMsg);
}

- (BOOL) isReadyToPlayAds {
    return preloadedSlotCount > 0;
    // return [temporalSlots count] > 0;
    
}

@end

char* cStringCopy(const char* string)
{
    if (string == NULL)
        return NULL;
    
    char* res = (char*)malloc(strlen(string) + 1);
    strcpy(res, string);
    
    return res;
}

void _FWInit(const char* gameObjectName, const char* gameObjectMsg){
    [FreeWheel createInstance];
    //[[FreeWheel instance] init];
    [[FreeWheel instance] setCallbackData:gameObjectName :gameObjectMsg];
}

void _FWRequestAds() {
    NSLog(@"request ads");
    [[FreeWheel instance] requestAds];
}

void _FWShowAds() {
    NSLog(@"Show ads");
    [[FreeWheel instance] showAds];
}

BOOL _FWIsReadyToPlayAds() {
    NSLog(@"Show ads");
    return [[FreeWheel instance] isReadyToPlayAds];
}

void _FWSetServerInfo(const char* serverURL, const char* networkID, const char* profile, const char* siteSection, const char* videoAssetID){
    NSString *_serverURL = [NSString stringWithUTF8String:serverURL];
    NSString *_networkID = [NSString stringWithUTF8String:networkID];
    NSString *_profile = [NSString stringWithUTF8String:profile];
    NSString *_siteSection = [NSString stringWithUTF8String:siteSection];
    NSString *_videoAssetID = [NSString stringWithUTF8String:videoAssetID];
    [[FreeWheel instance] setServerInfo:_serverURL :_networkID :_profile :_siteSection :_videoAssetID];
}
