
#import "CheckURLImpl.h"

@implementation CheckURLDelegate

- (id)init
{
    return self;
}
static CheckURLDelegate* delegateObject = nil;


- (BOOL)schemeAvailable:(NSString *)scheme {
  UIApplication *application = [UIApplication sharedApplication];
  NSURL *URL = [NSURL URLWithString:scheme];
  return [application canOpenURL:URL];
}

// When native code plugin is implemented in .mm / .cpp file, then functions
// should be surrounded with extern "C" block to conform C function naming rules
extern "C" {

	BOOL _CheckURL (const char* url)
	{
        if (delegateObject == nil)
            delegateObject = [[CheckURLDelegate alloc] init];
        
        return [delegateObject schemeAvailable:[NSString stringWithUTF8String:url]];
	}
	
	
}

@end
