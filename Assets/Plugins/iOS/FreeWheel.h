#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AdManager/AdManager.h>

#define FWPLAYER_CONTENT_VIDEO_URL [NSURL URLWithString:@""]
#define FWPLAYER_MOVIE_PLAYER_SUPERVIEW_FRAME CGRectMake(10, 1, 270, 165)
#define FWPLAYER_MOVIE_PLAYER_VIEW_FRAME CGRectMake(0, 0, 270, 165)
#define FWPLAYER_AD_REQUEST_TIMEOUT 60

#define XRETAIN // internal macro
#define XASSIGN // internal macro

@interface FreeWheel : NSObject {
    XRETAIN UIView *moviePlayerSuperview;
    UIView *videoContainer;
    XRETAIN MPMoviePlayerController *moviePlayerController;
    XRETAIN NSMutableArray *temporalSlots;
    XASSIGN NSTimer *_tickTimer;
    
    XRETAIN id<FWAdManager> adManager;
    XRETAIN id<FWContext> adContext;
    int preloadedSlotCount;
    BOOL waitingRequestResult;
}

@property (nonatomic, strong) id<FWAdManager> adManager;
@property (nonatomic, strong) id<FWContext> adContext;

@property (nonatomic, strong) UIView *moviePlayerSuperview;
@property (nonatomic, strong) MPMoviePlayerController *moviePlayerController;
@property (nonatomic, strong) NSMutableArray *temporalSlots;
@property (nonatomic, strong) UIView *videoContainer;

@property (nonatomic) const char *fwCallbackGameObjectName;
@property (nonatomic) const char *fwCallbackMethod;
@property (nonatomic, strong) NSString *fwServerURL;
@property (nonatomic, strong) NSString *fwNetworkID;
@property (nonatomic, strong) NSString *fwProfile;
@property (nonatomic, strong) NSString *fwSiteSection;
@property (nonatomic, strong) NSString *fwVideoAssetID;

@property (nonatomic) const char *kRequestStarted;
@property (nonatomic) const char *kRequestEnded;
@property (nonatomic) const char *kRequestFailed;
@property (nonatomic) const char *kShowStarted;
@property (nonatomic) const char *kShowEnded;
@property (nonatomic) const char *kError;
@property (nonatomic) const char *kSlotLoaded;


+ (FreeWheel*) createInstance;
+ (FreeWheel*) instance;
- (FreeWheel*) init;
- (void) requestAds;
- (void) showAds;
- (void) setServerInfo:(NSString *)url :(NSString *)networkID :(NSString *)profile :(NSString *)siteSection :(NSString *)assetID;
- (BOOL) isReadyToPlayAds;
- (void) cleanup;
- (void) dealloc;

@end

char* cStringCopy(const char* string);
void _FWInit(const char* gameObjectName, const char* gameObjectMsg);
void _FWRequestAds();
void _FWShowAds();
void _FWSetServerInfo();
BOOL _FWIsReadyToPlayAds();
void _FWSetServerInfo(const char* serverURL, const char* newtorkID, const char* profile, const char* siteSection, const char* videoAssetID);
