/**
 Constants in FreeWheel AdManager SDK
 */

#import <Foundation/NSObjCRuntime.h>

#ifndef FW_EXTERN
#ifdef __cplusplus
#define FW_EXTERN			extern "C"
#define FW_PRIVATE_EXTERN   FW_EXTERN __attribute__((visibility("hidden")))
#else
#define FW_EXTERN			extern
#define FW_PRIVATE_EXTERN   FW_EXTERN __attribute__((visibility("hidden")))
#endif

#define FW_LINK_RENDERER(r) \
@class r; \
@interface r : NSObject { \
} \
@end; \
extern void FWAdManager_Force_Link_##r (void) __attribute__ ((constructor)); \
void FWAdManager_Force_Link_##r (void) { \
@autoreleasepool{ \
NSLog(@"AdManager: registering renderer class: %@", [r description]); \
} \
}

#endif

#pragma mark - Enums

/**
	Enumeration of FreeWheel log levels
	
	- See: `FWSetLogLevel`
 */
typedef NS_ENUM(NSUInteger, FWLogLevel) {
	/** Log nothing */
	FWLogLevelQuiet = 0,
	/** Log something, default log level */
	FWLogLevelInfo = 3,
	/** Log everything */
	FWLogLevelVerbose = 5,
	/*!
		@internal is used to suppress Jazzy from generating docs for deprecated attributes.
	 */
	FW_LOG_LEVEL_QUIET DEPRECATED_MSG_ATTRIBUTE("Use FWLogLevelQuiet instead") = FWLogLevelQuiet,
	FW_LOG_LEVEL_INFO DEPRECATED_MSG_ATTRIBUTE("Use FWLogLevelInfo instead") = FWLogLevelInfo,
	FW_LOG_LEVEL_VERBOSE DEPRECATED_MSG_ATTRIBUTE("Use FWLogLevelVerbose instead") = FWLogLevelVerbose
};

/**
	Enumeration of non-temporal slot ad initial options
 
	- See: 
		- <a href="Protocols/FWContext.html#/c:objc(pl)FWContext(im)addSiteSectionNonTemporalSlot:adUnit:width:height:slotProfile:acceptCompanion:initialAdOption:acceptPrimaryContentType:acceptContentType:compatibleDimensions:">-addSiteSectionNonTemporalSlot:adUnit:width:height:slotProfile:acceptCompanion:initialAdOption:acceptPrimaryContentType:acceptContentType:compatibleDimensions:</a> of `FWContext`
		- <a href="Protocols/FWContext.html#/c:objc(pl)FWContext(im)addVideoPlayerNonTemporalSlot:adUnit:width:height:slotProfile:acceptCompanion:initialAdOption:acceptPrimaryContentType:acceptContentType:compatibleDimensions:">-addVideoPlayerNonTemporalSlot:adUnit:width:height:slotProfile:acceptCompanion:initialAdOption:acceptPrimaryContentType:acceptContentType:compatibleDimensions:</a> of `FWContext`
 */
typedef NS_ENUM(NSUInteger, FWSlotInitialAdOption) {
	/** Display an ad in the non-temporal slot */
	FWSlotInitialAdOptionStandAlone = 0,
	
	/** Keep the original ad in the non-temporal slot */
	FWSlotInitialAdOptionKeepOriginal = 1,
	
	/** Use the non-temporal ad in the first companion ad package */
	FWSlotInitialAdOptionFirstCompanionOnly = 2,

	/** Either FWSlotInitialAdOptionFirstCompanionOnly or FWSlotInitialAdOptionStandAlone  */
	FWSlotInitialAdOptionFirstCompanionOrStandAlone = 3,

	/** Fill this slot with the first companion ad, or display a new stand alone ad if there is no companion ad */
	FWSlotInitialAdOptionFirstCompanionThenStandAlone = 4,

	/** Fill this slot with the first companion ad, but never deliver a standalone ad if there is no companion ad */
	FWSlotInitialAdOptionFirstCompanionOrNoStandAlone = 5,

	/** Fill this slot only with the PREROLL slot’s companion ad if there is one */
	FWSlotInitialAdOptionNoStandAlone = 6,
	
	/** Fill this slot with a standalone ad only when no temporal ad will be delivered, if there is any temporal ad selected, let this slot stay unfilled */
	FWSlotInitialAdOptionNoStandAloneIfTemporal = 7,
	
	/** Fill this slot with a stand alone ad only when no temporal ad will be delivered, if there is any temporal ad selected, let this slot stay unfilled; however if there is companion, can use this companion to initialize this slot */
	FWSlotInitialAdOptionFirstCompanionOrNoStandAloneIfTemporal = 8,
	
	/*!
	 @internal
	 */
	FW_SLOT_OPTION_INITIAL_AD_STAND_ALONE DEPRECATED_MSG_ATTRIBUTE("Use FWSlotInitialAdOptionStandAlone instead") = FWSlotInitialAdOptionStandAlone,
	FW_SLOT_OPTION_INITIAL_AD_KEEP_ORIGINAL DEPRECATED_MSG_ATTRIBUTE("Use FWSlotInitialAdOptionKeepOriginal instead") =	FWSlotInitialAdOptionKeepOriginal,
	FW_SLOT_OPTION_INITIAL_AD_FIRST_COMPANION_ONLY DEPRECATED_MSG_ATTRIBUTE("Use FWSlotInitialAdOptionFirstCompanionOnly instead") = FWSlotInitialAdOptionFirstCompanionOnly,
	FW_SLOT_OPTION_INITIAL_AD_FIRST_COMPANION_OR_STAND_ALONE DEPRECATED_MSG_ATTRIBUTE("Use FWSlotInitialAdOptionFirstCompanionOrStandAlone instead") = FWSlotInitialAdOptionFirstCompanionOrStandAlone,
	FW_SLOT_OPTION_INITIAL_AD_FIRST_COMPANION_THEN_STAND_ALONE DEPRECATED_MSG_ATTRIBUTE("Use FWSlotInitialAdOptionFirstCompanionThenStandAlone instead") = FWSlotInitialAdOptionFirstCompanionThenStandAlone,
	FW_SLOT_OPTION_INITIAL_AD_FIRST_COMPANION_OR_NO_STAND_ALONE DEPRECATED_MSG_ATTRIBUTE("Use FWSlotInitialAdOptionFirstCompanionOrNoStandAlone instead") = FWSlotInitialAdOptionFirstCompanionOrNoStandAlone,
	FW_SLOT_OPTION_INITIAL_AD_NO_STAND_ALONE DEPRECATED_MSG_ATTRIBUTE("Use FWSlotInitialAdOptionNoStandAlone instead") = FWSlotInitialAdOptionNoStandAlone,
	FW_SLOT_OPTION_INITIAL_AD_NO_STAND_ALONE_IF_TEMPORAL DEPRECATED_MSG_ATTRIBUTE("Use FWSlotInitialAdOptionNoStandAloneIfTemporal instead") = FWSlotInitialAdOptionNoStandAloneIfTemporal,
	FW_SLOT_OPTION_INITIAL_AD_FIRST_COMPANION_OR_NO_STAND_ALONE_IF_TEMPORAL DEPRECATED_MSG_ATTRIBUTE("Use FWSlotInitialAdOptionFirstCompanionOrNoStandAloneIfTemporal instead") = FWSlotInitialAdOptionFirstCompanionOrNoStandAloneIfTemporal
};

/**
	Enumeration of capability status
 
	- See: <a href="Protocols/FWContext.html#/c:objc(pl)FWContext(im)setCapability:status:">-setCapability:status:</a> of `FWContext`
 */
typedef NS_ENUM(NSInteger, FWCapabilityStatus) {
	/** Capability status is off */
	FWCapabilityStatusOff = 0,
	
	/** Capability status is on */
	FWCapabilityStatusOn = 1,
	
	/** Default capability status. The default value can be either on or off for each individual capability. */
	FWCapabilityStatusDefault = -1,
	
	/*!
	 @internal
	 */
	FW_CAPABILITY_STATUS_OFF DEPRECATED_MSG_ATTRIBUTE("Use FWCapabilityStatusOff instead") = FWCapabilityStatusOff,
	FW_CAPABILITY_STATUS_ON DEPRECATED_MSG_ATTRIBUTE("Use FWCapabilityStatusOn instead") = FWCapabilityStatusOn,
	FW_CAPABILITY_STATUS_DEFAULT DEPRECATED_MSG_ATTRIBUTE("Use FWCapabilityStatusDefault instead") = FWCapabilityStatusDefault
};

/**
	Enumeration of id types
 
	- See: 
		- <a href="Protocols/FWContext.html#/c:objc(pl)FWContext(im)setVideoAssetId:idType:duration:durationType:location:autoPlayType:videoPlayRandom:networkId:fallbackId:">-setVideoAssetId:idType:duration:durationType:location:autoPlayType:videoPlayRandom:networkId:fallbackId:</a> of `FWContext`
		- <a href="Protocols/FWContext.html#/c:objc(pl)FWContext(im)setSiteSectionId:idType:pageViewRandom:networkId:fallbackId:">-setSiteSectionId:idType:pageViewRandom:networkId:fallbackId:</a> of `FWContext`
 */
typedef NS_ENUM(NSUInteger, FWIdType) {
	/** Custom id provided by non-FreeWheel parties */
	FWIdTypeCustom = 0,
	
	/** Unique id provided by FreeWheel */
	FWIdTypeFreeWheel = 1,
	
	/** Unique group id provided by FreeWheel */
	FWIdTypeFreeWheelGroup = 2,
	
	/*!
	 @internal
	 */
	FW_ID_TYPE_CUSTOM DEPRECATED_MSG_ATTRIBUTE("Use FWIdTypeCustom instead") = FWIdTypeCustom,
	FW_ID_TYPE_FW DEPRECATED_MSG_ATTRIBUTE("Use FWIdTypeFreeWheel instead") = FWIdTypeFreeWheel,
	FW_ID_TYPE_FWGROUP DEPRECATED_MSG_ATTRIBUTE("Use FWIdTypeFreeWheelGroup") =	FWIdTypeFreeWheelGroup
};

/**
	Enumeration of current video states
 
	- See: <a href="Protocols/FWContext.html#/c:objc(pl)FWContext(im)setVideoState:">-setVideoState:</a> of `FWContext`
 */
typedef NS_ENUM(NSUInteger, FWVideoState) {
	/** Current video is playing */
	FWVideoStatePlaying = 1,
	
	/** Current video is paused */
	FWVideoStatePaused = 2,
	
	/** Current video is stopped */
	FWVideoStateStopped = 3,
	
	/** Current video is completed */
	FWVideoStateCompleted = 4,
	
	/*!
	 @internal
	 */
	FW_VIDEO_STATE_PLAYING DEPRECATED_MSG_ATTRIBUTE("Use FWVideoStatePlaying instead") = FWVideoStatePlaying,
	FW_VIDEO_STATE_PAUSED DEPRECATED_MSG_ATTRIBUTE("Use FWVideoStatePaused instead") = FWVideoStatePaused,
	FW_VIDEO_STATE_STOPPED DEPRECATED_MSG_ATTRIBUTE("Use FWVideoStateStopped instead") = FWVideoStateStopped,
	FW_VIDEO_STATE_COMPLETED DEPRECATED_MSG_ATTRIBUTE("Use FWVideoStateCompleted instead") = FWVideoStateCompleted
};

/**
	Enumeration of time position classes
 
	- See:
		- <a href="Protocols/FWContext.html#/c:objc(pl)FWContext(im)getSlotsByTimePositionClass:">-getSlotsByTimePositionClass:</a> of `FWContext`
		- <a href="Protocols/FWSlot.html#/c:objc(pl)FWSlot(im)timePositionClass">-timePositionClass</a> of `FWSlot`
 */
typedef NS_ENUM(NSUInteger, FWTimePositionClass) {
	/** Time position class type: preroll */
	FWTimePositionClassPreroll = 1,
	
	/** Time position class type: midroll */
	FWTimePositionClassMidroll = 2,
	
	/** Time position class type: postroll */
	FWTimePositionClassPostroll = 3,
	
	/** Time position class type: overlay */
	FWTimePositionClassOverlay = 4,
	
	/** Time position class type: display */
	FWTimePositionClassDisplay = 5,
	
	/** Time position class type: pause_midroll */
	FWTimePositionClassPauseMidroll = 6,
	
	/*!
	 @internal
	 */
	FW_TIME_POSITION_CLASS_PREROLL DEPRECATED_MSG_ATTRIBUTE("Use FWTimePositionClassPreroll instead") =	FWTimePositionClassPreroll,
	FW_TIME_POSITION_CLASS_MIDROLL DEPRECATED_MSG_ATTRIBUTE("Use FWTimePositionClassMidroll instead") =	FWTimePositionClassMidroll,
	FW_TIME_POSITION_CLASS_POSTROLL DEPRECATED_MSG_ATTRIBUTE("Use FWTimePositionClassPostroll instead") =	FWTimePositionClassPostroll,
	FW_TIME_POSITION_CLASS_OVERLAY DEPRECATED_MSG_ATTRIBUTE("Use FWTimePositionClassOverlay instead") =	FWTimePositionClassOverlay,
	FW_TIME_POSITION_CLASS_DISPLAY DEPRECATED_MSG_ATTRIBUTE("Use FWTimePositionClassDisplay instead") =	FWTimePositionClassDisplay,
	FW_TIME_POSITION_CLASS_PAUSE_MIDROLL DEPRECATED_MSG_ATTRIBUTE("Use FWTimePositionClassPauseMidroll instead") = 	FWTimePositionClassPauseMidroll
};

/**
	Enumeration of slot types
 
	- See: <a href="Protocols/FWSlot.html#/c:objc(pl)FWSlot(im)type">-type</a> of `FWSlot`
 */
typedef NS_ENUM(NSUInteger, FWSlotType) {
	/** Type of slot: temporal slot */
	FWSlotTypeTemporal = 1,
	
	/** Type of slot: non-temporal slot in video player */
	FWSlotTypeVideoPlayerNonTemporal = 2,
	
	/** Type of slot: non-temporal slot in site section */
	FWSlotTypeSiteSectionNonTemporal = 3,
	
	/*!
	 @internal
	 */
	FW_SLOT_TYPE_TEMPORAL DEPRECATED_MSG_ATTRIBUTE("Use FWSlotTypeTemporal instead") =	FWSlotTypeTemporal,
	FW_SLOT_TYPE_VIDEOPLAYER_NONTEMPORAL DEPRECATED_MSG_ATTRIBUTE("Use FWSlotTypeVideoPlayerNonTemporal instead") =	FWSlotTypeVideoPlayerNonTemporal,
	FW_SLOT_TYPE_SITESECTION_NONTEMPORAL DEPRECATED_MSG_ATTRIBUTE("Use FWSlotTypeSiteSectionNonTemporal instead") =	FWSlotTypeSiteSectionNonTemporal
};

/**
	Enumeration of parameter level
 
	- See: <a href="Protocols/FWContext.html#/c:objc(pl)FWContext(im)setParameter:withValue:forLevel:">-setParameter:withValue:forLevel:</a> of `FWContext`
 */
typedef NS_ENUM(NSUInteger, FWParameterLevel) {
	/** profile level param */
	FWParameterLevelProfile = 0,
	
	/** global level param */
	FWParameterLevelGlobal = 1,
	
	/** slot level param */
	FWParameterLevelSlot = 2,
	
	/** creative level param */
	FWParameterLevelCreative = 3,
	
	/** rendition level param */
	FWParameterLevelRendition = 4,
	
	/** override level param, highest priority */
	FWParameterLevelOverride = 5,
	
	/*!
	 @internal
	 */
	FW_PARAMETER_LEVEL_PROFILE DEPRECATED_MSG_ATTRIBUTE("Use FWParameterLevelProfile instead") = FWParameterLevelProfile,
	FW_PARAMETER_LEVEL_GLOBAL DEPRECATED_MSG_ATTRIBUTE("Use FWParameterLevelGlobal instead") = FWParameterLevelGlobal,
	FW_PARAMETER_LEVEL_SLOT DEPRECATED_MSG_ATTRIBUTE("Use FWParameterLevelSlot instead") = FWParameterLevelSlot,
	FW_PARAMETER_LEVEL_CREATIVE DEPRECATED_MSG_ATTRIBUTE("Use FWParameterLevelCreative instead") = FWParameterLevelCreative,
	FW_PARAMETER_LEVEL_RENDITION DEPRECATED_MSG_ATTRIBUTE("Use FWParameterLevelRendition instead") = FWParameterLevelRendition,
	FW_PARAMETER_LEVEL_OVERRIDE DEPRECATED_MSG_ATTRIBUTE("Use FWParameterLevelOverride instead") = FWParameterLevelOverride
};

/**
	Enumeration of FWRendererState types
 
	- See: <a href="Protocols/FWRendererController.html#/c:objc(pl)FWRendererController(im)handleStateTransition:info:">-handleStateTransition:info:</a> of `FWRendererController`
 */
typedef NS_ENUM(NSUInteger, FWRendererStateType) {
	/** Renderer State: Preloaded. Renderer should transit to this state as soon as it finishes preloading. */
	FWRendererStatePreloaded = 2,
	/** Renderer State: Started. Renderer should transit to this state as soon as it starts. */
	FWRendererStateStarted = 3,
	/** Renderer State: Completed. Renderer should transit to this state when it has completed all its workflow and ready to be destroyed. */
	FWRendererStateCompleted = 5,
	/** Renderer State: Failed. Renderer should transit to this state when the workflow is interrupted due to some errors. */
	FWRendererStateFailed = 6,
	
	/*!
	 @internal
	 */
	FW_RENDERER_STATE_PRELOADED DEPRECATED_MSG_ATTRIBUTE("Use FWRendererStatePreloaded instead") = FWRendererStatePreloaded,
	FW_RENDERER_STATE_STARTED DEPRECATED_MSG_ATTRIBUTE("Use FWRendererStateStarted instead") = FWRendererStateStarted,
	FW_RENDERER_STATE_COMPLETED DEPRECATED_MSG_ATTRIBUTE("Use FWRendererStateCompleted instead") = FWRendererStateCompleted,
	FW_RENDERER_STATE_FAILED DEPRECATED_MSG_ATTRIBUTE("Use FWRendererStateFailed instead") = FWRendererStateFailed
};

/**
	Enumeration of RequestMode types
 
	- See: <a href="Protocols/FWContext.html#/c:objc(pl)FWContext(im)setRequestMode:">-setRequestMode:</a> of `FWContext`
 */
typedef NS_ENUM(NSUInteger, FWRequestMode) {
	/** Request Mode: On demand */
	FWRequestModeOnDemand,
	/** Request Mode: Live */
	FWRequestModeLive,
	
	/*!
	 @internal
	 */
	FW_REQUEST_MODE_ON_DEMAND DEPRECATED_MSG_ATTRIBUTE("Use FWRequestModeOnDemand instead") = FWRequestModeOnDemand,
	FW_REQUEST_MODE_LIVE DEPRECATED_MSG_ATTRIBUTE("Use FWRequestModeLive instead") = FWRequestModeLive
};

/**
	Enumeration of video asset duration types. 
 
	- See: <a href="Protocols/FWContext.html#/c:objc(pl)FWContext(im)setVideoAssetId:idType:duration:durationType:location:autoPlayType:videoPlayRandom:networkId:fallbackId:">-setVideoAssetId:idType:duration:durationType:location:autoPlayType:videoPlayRandom:networkId:fallbackId:</a> of `FWContext`
 */
typedef NS_ENUM(NSUInteger, FWVideoAssetDurationType) {
	/** Video asset duration type: Exact. This value should be used for video asset whose exact duration is known. */
	FWVideoAssetDurationTypeExact,
	
	/** Video asset duration type: Variable. This value should be used for live stream video asset whose exact duration is not available. */
	FWVideoAssetDurationTypeVariable,
	
	/*!
	 @internal
	 */
	FW_VIDEO_ASSET_DURATION_TYPE_EXACT DEPRECATED_MSG_ATTRIBUTE("Use FWVideoAssetDurationTypeExact instead") = FWVideoAssetDurationTypeExact,
	FW_VIDEO_ASSET_DURATION_TYPE_VARIABLE DEPRECATED_MSG_ATTRIBUTE("Use FWVideoAssetDurationTypeVariable instead") = FWVideoAssetDurationTypeVariable
};

/**
	Enumeration of video asset auto play types
 
	- See: <a href="Protocols/FWContext.html#/c:objc(pl)FWContext(im)setVideoAssetId:idType:duration:durationType:location:autoPlayType:videoPlayRandom:networkId:fallbackId:">-setVideoAssetId:idType:duration:durationType:location:autoPlayType:videoPlayRandom:networkId:fallbackId:</a> of `FWContext`
 */
typedef NS_ENUM(NSUInteger, FWVideoAssetAutoPlayType) {
	/** Video asset auto play type: None */
	FWVideoAssetAutoPlayTypeNone = 0,
	
	/** Video asset auto play type: Attended*/
	FWVideoAssetAutoPlayTypeAttended = 1,
	
	/** Video asset auto play type: Unattended */
	FWVideoAssetAutoPlayTypeUnattended = 2,
	
	/*!
	 @internal
	 */
	FW_VIDEO_ASSET_AUTO_PLAY_TYPE_NONE DEPRECATED_MSG_ATTRIBUTE("Use FWVideoAssetAutoPlayTypeNone instead") = FWVideoAssetAutoPlayTypeNone,
	FW_VIDEO_ASSET_AUTO_PLAY_TYPE_ATTENDED DEPRECATED_MSG_ATTRIBUTE("Use FWVideoAssetAutoPlayTypeAttended instead") = FWVideoAssetAutoPlayTypeAttended,
	FW_VIDEO_ASSET_AUTO_PLAY_TYPE_UNATTENDED DEPRECATED_MSG_ATTRIBUTE("Use FWVideoAssetAutoPlayTypeUnattended instead") = FWVideoAssetAutoPlayTypeUnattended
};

/**
	Enumeration of user actions. 
 
	- See: <a href="Protocols/FWContext.html#/c:objc(pl)FWContext(im)notifyUserAction:">-notifyUserAction:</a> of `FWContext`
 */
typedef NS_ENUM(NSUInteger, FWUserAction) {
	/** User action: video player's pause button is clicked */
	FWUserActionPauseButtonClicked,
	
	/** User action: video player's resume button is clicked */
	FWUserActionResumeButtonClicked,

	/** User action: airplay is activated */
	FWUserActionActivateAirPlay,

	/** User action: airplay is deactivated */
	FWUserActionDeactivateAirPlay,
	
	/*!
	 @internal
	 */
	FW_USER_ACTION_PAUSE_BUTTON_CLICKED DEPRECATED_MSG_ATTRIBUTE("Use FWUserActionPauseButtonClicked instead") = FWUserActionPauseButtonClicked,
	FW_USER_ACTION_RESUME_BUTTON_CLICKED DEPRECATED_MSG_ATTRIBUTE("Use FWUserActionResumeButtonClicked instead") = FWUserActionResumeButtonClicked
};

/**
	Enumeration of module types.
 */
typedef NS_ENUM(NSUInteger, FWModuleType) {
	/** Module type: renderer */
	FWModuleTypeRenderer,
	/** Module type: translator */
	FWModuleTypeTranslator
};

FW_EXTERN NSString *const FW_MODULE_TYPE_RENDERER DEPRECATED_MSG_ATTRIBUTE("Use FWModuleTypeRenderer instead");
FW_EXTERN NSString *const FW_MODULE_TYPE_TRANSLATOR DEPRECATED_MSG_ATTRIBUTE("Use FWModuleTypeTranslator instead");

#pragma mark - Notifications

/**
	Notification broadcast when ad request has been initiated.
 
 */
FW_EXTERN NSString *const FWRequestInitiatedNotification;

/**
	Notification broadcast when ad request has completed.
	`object` is the FWContext instance used to send the request;

	Check `userInfo` with `FWInfoKeyError` for errors; will get `nil` if request has been successful.
 */
FW_EXTERN NSString *const FWRequestCompleteNotification;
FW_EXTERN NSString *const FW_NOTIFICATION_REQUEST_COMPLETE DEPRECATED_MSG_ATTRIBUTE("Use FWRequestCompleteNotification instead");

/**
	Notification broadcast when a slot has finished preloading.
	Check `userInfo` with `FWInfoKeySlotCustomId` for the slot's custom ID.
 
	- SeeAlso: 
		- `FWSlotStartedNotification`
		- `FWSlotEndedNotification`
 */
FW_EXTERN NSString *const FWSlotPreloadedNotification;
FW_EXTERN NSString *const FW_NOTIFICATION_SLOT_PRELOADED DEPRECATED_MSG_ATTRIBUTE("Use FWSlotPreloadedNotification instead");

/**
	Notification broadcast when a slot has started.
	Check `userInfo` with `FWInfoKeySlotCustomId` for the slot's custom ID.
 
	- SeeAlso:
		- `FWSlotPreloadedNotification`
		- `FWSlotEndedNotification`
 */
FW_EXTERN NSString *const FWSlotStartedNotification;
FW_EXTERN NSString *const FW_NOTIFICATION_SLOT_STARTED DEPRECATED_MSG_ATTRIBUTE("Use FWSlotStartedNotification instead");

/**
 	Notification broadcast when a slot has ended.
 	Check userInfo with FWInfoKeySlotCustomId for the slot's custom ID.

	- SeeAlso:
		- `FWSlotStartedNotification`
		- `FWSlotPreloadedNotification`
 */
FW_EXTERN NSString *const FWSlotEndedNotification;
FW_EXTERN NSString *const FW_NOTIFICATION_SLOT_ENDED DEPRECATED_MSG_ATTRIBUTE("Use FWSlotEndedNotification instead");

/**
	Notification broadcast when inAppView is opened.
 
	- SeeAlso:
		- `FWInAppViewDidCloseNotification`
		- `FWInAppViewWillOpenMediaDocumentNotification`
 */
FW_EXTERN NSString *const FWInAppViewDidOpenNotification;
FW_EXTERN NSString *const FW_NOTIFICATION_IN_APP_VIEW_OPEN DEPRECATED_MSG_ATTRIBUTE("Use FWInAppViewDidOpenNotification instead");

/**
	Notification broadcast when inAppView is closed.

	- SeeAlso: 
		- `FWInAppViewDidOpenNotification`
		- `FWInAppViewWillOpenMediaDocumentNotification`
 */
FW_EXTERN NSString *const FWInAppViewDidCloseNotification;
FW_EXTERN NSString *const FW_NOTIFICATION_IN_APP_VIEW_CLOSE DEPRECATED_MSG_ATTRIBUTE("Use FWInAppViewDidCloseNotification instead");

/**
	Notification broadcast when inAppView will open a media document from URI.

	- SeeAlso:
		- `FWInAppViewDidOpenNotification`
		- `FWInAppViewDidCloseNotification`
 */
FW_EXTERN NSString *const FWInAppViewWillOpenMediaDocumentNotification;
FW_EXTERN NSString *const FW_NOTIFICATION_IN_APP_VIEW_WILL_OPEN_MEDIA_DOCUMENT DEPRECATED_MSG_ATTRIBUTE("Use FWInAppViewWillOpenMediaDocumentNotification instead");

/**
	Notification broadcast when AdManager needs your content video player to pause.
	It could be due to a midroll slot being about to start, or when the user taps on an ad that needs to interrupt the main video.

	You can query `userInfo` with `FWInfoKeySlotCustomId` to find out the slot that has caused the interruption.

	- SeeAlso: `FWContentResumeRequestNotification`
 */
FW_EXTERN NSString *const FWContentPauseRequestNotification;
FW_EXTERN NSString *const FW_NOTIFICATION_CONTENT_PAUSE_REQUEST DEPRECATED_MSG_ATTRIBUTE("Use FWContentPauseRequestNotification instead");

/**
	Notification broadcast when AdManager needs your content video player to resume.
	It could be due to a midroll slot has ended, or an interruptive ad has been closed.
 
	You can query `userInfo` with `FWInfoKeySlotCustomId` to find out the slot that has caused the interruption.
 
	- SeeAlso: `FWContentPauseRequestNotification`
*/
FW_EXTERN NSString *const FWContentResumeRequestNotification;
FW_EXTERN NSString *const FW_NOTIFICATION_CONTENT_RESUME_REQUEST DEPRECATED_MSG_ATTRIBUTE("Use FWContentResumeRequestNotification instead");

FW_EXTERN NSString *const FW_NOTIFICATION_AD_IMPRESSION DEPRECATED_MSG_ATTRIBUTE("Use FWAdEventNotification instead");

FW_EXTERN NSString *const FW_NOTIFICATION_AD_IMPRESSION_END DEPRECATED_MSG_ATTRIBUTE("Use FWAdEventNotification instead");

FW_EXTERN NSString *const FW_NOTIFICATION_AD_ERROR DEPRECATED_MSG_ATTRIBUTE("Use FWAdEventNotification instead");

/**
	Notification broadcast when a user action occurs.
	Query `userInfo` with `FW_INFO_KEY_USER_ACTION` to find out the user action type.
 
	- SeeAlso: `FWUserAction`
*/
FW_EXTERN NSString *const FWUserActionNotification;
FW_EXTERN NSString *const FW_NOTIFICATION_USER_ACTION_NOTIFIED DEPRECATED_MSG_ATTRIBUTE("Use FWUserActionNotification instead");

FW_EXTERN NSString *const FW_NOTIFICATION_AD_EXPAND_TO_FULLSCREEN DEPRECATED_ATTRIBUTE;
FW_EXTERN NSString *const FW_NOTIFICATION_AD_COLLAPSE_FROM_FULLSCREEN DEPRECATED_ATTRIBUTE;

/*!
 	Deprecated.
	Notification broadcast when a slot starts AirPlaying.
	Query `userInfo` with `FwInfoKeySlot` to find out the corresponding slot.
 
	- SeeAlso: `FWSlotExternalPlaybackDidStopNotification`
 */
FW_EXTERN NSString *const FWSlotExternalPlaybackDidStartNotification DEPRECATED_MSG_ATTRIBUTE("Implement FWVideoAdDelegate with your own AVPlayer instance to support AirPlay");
FW_EXTERN NSString *const FW_NOTIFICATION_SLOT_EXTERNAL_PLAYBACK_STARTED DEPRECATED_MSG_ATTRIBUTE("Implement FWVideoAdDelegate with your own AVPlayer instance to support AirPlay");

/*!
 	Deprecated.
	Notification broadcast when a slot stops AirPlaying.
	Query `userInfo` with `FwInfoKeySlot` to find out the corresponding slot.
 
	 - SeeAlso: `FWSlotExternalPlaybackDidStartNotification`
 */
FW_EXTERN NSString *const FWSlotExternalPlaybackDidStopNotification DEPRECATED_MSG_ATTRIBUTE("Implement FWVideoAdDelegate with your own AVPlayer instance to support AirPlay");
FW_EXTERN NSString *const FW_NOTIFICATION_SLOT_EXTERNAL_PLAYBACK_STOPPED DEPRECATED_MSG_ATTRIBUTE("Implement FWVideoAdDelegate with your own AVPlayer instance to support AirPlay");

/**
	Notification broadcast when an ad event occurs. An ad event will trigger a corresponding event callback request to FreeWheel ad server. For example, when an ad starts, a `FWAdEventNotification` with event name `FWAdImpressionEvent` will be broadcast, the corresponding impression beacon will fire at the same time.

	`userInfo` payloads:

	- `FWInfoKeyAdEventName`: Name of the event, see `FWAd*Event`
	- `FWInfoKeySlot`: Corresponding slot
	- `FWInfoKeyAdInstance`: Corresponding ad instance
 
	- SeeAlso: 
		- <a href="Protocols/FWAdInstance.html#/c:objc(pl)FWAdInstance(im)getEventCallbackUrlsByEventName:eventType:">-getEventCallbackUrlsByEventName:eventType:</a>
		- <a href="Protocols/FWAdInstance.html#/c:objc(pl)FWAdInstance(im)setEventCallbackUrls:forEventName:eventType:">-setEventCallbackUrls:forEventName:eventType:</a>
		- `FWEventType*`
 */
FW_EXTERN NSString *const FWAdEventNotification;
FW_EXTERN NSString *const FW_NOTIFICATION_RENDERER_EVENT DEPRECATED_MSG_ATTRIBUTE("Use FWAdEventNotification instead");

/**
	Notification broadcast when the video display base is changed.
	`userInfo` payload:
		
	- `FWInfoKeyVideoDisplayBase`: the new video display base UIView.
 
	- See: <a href="Protocols/FWContext.html#/c:objc(pl)FWContext(im)setVideoDisplayBase:">-setVideoDisplayBase:</a> of `FWContext`
 */
FW_EXTERN NSString *const FWVideoDisplayBaseDidChangeNotification;
FW_EXTERN NSString *const FW_NOTIFICATION_VIDEO_DISPLAY_BASE_CHANGED DEPRECATED_MSG_ATTRIBUTE("Use FWVideoDisplayBaseDidChangeNotification instead");

/**
	Notification broadcast when the video display's frame has been changed.
	`userInfo` payload:
 
	- `FWInfoKeyVideoDisplayBase`: the new video display base `UIView`.
 */
FW_EXTERN NSString *const FWVideoDisplayBaseFrameDidChangeNotification;
FW_EXTERN NSString *const FW_NOTIFICATION_VIDEO_DISPLAY_BASE_FRAME_CHANGED DEPRECATED_MSG_ATTRIBUTE("Use FWVideoDisplayBaseFrameDidChangeNotification instead");

/**
	Notification broadcast when an extension is loaded.
	If `notification.userInfo` has key `FWInfoKeyError`, the extension has failed to load with an error message.
 */
FW_EXTERN NSString *const FWExtensionLoadedNotification;
FW_EXTERN NSString *const FW_NOTIFICATION_EXTENSION_LOADED DEPRECATED_MSG_ATTRIBUTE("Use FWExtensionLoadedNotification instead");

/**
	Notification broadcast when the user presses Apple Remote's Menu button while an ad is AirPlaying.
	
	- Remark: Player/App should pause the current playing slot with `[slot pause]`, and provide a way to resume the ad playback using `[slot resume]`, for example, draw a play button on top of the paused ad.
 */
FW_EXTERN NSString *const FWExternalPlaybackInterruptedNotification;
FW_EXTERN NSString *const FW_NOTIFICATION_EXTERNALPLAYBACK_INTERRUPTED DEPRECATED_MSG_ATTRIBUTE("Use FWExternalPlaybackInterruptedNotification instead");

#pragma mark - Ad Units

/**
	Ad unit: Preroll
 
	- See: <a href="Protocols/FWContext.html#/c:objc(pl)FWContext(im)addTemporalSlot:adUnit:timePosition:slotProfile:cuePointSequence:minDuration:maxDuration:acceptPrimaryContentType:acceptContentType:">-addTemporalSlot:adUnit:timePosition:slotProfile:cuePointSequence:minDuration:maxDuration:acceptPrimaryContentType:acceptContentType:</a>

	- SeeAlso:
		- `FWAdUnitMidroll`
		- `FWAdUnitPostroll`
		- `FWAdUnitOverlay`
		- `FWAdUnitStreamPreroll`
		- `FWAdUnitStreamPostroll`
		- `FWAdUnitPauseMidroll`
 */
FW_EXTERN NSString *const FWAdUnitPreroll;
FW_EXTERN NSString *const FW_ADUNIT_PREROLL DEPRECATED_MSG_ATTRIBUTE("Use FWAdUnitPreroll instead");

/**
	Ad unit: Midroll

	- SeeAlso:
		- `FWAdUnitPreroll`
		- `FWAdUnitPostroll`
		- `FWAdUnitOverlay`
		- `FWAdUnitStreamPreroll`
		- `FWAdUnitStreamPostroll`
		- `FWAdUnitPauseMidroll`
 */
FW_EXTERN NSString *const FWAdUnitMidroll;
FW_EXTERN NSString *const FW_ADUNIT_MIDROLL DEPRECATED_MSG_ATTRIBUTE("Use FWAdUnitMidroll instead");

/**
	Ad unit: Postroll

	- SeeAlso:
		- `FWAdUnitPreroll`
		- `FWAdUnitMidroll`
		- `FWAdUnitOverlay`
		- `FWAdUnitStreamPreroll`
		- `FWAdUnitStreamPostroll`
		- `FWAdUnitPauseMidroll`
 */
FW_EXTERN NSString *const FWAdUnitPostroll;
FW_EXTERN NSString *const FW_ADUNIT_POSTROLL DEPRECATED_MSG_ATTRIBUTE("Use FWAdUnitPostroll instead");

/**
	Ad unit: Overlay

	- SeeAlso:
		- `FWAdUnitPreroll`
		- `FWAdUnitMidroll`
		- `FWAdUnitPostroll`
		- `FWAdUnitStreamPreroll`
		- `FWAdUnitStreamPostroll`
		- `FWAdUnitPauseMidroll`
 */
FW_EXTERN NSString *const FWAdUnitOverlay;
FW_EXTERN NSString *const FW_ADUNIT_OVERLAY DEPRECATED_MSG_ATTRIBUTE("Use FWAdUnitOverlay instead");


/**
	Ad unit: Pause-Midroll
 
	- SeeAlso:
		- `FWAdUnitPreroll`
		- `FWAdUnitMidroll`
		- `FWAdUnitPostroll`
		- `FWAdUnitOverlay`
		- `FWAdUnitStreamPreroll`
		- `FWAdUnitStreamPostroll`
 */
FW_EXTERN NSString *const FWAdUnitPauseMidroll;
FW_EXTERN NSString *const FW_ADUNIT_PAUSE_MIDROLL DEPRECATED_MSG_ATTRIBUTE("Use FWAdUnitPauseMidroll instead");

/**
	Ad unit: Stream Preroll
 
	- SeeAlso:
		- `FWAdUnitPreroll`
		- `FWAdUnitMidroll`
		- `FWAdUnitPostroll`
		- `FWAdUnitOverlay`
		- `FWAdUnitStreamPostroll`
		- `FWAdUnitPauseMidroll`
 */
FW_EXTERN NSString *const FWAdUnitStreamPreroll;
FW_EXTERN NSString *const FW_ADUNIT_STREAM_PREROLL DEPRECATED_MSG_ATTRIBUTE("Use FWAdUnitStreamPreroll instead");

/**
	Ad unit: Stream Postroll

	- SeeAlso:
		- `FWAdUnitPreroll`
		- `FWAdUnitMidroll`
		- `FWAdUnitPostroll`
		- `FWAdUnitOverlay`
		- `FWAdUnitStreamPreroll`
		- `FWAdUnitPauseMidroll`
 */
FW_EXTERN NSString *const FWAdUnitStreamPostroll;
FW_EXTERN NSString *const FW_ADUNIT_STREAM_POSTROLL DEPRECATED_MSG_ATTRIBUTE("Use FWAdUnitStreamPostroll instead");

#pragma mark - Capabilities

/**
	Capability: Player expects template-based slots generated by ad server.

	- Default: On (`FWCapabilityStatusOn`)
	- See: <a href="Protocols/FWContext.html#/c:objc(pl)FWContext(im)setCapability:status:">-setCapability:status:</a> of `FWContext`
 */
FW_EXTERN NSString *const FWCapabilitySlotTemplate;
FW_EXTERN NSString *const FW_CAPABILITY_SLOT_TEMPLATE DEPRECATED_MSG_ATTRIBUTE("Use FWCapabilitySlotTemplate instead");

/**
	Capability: Ad unit in multiple slots.

	- Default: On (`FWCapabilityStatusOn`)
	- See: <a href="Protocols/FWContext.html#/c:objc(pl)FWContext(im)setCapability:status:">-setCapability:status:</a> of `FWContext`
 */
FW_EXTERN NSString *const FWCapabilityAdUnitInMultipleSlots;
FW_EXTERN NSString *const FW_CAPABILITY_ADUNIT_IN_MULTIPLE_SLOTS DEPRECATED_MSG_ATTRIBUTE("Use FWCapabilityAdUnitInMultipleSlots instead");

/**
	Capability: Bypass commercial ratio restriction.

	- Default: Off (`FWCapabilityStatusOff`)
	- See: <a href="Protocols/FWContext.html#/c:objc(pl)FWContext(im)setCapability:status:">-setCapability:status:</a> of `FWContext`
 */
FW_EXTERN NSString *const FWCapabilityBypassCommercialRatioRestriction;
FW_EXTERN NSString *const FW_CAPABILITY_BYPASS_COMMERCIAL_RATIO_RESTRICTION DEPRECATED_MSG_ATTRIBUTE("Use FWCapabilityBypassCommercialRatioRestriction instead");

/**
	Capability: Player expects ad server to check companion for candidate ads.

	- Default: On (`FWCapabilityStatusOn`)
	- See: <a href="Protocols/FWContext.html#/c:objc(pl)FWContext(im)setCapability:status:">-setCapability:status:</a> of `FWContext`
 */
FW_EXTERN NSString *const FWCapabilityCheckCompanion;
FW_EXTERN NSString *const FW_CAPABILITY_CHECK_COMPANION DEPRECATED_MSG_ATTRIBUTE("Use FWCapabilityCheckCompanion instead");

/**
	Capability: Player expects ad server to check targeting for candidate ads.

	- Default: On (`FWCapabilityStatusOn`)
	- See: <a href="Protocols/FWContext.html#/c:objc(pl)FWContext(im)setCapability:status:">-setCapability:status:</a> of `FWContext`
 */
FW_EXTERN NSString *const FWCapabilityCheckTargeting;
FW_EXTERN NSString *const FW_CAPABILITY_CHECK_TARGETING DEPRECATED_MSG_ATTRIBUTE("Use FWCapabilityCheckTargeting instead");

/**
	Capability: Implicitly record video view.

	- Default: Off (`FWCapabilityStatusOff`)
	- See: <a href="Protocols/FWContext.html#/c:objc(pl)FWContext(im)setCapability:status:">-setCapability:status:</a> of `FWContext`
 */
FW_EXTERN NSString *const FWCapabilityRecordVideoView;
FW_EXTERN NSString *const FW_CAPABILITY_RECORD_VIDEO_VIEW DEPRECATED_MSG_ATTRIBUTE("Use FWCapabilityRecordVideoView instead");

/**
	Capability: Player expects ad server synchronize the request state between multiple requests.

	- Default: Off (`FWCapabilityStatusOff`)
	- See: <a href="Protocols/FWContext.html#/c:objc(pl)FWContext(im)setCapability:status:">-setCapability:status:</a> of `FWContext`
 */
FW_EXTERN NSString *const FWCapabilitySyncMultipleRequests;
FW_EXTERN NSString *const FW_CAPABILITY_SYNC_MULTI_REQUESTS DEPRECATED_MSG_ATTRIBUTE("Use FWCapabilitySyncMultipleRequests instead");

/**
	Capability: Reset the exclusivity scope. Player can turn on/off this capability before making any request.

	- Default: Off (`FWCapabilityStatusOff`)
	- Note: Once the capability is turned on, all subsequent requests will all carry this capability and reset the exclusivity scope until it's turned off again.
	- See: <a href="Protocols/FWContext.html#/c:objc(pl)FWContext(im)setCapability:status:">-setCapability:status:</a> of `FWContext`
 */
FW_EXTERN NSString *const FWCapabilityResetExclusivity;
FW_EXTERN NSString *const FW_CAPABILITY_RESET_EXCLUSIVITY DEPRECATED_MSG_ATTRIBUTE("Use FWCapabilityResetExclusivity instead");

/**
	Capability: Player expects the ad server to return fallback alternatives for every ad delivered if possible.

	- Default: On (`FWCapabilityStatusOn`)
	- See: <a href="Protocols/FWContext.html#/c:objc(pl)FWContext(im)setCapability:status:">-setCapability:status:</a> of `FWContext`
 */
FW_EXTERN NSString *const FWCapabilityFallbackAds;
FW_EXTERN NSString *const FW_CAPABILITY_FALLBACK_ADS DEPRECATED_MSG_ATTRIBUTE("Use FWCapabilityFallbackAds instead");

/**
	Capability: Player expects multiple creative renditions for an ad.
 
	- Default: On (`FWCapabilityStatusOn`)
	- See: <a href="Protocols/FWContext.html#/c:objc(pl)FWContext(im)setCapability:status:">-setCapability:status:</a> of `FWContext`
 */
FW_EXTERN NSString *const FWCapabilityMultipleCreativeRenditions;
FW_EXTERN NSString *const FW_CAPABILITY_MULTIPLE_CREATIVE_RENDITIONS DEPRECATED_MSG_ATTRIBUTE("Use FWCapabilityMultipleCreativeRenditions instead");

#pragma mark - Ad Events

/**
	Ad event: ad impression. Ad starts.
 
	- See: `FWAdEventNotification`
 */
FW_EXTERN NSString *const FWAdImpressionEvent;
FW_EXTERN NSString *const FW_EVENT_AD_IMPRESSION DEPRECATED_MSG_ATTRIBUTE("Use FWAdImpressionEvent instead");

/**
	Ad event: ad end impression. Ad ends.

	- See: `FWAdEventNotification`
 */
FW_EXTERN NSString *const FWAdImpressionEndEvent;
FW_EXTERN NSString *const FW_EVENT_AD_IMPRESSION_END DEPRECATED_MSG_ATTRIBUTE("Use FWAdImpressionEndEvent instead");


FW_EXTERN NSString *const FWAdQuartileEvent;
FW_EXTERN NSString *const FW_EVENT_AD_QUARTILE DEPRECATED_MSG_ATTRIBUTE("Use FWAdQuartileEvent instead");

/**
	Ad event: first quartile. Ad progress reaches 25%.
 
	- See: `FWAdEventNotification`
 */
FW_EXTERN NSString *const FWAdFirstQuartileEvent;
FW_EXTERN NSString *const FW_EVENT_AD_FIRST_QUARTILE DEPRECATED_MSG_ATTRIBUTE("Use FWAdFirstQuartileEvent instead");

/**
	Ad event: mid point. Ad progress reaches 50%.
 
	- See: `FWAdEventNotification`
 */
FW_EXTERN NSString *const FWAdMidpointEvent;
FW_EXTERN NSString *const FW_EVENT_AD_MIDPOINT DEPRECATED_MSG_ATTRIBUTE("Use FWAdMidpointEvent instead");

/**
	Ad event: third quartile. Ad progress reaches 75%.

	- See: `FWAdEventNotification`
 */
FW_EXTERN NSString *const FWAdThirdQuartileEvent;
FW_EXTERN NSString *const FW_EVENT_AD_THIRD_QUARTILE DEPRECATED_MSG_ATTRIBUTE("Use FWAdThirdQuartileEvent instead");

/**
	Ad event: complete. Ad progress reaches 100%.
 
	- See: `FWAdEventNotification`
 */
FW_EXTERN NSString *const FWAdCompleteEvent;
FW_EXTERN NSString *const FW_EVENT_AD_COMPLETE DEPRECATED_MSG_ATTRIBUTE("Use FWAdCompleteEvent instead");

/**
	Ad event: click. Ad is clicked.
 
	- See: `FWAdEventNotification`
 */
FW_EXTERN NSString *const FWAdClickEvent;
FW_EXTERN NSString *const FW_EVENT_AD_CLICK DEPRECATED_MSG_ATTRIBUTE("Use FWAdClickEvent instead");

/**
	Ad event: mute. Ad is muted.
 
	- See: `FWAdEventNotification`
 */
FW_EXTERN NSString *const FWAdMuteEvent;
FW_EXTERN NSString *const FW_EVENT_AD_MUTE DEPRECATED_MSG_ATTRIBUTE("Use FWAdMuteEvent instead");

/**
	Ad event: unmute. Ad is unmuted.
 
	- See: `FWAdEventNotification`
 */
FW_EXTERN NSString *const FWAdUnmuteEvent;
FW_EXTERN NSString *const FW_EVENT_AD_UNMUTE DEPRECATED_MSG_ATTRIBUTE("Use FWAdUnmuteEvent instead");

/**
	Ad event: collapse. Ad is collapsed.
 
	- See: `FWAdEventNotification`
 */
FW_EXTERN NSString *const FWAdCollapseEvent;
FW_EXTERN NSString *const FW_EVENT_AD_COLLAPSE DEPRECATED_MSG_ATTRIBUTE("Use FWAdCollapseEvent instead");

/**
	Ad event: expand. Ad is expanded.
 
	- See: `FWAdEventNotification`
 */
FW_EXTERN NSString *const FWAdExpandEvent;
FW_EXTERN NSString *const FW_EVENT_AD_EXPAND DEPRECATED_MSG_ATTRIBUTE("Use FWAdExpandEvent instead");

/**
	Ad event: pause. Ad is paused.
 
	- See: `FWAdEventNotification`
 */
FW_EXTERN NSString *const FWAdPauseEvent;
FW_EXTERN NSString *const FW_EVENT_AD_PAUSE DEPRECATED_MSG_ATTRIBUTE("Use FWAdPauseEvent instead");

/**
	Ad event: resume. Ad is resumed.
 
	- See: `FWAdEventNotification`
 */
FW_EXTERN NSString *const FWAdResumeEvent;
FW_EXTERN NSString *const FW_EVENT_AD_RESUME DEPRECATED_MSG_ATTRIBUTE("Use FWAdResumeEvent instead");

/**
	Ad event: rewind. Ad is rewound.
 
	- See: `FWAdEventNotification`
 */
FW_EXTERN NSString *const FWAdRewindEvent;
FW_EXTERN NSString *const FW_EVENT_AD_REWIND DEPRECATED_MSG_ATTRIBUTE("Use FWAdRewindEvent instead");

/**
	Ad event: accept invitation. User accepts invitation.
 
	- See: `FWAdEventNotification`
 */
FW_EXTERN NSString *const FWAdAcceptInvitationEvent;
FW_EXTERN NSString *const FW_EVENT_AD_ACCEPT_INVITATION DEPRECATED_MSG_ATTRIBUTE("Use FWAdAcceptInvitationEvent instead");

/**
	Ad event: close. Ad is closed.
 
	- See: `FWAdEventNotification`
 */
FW_EXTERN NSString *const FWAdCloseEvent;
FW_EXTERN NSString *const FW_EVENT_AD_CLOSE DEPRECATED_MSG_ATTRIBUTE("Use FWAdCloseEvent instead");

/**
	Ad event: minimize. Ad is minimized.
 
	- See: `FWAdEventNotification`
 */
FW_EXTERN NSString *const FWAdMinimizeEvent;
FW_EXTERN NSString *const FW_EVENT_AD_MINIMIZE DEPRECATED_MSG_ATTRIBUTE("Use FWAdMinimizeEvent instead");

/**
	Ad event: skipped. Ad has been skipped by the user.
 
	- See: `FWAdEventNotification`
 */
FW_EXTERN NSString *const FWAdSkippedByUserEvent;

/**
	Ad event: reseller no ad. AdManager fails to get any playable ad from a 3rd party reseller.
 
	- See: `FWAdEventNotification`
 */
FW_EXTERN NSString *const FWAdResellerNoAdEvent;
FW_EXTERN NSString *const FW_EVENT_AD_RESELLER_NO_AD DEPRECATED_MSG_ATTRIBUTE("Use FWAdResellerNoAdEvent instead");

/**
	Ad event: an error has occurred. When an error occurs, a `FWAdEventNotification` will be broadcast with the following `userInfo` payload:
	
	- `FWInfoKeyErrorCode`: Error code, see `FWError*`.
	- `FWInfoKeyErrorInfo`: Error info. Brief description of the error.
	- `FWInfoKeyErrorModule`: The module that reported this error.

	- See: `FWAdEventNotification`
 */
FW_EXTERN NSString *const FWAdErrorEvent;
FW_EXTERN NSString *const FW_EVENT_AD_ERROR DEPRECATED_MSG_ATTRIBUTE("Use FWAdErrorEvent instead");

/**
	Ad event: concrete event (for CPx integrations only)
 
	- See: 
		- `FWAdEventNotification`
		- `FWInfoKeyConcreteEventId`
 */
FW_EXTERN NSString *const FWAdMeasurementEvent;
FW_EXTERN NSString *const FW_EVENT_AD_MEASUREMENT DEPRECATED_MSG_ATTRIBUTE("Use FWAdMeasurementEvent instead");

/*!
	internal use only
 */
FW_EXTERN NSString *const FW_EVENT_ERROR;

/**
	Ad event: Ad buffering has started. Player can use this event to start displaying a buffering animation.

	- See:
		- `FWAdEventNotification`
	- SeeAlso:
		- `FWAdBufferingEndEvent`
 */
FW_EXTERN NSString *const FWAdBufferingStartEvent;
FW_EXTERN NSString *const FW_EVENT_AD_BUFFERING_START DEPRECATED_MSG_ATTRIBUTE("Use FWAdBufferingStartEvent instead");

/**
	Ad event: Ad buffering has ended. Player can use this event to remove the buffering animation.

	- See:
		- `FWAdEventNotification`
	- SeeAlso:
		- `FWAdBufferingStartEvent`
 */
FW_EXTERN NSString *const FWAdBufferingEndEvent;
FW_EXTERN NSString *const FW_EVENT_AD_BUFFERING_END DEPRECATED_MSG_ATTRIBUTE("Use FWAdBufferingEndEvent instead");


FW_EXTERN NSString *const FW_EVENT_RESELLER_NO_AD DEPRECATED_MSG_ATTRIBUTE("Use FWAdResellerNoAdEvent instead");

#pragma mark - Ad Event Types

/**
	Event type: Click tracking

	- See: `[FWAdInstance getEventCallbackUrlsByEventName:eventType:]`
	- SeeAlso:
		- `FWEventTypeImpression`
		- `FWEventTypeClick`
		- `FWEventTypeStandard`
		- `FWEventTypeCustom`
 */
FW_EXTERN NSString *const FWEventTypeClickTracking;
FW_EXTERN NSString *const FW_EVENT_TYPE_CLICK_TRACKING DEPRECATED_MSG_ATTRIBUTE("Use FWEventTypeClickTracking instead");

/**
	Event type: Impression

	- See: `[FWAdInstance getEventCallbackUrlsByEventName:eventType:]`
	- SeeAlso:
		- `FWEventTypeClickTracking`
		- `FWEventTypeClick`
		- `FWEventTypeStandard`
		- `FWEventTypeCustom`
 */
FW_EXTERN NSString *const FWEventTypeImpression;
FW_EXTERN NSString *const FW_EVENT_TYPE_IMPRESSION DEPRECATED_MSG_ATTRIBUTE("Use FWEventTypeImpression instead");

/**
	Event type: Click (through)

	- See: `[FWAdInstance getEventCallbackUrlsByEventName:eventType:]`
	- SeeAlso:
		- `FWEventTypeClickTracking`
		- `FWEventTypeImpression`
		- `FWEventTypeStandard`
		- `FWEventTypeCustom`

 */
FW_EXTERN NSString *const FWEventTypeClick;
FW_EXTERN NSString *const FW_EVENT_TYPE_CLICK DEPRECATED_MSG_ATTRIBUTE("Use FWEventTypeClick instead");

/**
	Event type: Standard IAB

	- See: `[FWAdInstance getEventCallbackUrlsByEventName:eventType:]`
	- SeeAlso:
		- `FWEventTypeClickTracking`
		- `FWEventTypeImpression`
		- `FWEventTypeClick`
		- `FWEventTypeCustom`
 */
FW_EXTERN NSString *const FWEventTypeStandard;
FW_EXTERN NSString *const FW_EVENT_TYPE_STANDARD DEPRECATED_MSG_ATTRIBUTE("Use FWEventTypeStandard instead");

/**
	Event type: User custom

	- See: `[FWAdInstance getEventCallbackUrlsByEventName:eventType:]`
	- SeeAlso:
		- `FWEventTypeClickTracking`
		- `FWEventTypeImpression`
		- `FWEventTypeClick`
		- `FWEventTypeStandard`
 */
FW_EXTERN NSString *const FWEventTypeCustom;
FW_EXTERN NSString *const FW_EVENT_TYPE_CUSTOM DEPRECATED_MSG_ATTRIBUTE("Use FWEventTypeCustom instead");

#pragma mark - Parameters
/**
	Parameter: `NSString` representing the timeout value of playing a video ad in seconds.
 
	Default value: `@"5"`
 
	- Note: When a timeout occurs, a `FWAdEventNotification` with error code `FWErrorTimeout` will be dispatched. The ad will be terminated on this error. 
	- SeeAlso: `FWAdEventNotification`
	- Note: Ad traffickers should use `renderer.video.timeout` instead of `FWParameterVideoAdRendererTimeout` when setting this parameter in MRM UI.
	- See:
		- `[FWContext setParameter:withValue:forLevel:]`
		- `[FWSlot setParameter:withValue:]`
		- `[FWCreativeRendition setParameter:withValue:]`
 */

FW_EXTERN NSString *const FWParameterVideoAdRendererTimeout;

/**
	Parameter: Open click through url in inAppView or external app. See <a href="http://hub.freewheel.tv/display/techdocs/iOS+InAppView+Extension" target="_blank">inAppView</a> for more information.
 
	Valid values:
		- `@"YES"`: Open in inAppView (default)
		- `@"NO"`: Open in external app
 
	- Note: Ad traffickers should use `tapOpensInApplication` instead of `FWParameterOpenClickThroughInApp` when setting this parameter in MRM UI.
	- See:
		- `[FWContext setParameter:withValue:forLevel:]`
		- `[FWSlot setParameter:withValue:]`
		- `[FWCreativeRendition setParameter:withValue:]`
 */
FW_EXTERN NSString *const FWParameterOpenClickThroughInApp;
FW_EXTERN NSString *const FW_PARAMETER_OPEN_IN_APP DEPRECATED_MSG_ATTRIBUTE("Use FWParameterOpenClickThroughInApp instead");

/**
	Parameter: `NSString` representing the timeout value of loading a url in inAppView in seconds.

	Default value: `@"3"`

	- Note: After the timeout, inAppView's close button will be enabled which allows the user to close the inAppView.
	- Note: Ad traffickers should use `loadTimeOutSecond` instead of `FWParameterInAppViewLoadingTimeout` when setting this parameter in MRM UI.
	- See:
		- `[FWContext setParameter:withValue:forLevel:]`
		- `[FWSlot setParameter:withValue:]`
		- `[FWCreativeRendition setParameter:withValue:]`
 */
FW_EXTERN NSString *const FWParameterInAppViewLoadingTimeout;
FW_EXTERN NSString *const FW_PARAMETER_IN_APP_VIEW_LOADING_TIMEOUT DEPRECATED_MSG_ATTRIBUTE("Use FWParameterInAppViewLoadingTimeout instead");

/**
	Parameter: inAppView navigation bar HTML.
	The value of the parameter is a HTML snippet that renders a toolbar inside the inAppView. The toolbar HTML snippet should contain a back button, a forward button and a close button.
	
	For example:
	```<div><img id="FW_IN_APP_VIEW_CONTROL_BAR_BACK_BUTTON"  src="data:image/png;base64,PNG_BASE64_STRING" width="" height=""/><img id="FW_IN_APP_VIEW_CONTROL_BAR_FORWARD_BUTTON"  src="data:image/png;base64,PNG_BASE64_STRING" width="" height=""/><img id="FW_IN_APP_VIEW_CONTROL_BAR_CLOSE_BUTTON"  src="data:image/png;base64,PNG_BASE64_STRING" width="" height=""/></div>```

	The `FW_IN_APP_VIEW_CONTROL_BAR_BACK_BUTTON`, `FW_IN_APP_VIEW_CONTROL_BAR_FORWARD_BUTTON` and `FW_IN_APP_VIEW_CONTROL_BAR_CLOSE_BUTTON` IDs have to be kept as such for handling event.
	`PNG_BASE64_STRING` should be replaced by the png images' base64 hash.

	All mobile Safari mobile compatible html5 tags are supported, for example, you can use `<table>` tags to manage the layout instead of `<div>` tags shown in the example.
 
	- Note: Ad traffickers should use `extension.inAppView.toolbarSurfaceRender` instead of `FWParameterInAppViewNavigationBarHtml` when setting this parameter in MRM UI.
	- See:
		- `[FWContext setParameter:withValue:forLevel:]`
		- `[FWSlot setParameter:withValue:]`
		- `[FWCreativeRendition setParameter:withValue:]`

 */
FW_EXTERN NSString *const FWParameterInAppViewNavigationBarHtml;
FW_EXTERN NSString *const FW_PARAMETER_IN_APP_VIEW_TOOLBAR_SURFACE_RENDER DEPRECATED_MSG_ATTRIBUTE("Use FWParameterInAppViewNavigationBarHtml instead");

/**
	Parameter: inAppView navigation bar background color.

	Valid values: NSString representing an integer/hexadecimal from 0 to 0xffffff, for example `@"256"`, `@"0xffffff"`
 
	- Note: Ad traffickers should use `extension.inAppView.navigationBarBackgroundColor` instead of `FWParameterInAppViewNavigationBarBackgroundColor` when setting this parameter in MRM UI.
	- See:
		- `[FWContext setParameter:withValue:forLevel:]`
		- `[FWSlot setParameter:withValue:]`
		- `[FWCreativeRendition setParameter:withValue:]`

 */
FW_EXTERN NSString *const FWParameterInAppViewNavigationBarBackgroundColor;
FW_EXTERN NSString *const FW_PARAMETER_IN_APP_VIEW_NAVIGATION_BAR_BACKGROUND_COLOR DEPRECATED_MSG_ATTRIBUTE("Use FWParameterInAppViewNavigationBarBackgroundColor instead");

/**
	Parameter: inAppView navigation bar alpha.

	Valid values: NSString representing a float from 0.0 to 1.0, where `@"0.0"` represents completely transparent and `@"1.0"` represents completely opaque.

	Default value: `@"1.0"`
 
	- Note: Ad traffickers should use `extension.inAppView.navigationBarAlpha` instead of `FWParameterInAppViewNavigationBarAlpha` when setting this parameter in MRM UI.
	- See:
		- `[FWContext setParameter:withValue:forLevel:]`
		- `[FWSlot setParameter:withValue:]`
		- `[FWCreativeRendition setParameter:withValue:]`
 */
FW_EXTERN NSString *const FWParameterInAppViewNavigationBarAlpha;
FW_EXTERN NSString *const FW_PARAMETER_IN_APP_VIEW_NAVIGATION_BAR_ALPHA DEPRECATED_MSG_ATTRIBUTE("Use FWParameterInAppViewNavigationBarAlpha instead");

/**
	Parameter: inAppView navigation bar height.

	Valid values: NSString representing a percentage in the range 0% to 100%, compared to screen height, e.g., `@"10%"`.

	Default value: `@"8.3%"` for iPhone and `@"3.9%"` for iPad

	- Note: Ad traffickers should use `extension.inAppView.navigationBarHeight` instead of `FWParameterInAppViewNavigationBarHeight` when setting this parameter in MRM UI.
	- See:
		- `[FWContext setParameter:withValue:forLevel:]`
		- `[FWSlot setParameter:withValue:]`
		- `[FWCreativeRendition setParameter:withValue:]`
 */
FW_EXTERN NSString *const FWParameterInAppViewNavigationBarHeight;
FW_EXTERN NSString *const FW_PARAMETER_IN_APP_VIEW_NAVIGATION_BAR_HEIGHT DEPRECATED_MSG_ATTRIBUTE("Use FWParameterInAppViewNavigationBarHeight instead");

/**
	Parameter: inAppView background color.

	Valid values: NSString representing an integer/hexadecimal from 0 to 0xffffff, for example `@"256"`, `@"0xffffff"`

	Default value: `@"0xffffff"`
 
	- Note: Ad traffickers should use `extension.inAppView.webViewBackgroundColor` instead of `FWParameterInAppViewWebViewBackgroundColor` when setting this parameter in MRM UI.
	- See:
		- `[FWContext setParameter:withValue:forLevel:]`
		- `[FWSlot setParameter:withValue:]`
		- `[FWCreativeRendition setParameter:withValue:]`
 */
FW_EXTERN NSString *const FWParameterInAppViewWebViewBackgroundColor;
FW_EXTERN NSString *const FW_PARAMETER_IN_APP_VIEW_WEB_VIEW_BACKGROUND_COLOR DEPRECATED_MSG_ATTRIBUTE("Use FWParameterInAppViewWebViewBackgroundColor instead");

/**
	Parameter: inAppView webview alpha.

	Valid values: NSString representing a float from 0.0 to 1.0, where `@"0.0"` represents completely transparent and `@"1.0"` represents completely opaque.

	Default value: `@"1.0"`

	- Note: Ad traffickers should use `extension.inAppView.webViewAlpha` instead of `FWParameterInAppViewWebViewAlpha` when setting this parameter in MRM UI.
	- See:
		- `[FWContext setParameter:withValue:forLevel:]`
		- `[FWSlot setParameter:withValue:]`
		- `[FWCreativeRendition setParameter:withValue:]`
 */
FW_EXTERN NSString *const FWParameterInAppViewWebViewAlpha;
FW_EXTERN NSString *const FW_PARAMETER_IN_APP_VIEW_WEB_VIEW_ALPHA DEPRECATED_MSG_ATTRIBUTE("Use FWParameterInAppViewWebViewAlpha instead");

/**
	Parameter: enable countdown timer for applicable ads.

	Valid values: NSString `@"YES"`, `@"NO"`

	Default value: `@"NO"`
	
	- Note: This parameter should be set before slot starts, otherwise it will have no effect.
	- Note: Ad traffickers should use `extension.countdown.display` instead of `FWParameterEnableCountdownTimer` when setting this parameter in MRM UI.
	- See:
		- `[FWContext setParameter:withValue:forLevel:]`
		- `[FWSlot setParameter:withValue:]`
		- `[FWCreativeRendition setParameter:withValue:]`
 */
FW_EXTERN NSString *const FWParameterEnableCountdownTimer;
FW_EXTERN NSString *const FW_PARAMETER_COUNTDOWN_TIMER_DISPLAY DEPRECATED_MSG_ATTRIBUTE("Use FWParameterEnableCountdownTimer instead");

/**
	Parameter: countdown timer refresh interval, in milliseconds.

	Valid values: NSString representing an integer from 1 to 1000.

	Default value: `@"300"`
	
	- Note: This parameter should be set before slot starts, otherwise it will have no effect.
	- Note: Ad traffickers should use `extension.countdown.refreshInterval` instead of `FWParameterCountdownTimerRefreshInterval` when setting this parameter in MRM UI.
	- See:
		- `[FWContext setParameter:withValue:forLevel:]`
		- `[FWSlot setParameter:withValue:]`
		- `[FWCreativeRendition setParameter:withValue:]`
 */
FW_EXTERN NSString *const FWParameterCountdownTimerRefreshInterval;
FW_EXTERN NSString *const FW_PARAMETER_COUNTDOWN_TIMER_REFRESH_INTERVAL DEPRECATED_MSG_ATTRIBUTE("FWParameterCountdownTimerRefreshInterval");

/**
	Parameter: javascript function to call when the countdown timer is updated, NSString.
	The javascript function should be available in the value defined by parameter `FWParameterCountdownTimerHtml` (see `FWParameterCountdownTimerHtml` for details). By default the function called is `updateTimer`. The function will be called with two parameters: the current playhead time of the slot in seconds; the total duration of the slot in seconds: `function updateTimer(playheadTime, duration);`

	- Note: This parameter should be set before slot starts, otherwise it will have no effect.
	- Note: Ad traffickers should use `extension.countdown.updateCallback` instead of `FWParameterCountdownTimerUpdateCallback` when setting this parameter in MRM UI.
	- See:
		- `[FWContext setParameter:withValue:forLevel:]`
		- `[FWSlot setParameter:withValue:]`
		- `[FWCreativeRendition setParameter:withValue:]`
 */
FW_EXTERN NSString *const FWParameterCountdownTimerUpdateCallback;
FW_EXTERN NSString *const FW_PARAMETER_COUNTDOWN_TIMER_UPDATE_CALLBACK DEPRECATED_MSG_ATTRIBUTE("Use FWParameterCountdownTimerUpdateCallback instead");

/**
	Parameter: countdown timer position.

	Valid values: NSString `@"bottom"`, `@"top"`, and `@"x,y"`, for example `@"10,20"`

	- Note: This parameter should be set before slot starts, otherwise it will have no effect.
	- Note: Ad traffickers should use `extension.countdown.updateCallback` instead of `FWParameterCountdownTimerUpdateCallback` when setting this parameter in MRM UI.
	- See:
		- `[FWContext setParameter:withValue:forLevel:]`
		- `[FWSlot setParameter:withValue:]`
		- `[FWCreativeRendition setParameter:withValue:]`
 */
FW_EXTERN NSString *const FWParameterCountdownTimerPosition;
FW_EXTERN NSString *const FW_PARAMETER_COUNTDOWN_TIMER_POSITION DEPRECATED_MSG_ATTRIBUTE("Use FWParameterCountdownTimerPosition instead");

/**
	Parameter: countdown timer alpha.

	Valid values: NSString representing a float in the range 0.0 to 1.0, where `@"0.0"` represents completely transparent and `@"1.0"` represents completely opaque.

	Default value: @"1.0".

	- Note: This parameter should be set before slot starts, otherwise it will have no effect.
	- Note: Ad traffickers should use `extension.countdown.updateCallback` instead of `FWParameterCountdownTimerUpdateCallback` when setting this parameter in MRM UI.
	- See:
		- `[FWContext setParameter:withValue:forLevel:]`
		- `[FWSlot setParameter:withValue:]`
		- `[FWCreativeRendition setParameter:withValue:]`
 */
FW_EXTERN NSString *const FWParameterCountdownTimerAlpha;
FW_EXTERN NSString *const FW_PARAMETER_COUNTDOWN_TIMER_ALPHA DEPRECATED_MSG_ATTRIBUTE("Use FWParameterCountdownTimerAlpha instead");

/**
	Parameter: countdown timer height in pixels.

	Valid values: NSString representing an integer greater than 0.

	Default value: `@"20"`

	- Note: This parameter should be set before slot starts, otherwise it will have no effect.
	- Note: Ad traffickers should use `extension.countdown.height` instead of `FWParameterCountdownTimerHeight` when setting this parameter in MRM UI.
	- See:
		- `[FWContext setParameter:withValue:forLevel:]`
		- `[FWSlot setParameter:withValue:]`
		- `[FWCreativeRendition setParameter:withValue:]`
 */
FW_EXTERN NSString *const FWParameterCountdownTimerHeight;
FW_EXTERN NSString *const FW_PARAMETER_COUNTDOWN_TIMER_HEIGHT DEPRECATED_MSG_ATTRIBUTE("Use FWParameterCountdownTimerHeight instead");

/**
	Parameter: countdown timer width in pixels.

	Valid values: NSString representing an integer greater than 0.

	Default value: width of the video display base

	- Note: This parameter should be set before slot starts, otherwise it will have no effect.
	- Note: Ad traffickers should use `extension.countdown.width` instead of `FWParameterCountdownTimerWidth` when setting this parameter in MRM UI.
	- See:
		- `[FWContext setParameter:withValue:forLevel:]`
		- `[FWSlot setParameter:withValue:]`
		- `[FWCreativeRendition setParameter:withValue:]`
 */
FW_EXTERN NSString *const FWParameterCountdownTimerWidth;
FW_EXTERN NSString *const FW_PARAMETER_COUNTDOWN_TIMER_WIDTH DEPRECATED_MSG_ATTRIBUTE("Use FWParameterCountdownTimerWidth instead");

/**
	Parameter: countdown timer font size in css syntax.

	Valid values: css-valid font-size values, for example, `@"medium"`, `@"12px"`

	Default value: `@"medium"`

	- Note: This parameter should be set before slot starts, otherwise it will have no effect.
	- Note: Ad traffickers should use `extension.countdown.textSize` instead of `FWParameterCountdownTimerTextSize` when setting this parameter in MRM UI.
	- See:
		- `[FWContext setParameter:withValue:forLevel:]`
		- `[FWSlot setParameter:withValue:]`
		- `[FWCreativeRendition setParameter:withValue:]`
 */
FW_EXTERN NSString *const FWParameterCountdownTimerTextSize;
FW_EXTERN NSString *const FW_PARAMETER_COUNTDOWN_TIMER_TEXT_SIZE DEPRECATED_MSG_ATTRIBUTE("Use FWParameterCountdownTimerTextSize instead");

/**
	Parameter: countdown timer background color.

	Valid values: NSString representing an integer/hexadecimal from 0 to 0xffffff, for example `@"256"`, `@"0xffffff"`

	Default value: `@"0x4a4a4a"`

	- Note: This parameter should be set before slot starts, otherwise it will have no effect.
	- Note: Ad traffickers should use `extension.countdown.bgColor` instead of `FWParameterCountdownTimerBackgroundColor` when setting this parameter in MRM UI.
	- See:
		- `[FWContext setParameter:withValue:forLevel:]`
		- `[FWSlot setParameter:withValue:]`
		- `[FWCreativeRendition setParameter:withValue:]`
 */
FW_EXTERN NSString *const FWParameterCountdownTimerBackgroundColor;
FW_EXTERN NSString *const FW_PARAMETER_COUNTDOWN_TIMER_BG_COLOR DEPRECATED_MSG_ATTRIBUTE("Use FWParameterCountdownTimerBackgroundColor instead");

/**
	Parameter: countdown timer font color.

	Valid values: NSString representing an integer/hexadecimal from 0 to 0xffffff, for example `@"256"`, `@"0xffffff"`
	
	Default value: `@"0xffffff"`

	- Note: This parameter should be set before slot starts, otherwise it will have no effect.
	- Note: Ad traffickers should use `extension.countdown.fontColor` instead of `FWParameterCountdownTimerFontColor` when setting this parameter in MRM UI.
	- See:
		- `[FWContext setParameter:withValue:forLevel:]`
		- `[FWSlot setParameter:withValue:]`
		- `[FWCreativeRendition setParameter:withValue:]`
 */
FW_EXTERN NSString *const FWParameterCountdownTimerFontColor;
FW_EXTERN NSString *const FW_PARAMETER_COUNTDOWN_TIMER_FONT_COLOR DEPRECATED_MSG_ATTRIBUTE("Use FWParameterCountdownTimerFontColor instead");

/**
	Parameter: countdown timer font family in css syntax.

	Valid values: css-valid font-family values

	Default value: `@"Arial"`

	- Note: This parameter should be set before slot starts, otherwise it will have no effect.
	- Note: Ad traffickers should use `extension.countdown.textFont` instead of `FWParameterCountdownTimerTextFont` when setting this parameter in MRM UI.
	- See:
		- `[FWContext setParameter:withValue:forLevel:]`
		- `[FWSlot setParameter:withValue:]`
		- `[FWCreativeRendition setParameter:withValue:]`
 */
FW_EXTERN NSString *const FWParameterCountdownTimerTextFont;
FW_EXTERN NSString *const FW_PARAMETER_COUNTDOWN_TIMER_TEXT_FONT DEPRECATED_MSG_ATTRIBUTE("Use FWParameterCountdownTimerTextFont instead");

/**
	Parameter: countdown timer html.

	Valid values: html snippet displaying a countdown timer, NSString.

	Default value: a default timer display html will be provided by default. If you set this parameter by yourself, there should also be a javascript function that updates it periodically. See `FWParameterCountdownTimerUpdateCallback` for details.

	- Note: This parameter should be set before slot starts, otherwise it will have no effect.
	- Note: Ad traffickers should use `extension.countdown.html` instead of `FWParameterCountdownTimerHtml` when setting this parameter in MRM UI.
	- See:
		- `[FWContext setParameter:withValue:forLevel:]`
		- `[FWSlot setParameter:withValue:]`
		- `[FWCreativeRendition setParameter:withValue:]`
 */
FW_EXTERN NSString *const FWParameterCountdownTimerHtml;
FW_EXTERN NSString *const FW_PARAMETER_COUNTDOWN_TIMER_HTML DEPRECATED_MSG_ATTRIBUTE("Use FWParameterCountdownTimerHtml instead");

/**
	Parameter: Enable / disable automatic visibility tracking of nontemporal slots. When enabled, non-temporal slots will be played automatically once they are visible on the device screen.

	Valid values: NSString `@"YES"`(enable), `@"NO"`(disable)

	Default value: `@"NO"`

	- Note: This parameter should be set before slot starts, otherwise it will have no effect.
	- Note: Ad traffickers should use `FW_PARAMETER_NONTEMPORAL_SLOT_VISIBILITY_AUTO_TRACKING` instead of `FWParameterNonTemporalSlotVisibilityAutoTracking` when setting this parameter in MRM UI.
	- See:
		- `[FWContext setParameter:withValue:forLevel:]`
		- `[FWSlot setParameter:withValue:]`
		- `[FWCreativeRendition setParameter:withValue:]`
 */
FW_EXTERN NSString *const FWParameterNonTemporalSlotVisibilityAutoTracking;
FW_EXTERN NSString *const FW_PARAMETER_NONTEMPORAL_SLOT_VISIBILITY_AUTO_TRACKING DEPRECATED_MSG_ATTRIBUTE("Use FWParameterNonTemporalSlotVisibilityAutoTracking instead");

/**
	Parameter: Enable the <a href="http://hub.freewheel.tv/display/publicdrafts/iOS+PauseAd+Extension" target="_blank">Pause Ad Extension</a>.

	Valid values: NSString `@"YES"`(enable), `@"NO"`(disable)
	
	Default value: `@"NO"`

	- Note: This parameter should be set before sending the ad request, otherwise it will have no effect.
	- Note: Ad traffickers should use `extension.pausead.enable` instead of `FWParameterEnablePauseAd` when setting this parameter in MRM UI.
	- See:
		- `[FWContext setParameter:withValue:forLevel:]`
		- `[FWSlot setParameter:withValue:]`
		- `[FWCreativeRendition setParameter:withValue:]`
 */
FW_EXTERN NSString *const FWParameterEnablePauseAd;
FW_EXTERN NSString *const FW_PARAMETER_PAUSEAD_ENABLE DEPRECATED_MSG_ATTRIBUTE("Use FWParameterEnablePauseAd instead");

/**
	Parameter: indicates whether AdManager should show buffering animation when a video ad is buffering.
 
	Valid values: NSString `@"YES"`, `@"NO"`
 
	Default value: `@"YES"`
 
	- Note: if the value is set to @"NO", the app itself should implement the buffering animation by listening to FreeWheel ad events `FWAdBufferingStartEvent` and `FWAdBufferingEndEvent`.
	- Note: Ad traffickers should use `renderer.video.showBufferIndicator` instead of `FWParameterVideoAdRendererShowBufferIndicator` when setting this parameter in MRM UI.
	- See:
 - `[FWContext setParameter:withValue:forLevel:]`
 - `[FWSlot setParameter:withValue:]`
 - `[FWCreativeRendition setParameter:withValue:]`
 */
FW_EXTERN NSString *const FWParameterVideoAdRendererShowBufferIndicator;

/**
	Parameter: indicates whether AdManager should handle user tappings on a video ad.

	Valid values: NSString `@"YES"`, `@"NO"`

	Default value: `@"YES"`

	- Note: if the value is set to @"NO", app should handle the ad click by itself, for example, opening the click through url in a UIWebView.
	- Note: Ad traffickers should use `renderer.video.clickDetection` instead of `FWParameterDetectClick` when setting this parameter in MRM UI.
	- See:
		- `[FWContext setParameter:withValue:forLevel:]`
		- `[FWSlot setParameter:withValue:]`
		- `[FWCreativeRendition setParameter:withValue:]`
 */
FW_EXTERN NSString *const FWParameterDetectClick;
FW_EXTERN NSString *const FW_PARAMETER_CLICK_DETECTION DEPRECATED_MSG_ATTRIBUTE("Use FWParameterDetectClick instead");

/**
	Parameter: desired bitrate. Current viewer's available bandwidth.

	Valid values: NSString representing a positive decimal float.

	Default value: `@"1000.0"`

	- Note: the value is used in creative rendition selection. AdManager will automatically choose the best fit rendition according to current view's bandwidth.
	- Note: Ad traffickers should use `desiredBitrate` instead of `FWParameterDesiredBitrate` when setting this parameter in MRM UI.
	- See:
		- `[FWContext setParameter:withValue:forLevel:]`
		- `[FWSlot setParameter:withValue:]`
		- `[FWCreativeRendition setParameter:withValue:]`
 */
FW_EXTERN NSString *const FWParameterDesiredBitrate;
FW_EXTERN NSString *const FW_PARAMETER_DESIRED_BITRATE DEPRECATED_MSG_ATTRIBUTE("Use FWParameterDesiredBitrate instead");

/**
	Parameter: Whether to automatically retrieve and send the Apple IDFA when it is available.

	Valid values: NSString `@"YES"`, `@"NO"`

	Default value: `@"YES"`

	- Note: Ad traffickers should use `_fw_did_idfa` instead of `FWParameterCollectIDFA` when setting this parameter in MRM UI.
	- See:
		- `[FWContext setParameter:withValue:forLevel:]`
		- `[FWSlot setParameter:withValue:]`
		- `[FWCreativeRendition setParameter:withValue:]`
 */
FW_EXTERN NSString *const FWParameterCollectIDFA;
FW_EXTERN NSString *const FW_PARAMETER_IDFA DEPRECATED_MSG_ATTRIBUTE("Use FWParameterCollectIDFA instead");

/**
	Parameter: Anchor point of image in overlay slots.
 
	Valid values: NSString `tc`, `bc`, `ml`, `mr`, `tl`, `tr`, `bl`, `br`. Here 't' means 'top', 'l' means 'left', 'b' means 'bottom', 'r' means 'right', 'c' means 'center', 'm' means 'middle'
 
	Default value: `bc`
 
	- Note: Ad traffickers should use `renderer.image.primaryAnchor` instead of `FWParameterImageRendererPrimaryAnchor` when setting this parameter in MRM UI.
	- See:
		- `[FWContext setParameter:withValue:forLevel:]`
		- `[FWSlot setParameter:withValue:]`
		- `[FWCreativeRendition setParameter:withValue:]`
 */
FW_EXTERN NSString *const FWParameterImageRendererPrimaryAnchor;

/**
	Parameter: X axis offset relative to the anchor point of image in overlay slots.
 
	Valid values: NSString representing a positive integer. Negative values will be considered as 0 instead.
 
	Default value: `0`
 
	- Note: Ad traffickers should use `renderer.image.marginWidth` instead of `FWParameterImageRendererMarginWidth` when setting this parameter in MRM UI.
	- See:
		- `[FWContext setParameter:withValue:forLevel:]`
		- `[FWSlot setParameter:withValue:]`
		- `[FWCreativeRendition setParameter:withValue:]`
 */
FW_EXTERN NSString *const FWParameterImageRendererMarginWidth;

/**
	Parameter: Y axis offset relative to the anchor point of image in overlay slots.
 
	Valid values: NSString representing a positive integer. Invalid values will be considered as 0 instead.
 
	Default value: `0`
 
	- Note: Ad traffickers should use `renderer.image.marginHeight` instead of `FWParameterImageRendererMarginHeight` when setting this parameter in MRM UI.
	- See:
		- `[FWContext setParameter:withValue:forLevel:]`
		- `[FWSlot setParameter:withValue:]`
		- `[FWCreativeRendition setParameter:withValue:]`
 */
FW_EXTERN NSString *const FWParameterImageRendererMarginHeight;

/**
	Parameter: Timeout in seconds for downloading an image to display
 
	Valid values: NSString representing a positive integer. Invalid values will be considered as the default value instead.
 
	Default value: `30`
 
	- Note: Ad traffickers should use `renderer.image.timeout` instead of `FWParameterImageRendererTimeout` when setting this parameter in MRM UI.
	- See:
		- `[FWContext setParameter:withValue:forLevel:]`
		- `[FWSlot setParameter:withValue:]`
		- `[FWCreativeRendition setParameter:withValue:]`
 */
FW_EXTERN NSString *const FWParameterImageRendererTimeout;

/*! internal */
FW_EXTERN NSString *const FW_PARAMETER_REQUEST_TEMPLATE_VARIABLES;
FW_EXTERN NSString *const FW_PARAMETER_REQUEST_ALTERNATIVE_URL;

#pragma mark - Request Keys

/**
	Ad request key: Postal code of the current viewer.

	- See: `[FWContext addValue:forKey:]`
 */
FW_EXTERN NSString *const FWRequestKeyPostalCode;
FW_EXTERN NSString *const FW_PARAMETER_POSTAL_CODE DEPRECATED_MSG_ATTRIBUTE("Use FWRequestKeyPostalCode instead");

/**
	Ad request key: Area code of the current user.

	- See: `[FWContext addValue:forKey:]`
 */
FW_EXTERN NSString *const FWRequestKeyAreaCode;
FW_EXTERN NSString *const FW_PARAMETER_AREA_CODE DEPRECATED_MSG_ATTRIBUTE("Use FWRequestKeyAreaCode instead");

/**
	Ad request key: User's date of birth.

	- See: `[FWContext addValue:forKey:]`
 */
FW_EXTERN NSString *const FWRequestKeyDateOfBirth;
FW_EXTERN NSString *const FW_PARAMETER_DATE_OF_BIRTH DEPRECATED_MSG_ATTRIBUTE("Use FWRequestKeyDateOfBirth instead");

/**
	Ad request key: User's gender.

	- See: `[FWContext addValue:forKey:]`
 */
FW_EXTERN NSString *const FWRequestKeyGender;
FW_EXTERN NSString *const FW_PARAMETER_GENDER DEPRECATED_MSG_ATTRIBUTE("Use FWRequestKeyGender instead");

/**
	Ad request key: Keywords.

	- See: `[FWContext addValue:forKey:]`
 */
FW_EXTERN NSString *const FWRequestKeyKeywords;
FW_EXTERN NSString *const FW_PARAMETER_KEYWORDS DEPRECATED_MSG_ATTRIBUTE("Use FWRequestKeyKeywords instead");

/**
	Ad request key: Search string.

	- See: `[FWContext addValue:forKey:]`
 */
FW_EXTERN NSString *const FWRequestKeySearchString;
FW_EXTERN NSString *const FW_PARAMETER_SEARCH_STRING DEPRECATED_MSG_ATTRIBUTE("Use FWRequestKeySearchString instead");

/**
	Ad request key: Marital status of the current user.

	- See: `[FWContext addValue:forKey:]`
 */
FW_EXTERN NSString *const FWRequestKeyMarital;
FW_EXTERN NSString *const FW_PARAMETER_MARITAL DEPRECATED_MSG_ATTRIBUTE("Use FWRequestKeyMarital instead");

/**
	Ad request key: Ethnicity of the current user.

	- See: `[FWContext addValue:forKey:]`
 */
FW_EXTERN NSString *const FWRequestKeyEthnicity;
FW_EXTERN NSString *const FW_PARAMETER_ETHNICITY DEPRECATED_MSG_ATTRIBUTE("Use FWRequestKeyEthnicity instead");

/**
	Ad request key: Orientation of the current user.

	- See: `[FWContext addValue:forKey:]`
 */
FW_EXTERN NSString *const FWRequestKeyOrientation;
FW_EXTERN NSString *const FW_PARAMETER_ORIENTATION DEPRECATED_MSG_ATTRIBUTE("Use FWRequestKeyOrientation instead");

/**
	Ad request key: User's income.

	- See: `[FWContext addValue:forKey:]`
 */
FW_EXTERN NSString *const FWRequestKeyIncome;
FW_EXTERN NSString *const FW_PARAMETER_INCOME DEPRECATED_MSG_ATTRIBUTE("Use FWRequestKeyIncome instead");

#pragma mark - Dictionary Keys

/**
	Notification `userInfo` dictionary key: url. The click through url of an ad click event.

	- See:
		- `FWAdEventNotification`
		- `FWAdClickEvent`
 */
FW_EXTERN NSString *const FWInfoKeyUrl;
FW_EXTERN NSString *const FW_INFO_KEY_URL DEPRECATED_MSG_ATTRIBUTE("Use FWInfoKeyUrl instead");


/**
	Notification `userInfo` dictionary key: error. Error message of a failed ad request or failed extension load.

	- See:
		- `FWRequestCompleteNotification`
		- `FWExtensionLoadedNotification`
 */
FW_EXTERN NSString *const FWInfoKeyError;
FW_EXTERN NSString *const FW_INFO_KEY_ERROR DEPRECATED_MSG_ATTRIBUTE("Use FWInfoKeyError instead");

/**
	Notification `userInfo` dictionary key: slot custom ID. Custom ID of the related slot

	- See: 
		- `FWSlotStartedNotification`
		- `FWSlotEndedNotification`
		- `FWSlotPreloadedNotification`
		- `FWContentPauseRequestNotification`
		- `FWContentResumeRequestNotification`
 */
FW_EXTERN NSString *const FWInfoKeySlotCustomId;
FW_EXTERN NSString *const FW_INFO_KEY_CUSTOM_ID DEPRECATED_MSG_ATTRIBUTE("Use FWInfoKeySlotCustomId instead");

/**
	Notification `userInfo` dictionary key: concrete event ID

	Concrete event ID of the related ad, should be used to pass the concrete event ID in `FWAdMeasurementEvent`'s details dictionary. Example:
```[rendererController processEvent:FWAdMeasurementEvent info:[NSDictionary dictionaryWithObject:@"concreteEventId" forKey:FWInfoKeyConcreteEventId]];```

	The concrete event ID should be a `NSString`.
 */
FW_EXTERN NSString *const FWInfoKeyConcreteEventId;
FW_EXTERN NSString *const FW_INFO_KEY_CONCRETE_EVENT_ID DEPRECATED_MSG_ATTRIBUTE("Use FWInfoKeyConcreteEventId instead");

/**
	Notification `userInfo` dictionary key: slot. The `FWSlot` instance of the relevant slot.
	
	- See:
		- `FWAdEventNotification`
 
	TODO: unify notification payloads
 */
FW_EXTERN NSString *const FWInfoKeySlot;
FW_EXTERN NSString *const FW_INFO_KEY_SLOT DEPRECATED_MSG_ATTRIBUTE("Use FWInfoKeySlot instead");

/**
	Notification `userInfo` dictionary key: adInstance. The `FWAdInstance` instance of the relevant ad instance.
	
	- See: `FWAdEventNotification`
 */
FW_EXTERN NSString *const FWInfoKeyAdInstance;
FW_EXTERN NSString *const FW_INFO_KEY_ADINSTANCE DEPRECATED_MSG_ATTRIBUTE("Use FWInfoKeyAdInstance instead");

/**
	Notification `userInfo` dictionary key: ad ID. ID of the relevant ad.

	- See: `FWAdEventNotification`
 */
FW_EXTERN NSString *const FWInfoKeyAdId;
FW_EXTERN NSString *const FW_INFO_KEY_AD_ID DEPRECATED_MSG_ATTRIBUTE("Use FWInfoKeyAdId instead");

/**
	Notification `userInfo` dictionary key: creative ID. ID of the relevant creative.

	- See: `FWAdEventNotification`
 */
FW_EXTERN NSString *const FWInfoKeyCreativeId;
FW_EXTERN NSString *const FW_INFO_KEY_CREATIVE_ID DEPRECATED_MSG_ATTRIBUTE("Use FWInfoKeyCreativeId instead");

/**
	Notification `userInfo` dictionary key: user action type. Type of the relevant user action.

	- See: 
		- `FWUserActionNotification`
		- `FWUserAction`
 */
FW_EXTERN NSString *const FWInfoKeyUserAction;
FW_EXTERN NSString *const FW_INFO_KEY_USER_ACTION DEPRECATED_MSG_ATTRIBUTE("Use FWInfoKeyUserAction instead");

/**
	Notification `userInfo` dictionary key: module name. Name of relevant module that triggered the notification.

	- See: `FWExtensionLoadedNotification`
 */
FW_EXTERN NSString *const FWInfoKeyModuleName;
FW_EXTERN NSString *const FW_INFO_KEY_MODULE_NAME DEPRECATED_MSG_ATTRIBUTE("Use FWInfoKeyModuleName instead");

/**
	Renderer `moduleInfo` dictionary key: module type, renderer or translator.

	- See: `FWModuleType`
 */
FW_EXTERN NSString *const FWInfoKeyModuleType;
FW_EXTERN NSString *const FW_INFO_KEY_MODULE_TYPE DEPRECATED_MSG_ATTRIBUTE("Use FWInfoKeyModuleType instead");

/**
	Renderer `moduleInfo` dictionary key: required SDK version. Expects a NSNumber.

	Optional. If present, the value should be the lowest SDK version the renderer supports. For example: `0x02060000` for v2.6
 */
FW_EXTERN NSString *const FWInfoKeyRequiredSDKVersion;
FW_EXTERN NSString *const FW_INFO_KEY_REQUIRED_API_VERSION DEPRECATED_MSG_ATTRIBUTE("Use FWInfoKeyRequiredSDKVersion instead");

/**
	Use this key in the `details` dictionary to provide the error code when the renderer/translator transits to `FWRendererStateFailed`.

	Example usage:
	```[rendererController handleStateTransition:FWRendererStateFailed info:[NSDictionary dictionaryWithObjectsAndKeys:FWErrorIO, FWInfoKeyErrorCode, ..., nil]];```
	
	- See: `FWError*`
 */
FW_EXTERN NSString *const FWInfoKeyErrorCode;
FW_EXTERN NSString *const FW_INFO_KEY_ERROR_CODE DEPRECATED_MSG_ATTRIBUTE("Use FWInfoKeyErrorCode instead");

/**
	Use this key in the `details` dictionary to provide detailed error info when the renderer/translator transits to `FWRendererStateFailed`. Expects NSString.
 
	Example usage:
	```[rendererController handleStateTransition:FWRendererStateFailed info:[NSDictionary dictionaryWithObjectsAndKeys:@"failed to load ad from url", FWInfoKeyErrorInfo, ..., nil]];```
 */
FW_EXTERN NSString *const FWInfoKeyErrorInfo;
FW_EXTERN NSString *const FW_INFO_KEY_ERROR_INFO DEPRECATED_MSG_ATTRIBUTE("Use FWInfoKeyErrorInfo instead");

/**
	Use this key in the `details` dictionary to provide the error'ed module's name when the renderer/translator transits to `FWRendererStateFailed`. Expects NSString.

	Example usage: 
	```[rendererController handleStateTransition:FWRendererStateFailed info:[NSDictionary dictionaryWithObjectsAndKeys:@"MyCustomRenderer", FWInfoKeyErrorModule, ..., nil]];```
 */
FW_EXTERN NSString *const FWInfoKeyErrorModule;
FW_EXTERN NSString *const FW_INFO_KEY_ERROR_MODULE DEPRECATED_MSG_ATTRIBUTE("Use FWInfoKeyErrorModule instead");

/**
	Notification `userInfo` dictionary key: ad event name. Name of the ad event.

	- See: `FWAd*Event`
 */
FW_EXTERN NSString *const FWInfoKeyAdEventName;
FW_EXTERN NSString *const FW_INFO_KEY_SUB_EVENT_NAME DEPRECATED_MSG_ATTRIBUTE("Use FWInfoKeyAdEventName instead");

/**
	Notification `userInfo` dictionary key: custom event name. Name of the custom event in NSString.

	- See: `FWAdEventNotification`
 */
FW_EXTERN NSString *const FWInfoKeyCustomEventName;
FW_EXTERN NSString *const FW_INFO_KEY_CUSTOM_EVENT_NAME DEPRECATED_MSG_ATTRIBUTE("Use FWInfoKeyCustomEventName instead");

/**
	Ad event `details` dictionary key: show browser. Should the click event result in the opening of a url. Expects `@"YES"` or `@"NO"`.

	- See:
		- `[FWRendererController processEvent:info:]`
		- `FWAdClickEvent`
 */
FW_EXTERN NSString *const FWInfoKeyShowBrowser;
FW_EXTERN NSString *const FW_INFO_KEY_SHOW_BROWSER DEPRECATED_MSG_ATTRIBUTE("Use FWInfoKeyShowBrowser instead");

/**
	Notification `userInfo` dictionary key: video display base.

	- See:
		- `FWVideoDisplayBaseDidChangeNotification`
		- `FWVideoDisplayBaseFrameDidChangeNotification`
 */
FW_EXTERN NSString *const FWInfoKeyVideoDisplayBase;
FW_EXTERN NSString *const FW_INFO_KEY_VIDEO_DISPLAY_BASE DEPRECATED_MSG_ATTRIBUTE("Use FWInfoKeyVideoDisplayBase instead");


#pragma mark - Error Codes

/**
	Error code: IO error.

	- See: `FWInfoKeyErrorCode`
 */
FW_EXTERN NSString *const FWErrorIO;
FW_EXTERN NSString *const FW_ERROR_IO DEPRECATED_MSG_ATTRIBUTE("Use FWErrorIO instead");

/**
	Error code: timeout error.

	- See: `FWInfoKeyErrorCode`
 */
FW_EXTERN NSString *const FWErrorTimeout;
FW_EXTERN NSString *const FW_ERROR_TIMEOUT DEPRECATED_MSG_ATTRIBUTE("Use FWErrorTimeout instead");

/**
	Error code: creative rendition asset is null.
 
	- See: `FWInfoKeyErrorCode`
 */
FW_EXTERN NSString *const FWErrorNullAsset;
FW_EXTERN NSString *const FW_ERROR_NULL_ASSET DEPRECATED_MSG_ATTRIBUTE("Use FWErrorNullAsset instead");

/**
	Error code: ad instance is unavailable.

	- See: `FWInfoKeyErrorCode`
 */
FW_EXTERN NSString *const FWErrorAdInstanceUnavailable;
FW_EXTERN NSString *const FW_ERROR_ADINSTANCE_UNAVAILABLE DEPRECATED_MSG_ATTRIBUTE("Use FWErrorAdInstanceUnavailable instead");

/**
	Error code: unknown.

	- See: `FWInfoKeyErrorCode`
 */
FW_EXTERN NSString *const FWErrorUnknown;
FW_EXTERN NSString *const FW_ERROR_UNKNOWN DEPRECATED_MSG_ATTRIBUTE("Use FWErrorUnknown instead");

/**
	Error code: missing parameter.
 
	- See: `FWInfoKeyErrorCode`
 */
FW_EXTERN NSString *const FWErrorMissingParameter;
FW_EXTERN NSString *const FW_ERROR_MISSING_PARAMETER DEPRECATED_MSG_ATTRIBUTE("Use FWErrorMissingParameter instead");

/**
	Error code: no ad is available from 3rd party ad source.

	- See: `FWInfoKeyErrorCode`
 */
FW_EXTERN NSString *const FWErrorNoAdAvailable;
FW_EXTERN NSString *const FW_ERROR_NO_AD_AVAILABLE DEPRECATED_MSG_ATTRIBUTE("Use FWErrorNoAdAvailable instead");

/**
	Error code: parsing error.
 
	- See: `FWInfoKeyErrorCode`
 */
FW_EXTERN NSString *const FWErrorParse;
FW_EXTERN NSString *const FW_ERROR_PARSE DEPRECATED_MSG_ATTRIBUTE("Use FWErrorParse instead");

/**
	Error code: invalid value.
 
	- See: `FWInfoKeyErrorCode`
 */
FW_EXTERN NSString *const FWErrorInvalidValue;
FW_EXTERN NSString *const FW_ERROR_INVALID_VALUE DEPRECATED_MSG_ATTRIBUTE("Use FWErrorInvalidValue instead");

/**
	Error code: invalid slot.
 
	- See: `FWInfoKeyErrorCode`
 */
FW_EXTERN NSString *const FWErrorInvalidSlot;
FW_EXTERN NSString *const FW_ERROR_INVALID_SLOT DEPRECATED_MSG_ATTRIBUTE("Use FWErrorInvalidSlot instead");

/**
	Error code: no renderer.
 
	- See: `FWInfoKeyErrorCode`
 */
FW_EXTERN NSString *const FWErrorNoRenderer;
FW_EXTERN NSString *const FW_ERROR_NO_RENDERER DEPRECATED_MSG_ATTRIBUTE("Use FWErrorNoRenderer instead");

/**
	Error code: `preload` is not implemented by the translator.
 
	- See: `FWInfoKeyErrorCode`
 */
FW_EXTERN NSString *const FWErrorPreloadNotImplementedByTranslator;
FW_EXTERN NSString *const FW_ERROR_NO_PRELOAD_IN_TRANSLATOR DEPRECATED_MSG_ATTRIBUTE("Use FWErrorPreloadNotImplementedByTranslator instead");

/**
	Error code: inAppView error.
 
	- See: `FWInfoKeyErrorCode`
 */
FW_EXTERN NSString *const FWErrorInAppView;
FW_EXTERN NSString *const FW_ERROR_IN_APP_VIEW DEPRECATED_MSG_ATTRIBUTE("Use FWErrorInAppView instead");

/**
	Error code: third party component error.
 
	- See: `FWInfoKeyErrorCode`
 */
FW_EXTERN NSString *const FWErrorThirdPartyComponent;
FW_EXTERN NSString *const FW_ERROR_3P_COMPONENT DEPRECATED_MSG_ATTRIBUTE("Use FWErrorThirdPartyComponent instead");

/**
	Error code: unsupported third party feature.
 
	- See: `FWInfoKeyErrorCode`
 */
FW_EXTERN NSString *const FWErrorUnsupportedThirdPartyFeature;
FW_EXTERN NSString *const FW_ERROR_UNSUPPORTED_3P_FEATURE DEPRECATED_MSG_ATTRIBUTE("Use FWErrorUnsupportedThirdPartyFeature instead");

/**
	Error code: device limitation.
 
	- See: `FWInfoKeyErrorCode`
 */
FW_EXTERN NSString *const FWErrorDeviceLimitation;
