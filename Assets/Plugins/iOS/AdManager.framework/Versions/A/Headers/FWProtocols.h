/**
 * \file FWProtocols.h
 * \brief Protocols in FreeWheel AdManager SDK
 */
@class UIView;
@class CLLocation;
@class UIViewController;
@class AVPlayerItem;

@protocol FWAdManager;
@protocol FWContext;
@protocol FWSlot;
@protocol FWAdInstance;
@protocol FWCreativeRendition;
@protocol FWCreativeRenditionAsset;
@protocol FWRendererController;
@protocol FWRenderer;
@protocol FWVideoAdDelegate;

#pragma clang arc_cf_code_audited begin
/** 
	Create a new AdManager instance

	@return a new AdManager instance
	- See: `FWAdManager`
 */
FW_EXTERN id<FWAdManager> newAdManager(void);

/**
	Set logging preference

	- Parameter: `level` - See `FWLogLevel`
 */
FW_EXTERN void FWSetLogLevel(FWLogLevel level);

/**
	AdManager registers `NSSetUncaughtExceptionHandler()` to report uncaught exception.
	If app needs to perform last-minute logging before the program terminates, use this function instead of NSSetUncaughtExceptionHandler.

	- Parameter: `handler` - Your `NSUncaughtExceptionHandler` instance
 */
FW_EXTERN void FWSetUncaughtExceptionHandler(NSUncaughtExceptionHandler *handler);

/**
	Clear all cookies from fwmrm.net domains.
 */
FW_EXTERN void FWClearCookie(void);

/**
	Opt-out cookies from fwmrm.net domains.

	- Parameter: `value` - `YES` to opt-out, `NO` to opt-in.
 */
FW_EXTERN void FWSetCookieOptOutState(BOOL value);

/**
	Get fwmrm.net domains cookie opt-out state.
 
	@return boolean indicator of the opt-out state.
 */
FW_EXTERN BOOL FWGetCookieOptOutState(void);

/**
	Link a third party renderer framework.
 
	- Note: When you include a third party renderer framework in your project, during the compilation it could be stripped by the compiler because it is likely to appear unused in the project. To avoid this, either add "-all_load" to Other Linker Flags in your project's Build Settings, or call `FWLinkRenderer(RendererClass)` in your AppDelegate.m.
	
	@param rendererClass The third party renderer class.
 */
FW_EXTERN void FWLinkRenderer(Class rendererClass);

/**
	FWAdManager protocol.
 
	`FWAdManager` is a factory for `FWContext` objects. Use `newContext` to create a new FWContext object.
	Normally, a single instance of `FWAdManager` is recommended for each app. With each session, a `FWContext` should be created to handle ad request / response, and the delivered slots. Destroy your `FWAdManager` instance only when your app exits.

	- See: `newAdManager`
 */
@protocol FWAdManager <NSObject>
#pragma mark - Properties
/**
	Get major version of AdManager

	@return Version of AdManager, e.g. 0x02060000 for v2.6
 */
- (NSUInteger)version;

#pragma mark - Methods
/**
	Set application's current view controller. 

	You are required to call this method if using any of the following third party components:

	- FWAdMobRenderer
	- FWMillennialDisplayAdRenderer
	- FWMillennialTakeoverAdRenderer
	- FWTremorAdRenderer
 
	Otherwise optional.

	- Note: Once set, the view controller will be retained by AdManager.

	@param viewController   current view controller
 */
- (void)setCurrentViewController:(UIViewController *)viewController;

/**
	Set the ad server URL provided by FreeWheel. Required. Consult your FreeWheel solution engineer for the value to set.

	@param value   url of the FreeWheel Ad Server
 */
- (void)setServerUrl:(NSString *)value;

/**
	Set the network ID. Required. Consult your FreeWheel solution engineer for the value to set.

	@param value	network ID
 */
- (void)setNetworkId:(NSUInteger)value;

/**
	Set the current location of the device. This value will only be used for geo targeting purposes.

	@param value	device's current location, `CLLocation`. nil by default.
 */
- (void)setLocation:(CLLocation *)value;

/**
	Create a new `FWContext` instance.

	A `FWContext` instance is used to set ad request information for a particular ad or ad set.  Multiple contexts can be created throughout the lifecycle of the FreeWheel AdManager and may exist simultaneously without consequence. Multiple simultaneous contexts are useful to optimize user experience in the network-resource limited environment.

	@return A new `FWContext` instance
 */
- (id<FWContext>)newContext;

/**
	Create a new context from the given context. The new context copies internal state and information of the old one, so you can use this method to create an identical context without having to set all the information again.

	The following methods are called automatically on the new context with values of the old one.

	- `[FWContext setRequestMode:]`
	- `[FWContext setCapability:status:]`
	- `[FWContext setVisitorId:ipV4Address:bandwidth:bandwidthSource:]`
	- `[FWContext setVisitorHTTPHeader:withValue:]`
	- `[FWContext setSiteSectionId:idType:pageViewRandom:networkId:fallbackId:]`
	- `[FWContext setVideoAssetId:idType:duration:durationType:location:autoPlayType:videoPlayRandom:networkId:fallbackId:]`
	- `[FWContext setPlayerProfile:defaultTemporalSlotProfile:defaultVideoPlayerSlotProfile:defaultSiteSectionSlotProfile:]`
	- `[FWContext startSubsessionWithToken:]`
	- `[FWContext setRequestDuration:]`
	- `[FWContext setVideoDisplayCompatibleSizes:]`
	- `[FWContext setVideoState:]`

	You are required to call the following methods again on the new context:

	- `[NSNotificationCenter addObserver:selector:name:object:]` 	(add notification observer for the new context)
	- `[FWContext addTemporalSlot:adUnit:timePosition:slotProfile:cuePointSequence:minDuration:maxDuration:acceptPrimaryContentType:acceptContentType:]` (if there's any)
	- `[FWContext addSiteSectionNonTemporalSlot:adUnit:width:height:slotProfile:acceptCompanion:initialAdOption:acceptPrimaryContentType:acceptContentType:compatibleDimensions:]` (if there's any)
	- `[FWContext addVideoPlayerNonTemporalSlot:adUnit:width:height:slotProfile:acceptCompanion:initialAdOption:acceptPrimaryContentType:acceptContentType:compatibleDimensions:]` (if there's any)
	- `[FWContext submitRequestWithTimeout:]`
	
	@return A new `FWContext` instance
 */
- (id<FWContext>)newContextWithContext:(id<FWContext>)context;

/**
	Disable FreeWheel crash reporter. By default AdManager will send a crash report to ad server when an app crash is detected.
 */
- (void)disableFWCrashReporter;
@end

/**
	Protocol for AdManager context
 */
@protocol FWContext <NSObject>

#pragma mark - Properties
/**
	The delegate to handle video ads.
 
	- See: `FWVideoAdDelegate`
 */
@property (nonatomic, assign) id<FWVideoAdDelegate> videoAdDelegate;

/**
	Get all temporal slots

	@return An `NSArray` of `FWSlot` objects
 */
- (NSArray * /* id<FWSlot> */)temporalSlots;

/**
	Get all video player non-temporal slots

	@return An `NSArray` of `FWSlot` objects
 */
- (NSArray * /* id<FWSlot> */)videoPlayerNonTemporalSlots;

/**
	Get all site section non-temporal slots

	@return An `NSArray` of `FWSlot` objects
 */
- (NSArray * /* id<FWSlot> */)siteSectionNonTemporalSlots;

/**
	Get video asset's location URL

	@return video asset's URL, `nil` if not set
 */
- (NSString *)videoLocation;
- (NSString *)getVideoLocation DEPRECATED_MSG_ATTRIBUTE("Use videoLocation instead");

/**
	Get the AdManager instance for this context

	@return The `FWAdManager` instance
 */
- (id<FWAdManager>)adManager;
- (id<FWAdManager>)getAdManager DEPRECATED_MSG_ATTRIBUTE("Use adManager instead");

/**
	Return the object [NSNotificationCenter defaultCenter]
 */
- (NSNotificationCenter *)notificationCenter;

- (NSString *)transactionId;

#pragma mark - Methods
/**
	Set video display base `UIView`. REQUIRED. Video ads are rendered within the base view in the same frame.

	When video display base view changes, app needs to call this method again with the new video display to notify AdManager to render video ads in the updated video display. App does not need to call this method when video display view's frame changes.

	@param value Video display base `UIView`
 */
- (void)setVideoDisplayBase:(UIView *)value;

/**
	Set the capabilities supported by the player

	@param capability capability name, see `FWCapability*`
	@param status indicates whether to enable this capability, see `FWCapabilityStatus`
	@return Boolean value, indicating whether the capability is set successfully
 */
- (BOOL)setCapability:(NSString *)capability status:(FWCapabilityStatus)status;

/**
	Add a key-value pair to ad request. The key-value pair is used in ad targeting. If called with the same key multiple times, all the values will be added to the same key.

	@param key		key of the key-value pair, NSString. Can not be nil or empty
	@param value	value of the key-value pair, NSString. Can not be nil
 */
- (void)addValue:(NSString *)value forKey:(NSString *)key;

/**
	Set the profiles names. Consult your FreeWheel sale engineer for available values.

	@param playerProfile					name of the global profile
	@param defaultTemporalSlotProfile		name of the temporal slot default profile, nil by default
	@param defaultVideoPlayerSlotProfile	name of the video player slot default profile, nil by default
	@param defaultSiteSectionSlotProfile	name of the site section slot default profile, nil by default
 */
- (void)setPlayerProfile:(NSString *)playerProfile defaultTemporalSlotProfile:(NSString *)defaultTemporalSlotProfile defaultVideoPlayerSlotProfile:(NSString *)defaultVideoPlayerSlotProfile defaultSiteSectionSlotProfile:(NSString *)defaultSiteSectionSlotProfile;

/**
	Set the attributes of the visitor

	@param customId		custom ID of the visitor
	@param ipV4Address		ip address of the visitor
	@param bandwidth		bandwidth of the visitor
	@param bandwidthSource	bandwidth source of the visitor
 */
- (void)setVisitorId:(NSString *)customId ipV4Address:(NSString *)ipV4Address bandwidth:(NSUInteger)bandwidth bandwidthSource:(NSString *)bandwidthSource;

/**
	Set the HTTP headers of the visitor

	@param name	name of the header
	@param value	value of the header. If set to nil, the original value of the HTTP header name will be removed.
 */
- (void)setVisitorHTTPHeader:(NSString *)name withValue:(NSString *)value;

/**
	Set the attributes of the current video asset

	@param videoAssetId	id of the video asset
	@param idType		Type of video id, see `FWIdType`
	@param duration		Duration of the video in seconds
	@param durationType	Type of duration, see `FWVideoAssetDurationType`
	@param location		Location(URI) of the video, nil by default
	@param autoPlayType	Whether the video starts playing automatically without user interaction. See `FWVideoAssetAutoPlayType`
	@param videoPlayRandom	Random number generated everytime a user watches the video asset
	@param networkId		ID of the network the video belongs to, 0 by default
	@param fallbackId		Video ID to fallback to. When ad server fails to find the video asset specified by videoAssetId, this ID will be used. 0 by default
 */
- (void)setVideoAssetId:(NSString *)videoAssetId idType:(FWIdType)idType duration:(NSTimeInterval)duration durationType:(FWVideoAssetDurationType)durationType location:(NSString *)location autoPlayType:(FWVideoAssetAutoPlayType)autoPlayType videoPlayRandom:(NSUInteger)videoPlayRandom networkId:(NSUInteger)networkId fallbackId:(NSUInteger)fallbackId;

/**
	Set the current time position of the content asset.

	@param timePosition	time position value in seconds.
	
	- Note: If the stream is broken into multiple distinct files, this should be the time position within the asset as a whole.
 */
- (void)setVideoAssetCurrentTimePosition:(NSTimeInterval)timePosition;

/**
	Set the attributes of the site section

	@param siteSectionId	ID of the site section
	@param idType			Type of site section id, see `FWIdType`
	@param pageViewRandom	Random number generated everytime a user visits current site section
	@param networkId		ID of the network the site section belongs to, 0 by default
	@param fallbackId		Site section ID to fallback to. When ad server fails to find the site section specified by siteSectionId, this ID will be used. 0 by default
 */
- (void)setSiteSectionId:(NSString *)siteSectionId idType:(FWIdType)idType pageViewRandom:(NSUInteger)pageViewRandom networkId:(NSUInteger)networkId fallbackId:(NSUInteger)fallbackId;

/**
	Add candidate ads into the ad request

	@param candidateAdId	ID of the candidate ad
 */
- (void)addCandidateAdId:(NSUInteger)candidateAdId;

/**
	Add a temporal slot

	@param customId				 Custom ID of the slot. If slot with specified ID already exists, the function call will be ignored.
	@param adUnit					Ad unit supported by the slot
	@param timePosition				Time position of the slot
	@param slotProfile				Profile name of the slot, nil by default
	@param cuePointSequence			Slot cue point sequence
	@param minDuration				Minimum duration of the slot allowed, 0 by default
	@param maxDuration				Maximum duration of the slot allowed, 0 by default
	@param acceptPrimaryContentType	Accepted primary content types, comma separated values, use "," as delimiter, nil by default
	@param acceptContentType		Accepted content types, comma separated values, use "," as delimiter, nil by default
 */
- (void)addTemporalSlot:(NSString *)customId adUnit:(NSString *)adUnit timePosition:(NSTimeInterval)timePosition slotProfile:(NSString *)slotProfile cuePointSequence:(NSUInteger)cuePointSequence minDuration:(NSTimeInterval)minDuration maxDuration:(NSTimeInterval)maxDuration acceptPrimaryContentType:(NSString *)acceptPrimaryContentType acceptContentType:(NSString *)acceptContentType;

/**
	Add a video player non-temporal slot.

	@param customId Custom ID of the slot. If slot with specified ID already exists, the function call will be ignored.
	@param adUnit Ad unit supported by the slot
	@param width Width of the slot
	@param height Height of the slot
	@param slotProfile Profile name of the slot, nil by default
	@param acceptCompanion Whether companion ads are accepted
	@param initialAdOption See `FWSlotInitialAdOption`
	@param acceptPrimaryContentType Accepted primary content types, comma separated values, use "," as delimiter, `nil` by default
	@param acceptContentType Accepted content types, comma separated values, use "," as delimiter, `nil` by default
	@param compatibleDimensions Array of compatible dimensions, The dimension must be a NSDictionary object with key "width" and "height", the value of should be a positive integer. `nil` by default. Example:
		`@@[@@{@@"width": @@800, @@"height": @@600}, @@{@@"width": @@640, @@"height": @@480}]`
 */
- (void)addVideoPlayerNonTemporalSlot:(NSString *)customId adUnit:(NSString *)adUnit width:(NSUInteger)width height:(NSUInteger)height slotProfile:(NSString *)slotProfile acceptCompanion:(BOOL)acceptCompanion initialAdOption:(FWSlotInitialAdOption)initialAdOption acceptPrimaryContentType:(NSString *)acceptPrimaryContentType acceptContentType:(NSString *)acceptContentType compatibleDimensions:(NSArray *)compatibleDimensions;

/**
	Add a site section non-temporal slot

	@param customId Custom ID of the slot. If slot with specified ID already exists, the function call will be ignored.
	@param adUnit Ad unit supported by the slot
	@param width Width of the slot
	@param height Height of the slot
	@param slotProfile Profile name of the slot, `nil` by default
	@param acceptCompanion Whether companion ads are accepted
	@param initialAdOption See `FWSlotInitialAdOption`
	@param acceptPrimaryContentType Accepted primary content types, comma separated values, use "," as delimiter, `nil` by default
	@param acceptContentType Accepted content types, comma separated values, use "," as delimiter, `nil` by default
	@param compatibleDimensions Array of compatible dimensions, The dimension must be a NSDictionary object with key "width" and "height", the value of should be a positive integer. `nil` by default. Example:
		`@@[@@{@@"width": @@800, @@"height": @@600}, @@{@@"width": @@640, @@"height": @@480}]`
 */
- (void)addSiteSectionNonTemporalSlot:(NSString *)customId adUnit:(NSString *)adUnit width:(NSUInteger)width height:(NSUInteger)height slotProfile:(NSString *)slotProfile acceptCompanion:(BOOL)acceptCompanion initialAdOption:(FWSlotInitialAdOption)initialAdOption acceptPrimaryContentType:(NSString *)acceptPrimaryContentType acceptContentType:(NSString *)acceptContentType compatibleDimensions:(NSArray *)compatibleDimensions;

/**
	Set the current state of the video

	@param videoState	See `FWVideoState`
 */
- (void)setVideoState:(FWVideoState)videoState;

/**
	Set playhead time of the content video

	@param playheadTime  video playhead time
 */
- (void)setVideoPlayheadTime:(NSTimeInterval *)playheadTime;

/**
	Get all slots in specified time position class

	@param timePositionClass Time position class, see `FWTimePositionClass`
	@return An `NSArray` of `FWSlot` objects
 */
- (NSArray * /* id<FWSlot> */)getSlotsByTimePositionClass:(FWTimePositionClass)timePositionClass;

/**
	Get a slot by its custom ID

	@param customId	Custom ID of the slot
	@return An `FWSlot` object, or `nil` if not found
 */
- (id<FWSlot>)getSlotByCustomId:(NSString *)customId;
/**
	Submit the request to FreeWheel Ad Server

	@param timeoutInterval ad request timeout value in seconds. 10 by default.
 */
- (void)submitRequestWithTimeout:(NSTimeInterval)timeoutInterval;

/**
	Add renderer class to context which takes higher priority than renderers set in renderer manifest returned from server.

	@param className	The class name of the renderer.
	@param contentType	The content types that the renderer can support, separated by comma.
	@param creativeAPI The creative APIs that the renderer can support, separated by comma.
	@param slotType	The slot types that the renderer can support, separated by comma.
	@param baseAdUnit	The base ad units that the renderer can support, separated by comma.
	@param soldAsAdUnit	The soldAs ad unit that the renderer can support, separated by comma.
	@param parameters	The parameters set on renderer level.
 */
- (void)addRendererClass:(NSString *)className forContentType:(NSString *)contentType creativeAPI:(NSString *)creativeAPI slotType:(NSString *)slotType baseUnit:(NSString *)baseAdUnit adUnit:(NSString *)soldAsAdUnit withParameters:(NSDictionary *)parameters;

/**
	Set a parameter on a specified level. 
	
	@param name	parameter name
	@param value	parameter value
	@param level	level of the parameter, must be `FWParameterLevelGlobal` or `FWParameterLevelOverride`
 */
- (void)setParameter:(NSString *)name withValue:(id)value forLevel:(FWParameterLevel)level;

/**
	Retrieve the value of a parameter

	@param name  The name of the parameter
	@return The value of the parameter
 */
- (id)getParameter:(NSString *)name;

/**
	Set a list of acceptable alternative dimensions
	
	@param compatibleDimensions	Array of compatible dimensions, The dimension must be a `NSDictionary` object with key "width" and "height", the value of should be a positive integer. `nil` by default. Example:
		`@@[@@{@@"width": @@800, @@"height": @@600}, @@{@@"width": @@640, @@"height": @@480}]`
 */
- (void)setVideoDisplayCompatibleSizes:(NSArray *)compatibleDimensions;

/**
	Set request mode of AdManager.

	@param mode	request mode, see `FWRequestMode`
			
	- Note: AdManager runs in On-Demand mode by default. If your video asset is a live stream, invoke this method to set the player to live mode. This method should be called (if needed) right after a new `FWContext` instance is created.
 */
- (void)setRequestMode:(FWRequestMode)mode;

/**
	Set the duration for which the player is requesting ads. Optional.

	@param requestDuration		requesting duration value, in seconds.
 */
- (void)setRequestDuration:(NSTimeInterval)requestDuration;

/**
	Reset the exclusivity scope
 */
- (void)resetExclusivity;

/**
	Start a subsession in live mode. Subsequent requests will be in the same subsession, until this method is called again with a different token.

	@param subsessionToken		a token to identify the subsession, should be unique across different subsessions. Use a different token to start a new subsession.
		
	- Note: Calling this method multiple times with the same token will have no effect. Subsession only works when FWCapabilitySyncMultipleRequests is on, calling this method will turn on this capability automatically.
 */
- (void)startSubsessionWithToken:(NSUInteger)subsessionToken;

/**
	Notify AdManager about user actions
	@param userAction User action, see `FWUserAction`
 */
- (void)notifyUserAction:(FWUserAction)userAction;

/**
	Load player extension by its class name

	@param extensionName The class name of the extension

	- SeeAlso: `FWExtensionLoadedNotification`
 */
- (void)loadExtension:(NSString *)extensionName;

/**
	Request timeline to pause. The timeline consists of the content video and all linear slots.

	When the renderer or extension requires the timeline to be temporarily paused, e.g. when expanding to a fullscreen view that covers the whole player and other ads, calling this method will result in the notification FWContentPauseRequestNotification being dispatched from the current FWContext instance if content video is currently playing, and send pause requests to all active temporal slots.
 */
- (void)requestTimelinePause;

/**
	Request timeline to resume. The timeline consists of the content video and all linear slots.

	When the renderer or extension requires the timeline to be resumed, e.g. when dismissing a fullscreen view that covers the whole player and other ads, calling this method will result in the notification FWContentResumeRequestNotification being dispatched from the current FWContext instance if content video is currently paused, and send resume requests to all active temporal slots.
 */
- (void)requestTimelineResume;

@end


/**
	Protocol for slot 
 */
@protocol FWSlot <NSObject>

#pragma mark - Properties

/**
	Get the slot's custom ID

	@return Custom ID of the slot
 */
- (NSString *)customId;  

/**
	Get the slot's type

	@return Type of the slot, See `FWSlotType`
 */
- (FWSlotType)type;				

/**
	Get the slot's time position
	@return Time position of the slot
 */
- (NSTimeInterval)timePosition;	 

/**
	Get slot's time position class

	@return Time position class of the slot, see `FWTimePositionClass`
 */
- (FWTimePositionClass)timePositionClass;	   

/**
	Get the slot's embedded ads duration

	@return The embeded ads duration of the temporal slot. -1 if not available.
 */
- (NSTimeInterval)embeddedAdsDuration;

/**
	Get the slot's end time position

	@return The end time position of the temporal slot. -1 if not available.
 */
- (NSTimeInterval)endTimePosition;

/**
	Get the ad instances in the slot

	@return An array of id<FWAdInstance>
 */
- (NSArray * /* <id>FWAdInstance */)adInstances;

/**
	Get the width of the slot in pixels as returned in ad response.

	@return Width in pixels
 */
- (NSInteger)width;

/**
	Get the height of the slot in pixels as returned in ad response.

	@return Height in pixels
 */
- (NSInteger)height;

/**
	Get slot's visibility. Only applicable to non-temporal slots. YES by default.

	@return `YES` for visible, otherwise `NO`
 */
- (BOOL)visible;

/**
	Get slot base `UIView` object.

	For nontemporal slots, the returned `UIView` should be added to a parent `UIView` that is already in your apps view hierarchy.

	For temporal slot, return value is the object set by `-[FWContext setVideoDisplayBase:]`.

	@return The slot base `UIView`
 */
- (UIView *)slotBase;

/**
	Get the slot's duration

	@return the duration in seconds, greater than or equal to 0
 */
- (NSTimeInterval)totalDuration;

/**
	Get the slot's playhead time

	@return the playhead time in seconds, greater than or equal to 0
 */
- (NSTimeInterval)playheadTime;

/**
	Get the currently playing ad instance

	@return the `FWAdInstance` object of the currently playing ad in this slot. Return `nil` if no ad instance is currently playing.
 */
- (id<FWAdInstance>)currentAdInstance;

#pragma mark - Methods

- (void)processEvent:(NSString *)eventName DEPRECATED_ATTRIBUTE;

/**
	Preload the slot.
	The notification `FWSlotPreloadedNotification` will be dispatched when the slot has finished preloading.

	- Note: The preload behaviour may differ due to different ad types. Based on the current implementation, it only supports VAST ad.
 */
- (void)preload;

/**
	Play the slot.
 */
- (void)play;

/**
	Stop the slot
 */
- (void)stop;

/**
	Pause the slot.
 */
- (void)pause;

/**
	Resume the slot.
 */
- (void)resume;

/**
	Skips the current ad in the slot.
	
	- Note: The fallback ads associated with the skipped ad will not be played.
 */
- (void)skipCurrentAd;

/**
	Set the visibility for a nontemporal slot. 

	If a nontemporal slot view should not be visible, call set the visibility to `NO` before the slot starts. In this case there will be no impression sent to FreeWheel ad server, even if `[slot play]` is called afterwards.

	If a nontemporal slot has already started when it is invisible, setting the visibility to `YES` will display the slot and send an impression.

	- Note: This method has no effect on temporal slots. This method has no effect anymore if an impression has been sent.

	@param value `YES` for visible, `NO` for otherwise
 */
- (void)setVisible:(BOOL)value;

/**
	Set a parameter on the slot level

	@param name	Name of the parameter
	@param value Value of the parameter
 */
- (void)setParameter:(NSString *)name withValue:(id)value;

/**
	Get the value of a parameter by its name

	@param name	Parameter name
	@return Value of the parameter
 */
- (id)getParameter:(NSString *)name;

@end


/**
	Protocol for ad instance
 */
@protocol FWAdInstance <NSObject>

#pragma mark - Properties

/**
	Get the ad ID of the ad instance. This value can also be found in the advertising module of the FreeWheel MRM UI.

	@return ID of the ad instance
 */
- (NSUInteger)adId;

- (NSString *)adUniqueId;

/**
	Get the creative ID of the ad instance
	This is the creative ID associated with this ad. The value can also be found in the advertising module of the FreeWheel MRM UI.

	@return Creative ID as an unsigned int
 */
- (NSUInteger)creativeId;

/**
	Get the primary rendition of the ad instance

	@return A `FWCreativeRendition` instance
 */
- (id<FWCreativeRendition>)primaryCreativeRendition;

/**
	Get the renderer controller of the ad instance

	@return A `FWRendererController` instance
 */
- (id<FWRendererController>)rendererController;

/**
	Get the companion slots of the ad instance

	@return An array of `FWSlot`
 */
- (NSArray *)companionSlots;

/**
	Get the playable companion ad instances

	@return An array of `FWAdInstance`
 */
- (NSArray *)companionAdInstances;

/**
	Whether the ad is required to be shown by law, usually the ad is a companion ad

	@return `YES` if this ad is required to be shown, `NO` if otherwise
 */
- (BOOL)isRequiredToShow;

/**
	Get all creative renditions of the ad instance

	@return An array of `FWCreativeRendition`
 */
- (NSArray* /*id<FWCreativeRendition>*/)creativeRenditions;

/**
	Get the slot that contains this ad instance
 */
- (id<FWSlot>)slot;

/**
	Get duration of the ad instance

	@return Duration in seconds
 */
- (NSTimeInterval)duration;

/**
	Get the ad instance's playhead

	@return the playhead time in seconds, greater than or equal to 0
 */
- (NSTimeInterval)playheadTime;

#pragma mark - Methods

/**
	Get the callback urls for the specified event
 
	Valid eventName, eventType pairs:

	- (`FWAdImpressionEvent`， `FWEventTypeImpression`): ad impression
	- (`FWAdFirstQuartileEvent`, `FWEventTypeImpression`): firstQuartile
	- (`FWAdMidpointEvent`, `FWEventTypeImpression`): midpoint
	- (`FWAdThirdQuartileEvent`, `FWEventTypeImpression`): 3rd quartile
	- (`FWAdCompleteEvent`, `FWEventTypeImpression`): complete
	- (`FWAdClickEvent`, `FWEventTypeClick`): click through
	- (`FWAdClickEvent`, `FWEventTypeClickTracking`): click tracking
	- (`"custom_click_name"`, `FWEventTypeClick`): custom click through
	- (`"custom_click_name"`, `FWEventTypeClickTracking`): custom click tracking
	- (`FWAdPauseEvent`, `FWEventTypeStandard`): IAB metric, pause
	- (`FWAdResumeEvent`, `FWEventTypeStandard`): IAB metric, resume
	- (`FWAdRewindEvent`, `FWEventTypeStandard`): IAB metric, rewind
	- (`FWAdMuteEvent`, `FWEventTypeStandard`): IAB metric, mute
	- (`FWAdUnmuteEvent`, `FWEventTypeStandard`): IAB metric, unmute
	- (`FWAdCollapseEvent`, `FWEventTypeStandard`): IAB metric, collapse
	- (`FWAdExpandEvent`, `FWEventTypeStandard`): IAB metric, expand
	- (`FWAdMinimizeEvent`, `FWEventTypeStandard`): IAB metric, minimize
	- (`FWAdCloseEvent`, `FWEventTypeStandard`): IAB metric, close
	- (`FWAdAcceptInvitationEvent`, `FWEventTypeStandard`): IAB metric, accept invitation

	@param eventName	Name of the event, see `FWAd*Event`
	@param eventType	Type of the event, see `FWEventType*`
	@return Array of urls in `NSString`
 */
- (NSArray *)getEventCallbackUrlsByEventName:(NSString *)eventName eventType:(NSString *)eventType;

/**
	Set callback urls for the specified event

	Valid eventName, eventType pairs:

	- (`FWAdImpressionEvent`， `FWEventTypeImpression`): ad impression
	- (`FWAdFirstQuartileEvent`, `FWEventTypeImpression`): firstQuartile
	- (`FWAdMidpointEvent`, `FWEventTypeImpression`): midpoint
	- (`FWAdThirdQuartileEvent`, `FWEventTypeImpression`): 3rd quartile
	- (`FWAdCompleteEvent`, `FWEventTypeImpression`): complete
	- (`FWAdClickEvent`, `FWEventTypeClick`): click through
	- (`FWAdClickEvent`, `FWEventTypeClickTracking`): click tracking
	- (`"custom_click_name"`, `FWEventTypeClick`): custom click through
	- (`"custom_click_name"`, `FWEventTypeClickTracking`): custom click tracking
	- (`FWAdPauseEvent`, `FWEventTypeStandard`): IAB metric, pause
	- (`FWAdResumeEvent`, `FWEventTypeStandard`): IAB metric, resume
	- (`FWAdRewindEvent`, `FWEventTypeStandard`): IAB metric, rewind
	- (`FWAdMuteEvent`, `FWEventTypeStandard`): IAB metric, mute
	- (`FWAdUnmuteEvent`, `FWEventTypeStandard`): IAB metric, unmute
	- (`FWAdCollapseEvent`, `FWEventTypeStandard`): IAB metric, collapse
	- (`FWAdExpandEvent`, `FWEventTypeStandard`): IAB metric, expand
	- (`FWAdMinimizeEvent`, `FWEventTypeStandard`): IAB metric, minimize
	- (`FWAdCloseEvent`, `FWEventTypeStandard`): IAB metric, close
	- (`FWAdAcceptInvitationEvent`, `FWEventTypeStandard`): IAB metric, accept invitation
 
 	@param urls		NSArray of urls to ping
	@param eventName	Name of the event, see `FWAd*Event`
	@param eventType	Type of the event, see `FWEventType*`
 */
- (void)setEventCallbackUrls:(NSArray *)urls forEventName:(NSString *)eventName eventType:(NSString *)eventType;

/**
	Add a creative rendition to the ad instance

	@return the `FWCreativeRendition` object added to the ad instance
 */
- (id<FWCreativeRendition>)addCreativeRendition;

/**
	Retrieve the parameter from the ad instance.

	The parameter value will be retrieved from levels in the following order: 
	override, creative rendition, creative, slot, profile, global.

	@param name  Parameter name
	@return The value of the parameter
 */
- (id)getParameter:(NSString *)name;

/**
	Set the primary creative rendition

	@param primaryCreativeRendition	 a pointer to the primary creative rendition
 */
- (void)setPrimaryCreativeRendition:(id<FWCreativeRendition>)primaryCreativeRendition;

/*!
	internal use only. For Hybrid Stream Stitcher.
 */
- (void)play2;

@end


/**
	Protocol for creative rendition
 */
@protocol FWCreativeRendition <NSObject>

#pragma mark - Properties

/**
	Get content type of the rendition

	@return  Content type in string
 */
- (NSString *)contentType;

/**
	Get wrapper type of the rendition

	@return  Wrapper type in string
 */
- (NSString *)wrapperType;

/**
	Get wrapper url of the rendition

	@return  Wrapper url in string
 */
- (NSString *)wrapperUrl;

/**
	Get creativeAPI of the rendition

	@return  creativeAPI in string
 */
- (NSString *)creativeAPI;

/**
	Get base unit of the rendition

	@return Base unit in a string
 */
- (NSString *)baseUnit;

/**
	Get preference of the rendition

	@return A number, the higher is preferred among all renditions in the creative
 */
- (int)preference;

/**
	Get width of the rendition

	@return Width in pixels
 */
- (NSUInteger)width;

/**
	Get height of the rendition

	@return Height in pixels
 */
- (NSUInteger)height;

/**
	Get duration of the rendition

	@return Duration in seconds
 */
- (NSTimeInterval)duration;

/**
	Get primary asset of the rendition

	@return An `FWCreativeRenditionAsset` instance
 */
- (id<FWCreativeRenditionAsset>)primaryCreativeRenditionAsset;

/**
	Get all non-primary assets of the rendition

	@return An array of `FWCreativeRenditionAsset`
 */
- (NSArray * /* <id>FWCreativeRenditionAsset */)otherCreativeRenditionAssets;

#pragma mark - Methods

/**
	Set content type of the rendition
 */
- (void)setContentType:(NSString *)value;

/**
	Set wrapper type of the rendition

	@param value Wrapper type of the creative rendition
 */
- (void)setWrapperType:(NSString *)value;

/**
	Set wrapper url of the rendition

	@param value Wrapper URL in string
 */
- (void)setWrapperUrl:(NSString *)value;

/**
	Set creativeAPI of the rendition

	@param: Creative API in string
 */
- (void)setCreativeAPI:(NSString *)value;

/**
	Set preference of the rendition

	@param value Rendition's preference
 */
- (void)setPreference:(int)value;

/**
	Set width of the rendition

	@param value Width of the rendition in pixels.
 */
- (void)setWidth:(NSUInteger)value;

/**
	Set height of the rendition

	@param value	Height of the rendition in pixels.
 */
- (void)setHeight:(NSUInteger)value;

/**
	Set duration of the rendition

	@return Duration in seconds
 */
- (void)setDuration:(NSTimeInterval)value;

/**
	Set parameter on the rendition

	@param name Name of the parameter
	@param value Value of the parameter
 */
- (void)setParameter:(NSString *)name withValue:(NSString *)value;

/**
	Add an asset to the rendition
 */
- (id<FWCreativeRenditionAsset>)addCreativeRenditionAsset;
@end


/**
	Protocol for creative rendition asset
 */
@protocol FWCreativeRenditionAsset <NSObject>

#pragma mark - Properties

/**
	Get name of the asset

	@return Name in a string
 */
- (NSString *)name;

/**
	Get URL of the asset

	@return URL in a string
 */
- (NSString *)url;

/**
	Get content of the asset

	@return Content in a string
 */
- (NSString *)content;

/**
	Get mime type of the asset

	@return Mime type in a string
 */
- (NSString *)mimeType;

/**
	Get content type of the asset

	@return Content type in a string
 */
- (NSString *)contentType;

/**
	Get size of the asset

	@return Size in bytes, or -1 if unknown
 */
- (NSInteger)bytes;

#pragma mark - Methods

/**
	Set name of the asset

	@param value Name of the asset
 */
- (void)setName:(NSString *)value;

/**
	Set URL of the asset

	@param value URL of the asset
 */
- (void)setUrl:(NSString *)value;

/**
	Set the content of the asset

	@param value Content of the asset
 */
- (void)setContent:(NSString *)value;

/**
	Set mime type of the asset

	@param value MIME type of the asset
 */
- (void)setMimeType:(NSString *)value;

/**
	Set content type of the asset

	@param value	Content type of the asset
 */
- (void)setContentType:(NSString *)value;

/**
	Set size of the asset

	@return Size in bytes, or -1 if unknown
 */
- (void)setBytes:(NSInteger)value;
@end


@protocol FWNotificationContext

- (void)postNotificationName:(NSString *)notificationName userInfo:(NSDictionary *)userInfo;

@end


/**
	Protocol for renderer controller

	The FWRendererController class provides methods for reporting metric events and changing renderer states.
 */
@protocol FWRendererController <NSObject>

#pragma mark - Properties

/**
	Return the current location.

	- SeeAlso: `setLocation:`

	@return current location
 */
- (CLLocation *)location;

/**
	Return application's current view controller. 

	- SeeAlso: `setCurrentViewController:`

	@return current view controller
 */
- (UIViewController *)currentViewController;

/**
	Return all renderable renditions for Renderer

	@return an array of `FWCreativeRendition`
*/
- (NSArray * /* id<FWCreativeRendition> */)renderableCreativeRenditions;

/**
	Return the Major version of AdManager, e.g. 0x02060000 for v2.6

	@return version as NSUInteger
 */
- (NSUInteger)version;

/**
	Get the current ad instance

	@return the current `FWAdInstance`
 */
- (id<FWAdInstance>)adInstance;

/**
	Returns the sender of all FreeWheel related notifications.
 */
- (id<FWNotificationContext>)notificationContext;

#pragma mark - Methods

/**
	Process an ad event

	@param eventName Ad event to be processed, see `FWAd*Event`
	@param details  Additional information. Available keys: `FWInfoKeyCustomEventName`: Optional. Name of the custom event. `FWInfoKeyShowBrowser`: Optional. Force opening / not opening click through url in WebView or Mobile Safari. If this key is not provided, AdManager will use the setting booked in MRM UI (recommended). `FWInfoKeyUrl`: Optional. URL to open or used as redirect url on FWAdClickEvent. If this key is not provided, the URL booked in MRM UI will be used.

 	- Note: This method should not be used to process `FWAdImpressionEvent` or `FWAdImpressionEndEvent`. Please use `[_rendererController handleStateTransition:FW_RENDERER_STATE_* info:nil]` for these events.
 */
- (void)processEvent:(NSString *)eventName info:(NSDictionary *)details;

/**
	Declare a capability of the renderer

	@param adEventName One of `FWAd*Event` excluding `FWAdImpressionEvent`, `FWAdFirstQuartileEvent`, `FWAdMidPointEvent`, `FWAdThirdQuartileEvent`, `FWAdCompleteEvent`
	@param supported `YES` if renderer is able to process the specified ad event

	- Note: Changing renderer capability after renderer starts playing may result in undefined behaviour.
 */
- (void)setSupportedAdEvent:(NSString *)adEventName supported:(BOOL)supported;

- (void)setCapability:(NSString *)eventCapability status:(FWCapabilityStatus)status DEPRECATED_MSG_ATTRIBUTE("Use setSupportedAdEvent:supported: instead");

/**
	Retrieve a parameter

	@param name  Parameter name
 */
- (id)getParameter:(NSString *)name;

/**
	Transit renderer state

	@param state	Destination transition state to attempt, see `FWRendererState`
	@param details	Detailed info
	
	- Note: For `FWRendererStateFailed`: `FWInfoKeyErrorCode` are required. `FWInfoKeyErrorInfo` is optional.
 */
- (void)handleStateTransition:(FWRendererStateType)state info:(NSDictionary *)details;

- (NSArray * /* id<FWAdInstance> */)scheduleAdInstances:(NSArray * /* id<FWSlot> */)slots DEPRECATED_ATTRIBUTE;

/**
	Schedule ad instances for the given slots.

	@param slots	`NSArray` of `FWSlot`, slots to schedule ads for
	@return `NSArray` of scheduled `FWAdInstance` in the same sequence of the passed in slot. `nil` if no ad instance can be scheduled for the corresponding slot.
 */
- (NSArray * /* id<FWAdInstance> */)scheduleAdInstancesInSlots:(NSArray * /* id<FWSlot> */)slots;

/**
	Request timeline to pause. The timeline consists of the content video and all linear slots.

	When the renderer or extension requires the timeline to be temporarily paused, e.g. when expanding to a fullscreen view that covers the whole player and other ads, calling this method will result in the notification FWContentPauseRequestNotification being dispatched from the current FWContext instance if content video is currently playing, and pause requests sent to all active temporal slots.
 */
- (void)requestTimelinePause;

/**
	Request timeline to resume. The timeline consists of the content video and all linear slots.

	When the renderer or extension requires the timeline to be resumed, e.g. when dismissing a fullscreen view that covers the whole player and other ads, calling this method will result in the notification FWContentResumeRequestNotification being dispatched from the current FWContext instance if content video is currently paused, and send resume requests to all active temporal slots.
 */
- (void)requestTimelineResume;

/*!
	Deprecated. Please use `requestTimelinePause` and `requestTimelineResume` instead.
 */
- (void)requestContentStateChange:(BOOL)pause DEPRECATED_ATTRIBUTE;

/**
	Renderer should use this API to trace all logs
 
	@param msg Message to print in the log
 */
- (void)log:(NSString *)msg;
@end


/**
	Protocol for FWRenderer
 */
@protocol FWRenderer <NSObject>

#pragma mark - Properties

/**
	Get module info. The returned dictionary should contain key `FWInfoKeyModuleType` with `FWModuleType*` value,
	and should contain key `FWInfoKeyRequiredSDKVersion` with the FreeWheel RDK version when the component is compiled.
 */
- (NSDictionary *)moduleInfo;


/**
	Get duration of the ad

	@return a positive number in seconds as `NSTimeInterval`, or -1 if the duration is N/A
 */
- (NSTimeInterval)duration;

/**
	Get playheadTime of the ad

	@return a positive number in seconds as `NSTimeInterval`, or -1 if the playhead time is N/A
 */
- (NSTimeInterval)playheadTime;

#pragma mark - Required Methods

/**
	Initialize the renderer with a renderer controller. 
	
	@param rendererController	reference to `FWRendererController`
		
	- Note: Typically the renderer declares all available capabilities and events when this method is called.
 */
- (id)initWithRendererController:(id<FWRendererController>)rendererController;

/**
	Start ad playback.
	
	- Note: The renderer should start the ad playback when this method is called,
		and transit to `FWRendererStateStarted` state as soon as the ad has started.
		
		When the ad stops (either interrupted or reached the end), the renderer should
		transit to `FWRendererStateCompleted` state.
 */
- (void)start;

/**
	Stop ad playback.

	- Note: Typically the renderer will dispose playing images/videos from screen when receive this notification, and transit to `FWRendererStateCompleted` state as soon as the ad is stopped.
 */
- (void)stop;

#pragma mark - Optional Methods
@optional
/**
	Preload the ad.

	- Note: Renderers should start preloading the ad asynchronously when this method is called, and transit to `FWRendererStatePreloaded` state when the ad finishes preloading. Translators should translate and schedule ads in the preloading stage.
 */
- (void)preload;

/**
	User intended pause. Should pause ad playback and send IAB _pause callback by calling `[rendererController processEvent:FWAdPauseEvent info:nil]`
 */
- (void)pause;

/**
	User intended resume. Should resume ad playback and send IAB _resume callback by calling `[rendererController processEvent:FWAdResumeEvent info:nil]`
 */
- (void)resume;

@end

/**
	Protocol for FWExtension
 */
@protocol FWExtension <NSObject>

#pragma mark - Methods
/**
	Initialize the extension with a id<FWContext> instance.

	@param context	reference to id<FWContext>

	- Note: The constructor of the extension. AdManager will use this method to get an object of the extension.
 */
- (id)initWithFWContext:(id<FWContext>)context;

/**
	Stop the extension.

	- Note: Extension should stop all its work and do cleanup in the method.
 */
- (void)stop;

@end

/**
	FWVideoAdDelegate Protocol
 
	When your app needs to support AirPlay when app is backgrounded, you can no longer rely on the default Video Ad Renderer to render video ads since iOS only allows one AVPlayer instance to run in the background. Instead, your player should implement `FWVideoAdDelegate` protocol, and set itself as the `videoAdDelegate` of the current `FWContext` instance. When a video ad needs to be played / paused, the corresponding delegation method will be called and supplied with the video ad wrapped in a loaded `AVPlayerItem` instance.
 */
@protocol FWVideoAdDelegate <NSObject>

#pragma mark - Methods
/**
	The app should insert the provided `AVPlayerItem` object after another item in the item queue of an AVQueuePlayer instance.
	
	@param playerItem An `AVPlayerItem` object prepared by AdManager.
			item The `AVPlayerItem` object after which the object playerItem will be inserted. Nil value means that the object playerItem should be inserted at the end of the queue.
 
 	@return whether the provided AVPlayerItem instance can be inserted into the specified position in the current queue.
 
	- See: `videoAdDelegate`
 */
- (BOOL)playerShouldInsertItem:(AVPlayerItem *)playerItem afterItem:(AVPlayerItem *)item;

/**
	The app should start playing the provided `AVPlayerItem` object.
	
	@param playerItem An `AVPlayerItem` object that was prepared by AdManager and has been added to the queue. If the item hasn't started playing, start playing the item. If the item has started playing but has been paused during the playback, resume the item.
 
	- See: `videoAdDelegate`
 */
- (void)playerShouldPlayItem:(AVPlayerItem *)playerItem;

/**
	The app should pause the provided `AVPlayerItem` object if it's the currently playing item in the AVQueuePlayer instance.

	@param playerItem An `AVPlayerItem` object prepared by AdManager.

	- See: `videoAdDelegate`
 */
- (void)playerShouldPauseItem:(AVPlayerItem *)playerItem;

/**
	The app should remove the provided `AVPlayerItem` object from the item queue in the AVQueuePlayer instance that the app maintains. Note that the app should pause the player if the item is the current playing item in the AVQueuePlayer instance.
 
	@param playerItem The `AVPlayerItem` object that is to be removed from the queue.
 
	- See: `videoAdDelegate`
 */
- (void)playerShouldRemoveItem:(AVPlayerItem *)playerItem;

@end

#pragma clang arc_cf_code_audited end
