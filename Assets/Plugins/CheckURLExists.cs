using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.IO;
using System;

public class CheckURLExists {

	/* Interface to native implementation */
	
	[DllImport ("__Internal")]
	private static extern bool _CheckURL (string url);


	
	/* Public interface for use inside C# / JS code */
	
	// Starts lookup for some bonjour registered service inside specified domain
	public static bool CheckURL(string url)
	{
		
			return _CheckURL(url);

	}

	/// <summary>
	/// Android method
	/// </summary>
	/// <returns><c>true</c> if this instance is app installed the specified bundleID; otherwise, <c>false</c>.</returns>
	/// <param name="bundleID">Bundle I.</param>
	public static bool AndroidAppInstalled(string bundleID){
		#if UNITY_ANDROID
		AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject ca = up.GetStatic<AndroidJavaObject>("currentActivity");
		AndroidJavaObject packageManager = ca.Call<AndroidJavaObject>("getPackageManager");
		Debug.Log(" ********LaunchOtherApp ");
		AndroidJavaObject launchIntent = null;
		//if the app is installed, no errors. Else, doesn't get past next line
		try{
			launchIntent = packageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage",bundleID);
			//        
			//        ca.Call("startActivity",launchIntent);
		}catch(Exception ex){
			Debug.Log("exception"+ex.Message);
		}
		if(launchIntent == null){
			Debug.Log(bundleID+" dont got it");
			return false;
		}
		else{
			Debug.Log(bundleID+" got it");
			return true;
		}

		#else
		return false;
		#endif
	}
}
